# Phaser中文

[Phaser中文](http://phaser.cc/)汇集了Phaser相关的中文文档、实例教程和游戏分享。本源码是[Phaser中文](http://phaser.cc/)网站中涉及的游戏Demo源代码，Demo相关说明可登录[Phaser中文](http://phaser.cc/)网站后使用站内搜索功能获取。
如果你有或者知道哪里有用Phaser制作的游戏，不论开源闭源，不论是否混淆，请告诉我，我将会把游戏添加到这里，非常感谢！

## Phaser简介
[Phaser](http://phaser.io)是一款专门用于桌面及移动HTML5 2D游戏开发的开源免费框架，提供JavaScript和TypeScript双重支持，内置游戏对象的物理属性，采用Pixi.js引擎以加快Canvas和WebGL渲染，基于浏览器支持可自由切换。
