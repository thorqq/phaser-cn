BasicGame.niveles = function(game){
	var botonl1=null;
	var botonl2=null;
	var botonl3=null;
	var score=score;
	var score2=score2;
	var scoreTotal=0;
	var scoreText = null;
	var text1=null;
	var text2=null;
	var text2=null;
	var awesome=null;
	var superAwesome=null;
	var twitter;
};
BasicGame.niveles.prototype = {
	create:function(){
		logo = this.add.sprite(130,0,'logo');
		logo.scale.setTo(0.2,0.2)
		botonl1 = this.add.button(450,180,'botonl1',empezarJuego1,this,1,0,0);
		botonl2 = this.add.button(450,200,'botonl2',empezarJuego2,this,1,0,0);
		botonl3 = this.add.button(450,220,'botonl3',empezarJuego3,this,1,0,0);
		
		scoreText = this.add.bitmapText(16,16,'arcade',"STARS "+(score+score2+score3)+"x110");
        scoreText.fixedToCamera = true;
		
		text1 = this.add.bitmapText(16,186,'arcade',"Level2  x 29  stars");
		text1.update();
		text2 = this.add.bitmapText(16,216,'arcade',"Level3  x 79  stars");
		text2.update();
		scoreTotal = score+score2+score3;
		
		muertes = this.add.bitmapText(350,16,'arcade',"DEATHS "+muertesCount);
		muertes.inputEnabled = true;
		muertes.fixedToCamera = true;
		
		if(muertesCount == 0){
			awesome = this.add.bitmapText(390,56,'arcade',"YOU ARE \nAWESOME");
			awesome.update();
		}
		if(scoreTotal == 109){
			superAwesome = this.add.bitmapText(16,56,'arcade',"YOU ARE \nGOD AND\nAWESOME");
			superAwesome.update();
		}
		twitter = this.add.sprite(460,126,'twitter');
		twitter.inputEnabled = true;
		twitter.events.onInputDown.add(tweetscore, this);
		
	},
	update:function(){
		
	}
};

function empezarJuego1(){
	if(score >= 0){
		this.game.state.start('preloaderNivel1');
		}
		
}
function empezarJuego2(){
	if(score >= 29){
		
		this.game.state.start('preloaderNivel2');
	}
	
}
function empezarJuego3(){
	if(score+score2 >= 79){
		this.game.state.start('preloaderNivel3');
	}
	
}

function tweetscore(){
        //share score on twitter
        var tweetbegin = 'http://twitter.com/home?status=';
        var tweettxt = 'I scored '+(score+score2+score3)+' at this game -' + window.location.href + ' and I have died '+muertesCount+' times #nutmegAdventures';
        var finaltweet = tweetbegin +encodeURIComponent(tweettxt);
        window.open(finaltweet,'_blank');
}
