BasicGame.preloaderNivel1 = function(game){
	this.preloaderBar = null;
	
	var text ="";
};
BasicGame.preloaderNivel1.prototype = {
	preload:function(){
		
		this.game.world.setBounds(0,0,200,0);
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.preloaderBar = this.add.sprite(100,200,'preloaderBar');
		this.load.bitmapFont('arcade','img/font.png','img/font.fnt');
		this.load.tilemap('mapa1','mapa1.json',null,Phaser.Tilemap.TILED_JSON);
		this.load.image('cielo','img/cielo.png');
		this.load.image('pinchos2','img/pinchos2.png');
		this.load.image('star','img/star.png');
	},
	create:function(){
		text = this.add.bitmapText(150,150,'arcade',"Loading Level 1...");
		text.update();
		this.preloaderBar.cropEnabled = false;
		
	},
	update:function(){
		if(this.cache.isSoundDecoded('musica')){
			
			this.state.start('nivel1');	
		}
	}
};