BasicGame.nivel2 = function(game){
	
};

var prota;
var mapa2;
var layer;
var layer2;
var agua;
var pinchos;
var estrellas;
var score=score;
var score2=score;
var scoreText = null;
var tiempo;
var tiempoTexto=null;
var counter=0;
var cascada;
var meta;

BasicGame.nivel2.prototype = {
	preload: function(){
		this.load.image('pinchos2','img/pinchos2.png');
		this.load.image('tiles','img/tiles.png');
		this.load.spritesheet('cascada','img/cascada.png',16,16,18);
		//this.load.image('meta','img/43270v2.png');
		//this.game.time.advancedTiming = true;
		
	},
	create: function(){
		this.physics.startSystem(Phaser.Physics.ARCADE);
		this.stage.backgroundColor = '#857B79';
		musica.stop();
        musica2.play();
        musica2.volume = .1;
		score2=0;
		mapa2 = this.add.tilemap('mapa2');
		mapa2.addTilesetImage('tiles','tiles');
		mapa2.addTilesetImage('pinchos2','pinchos2');
		mapa2.addTilesetImage('cielo2','cielo2');
		mapa2.addTilesetImage('cascada','cascada');
		//mapa2.addTilesetImage('43270v2','meta');
		
		mapa2.setCollision(41); //suelo
		mapa2.setCollision(115); //pinchos
		mapa2.setCollisionByExclusion([  ], true, "Capa de patrones 2");
		mapa2.setCollisionByExclusion([  ],true,"agua");
		mapa2.setCollisionByExclusion([  ],true,"meta");
		
		
		layer = mapa2.createLayer('Capa de Patrones 1');
		meta = mapa2.createLayer('meta');
		agua = mapa2.createLayer('agua');
		
		layer2 = mapa2.createLayer('Capa de patrones 2');
		layer4 = mapa2.createLayer('Capa de patrones 4');
		
		layer.resizeWorld();
		
		
		pinchos = this.game.add.group();
		pinchos.enableBody = true;
		mapa2.createFromObjects('pinchos2',115,'pinchos2',0,true,false,pinchos);
		
		
		estrellas = this.game.add.group();
		estrellas.enableBody = true;
		mapa2.createFromObjects('star',116,'star',0,true,false,estrellas);
		
		prota = this.game.add.sprite(32,93,'prota');
		prota.animations.add('right',[0,1,2],10,true);
		
		cascada = this.game.add.group();
		mapa2.createFromObjects('cascada',3470,'cascada',0,true,false,cascada);
		mapa2.createFromObjects('cascada',3472,'cascada',0,true,false,cascada);
		cascada.forEach(movimientoCascada,this);
		this.physics.enable(prota);
		this.physics.arcade.gravity.y = 309;
		
		
		prota.body.collideWorldBounds = true;
		prota.body.immovable = true;
		this.camera.follow(prota);
		
		
		scoreText = this.add.bitmapText(16,16,'arcade',"STARS "+(score+score2)+"x110");
        scoreText.fixedToCamera = true;
        /*
        tiempoText= this.add.bitmapText(16,36,'arcade',"TIME "+counter);
        tiempoText.fixedToCamera = true;
        tiempo = this.time.events.loop(Phaser.Timer.SECOND, contador, this);*/
		/*
		mute = this.add.bitmapText(456,16,'arcade',"MUTE");
		mute.inputEnabled = true;
		mute.fixedToCamera = true;
		mute.events.onInputDown.add(mutear2,this);*/
		muertes = this.add.bitmapText(390,16,'arcade',"DEATHS "+muertesCount);
		muertes.inputEnabled = true;
		muertes.fixedToCamera = true;
		muertes.events.onInputDown.add(mutear,this);
	},
	update: function(){
		this.physics.arcade.collide(prota,layer);
		this.physics.arcade.collide(pinchos,layer);
		this.physics.arcade.collide(estrellas,layer);
		this.physics.arcade.collide(pinchos,layer2);
		this.physics.arcade.collide(estrellas,layer2);
		this.physics.arcade.collide(estrellas,agua);
		this.physics.arcade.collide(prota,layer2);
		this.physics.arcade.collide(prota,agua);
		this.physics.arcade.collide(layer,agua);
		this.physics.arcade.collide(pinchos,agua);
		this.physics.arcade.collide(cascada,layer);
		this.physics.arcade.collide(pinchos,prota,reintentar2,null,this);
		this.physics.arcade.collide(prota,meta,nivel3,null,this);
		this.physics.arcade.overlap(prota,estrellas,conseguirEstrella2,null,this);
		prota.animations.play('right');
		
		
		if(prota.body.onFloor() && prota.body.x >= 32 && prota.body.y < 300){
			prota.body.velocity.x = 200;
			this.physics.arcade.gravity.y = 408;
		}
		this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.SPACEBAR]);
		var barraEspaciadora = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		if(barraEspaciadora.isDown && prota.body.onFloor()){
			prota.body.velocity.y = -180;
			prota.body.velocity.x -= 3;
			this.physics.arcade.gravity.y = 408;
			prota.frame = 3;
			salto.play();
		}
		if(prota.body.onWall()){
			prota.body.velocity.x = 200;
			
		}
		if(prota.body.onWall() && prota.scale.x < 0){
			prota.body.velocity.x -= 200;
			if(!prota.body.onFloor() && prota.body.onWall() && prota.scale.x < 0){
				prota.body.velocity.x -= 200;
			}
			 
		}
		/*bajo agua choca piedras*/
		if(prota.body.onWall() && prota.scale.x < 0 && prota.body.y > 520 && prota.body.y < 830){
			prota.body.velocity.x = -90;		 
		}
		else if(prota.body.onWall() && prota.scale.x > 0 && prota.body.y > 520 && prota.body.y < 830){
			prota.body.velocity.x = 90;		 
		}
		
		if(prota.body.x == 976 && prota.body.y > 290){
			prota.anchor.setTo(.5, 0);
			prota.body.velocity.x -= 200;
			prota.scale.x -= 1.0;
			
			
		}
		if(prota.body.x == 984.0 && prota.body.y > 580){
			prota.anchor.setTo(.5, 0);
			prota.body.velocity.x -= 200;
			prota.scale.x -= 1.0;
	
		}
		if(prota.body.y > 920 && prota.scale.x > 0){
			prota.body.velocity.x =250;
		}else if(prota.body.y > 920 && prota.scale.x < 0){
			prota.body.velocity.x = -250;
		}
		if(prota.body.y > 1100){
			this.stage.backgroundColor = '#e34f62';
		}
		
		if(prota.body.x == 0){
			prota.body.velocity.x = 200;
			prota.scale.x = 1.0;
		}
		
		if(prota.body.x > 980 &&  prota.body.y > 520){
			prota.alpha = 0.5;
			pinchos.alpha = 0.5;
			prota.anchor.setTo(.5, 0);
			prota.body.velocity.x = -90;
			prota.scale.x = -1.0;
			prota.body.bounce.set(0.4);
			estrellas.alpha = 0.5;
			
		}
		if(prota.body.x == 0 &&  prota.body.y > 520){	
			prota.alpha = 0.5;
			pinchos.alpha = 0.5;
			prota.anchor.setTo(.5, 0);
			prota.body.velocity.x = 90;
			prota.scale.x = 1.0;
			estrellas.alpha = 0.5;
		}
		if(prota.y > 830){
			prota.alpha = 1.0;
			estrellas.alpha = 1.0;
			//prota.body.bounce.set(0.0);
		}
		

		
	},
	render:function(){
		//this.game.debug.body(prota);
		//this.game.debug.spriteInfo(prota,90,50);
		//layer.debug = true;
		//this.game.debug.debugMap
		//this.game.debug.text(this.game.time.fps || '--', 2, 14, "#00ff00");   
	}
	
};
function conseguirEstrella2(prota,estrella){
	sonido.play();
    estrella.kill();
    score2 +=1;
    scoreText.text = 'STARS: '+(score+score2)+"x110";
}
function reintentar2(){
    this.game.paused = false;
	musica2.stop();
	score2=0;
    this.state.restart();
    counter = 0;
	//musica2.play();
	muertesCount++;
	
}
function contador(){
    counter++;
    tiempoText.setText('TIME: ' + counter);
}
function movimientoCascada(cascadilla){
	cascadilla.animations.add('mover',[0,1,2,3,4],10,true); //arreglar los frames 7,8
	cascadilla.animations.play('mover');
}
function nivel3(){
	musica2.stop();
	this.state.start('niveles'); //cambiar a this.state.start('preloaderNivel3);
}

function mutear2(){
	musica2.pause();
}

