BasicGame.preloaderNivel3 = function(game){
	this.preloaderBar = null;
	var text ="";
};
BasicGame.preloaderNivel3.prototype = {
	preload:function(){
		this.game.world.setBounds(0,0,200,0);
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.preloaderBar = this.add.sprite(100,200,'preloaderBar');
		this.load.bitmapFont('arcade','img/font.png','img/font.fnt');
		this.load.tilemap('mapa3','mapa3.json',null,Phaser.Tilemap.TILED_JSON);
		this.load.audio('musica3','sounds/Rocky (Master System)- 08 - Speed Training.mp3');
		this.load.image('star','img/star.png');
		this.load.image('pinchos2','img/pinchos2.png');
		this.load.spritesheet('cascada','img/cascada.png',16,16,18);
		this.load.image('meta','img/43270v2.png');
		this.load.spritesheet('enemigos','img/baddie_cat_1.png',16,16);
	},
	create:function(){
		text = this.add.bitmapText(150,150,'arcade',"Loading Level 3...");
		text.update();
		this.preloaderBar.cropEnabled = false;
	},
	update:function(){
		if(this.cache.isSoundDecoded('musica3')){
			this.ready = true;
			this.state.start('nivel3');
		}
	}
};