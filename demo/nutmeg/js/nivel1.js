BasicGame.nivel1 = function(game){
	
};
var prota;
var mapa1;
var layer;
var layer2;
var pinchos;
var estrellas;
var counter=0;
var score=0;
var scoreText = null;
var tiempo;
var tiempoTexto=null;
var mute=null;
var meta;
var muertes;
var muertesCount=0;

BasicGame.nivel1.prototype = {
	preload:function(){
		this.load.image('tiles','img/tiles.png');

		
		//this.load.image('43270','img/43270.png');
		//this.game.time.advancedTiming = true; //fps
		
	},
	create:function(){
		
		this.stage.backgroundColor = '#4949FF';
		musica.play();
		score=0;
		mapa1 = this.add.tilemap('mapa1');
		mapa1.addTilesetImage('tiles','tiles');
		mapa1.addTilesetImage('pinchos2','pinchos2');
		mapa1.addTilesetImage('star','star');
		mapa1.addTilesetImage('cielo','cielo');
		//mapa1.addTilesetImage('43270','43270'); //meta
		
		mapa1.setCollision(26);
		mapa1.setCollision(4); //todo lo marron del suelo
		mapa1.setCollision(28);
		mapa1.setCollision(34);
		mapa1.setCollisionBetween(41,43);
		mapa1.setCollisionBetween(105,108);
		mapa1.setCollision(115); //pinchos
		
		mapa1.setCollisionByExclusion([  ], true, "Capa de patrones 2");
		mapa1.setCollisionByExclusion([  ], true, "Capa de patrones 4");
		
		layer4 = mapa1.createLayer('Capa de patrones 4');
		layer4.resizeWorld();
		layer = mapa1.createLayer('Capa de Patrones 1');
		layer.resizeWorld();
		layer2 = mapa1.createLayer('Capa de patrones 2');
		layer3 = mapa1.createLayer('Capa de patrones 3');
		
		this.game.physics.enable(layer4);	
		this.game.physics.enable(layer2);	
		layer2.resizeWorld();
		layer4.resizeWorld();
		prota = this.game.add.sprite(32,93,'prota');
		prota.animations.add('right',[0,1,2],10,true);
		
		this.physics.enable(prota);
		this.physics.arcade.gravity.y = 309;
		
		
		prota.body.collideWorldBounds = true;
		prota.body.immovable = true;
		this.camera.follow(prota);
		
		pinchos = this.game.add.group();
		pinchos.enableBody = true;
		
		mapa1.createFromObjects('pinchos2',115,'pinchos2',0,true,false,pinchos);
		
		estrellas = this.game.add.group();
		estrellas.enableBody = true;
		
		mapa1.createFromObjects('star',116,'star',0,true,false,estrellas);
		score = 0;
		scoreText = this.add.bitmapText(16,16,'arcade',"STARS 0x110");
        scoreText.fixedToCamera = true;
        /*
        tiempoText= this.add.bitmapText(16,36,'arcade',"TIME 0");
        tiempoText.fixedToCamera = true;
        tiempo = this.time.events.loop(Phaser.Timer.SECOND, contador, this);*/
		/*
		mute = this.add.bitmapText(456,16,'arcade',"MUTE");
		mute.inputEnabled = true;
		mute.fixedToCamera = true;
		mute.events.onInputDown.add(mutear,this);*/
		//mute.events.onInputDown.add(noMutear,this);
		muertes = this.add.bitmapText(350,16,'arcade',"DEATHS "+muertesCount);
		muertes.inputEnabled = true;
		muertes.fixedToCamera = true;
		muertes.events.onInputDown.add(mutear,this);
		
		
	},
	update:function(){
		
		this.physics.arcade.collide(prota,layer);
		this.physics.arcade.collide(prota,layer2);
		this.physics.arcade.collide(pinchos,layer);
		this.physics.arcade.collide(pinchos,layer2);
		this.physics.arcade.collide(pinchos,prota,reintentar,null,this);
		this.physics.arcade.collide(estrellas,layer);
		this.physics.arcade.collide(estrellas,layer2);
		this.physics.arcade.collide(layer,layer4);
		this.physics.arcade.overlap(prota,estrellas,conseguirEstrella,null,this);
		this.physics.arcade.collide(prota,layer4,nivel2,null,this);

		prota.animations.play('right');
		
		if(prota.body.onFloor() && prota.body.x >= 32 && prota.body.y < 300){
			prota.body.velocity.x = 150;
			this.physics.arcade.gravity.y = 408;
		}
		this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.SPACEBAR]);
		var barraEspaciadora = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		if(barraEspaciadora.isDown && prota.body.onFloor()){
			prota.body.velocity.y = -180;
			prota.body.velocity.x -= 3;
			this.physics.arcade.gravity.y = 408;
			prota.frame = 3;
			salto.play();
		}
		if(prota.body.onWall()){
			prota.body.velocity.x = 150;
			
		}
		if(prota.body.onWall() && prota.scale.x < 0){
			prota.body.velocity.x -= 150;
			if(!prota.body.onFloor() && prota.body.onWall() && prota.scale.x < 0){
				prota.body.velocity.x -= 150;
			}
			 
		}
		
		if(prota.body.x == 976 && prota.body.y > 290){
			prota.anchor.setTo(.5, 0);
			prota.body.velocity.x -= 150;
			prota.scale.x -= 1.0;
			
			
		}
		if(prota.body.x == 984.0 && prota.body.y > 580){
			prota.anchor.setTo(.5, 0);
			prota.body.velocity.x -= 150;
			prota.scale.x -= 1.0;
	
		}
		if(prota.body.y > 920 && prota.scale.x > 0){
			prota.body.velocity.x =200;
		}else if(prota.body.y > 920 && prota.scale.x < 0){
			prota.body.velocity.x = -200;
		}
		if(prota.body.y > 1100){
			//this.stage.backgroundColor = '#e34f62'; cambia color, desactivo de momento
		}
		
		if(prota.body.x == 0){
			prota.body.velocity.x = 150;
			prota.scale.x = 1.0;
		}
	},
	render:function(){
		//this.game.debug.body(prota);
		//this.game.debug.spriteInfo(prota,90,50);
		//layer.debug = true;
		//this.game.debug.debugMap
		//this.game.debug.text(this.game.time.fps || '--', 2, 14, "#00ff00");   
	}
};

function reintentar(){
    this.game.paused = false;
	musica.stop();
    this.state.restart();
	score = 0;
    counter = 0;
	muertesCount++;
	
	
}

function conseguirEstrella(prota,estrella){
	sonido.play();
    estrella.kill();
    score +=1;
    scoreText.text = 'STARS: '+score+"x110";
   
}
/*
function contador(){
    counter++;
    tiempoText.setText('TIME: ' + counter);
}*/
function mutear(){
	musica.pause();
	return true;
}
/*
function metaFinal(prota,layer4){
	this.state.restart();
}*/
function nivel2(){
	this.state.start('niveles');
}