BasicGame.preloaderNivel2 = function(game){
	this.preloaderBar = null;
	var text ="";
};
BasicGame.preloaderNivel2.prototype = {
	preload:function(){
		this.game.world.setBounds(0,0,200,0);
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.preloaderBar = this.add.sprite(100,200,'preloaderBar');
		this.load.bitmapFont('arcade','img/font.png','img/font.fnt');
		this.load.tilemap('mapa2','mapa2.json',null,Phaser.Tilemap.TILED_JSON);
		this.load.image('star','img/star.png');
		this.load.image('cielo2','img/cielo2.png');
		this.load.audio('musica2','sounds/Zillion Music (SMS) - Push!.mp3');
		
		
	},
	create:function(){
		text = this.add.bitmapText(150,150,'arcade',"Loading Level 2...");
		text.update();
		this.preloaderBar.cropEnabled = false;
	},
	update:function(){
		if(this.cache.isSoundDecoded('musica2')){
			this.state.start('nivel2');
		}
	}
};