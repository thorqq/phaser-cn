BasicGame.MainMenu = function(game){
	var boton1 = null;
	var boton2 = null;
	var boton3 = null;
	var titulo = null;
	var musica = null;
  	var sonido = null; 
  	var salto = null;
	var musica3 = null;
	var logo = null;
	var tween=null;
};
BasicGame.MainMenu.prototype = {
	create:function(){
		boton1 = this.add.button(this.game.world.centerX-50,180,'boton1',empezarJuego,this,1,0,0);
		boton2 = this.add.button(this.game.world.centerX-50,200,'boton2',verNiveles,this,1,0,0);
		boton3 = this.add.button(this.game.world.centerX-50,220,'boton3',creditos,this,1,0,0);
		//titulo.update(); //hay que hacer update para cada cambio de posicion...
		//titulo = this.add.bitmapText(100,50,'arcade',"NutMeg   adventures");
		logo = this.add.sprite(-230,0,'logo');
		logo.scale.setTo(0.2,0.2)
		//titulo.update();
		
		musica = this.add.audio('musica',1,true);
		musica2 = this.add.audio('musica2',1,true);
		musica3 = this.add.audio('musica3',1,true);
        sonido = this.add.audio('fx',1,false);
		salto = this.add.audio('salto',1,false);
		salto.volume= .4;
		sonido.volume= .2;
        musica.play();
        musica.volume = .1;
		
		tween = this.game.add.tween(logo);
		tween.to({x: 130}, 3000); //lo movera a la posicion 200 en 3 segundos
		tween.start();
		this.physics.startSystem(Phaser.Physics.ARCADE);

	},
	update:function(){
		//this.input.onDown.addOnce(empezarJuego,this);
	}
};

function empezarJuego(){
	this.state.start('preloaderNivel1');
}
function verNiveles(){
	this.state.start('niveles');
}
function creditos(){
	this.state.start('creditos');
}