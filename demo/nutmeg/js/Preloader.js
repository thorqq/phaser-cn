BasicGame.Preloader = function(game){
	this.preloaderBar = null;
	this.ready = false;
	var text="";
};

BasicGame.Preloader.prototype = {
	preload:function(){
		//los assets cargados desde Boot.js
		this.preloaderBar = this.add.sprite(100,190,'preloaderBar');
		this.load.setPreloadSprite(this.preloaderBar);
		this.load.spritesheet('boton1','img/boton1.png',80,19);
		this.load.spritesheet('boton2','img/boton2.png',80,19);
		this.load.spritesheet('boton3','img/boton3.png',80,19);
		this.load.spritesheet('botonl1','img/botonlevel1.png',80,20);
		this.load.spritesheet('botonl2','img/botonlevel2.png',80,20);
		this.load.spritesheet('botonl3','img/botonlevel3.png',80,20);
		this.load.spritesheet('volver','img/return.png',80,21);
		this.load.bitmapFont('arcade','img/font.png','img/font.fnt');
		this.load.spritesheet('prota','img/player.png',16,18);
		this.load.image('logo','img/nutmeg.png');
		this.load.image('twitter','img/twitter2.png');
		this.load.audio('musica','sounds/musica.mp3');
		this.load.audio('fx','sounds/sfx.wav');
		this.load.audio('salto','sounds/Jump2.wav');
	},
	create:function(){
		//text = this.add.bitmapText(190,350,'arcade',"Cargando...");
		text = this.add.bitmapText(190,150,'arcade',"Loading...");
		text.update();
		this.preloaderBar.cropEnabled = false; //hasta que la barra no haya cargado no sera true
	},
	update:function(){
		if(this.ready == false && this.cache.isSoundDecoded('musica')){
			this.ready = true;
			this.state.start('MainMenu');
		}
	}
	
};