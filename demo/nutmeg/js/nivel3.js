BasicGame.nivel3 = function(game){
	
};

var prota;
var mapa3;
var layer;
var layer2;
var layer3;
var layer4;
var layer5;
var layer6;
var enemigos; //CREAR OTRO GRUPO DE ENEMIGOS PARA EL PISO INVERTIDO, PARA USAR DISTINTAS FUNCIONES AL CHOCAR
var enemigos2;
var enemigos3;
var enemigos4;
var estrellas;
var score3=score2;
var scoreText = null;
var tiempo;
var tiempoTexto=null;
var counter=60;
var estrellas;
var pinchos;
var cascada;
var meta;

BasicGame.nivel3.prototype = {
	preload: function(){
		this.load.image('tiles','img/tiles.png');
		
		//this.game.time.advancedTiming = true;
		
	},
	create: function(){
		this.physics.startSystem(Phaser.Physics.ARCADE);
		this.stage.backgroundColor = '#e34f62';
		musica.stop();
		musica2.stop();
        musica3.play();
        musica3.volume = .1;
		score3=0;
		
		mapa3 = this.add.tilemap('mapa3');
		mapa3.addTilesetImage('tiles','tiles');
		mapa3.addTilesetImage('43270v2','meta');
		//mapa3.setCollision(41); //suelo
		mapa3.setCollisionByExclusion([  ],true,"Capa de Patrones 1");
		mapa3.setCollisionByExclusion([  ],true,"Capa de patrones 2");
		mapa3.setCollisionByExclusion([  ],true,"piedras");
		mapa3.setCollisionByExclusion([  ],true,"agua");
		mapa3.setCollisionByExclusion([  ],false,"aguafondo");
		mapa3.setCollisionByExclusion([  ],true,"meta");
		
		layer = mapa3.createLayer('Capa de Patrones 1');
		layer3 = mapa3.createLayer('fondo');
		layer2 = mapa3.createLayer('Capa de patrones 2');
		layer4 = mapa3.createLayer('piedras');
		layer5 = mapa3.createLayer('agua');
		layer6 = mapa3.createLayer('aguafondo');
		meta = mapa3.createLayer('meta');
		layer.resizeWorld();
		
		
		prota = this.game.add.sprite(32,93,'prota');
		prota.animations.add('right',[0,1,2],10,true);
		
		this.physics.enable(prota);
		this.physics.arcade.gravity.y = 309;
		
		
		prota.body.collideWorldBounds = true;
		prota.body.immovable = true;
		this.camera.follow(prota);
		
		enemigos = this.game.add.group();
		enemigos.enableBody = true;
		mapa3.createFromObjects('enemigos',116,'enemigos',1,true,false,enemigos);
		enemigos.callAll('animations.add','animations','andar',[0,1],10,true);
		enemigos.callAll('animations.play','animations','andar');
		enemigos.setAll('body.velocity.x',-120);
		enemigos.setAll('body.velocity.y',20);
		enemigos.setAll('body.setBounce',12);
		enemigos.setAll('body.collideWorldBounds',false);
		
		enemigos2 = this.game.add.group();
		enemigos2.enableBody = true;
		mapa3.createFromObjects('enemigos2',117,'enemigos',1,true,false,enemigos2);
		enemigos2.callAll('animations.add','animations','andar',[2,3],10,true);
		enemigos2.callAll('animations.play','animations','andar');
		
		
		enemigos3 = this.game.add.group();
		enemigos3.enableBody = true;
		mapa3.createFromObjects('enemigos3',116,'enemigos',1,true,false,enemigos3);
		enemigos3.callAll('animations.add','animations','andar',[0,1],10,true);
		enemigos3.callAll('animations.play','animations','andar');
		
		enemigos4 = this.game.add.group();
		enemigos4.enableBody = true;
		mapa3.createFromObjects('enemigos4',117,'enemigos',1,true,false,enemigos4); //podria haber reciclado el 2 en tiled
		enemigos4.callAll('animations.add','animations','andar',[2,3],10,true);
		enemigos4.callAll('animations.play','animations','andar');
		
		
		estrellas = this.game.add.group();
		estrellas.enableBody = true;
		mapa3.createFromObjects('star',119,'star',0,true,false,estrellas);
		
		pinchos = this.game.add.group();
		pinchos.enableBody = true;
		mapa3.createFromObjects('pinchos2',120,'pinchos2',0,true,false,pinchos);
		
		
		cascada = this.game.add.group();
		mapa3.createFromObjects('cascada',146,'cascada',0,true,false,cascada);
		cascada.forEach(movimientoCascada,this);
		
		
		scoreText = this.add.bitmapText(16,16,'arcade',"STARS "+(score+score2)+"x110");
        scoreText.fixedToCamera = true;
        
        tiempoText= this.add.bitmapText(16,36,'arcade',"TIME "+counter);
        tiempoText.fixedToCamera = true;
        tiempo = this.time.events.loop(Phaser.Timer.SECOND, contador, this);
		
		/*mute = this.add.bitmapText(456,16,'arcade',"MUTE");
		mute.inputEnabled = true;
		mute.fixedToCamera = true;
		mute.events.onInputDown.add(mutear3,this);*/
		muertes = this.add.bitmapText(350,16,'arcade',"DEATHS "+muertesCount);
		muertes.inputEnabled = true;
		muertes.fixedToCamera = true;
		muertes.events.onInputDown.add(mutear,this);
			
	},
	update: function(){
		this.physics.arcade.collide(prota,layer);
		this.physics.arcade.collide(layer,layer3);
		this.physics.arcade.collide(enemigos,layer);
		this.physics.arcade.collide(prota,layer4);
		this.physics.arcade.collide(enemigos,layer4,chocaIzquierda,null,this);
		this.physics.arcade.collide(enemigos,layer2,chocaIzquierda,null,this);
		//this.physics.arcade.collide(enemigos,layer2);
		this.physics.arcade.collide(prota,layer2);
		this.physics.arcade.collide(prota,enemigos,chocaEnemigo,null,this);
		this.physics.arcade.collide(estrellas,layer);
		this.physics.arcade.collide(estrellas,layer2);
		this.physics.arcade.collide(estrellas,layer4);
		this.physics.arcade.overlap(prota,estrellas,conseguirEstrella3,null,this);
		this.physics.arcade.collide(enemigos2,layer);
		this.physics.arcade.collide(prota,enemigos2,chocaEnemigo,null,this);
		this.physics.arcade.collide(pinchos,prota,reintentar3,null,this);
		this.physics.arcade.collide(pinchos,layer);
		this.physics.arcade.collide(pinchos,layer2);
		this.physics.arcade.collide(enemigos3,layer);
		this.physics.arcade.collide(prota,enemigos3,chocaEnemigo,null,this);
		this.physics.arcade.collide(enemigos4,layer);
		this.physics.arcade.collide(prota,enemigos4,chocaEnemigo,null,this);
		this.physics.arcade.collide(prota,layer5);
		this.physics.arcade.collide(layer,layer5);
		this.physics.arcade.collide(layer,layer6);
		this.physics.arcade.collide(cascada,layer);
		this.physics.arcade.collide(cascada,layer5);
		this.physics.arcade.collide(meta,layer2);
		this.physics.arcade.collide(pinchos,layer5);
		this.physics.arcade.collide(estrellas,layer5);
		this.physics.arcade.collide(prota,meta,fin,null,this);
		
		prota.animations.play('right');
		
		if(prota.body.onFloor() && prota.body.x >= 32 && prota.body.y < 300){
			prota.body.velocity.x = 250;
			this.physics.arcade.gravity.y = 408;
		}
		this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.SPACEBAR]);
		var barraEspaciadora = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		if(barraEspaciadora.isDown && prota.body.onFloor()){
			prota.body.velocity.y = -180;
			prota.body.velocity.x -= 3;
			this.physics.arcade.gravity.y = 408;
			prota.frame = 3;
			salto.play();
		}
		if(prota.body.onWall()){
			prota.body.velocity.x = 250;
			
		}
		if(prota.body.onWall() && prota.scale.x < 0){
			prota.body.velocity.x = -250;	 
		}
		if(prota.body.onWall() && prota.scale.x < 0){
			prota.body.velocity.x = -250;
			if(!prota.body.onFloor() && prota.body.onWall() && prota.scale.x < 0){
				prota.body.velocity.x = -250;
			}
			 
		}
		
		if(prota.body.x > 970 && prota.body.y > 290){
			prota.anchor.setTo(.5, 0);
			prota.body.velocity.x = -250;
			prota.scale.x = -1.0;
		}
		if(prota.body.x == 0){
			prota.anchor.setTo(.5, 0);
			prota.body.velocity.x = 250;
			prota.scale.x = 1.0;
		}
		
		if(this.game.camera.y > 161){
			enemigosPiso2();
			this.physics.arcade.collide(enemigos2,layer2,chocaDerecha,null,this);
			this.physics.arcade.collide(enemigos2,layer4,chocaDerecha,null,this);
		}
		if(this.game.camera.y > 410){
			enemigosPiso3();
			this.physics.arcade.collide(enemigos3,layer2,chocaIzquierda,null,this);
			this.physics.arcade.collide(enemigos3,layer4,chocaIzquierda,null,this);
		}
		if(this.game.camera.y > 621){
			enemigosPiso4();
			this.physics.arcade.collide(enemigos4,layer2,chocaDerecha,null,this);
			this.physics.arcade.collide(enemigos4,layer4,chocaDerecha,null,this);
		}
		
		
		
		/*bajo agua choca piedras*/
		if(prota.body.onWall() && prota.scale.x < 0 && prota.body.y > 2351 && prota.body.y < 2480){
			prota.body.velocity.x = -90;		 
		}
		else if(prota.body.onWall() && prota.scale.x > 0 && prota.body.y > 2351 && prota.body.y < 2480){
			prota.body.velocity.x = 90;		 
		}
		
		if(prota.body.y > 2351 && prota.scale.x < 0 && prota.body.y < 2480){
			prota.alpha = 0.5;
			pinchos.alpha = 0.5;
			estrellas.alpha = 0.5;
			prota.anchor.setTo(.5, 0);
			prota.body.velocity.x = -90;
			prota.scale.x = -1.0;
			prota.body.bounce.set(0.4);
			
		}else if(prota.body.y > 2351 &&  prota.scale.x > 0 && prota.body.y < 2480){	
			prota.alpha = 0.5;
			pinchos.alpha = 0.5;
			estrellas.alpha = 0.5;
			prota.anchor.setTo(.5, 0);
			prota.body.velocity.x = 90;
			prota.scale.x = 1.0;
			prota.alpha = 0.5;
		}
		if(prota.body.y > 2496){
			prota.alpha = 1.0;
			estrellas.alpha = 1.0;
		}
		
		if(counter == 0){
			this.game.paused = false;
			musica3.stop();
			score3=0;
    		this.state.restart();
    		counter = 60;
			//musica2.play();
		}
	},
	render:function(){
		//this.game.debug.body(prota);
		//this.game.debug.spriteInfo(prota,90,50);
		//this.game.debug.cameraInfo(game.camera, 32, 132);
		//layer.debug = true;
		//layer2.debug = true;
		//layer3.debug=true;
		//layer4.debug = true;
		//layer5.debug = true;
		//layer6.debug = true;
		//this.game.debug.debugMap;
		//this.game.debug.text(this.game.time.fps || '--', 2, 14, "#00ff00");   
	}
	
};

function chocaEnemigo(prota,enemigos){
	this.game.paused = false;
	musica3.stop();
	score3=0;
    this.state.restart();
    counter = 60;
    muertesCount++;
}

function chocaIzquierda(enemigo){
		enemigo.body.velocity.y = -180;
		enemigo.body.velocity.x = -120;
		this.physics.arcade.gravity.y = 508;
		enemigo.frame = 0;
	
}

function chocaDerecha(enemigo){
		enemigo.body.velocity.y = 180;
		enemigo.body.velocity.x = 120;
		this.physics.arcade.gravity.y = 508;
		enemigo.frame = 0;	
}

function contador(){
    counter--;
    tiempoText.setText('TIME: ' + counter);
}
function conseguirEstrella3(prota,estrella){
	sonido.play();
    estrella.kill();
    score3 +=1;
    scoreText.text = 'STARS: '+(score+score2+score3)+"x110";
}
function enemigosPiso2(){
		enemigos2.setAll('body.velocity.x',120);
		enemigos2.setAll('body.velocity.y',20);
		enemigos2.setAll('body.setBounce',12);
		enemigos2.setAll('body.collideWorldBounds',false);
		
}
function enemigosPiso3(){
		enemigos3.setAll('body.velocity.x',-120);
		enemigos3.setAll('body.velocity.y',20);
		enemigos3.setAll('body.setBounce',12);
		enemigos3.setAll('body.collideWorldBounds',false);
}

function enemigosPiso4(){
		enemigos4.setAll('body.velocity.x',120);
		enemigos4.setAll('body.velocity.y',20);
		enemigos4.setAll('body.setBounce',12);
		enemigos4.setAll('body.collideWorldBounds',false);
		
}
function reintentar3(){
    this.game.paused = false;
	score3=0;
    this.state.restart();
    counter = 60;
	//musica2.play();
	muertesCount++;
}

function mutear3(){
	musica3.pause();
}
function fin(){
	this.state.start('niveles');
}
