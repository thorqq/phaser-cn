window.onload = function() {
	
	var game = new Phaser.Game(640, 480, Phaser.CANVAS, "", {create: onCreate});                

	// 用于显示障碍物方块的画布
	var obstaclesCanvas;
	// 用于显示灯光的画布
	var lightCanvas; 
	// 障碍物（方块）的数量
	var numBoxes = 5;
	// 存储多边形数据
	var polygons = [];
	
	function onCreate() {
		// 监听鼠标移动事件	
          moveIndex = game.input.addMoveCallback(move, this);
		// 用于显示障碍物的画布
		obstaclesCanvas = game.add.graphics(0,0); 
		// line style of obstacle canvas
		obstaclesCanvas.lineStyle(4, 0xffffff, 1);
		
        // 用于显示灯光的画布
		lightCanvas = game.add.graphics(0,0);
		// 随机放置几个方块
        for(var i = 0; i < numBoxes; i++){
			randomBox();	
		}
		// 存放游戏边界数据
		polygons.push([[-1,-1],[game.width+1,-1],[game.width+1,game.height+1],[-1,game.height+1]]);			
	}
	
	function randomBox(){
		do{
			// 用随机的宽、高、左上角坐标画方块
			var width = game.rnd.between(50,150);
			var height = game.rnd.between(50,150);
			var startX = game.rnd.between(10,game.width-160);
			var startY = game.rnd.between(10,game.height-160);
		}while(boxesIntersect(startX,startY,width,height))
		// 画方块
		obstaclesCanvas.drawRect(startX, startY, width, height);
		// 保存到 polygons 数组中
		polygons.push([[startX,startY],[startX+width,startY],[startX+width,startY+height],[startX,startY+height]]);		
	}
	
	// 防止方块互相之间重叠，否则visibility_polygon_dev就不能使用了
	function boxesIntersect(x,y,w,h){  
		for(var i = 0; i < polygons.length; i++){
			if(x<polygons[i][1][0] && x+w>polygons[i][0][0] && y<polygons[i][3][1] && y+h>polygons[i][0][1]){
				return true;
			}
		}
		return false;
	}
          
     function move(){ 
		// 鼠标移动的时候创建新的可视的多边形	
     	var visibility = createLightPolygon(game.input.worldX, game.input.worldY);
     	// 绘制多边形
		lightCanvas.clear();
		lightCanvas.lineStyle(2, 0xff8800, 1);
		lightCanvas.beginFill(0xffff00); 
		lightCanvas.moveTo(visibility[0][0],visibility[0][1]);	
     	for(var i=1;i<=visibility.length;i++){
			lightCanvas.lineTo(visibility[i%visibility.length][0],visibility[i%visibility.length][1]);		
		}
		lightCanvas.endFill();
	}
	
	// 调用visibility_polygon_dev.js 根据障碍物方块和视点创建可视的多边形
	function createLightPolygon(x,y){
		var segments = VisibilityPolygon.convertToSegments(polygons);
		segments = VisibilityPolygon.breakIntersections(segments);
		var position = [x, y];
		if (VisibilityPolygon.inPolygon(position, polygons[polygons.length-1])) {
  			return VisibilityPolygon.compute(position, segments);
		}      
		return null;
	}	
}