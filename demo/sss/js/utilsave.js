var UtilSave = {};

UtilSave.getHighscore = function(id, length)
{
	if(length == undefined) length = 10;

	var scoreArr = [];
	for(var i=0;i<length;i++)
	{
		var score = localStorage[id+i];

		if(score == undefined) score = 0;
		else score = parseInt(score);

		scoreArr.push(score);
	}

	return scoreArr;
}

UtilSave.setHighscore = function(new_score, id, length)
{
	if(length == undefined) length = 10;

	var scoreArr = [];
	for(var i=0;i<length;i++)
	{
		var score = localStorage[id+i];

		if(score == undefined) score = 0;
		else score = parseInt(score);

		scoreArr.push(score);
	}

	scoreArr.push(parseInt(new_score));
	for(i=0;i<length+1;i++)
	{
		for(var j=i;j<length+1;j++)
		{
			if(scoreArr[j] > scoreArr[i])
			{
				var score1 = scoreArr[i];
				var score2 = scoreArr[j];
				scoreArr[i] = score2;
				scoreArr[j] = score1;
			}
		}
		localStorage[id+i] = scoreArr[i];
	}
}

UtilSave.getAchievement = function(id)
{
	var achievement = localStorage[id];
	if(achievement == undefined) achievement = 0;
	else achievement = parseInt(achievement);

	return achievement;
}

UtilSave.setAchievement = function(value, id)
{
	localStorage[id] = value;
}