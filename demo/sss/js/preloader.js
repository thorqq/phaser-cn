
SSS.Preloader = function (game)
{
	this.game = game;
};

SSS.Preloader.prototype =
{
	preload: function ()
	{
		this.game.load.image('sponsor_banner', 'asset/sponsor_banner.png');
		this.game.load.image('sponsor_splash', 'asset/sponsor_splash.jpg');
		this.game.load.image('rotate', 'asset/rotate.jpg');
		this.game.load.image('title_logo', 'asset/title/title_logo.png');
		//this.game.load.image('bg_game_1', 'asset/game/bg_game_1.jpg');
		this.game.load.image('bg_game_1_a', 'asset/game/bg_game_1_a.png');
		this.game.load.image('bg_game_1_b', 'asset/game/bg_game_1_b.png');
		this.game.load.image('bg_game_1_c', 'asset/game/bg_game_1_c.png');
		//this.game.load.image('bg_game_2', 'asset/game/bg_game_2.jpg');
		this.game.load.image('bg_game_2_a', 'asset/game/bg_game_2_a.png');
		this.game.load.image('bg_game_2_b', 'asset/game/bg_game_2_b.png');
		this.game.load.image('bg_game_2_c', 'asset/game/bg_game_2_c.png');
		this.game.load.image('bg_result', 'asset/game/bg_result.png');

		this.game.load.spritesheet('btn_audio', 'asset/button/btn_audio.png', 115, 124);
		this.game.load.spritesheet('btn_pause', 'asset/button/btn_pause.png', 115, 124);
		this.game.load.spritesheet('btn_back', 'asset/button/btn_back.png', 115, 124);
		this.game.load.spritesheet('btn_highscore', 'asset/button/btn_highscore.png', 115, 124);
		this.game.load.spritesheet('btn_left', 'asset/button/btn_left.png', 115, 124);
		this.game.load.spritesheet('btn_right', 'asset/button/btn_right.png', 115, 124);
		this.game.load.spritesheet('btn_next', 'asset/button/btn_next.png', 115, 124);
		this.game.load.spritesheet('btn_retry', 'asset/button/btn_retry.png', 115, 124);
		this.game.load.spritesheet('btn_play', 'asset/button/btn_play.png', 151, 164);
		this.game.load.spritesheet('btn_level', 'asset/button/btn_level.png', 149, 163);
		this.game.load.spritesheet('btn_sword', 'asset/button/btn_sword.png', 132, 136);
		this.game.load.spritesheet('btn_spear', 'asset/button/btn_spear.png', 132, 136);
		this.game.load.spritesheet('btn_staff', 'asset/button/btn_staff.png', 132, 136);

		this.game.load.image('popup_credits', 'asset/popup/popup_credits.png');
		this.game.load.image('popup_highscore', 'asset/popup/popup_highscore.png');
		this.game.load.image('popup_level', 'asset/popup/popup_level.png');

		this.game.load.image('tutorial1', 'asset/tutorial/tutorial1.png');
		this.game.load.image('tutorial2', 'asset/tutorial/tutorial2.png');
		this.game.load.image('tutorial3', 'asset/tutorial/tutorial3.png');
		this.game.load.image('tutorial4', 'asset/tutorial/tutorial4.png');
		this.game.load.image('tutorial5', 'asset/tutorial/tutorial5.png');
		this.game.load.image('tutorial6', 'asset/tutorial/tutorial6.png');

		this.game.load.image('red', 'asset/game/red.jpg');
		this.game.load.image('gui_bar', 'asset/game/gui_bar.png');
		this.game.load.image('gui_bar_empty', 'asset/game/gui_bar_empty.png');
		this.game.load.image('gui_bar_full', 'asset/game/gui_bar_full.png');
		this.game.load.image('gui_heart', 'asset/game/gui_heart.png');
		this.game.load.image('gui_star', 'asset/game/gui_star.png');
		this.game.load.image('gui_star_empty', 'asset/game/gui_star_empty.png');

    	this.game.load.atlasXML('effect', 'asset/game/effect.png', 'asset/game/effect.xml');
    	this.game.load.atlasXML('enemy', 'asset/game/enemy.png', 'asset/game/enemy.xml');
    	this.game.load.atlasXML('general', 'asset/game/general.png', 'asset/game/general.xml');

		//Font
		this.game.load.bitmapFont('grobold', 'asset/grobold.png', 'asset/grobold.xml');

		//Audio
		this.game.load.audio('bgm1', ['asset/audio/bgm1.mp3', 'asset/audio/bgm1.ogg']);
		this.game.load.audio('bgm2', ['asset/audio/bgm2.mp3', 'asset/audio/bgm2.ogg']);
		this.game.load.audio('sfx_button', ['asset/audio/sfx_button.mp3', 'asset/audio/sfx_button.wav']);
		this.game.load.audio('sfx_sword', ['asset/audio/sfx_sword.mp3', 'asset/audio/sfx_sword.wav']);
		this.game.load.audio('sfx_spear', ['asset/audio/sfx_spear.mp3', 'asset/audio/sfx_spear.wav']);
		this.game.load.audio('sfx_staff', ['asset/audio/sfx_staff.mp3', 'asset/audio/sfx_staff.wav']);

		this.bgC = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.5, 'bg_loading_c'); this.bgC.anchor.setTo(0.5,0.5);
		this.bgB = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.5, 'bg_loading_b'); this.bgB.anchor.setTo(0.5,0.5);
		this.bgA = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.5, 'bg_loading_a'); this.bgA.anchor.setTo(0.5,0.5);

		this.preloadBar = this.add.sprite(174, 397, 'bar_loading');
		this.load.setPreloadSprite(this.preloadBar);

		/*this.tempalabsBtn = this.add.button(this.game.world.width*0.5, this.game.world.height*0.75, 'tempalabs', this.onTempalabs, this, null);
		this.tempalabsBtn.anchor.setTo(0.5, 0.5);*/
	},

	loadUpdate: function()
	{
		this.bgC.x = this.game.world.width*0.5 + UtilAccel.tiltX * 150;
		this.bgC.y = this.game.world.height*0.5 + UtilAccel.tiltY * 150;
		this.bgB.x = this.game.world.width*0.5 + UtilAccel.tiltX * 100;
		this.bgB.y = this.game.world.height*0.5 + UtilAccel.tiltY * 100;
		this.preloadBar.x = 174 + UtilAccel.tiltX * 100;
		this.preloadBar.y = 397 + UtilAccel.tiltY * 100;
		this.bgA.x = this.game.world.width*0.5 + UtilAccel.tiltX * 50;
		this.bgA.y = this.game.world.height*0.5 + UtilAccel.tiltY * 50;
	},

	onTempalabs: function()
	{
		window.open("http://www.tempalabs.com/","_blank");
	},

	create: function ()
	{
		UtilSound.addSound('bgm1', this.game, true);
		UtilSound.addSound('bgm2', this.game, true);
		UtilSound.addSound('sfx_button', this.game);
		UtilSound.addSound('sfx_sword', this.game);
		UtilSound.addSound('sfx_spear', this.game);
		UtilSound.addSound('sfx_staff', this.game);

		UtilSound.playSound('bgm1');

		this.game.firstTime = true;
		if(this.game.device.desktop == false) this.game.stage.scale.forceOrientation(false, true, 'rotate');

		var splash = this.game.add.sprite(0,0,'sponsor_splash');
		splash.alpha = 0;
		this.game.add.tween(splash).to({alpha:1}, 1000, Phaser.Easing.Linear.In, true);
		var tween = this.game.add.tween(splash).to({}, 3000, Phaser.Easing.Linear.In, true);
		tween.onComplete.add(this.onChangeScreen, this);
	},

	onChangeScreen: function()
	{
		this.game.state.start('title');
	}
}