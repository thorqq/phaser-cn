
SSS.Title = function (game)
{
	this.game = game;
};

SSS.Title.prototype =
{
	create: function ()
	{
		//this.game.add.sprite(0, 0, 'bg_game_1');
		this.bgC = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.4, 'bg_game_1_c'); this.bgC.anchor.setTo(0.5,0.5);
		this.bgB = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.4, 'bg_game_1_b'); this.bgB.anchor.setTo(0.5,0.5);
		this.bgA = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.4, 'bg_game_1_a'); this.bgA.anchor.setTo(0.5,0.5);
		this.titleLogo = this.game.add.sprite(0, 0, 'title_logo');

		var banner = this.game.add.sprite(this.game.world.width*0.5, 60,'sponsor_banner');
		banner.anchor.setTo(0.5,0.5);

		this.playBtn = this.game.add.button(this.game.world.width*0.5, this.game.world.height*0.7, 'btn_play', this.onLevel, this, null, 0, 1);
		this.playBtn.anchor.setTo(0.5,0.5);
		this.creditBtn = this.game.add.button(this.game.world.width*0.75, this.game.world.height*0.8, 'btn_highscore', this.onCredit, this, null, 0, 1);
		this.creditBtn.anchor.setTo(0.5,0.5);
		this.audioBtn = this.game.add.sprite(this.game.world.width*0.25, this.game.world.height*0.8, 'btn_audio');
		this.audioBtn.anchor.setTo(0.5,0.5);
		this.audioBtn.inputEnabled = true;
		this.audioBtn.events.onInputDown.add(this.onAudio, this);

		/*this.tempalabsBtn = this.add.button(this.game.world.width - 5, this.game.world.height - 5, 'tempalabs', this.onTempalabs, this, null);
		this.tempalabsBtn.anchor.setTo(1, 1);
		this.tempalabsBtn.scale.setTo(0.5, 0.5);*/

		//Credit
		this.creditGroup = this.game.add.group();
		this.creditGroup.y = -50;
		this.creditGroup.create(0,0,'popup_credits');
		var backBtn = this.game.add.button(this.game.world.width*0.5, this.game.world.height*0.86, 'btn_back', this.onCredit, this, null, 0, 1);
		backBtn.anchor.setTo(0.5, 0.5);
		this.creditGroup.add(backBtn);
		var contentTxt = this.game.add.bitmapText(this.game.world.width*0.3, this.game.world.height*0.34, 'CREDITS', { font: '40px grobold', align: 'center' });
		contentTxt.setText('DEVELOPED BY\nTEMPA LABS\n\nPROGRAMMER\nAlif Harsan Pradipto\n\nART\nNovpixel\n\nMUSIC\nAnto');
		contentTxt.anchor.setTo(0.5,0);
		this.creditGroup.add(contentTxt);
		this.creditGroup.visible = false;

		//Level
		this.levelGroup = this.game.add.group();
		this.levelGroup.y = -50;
		this.levelGroup.create(0,0,'popup_level');
		var leftBtn = this.game.add.button(this.game.world.width*0.25, this.game.world.height*0.86, 'btn_left', this.levelPrev, this, null, 0, 1);
		leftBtn.anchor.setTo(0.5, 0.5);
		leftBtn.visible = false;
		this.levelGroup.leftBtn = leftBtn;
		this.levelGroup.add(leftBtn);
		var rightBtn = this.game.add.button(this.game.world.width*0.75, this.game.world.height*0.86, 'btn_right', this.levelNext, this, null, 0, 1);
		rightBtn.anchor.setTo(0.5, 0.5);
		this.levelGroup.rightBtn = rightBtn;
		this.levelGroup.add(rightBtn);
		backBtn = this.game.add.button(this.game.world.width*0.5, this.game.world.height*0.86, 'btn_back', this.onLevel, this, null, 0, 1);
		backBtn.anchor.setTo(0.5, 0.5);
		this.levelGroup.add(backBtn);

		//Highscore
		this.highscoreGroup = this.game.add.group();
		this.highscoreGroup.y = -45;
		this.highscoreGroup.create(0,0,'popup_highscore');
		backBtn = this.game.add.button(this.game.world.width*0.25, this.game.world.height*0.86 - 5, 'btn_back', this.onHighscoreBack, this, null, 0, 1);
		backBtn.anchor.setTo(0.5, 0.5);
		this.highscoreGroup.add(backBtn);
		playBtn = this.game.add.button(this.game.world.width*0.75, this.game.world.height*0.86 - 5, 'btn_right', this.onPlay, this, null, 0, 1);
		playBtn.anchor.setTo(0.5, 0.5);
		this.highscoreGroup.add(playBtn);
		var titleTxt = this.game.add.bitmapText(this.game.world.width*0.5, this.game.world.height*0.33, 'LEVEL 1', { font: '40px grobold', align: 'CENTER' });
		titleTxt.setText('LEVEL 1');
		titleTxt.anchor.setTo(0.5,0);
		this.highscoreGroup.add(titleTxt);
		this.highscoreGroup.titleTxt = titleTxt
		contentTxt = this.game.add.bitmapText(this.game.world.width*0.2, this.game.world.height*0.37, '1. 0000', { font: '40px grobold', align: 'left' });
		contentTxt.setText('1. 0000');
		contentTxt.anchor.setTo(0.5,0);
		this.highscoreGroup.add(contentTxt);
		this.highscoreGroup.contentTxt = contentTxt
		this.highscoreGroup.visible = false;


		this.levelArr = [];
		var levelUnlocked = UtilSave.getAchievement('tempalabs.sss.level');

		for(var i=0;i<6;i++)
		{
			var tempX = i%2 * 200 + 140;
			var tempY = Math.floor(i/2) * 170 + 200;

			var levelBtn = this.game.add.group();
			var levelImg = this.levelGroup.create(tempX, tempY, 'btn_level');
			if(i <= levelUnlocked) levelImg.frame = 0;
			else levelImg.frame = 1;
			levelBtn.add(levelImg);

			levelBtn.star1 = levelBtn.create(tempX + 0, tempY + 110, 'gui_star');
			levelBtn.star2 = levelBtn.create(tempX + 51, tempY + 110, 'gui_star');
			levelBtn.star3 = levelBtn.create(tempX + 102, tempY + 110, 'gui_star');
			levelBtn.star1.scale.setTo(0.4, 0.4);
			levelBtn.star2.scale.setTo(0.4, 0.4);
			levelBtn.star3.scale.setTo(0.4, 0.4);
			levelBtn.star1.visible = false;
			levelBtn.star2.visible = false;
			levelBtn.star3.visible = false;

			if(i <= levelUnlocked)
			{
				var starCount = UtilSave.getAchievement('tempalabs.sss.star.'+i);
				console.log(i+ ' starCount='+starCount);
				for(var j=0;j<starCount;j++)
				{
					if(j == 0) levelBtn.star1.visible = true;
					else if(j == 1) levelBtn.star2.visible = true;
					else if(j == 2) levelBtn.star3.visible = true;
				}
			}

			var levelTxt = this.game.add.bitmapText(tempX + 76, tempY + 50, ''+(i+1), { font: '60px grobold', align: 'center' });
			levelTxt.anchor.setTo(0.5,0);
			levelBtn.add(levelTxt);

			levelBtn.levelTxt = levelTxt;
			levelBtn.levelImg = levelImg;
			levelImg.inputEnabled = true;
			levelImg.events.onInputDown.add(this.onHighscore, this);

			this.levelArr.push(levelBtn);
		}
		this.page = 0;
		for(var i=0;i<this.levelArr.length;i++) this.levelArr[i].visible = false;
		this.levelGroup.visible = false;
	},

	onTempalabs: function()
	{
		window.open("http://www.tempalabs.com/","_blank");
	},

	update: function()
	{
		this.bgC.x = this.game.world.width*0.5 + UtilAccel.tiltX * 150;
		this.bgC.y = this.game.world.height*0.4 + UtilAccel.tiltY * 150;
		this.bgB.x = this.game.world.width*0.5 + UtilAccel.tiltX * 110;
		this.bgB.y = this.game.world.height*0.4 + UtilAccel.tiltY * 110;
		this.bgA.x = this.game.world.width*0.5 + UtilAccel.tiltX * 70;
		this.bgA.y = this.game.world.height*0.4 + UtilAccel.tiltY * 70;
	},

	onLevel: function()
	{
		UtilSound.playSound('sfx_button');
		if(this.levelGroup.visible == true)
		{
			for(var i=0;i<this.levelArr.length;i++) this.levelArr[i].visible = false;
			this.levelGroup.visible = false;
		}
		else
		{
			for(var i=0;i<this.levelArr.length;i++) this.levelArr[i].visible = true;
			this.levelGroup.visible = true;
		}
	},

	levelPrev: function()
	{
		UtilSound.playSound('sfx_button');
		this.page--;
		if(this.page == 0) this.levelGroup.leftBtn.visible = false;
		this.levelGroup.rightBtn.visible = true;

		var levelUnlocked = UtilSave.getAchievement('tempalabs.sss.level');
		console.log('levelUnlocked='+levelUnlocked);

		for(var i=0;i<this.levelArr.length;i++)
		{
			var levelBtn = this.levelArr[i];
			var levelNum = this.page*6 + i;
			levelBtn.levelTxt.setText(''+(levelNum+1));
			console.log(i+' levelNum='+levelNum);

			if(levelNum <= levelUnlocked) levelBtn.levelImg.frame = 0;
			else levelBtn.levelImg.frame = 1;

			levelBtn.star1.visible = false;
			levelBtn.star2.visible = false;
			levelBtn.star3.visible = false;

			if(levelNum <= levelUnlocked)
			{
				var starCount = UtilSave.getAchievement('tempalabs.sss.star.'+levelNum);
				console.log('starCount='+starCount);
				for(var j=0;j<starCount;j++)
				{
					if(j == 0) levelBtn.star1.visible = true;
					else if(j == 1) levelBtn.star2.visible = true;
					else if(j == 2) levelBtn.star3.visible = true;
				}
			}
		}
	},

	levelNext: function()
	{
		UtilSound.playSound('sfx_button');
		this.page++;
		if(this.page == 2) this.levelGroup.rightBtn.visible = false;
		this.levelGroup.leftBtn.visible = true;

		var levelUnlocked = UtilSave.getAchievement('tempalabs.sss.level');

		for(var i=0;i<this.levelArr.length;i++)
		{
			var levelBtn = this.levelArr[i];
			var levelNum = this.page*6 + i;
			levelBtn.levelTxt.setText(''+(levelNum+1));

			if(levelNum <= levelUnlocked) levelBtn.levelImg.frame = 0;
			else levelBtn.levelImg.frame = 1;

			levelBtn.star1.visible = false;
			levelBtn.star2.visible = false;
			levelBtn.star3.visible = false;

			if(levelNum <= levelUnlocked)
			{
				var starCount = UtilSave.getAchievement('tempalabs.sss.star.'+levelNum);
				for(var j=0;j<starCount;j++)
				{
					if(j == 0) levelBtn.star1.visible = true;
					else if(j == 1) levelBtn.star2.visible = true;
					else if(j == 2) levelBtn.star3.visible = true;
				}
			}
		}
	},

	onPlay: function(target)
	{
		UtilSound.playSound('sfx_button');
		this.game.state.start('games');
	},

	onAudio: function()
	{
		UtilSound.playSound('sfx_button');
		if(this.audioBtn.frame == 0)
		{
			this.audioBtn.frame = 1;
			UtilSound.mute();
		}else
		{
			this.audioBtn.frame = 0;
			UtilSound.unmute();
		}
	},

	onHighscoreBack: function()
	{
		UtilSound.playSound('sfx_button');
		this.onLevel();
		this.highscoreGroup.visible = false;
	},

	onHighscore: function(target)
	{
		UtilSound.playSound('sfx_button');
		for(var i=0;i<this.levelArr.length;i++)
		{
			if(this.levelArr[i].levelImg == target)
			{
				this.game.level = this.page*6 + i;
				break;
			}
		}

		this.onLevel();
		this.highscoreGroup.visible = true;
		this.highscoreGroup.titleTxt.setText('LEVEL '+(this.game.level+1));
		if(this.highscoreGroup.visible == true)
		{
			var scoreArr = UtilSave.getHighscore('tempalabs.sss.score.'+this.game.level);
			var tempStr = '';
			for(var i=0;i<scoreArr.length;i++)
			{
				tempStr += (i+1)+'. '+scoreArr[i]+'\n';
			}
			this.highscoreGroup.contentTxt.setText(tempStr);
		}
	},

	onCredit: function()
	{
		UtilSound.playSound('sfx_button');
		this.creditGroup.visible = !this.creditGroup.visible;
	}
}