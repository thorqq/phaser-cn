var SSS = {};

SSS.Boot = function (game)
{
	this.game = game;
};

SSS.Boot.prototype =
{
	preload: function ()
	{
        //this.game.load.image('bg_loading', 'asset/preloader/bg_loading.png');
        this.game.load.image('bg_loading_a', 'asset/preloader/bg_loading_a.png');
        this.game.load.image('bg_loading_b', 'asset/preloader/bg_loading_b.png');
        this.game.load.image('bg_loading_c', 'asset/preloader/bg_loading_c.png');
        this.game.load.image('bar_loading', 'asset/preloader/bar_loading.png');
        this.game.load.image('tempalabs', 'asset/tempalabs.png');

		this.game.stage.backgroundColor = '#a55405';
        this.game.stage.scaleMode = Phaser.StageScaleMode.SHOW_ALL; //resize your window to see the stage resize too
		this.game.stage.scale.setShowAll();
		this.game.stage.scale.pageAlignHorizontally = true;
		this.game.stage.scale.pageAlignVertically = true;
		this.game.stage.scale.refresh();
	},

	create: function ()
	{
		UtilAccel.init();

		this.game.state.start('preloader');
	}
}