
var LevelPack = new Array();
var level;

//1
level = {enemyCount:10, chargeSpeed:1, chargeBar:50};
LevelPack.push(level);
//2
level = {enemyCount:14, chargeSpeed:1, chargeBar:50};
LevelPack.push(level);
//3
level = {enemyCount:18, chargeSpeed:2, chargeBar:48};
LevelPack.push(level);
//4
level = {enemyCount:22, chargeSpeed:2, chargeBar:48};
LevelPack.push(level);
//5
level = {enemyCount:26, chargeSpeed:2, chargeBar:46};
LevelPack.push(level);
//6
level = {enemyCount:30, chargeSpeed:3, chargeBar:46};
LevelPack.push(level);

//7
level = {enemyCount:20, chargeSpeed:2, chargeBar:48};
LevelPack.push(level);
//8
level = {enemyCount:24, chargeSpeed:2, chargeBar:48};
LevelPack.push(level);
//9
level = {enemyCount:28, chargeSpeed:3, chargeBar:46};
LevelPack.push(level);
//10
level = {enemyCount:32, chargeSpeed:3, chargeBar:46};
LevelPack.push(level);
//11
level = {enemyCount:36, chargeSpeed:3, chargeBar:44};
LevelPack.push(level);
//12
level = {enemyCount:40, chargeSpeed:4, chargeBar:44};
LevelPack.push(level);

//13
level = {enemyCount:30, chargeSpeed:3, chargeBar:46};
LevelPack.push(level);
//14
level = {enemyCount:34, chargeSpeed:3, chargeBar:46};
LevelPack.push(level);
//15
level = {enemyCount:38, chargeSpeed:4, chargeBar:44};
LevelPack.push(level);
//16
level = {enemyCount:42, chargeSpeed:4, chargeBar:44};
LevelPack.push(level);
//17
level = {enemyCount:46, chargeSpeed:4, chargeBar:42};
LevelPack.push(level);
//18
level = {enemyCount:50, chargeSpeed:5, chargeBar:42};
LevelPack.push(level);

SSS.LevelPack = LevelPack;



Enemy = function (game, x, y, id)
{
	this.enemyId = id-1;

	if(id >= 1 && id <= 3)
	{
    	Phaser.Sprite.call(this, game, x, y, 'enemy');
	}else
	{
    	Phaser.Sprite.call(this, game, x, y, 'general');
	}

	this.ready();
};

Enemy.prototype = Object.create(Phaser.Sprite.prototype);
Enemy.prototype.constructor = Enemy;

Enemy.prototype.attack = function()
{
	if(this.enemyId < 3) this.frame = 4*this.enemyId;
	else this.frame = 4*(this.enemyId-3);

	if(this.enemyId == 0 || this.enemyId == 3) UtilSound.playSound('sfx_sword');
	else if(this.enemyId == 1 || this.enemyId == 4) UtilSound.playSound('sfx_spear');
	else if(this.enemyId == 2 || this.enemyId == 5) UtilSound.playSound('sfx_staff');

	this.game.add.tween(this.scale).to({x:2, y:2}, 500, Phaser.Easing.Back.Out, true, 0);
	this.game.add.tween(this).to({alpha:0}, 500, Phaser.Easing.Linear.Out, true, 500);
}
Enemy.prototype.dash = function()
{
	if(this.enemyId < 3) this.frame = 4*this.enemyId + 1;
	else this.frame = 4*(this.enemyId-3) + 1;
}
Enemy.prototype.hit = function()
{
	console.log('hit');
	if(this.enemyId < 3) this.frame = 4*this.enemyId + 2;
	else this.frame = 4*(this.enemyId-3) + 2;

	this.game.add.tween(this.scale).to({x:0.3, y:0.3}, 800, Phaser.Easing.Linear.Out, true, 0);
	this.game.add.tween(this).to({y:this.y-200}, 800, Phaser.Easing.Back.Out, true, 0);
	var tween = this.game.add.tween(this).to({alpha:0}, 500, Phaser.Easing.Linear.Out, true, 300);
	tween.onComplete.add(this.destroy, this);
}
Enemy.prototype.ready = function()
{
	if(this.enemyId < 3) this.frame = 4*this.enemyId + 3;
	else this.frame = 4*(this.enemyId-3) + 3;
}