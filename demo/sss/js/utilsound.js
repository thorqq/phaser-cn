var UtilSound = {};
UtilSound.soundArr = [];
UtilSound.isMute = false;

UtilSound.addSound = function(id, game, isBGM)
{
	if(isBGM == undefined) isBGM = false;

	for(var i=0;i<this.soundArr.length;i++)
	{
		if(id == this.soundArr[i].id)
		{
			console.log('sound exist!');
			return;
		}
	}

	var sound = game.add.audio(id);
	this.soundArr.push({id:id, sound:sound, isBGM:isBGM});
}

UtilSound.playSound = function(id)
{
	if(this.isMute == true) return;

	for(var i=0;i<this.soundArr.length;i++)
	{
		if(id == this.soundArr[i].id)
		{
			if(this.soundArr[i].isBGM == true) this.soundArr[i].sound.play(0, 0, 1, true);
			else this.soundArr[i].sound.play();
			break;
		}
	}
}

UtilSound.stopSound = function(id)
{
	for(var i=0;i<this.soundArr.length;i++)
	{
		if(id == this.soundArr[i].id)
		{
			this.soundArr[i].sound.stop();
			break;
		}
	}
}

UtilSound.mute = function()
{
	for(var i=0;i<this.soundArr.length;i++)
	{
		if(this.soundArr[i].isBGM == true)
		{
			this.soundArr[i].sound.mute = true;
		}
	}
	this.isMute = true;
}

UtilSound.unmute = function()
{
	for(var i=0;i<this.soundArr.length;i++)
	{
		if(this.soundArr[i].isBGM == true)
		{
			this.soundArr[i].sound.mute = false;
		}
	}
	this.isMute = false;
}