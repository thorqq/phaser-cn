var UtilAccel = {};


UtilAccel.accel = function(event)
{
	var averageGamma = (this.lastOrientation.gamma + event.gamma) / 2;
	var averageBeta = (this.lastOrientation.beta + event.beta) / 2;

	this.lastOrientation.gamma = event.gamma;
	this.lastOrientation.beta = event.beta;

	switch (Object(window).orientation) {
		case 0:
			this.tiltX = averageGamma;
			this.tiltY = averageBeta;
			break;
		case 180:
			this.tiltX = averageGamma * -1;
			this.tiltY = averageBeta * -1;
			break;
		case 90:
			this.tiltX = averageBeta;
			this.tiltY = averageGamma * -1;
			break;
		case -90:
			this.tiltX = averageBeta * -1;
			this.tiltY = averageGamma;
			break;
		default:
			this.tiltX = averageGamma;
			this.tiltY = averageBeta;
	}

	this.tiltX = Math.floor(this.tiltX * 10) / 10;
	this.tiltY = Math.floor(this.tiltY * 10) / 10;
	this.tiltY -= 90;
	this.tiltX /= 90;
	this.tiltY /= 90;

	if(this.tiltX < -0.8) this.tiltX = -0.8;
	else if(this.tiltX > 0.8) this.tiltX = 0.8;
	if(this.tiltY < -0.8) this.tiltY = -0.8;
	else if(this.tiltY > 0.8) this.tiltY = 0.8;
}

UtilAccel.init = function()
{
	this.tiltX = 0;
	this.tiltY = 0;
	this.lastOrientation = {gamma:0, beta:0};

	window.addEventListener('deviceorientation', function (event)
	{
		return UtilAccel.accel(event);
	}, false);
}