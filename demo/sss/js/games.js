
TutorialPopup = function (game, x, y, gameScreen)
{
	this.gameScreen = gameScreen;
	Phaser.Sprite.call(this, game, x, y, 'tutorial1');

	this.visible = false;
	this.inputEnabled = true;
	this.events.onInputDown.add(this.hideTutorial, this);
};

TutorialPopup.prototype = Object.create(Phaser.Sprite.prototype);
TutorialPopup.prototype.constructor = TutorialPopup;

TutorialPopup.prototype.showTutorial = function(tutorialId)
{
	if(this.visible == true) return;

	var tutorialDone = UtilSave.getAchievement('com.tempalabs.sss.1tutorial'+tutorialId);
	//console.log('tutorialId='+tutorialId+',tutorialDone='+tutorialDone);
	if(tutorialDone == 0)
	{
		tutorialDone = 1;
		UtilSave.setAchievement(tutorialDone, 'com.tempalabs.sss.1tutorial'+tutorialId);

		this.loadTexture('tutorial'+(tutorialId), 0);
		this.visible = true;
	}
}
TutorialPopup.prototype.hideTutorial = function()
{
	this.visible = false;
}

SSS.Games = function (game)
{
	this.game = game;
};

SSS.Games.prototype =
{
	create: function ()
	{
		console.log('games!');
		UtilSound.stopSound('bgm1');
		UtilSound.stopSound('bgm2');

		if((this.game.level+1) % 6 == 0)
		{
			UtilSound.playSound('bgm2');
			//this.game.add.sprite(0, 0, 'bg_game_2');
			this.bgC = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.4, 'bg_game_2_c'); this.bgC.anchor.setTo(0.5,0.5);
			this.bgB = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.4, 'bg_game_2_b'); this.bgB.anchor.setTo(0.5,0.5);
			this.bgA = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.4, 'bg_game_2_a'); this.bgA.anchor.setTo(0.5,0.5);
		}
		else
		{
			UtilSound.playSound('bgm1');
			//this.game.add.sprite(0, 0, 'bg_game_1');
			this.bgC = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.4, 'bg_game_1_c'); this.bgC.anchor.setTo(0.5,0.5);
			this.bgB = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.4, 'bg_game_1_b'); this.bgB.anchor.setTo(0.5,0.5);
			this.bgA = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.4, 'bg_game_1_a'); this.bgA.anchor.setTo(0.5,0.5);
		}

		this.loadLevel();

		this.effect = this.game.add.sprite(this.game.world.width/2, this.game.world.height/2, 'effect');
		this.effect.anchor.setTo(0.5, 0.5);
		this.effect.animations.add('spear',[0, 1, 2, 3, 4, 5, 17, 25], 15, false);
		this.effect.animations.add('staff',[6, 7 ,8, 9, 10, 11, 12, 13 ,14, 15, 16, 17, 25], 25, false);
		this.effect.animations.add('sword',[18, 19, 20, 21, 22, 23, 24, 25], 15, false);
		this.effect.frame = 25;
		//this.enemyGroup.add(this.effect);

		//Weapon
		this.sword = this.game.add.button(this.game.world.width*0.25, this.game.world.height*0.9, 'btn_sword', this.onAttack, this, null, 0, 1);//this.game.add.sprite(this.game.world.width*0.25, this.game.world.height*0.9, 'btn_sword');
		this.sword.anchor.setTo(0.5,0.5);
		//this.sword.inputEnabled = true;
		//this.sword.events.onInputDown.add(this.onAttack, this);

		this.spear = this.game.add.button(this.game.world.width*0.5, this.game.world.height*0.9, 'btn_spear', this.onAttack, this, null, 0, 1);//this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.9, 'btn_spear');
		this.spear.anchor.setTo(0.5,0.5);
		//this.spear.inputEnabled = true;
		//this.spear.events.onInputDown.add(this.onAttack, this);

		this.staff = this.game.add.button(this.game.world.width*0.75, this.game.world.height*0.9, 'btn_staff', this.onAttack, this, null, 0, 1);//this.game.add.sprite(this.game.world.width*0.75, this.game.world.height*0.9, 'btn_staff');
		this.staff.anchor.setTo(0.5,0.5);
		//this.staff.inputEnabled = true;
		//this.staff.events.onInputDown.add(this.onAttack, this);

		//Interface
		this.game.add.sprite(4, 6, 'gui_bar_empty');
		this.guiBar = this.game.add.sprite(4, 6, 'gui_bar_full');
		this.guiBar.cropEnabled = true;
		this.game.add.sprite(4, 47, 'gui_bar');

		var pauseBtn = this.game.add.button(this.game.world.width*0.5, this.game.world.height*0.14, 'btn_pause', this.onPause, this, null, 0, 1);
		pauseBtn.anchor.setTo(0.5,0.5);

		this.scoreTxt = this.game.add.bitmapText(this.game.world.width*0.09, this.game.world.height*0.1, '00000', { font: '40px grobold', align: 'left' });

		this.heart1 = this.game.add.sprite(495, 115, 'gui_heart'); this.heart1.anchor.setTo(0.5, 0.5);
		this.heart2 = this.game.add.sprite(541, 115, 'gui_heart'); this.heart2.anchor.setTo(0.5, 0.5);
		this.heart3 = this.game.add.sprite(587, 115, 'gui_heart'); this.heart3.anchor.setTo(0.5, 0.5);

		this.red = this.game.add.sprite(0, 0, 'red');
		this.red.alpha = 0;

		//Pause Popup
		this.pausePopup = this.game.add.group();
		this.pausePopup.y = -50;
		this.pausePopup.create(0,0, 'popup_level');
		this.pausePopup.visible = false;
		var exitBtn = this.game.add.button(this.game.world.width*0.25, this.game.world.height*0.75, 'btn_next', this.onResultExit, this, null, 0, 1);
		exitBtn.anchor.setTo(0.5,0.5);
		this.pausePopup.add(exitBtn);
		var retryBtn = this.game.add.button(this.game.world.width*0.5, this.game.world.height*0.65, 'btn_retry', this.onResultRetry, this, null, 0, 1);
		retryBtn.anchor.setTo(0.5,0.5);
		this.pausePopup.add(retryBtn);
		this.audioBtn = this.game.add.sprite(this.game.world.width*0.5, this.game.world.height*0.85, 'btn_audio');//this.game.add.button(this.game.world.width*0.5, this.game.world.height*0.85, 'btn_audio', this.onResultRetry, this, null, 0, 1);
		this.audioBtn.anchor.setTo(0.5,0.5);
		this.audioBtn.inputEnabled = true;
		this.audioBtn.events.onInputDown.add(this.onAudio, this);
		this.pausePopup.add(this.audioBtn);
		var resumeBtn = this.game.add.button(this.game.world.width*0.75, this.game.world.height*0.75, 'btn_right', this.onPause, this, null, 0, 1);
		resumeBtn.anchor.setTo(0.5,0.5);
		this.pausePopup.add(resumeBtn);
		var contentTxt = this.game.add.bitmapText(this.game.world.width*0.5, this.game.world.height*0.3, 'GAME\nPAUSED', { font: '100px grobold', align: 'center' });
		contentTxt.anchor.setTo(0.5,0);
		this.pausePopup.add(contentTxt);

		//Result Popup
		this.resultPopup = this.game.add.group();
		this.resultPopup.create(0,0, 'bg_result');
		this.resultPopup.visible = false;
		var exitBtn = this.game.add.button(this.game.world.width*0.25, this.game.world.height*0.85, 'btn_next', this.onResultExit, this, null, 0, 1);
		exitBtn.anchor.setTo(0.5,0.5);
		this.resultPopup.add(exitBtn);
		var retryBtn = this.game.add.button(this.game.world.width*0.5, this.game.world.height*0.85, 'btn_retry', this.onResultRetry, this, null, 0, 1);
		retryBtn.anchor.setTo(0.5,0.5);
		this.resultPopup.add(retryBtn);
		var nextBtn = this.game.add.button(this.game.world.width*0.75, this.game.world.height*0.85, 'btn_right', this.onResultNext, this, null, 0, 1);
		nextBtn.anchor.setTo(0.5,0.5);
		this.resultPopup.add(nextBtn);
		this.resultPopup.nextBtn = nextBtn;
		var contentTxt = this.game.add.bitmapText(this.game.world.width*0.5, this.game.world.height*0.38, 'YOU WIN', { font: '50px grobold', align: 'center' });
		contentTxt.setText('YOU WIN');
		contentTxt.anchor.setTo(0.5,0);
		this.resultPopup.add(contentTxt);
		this.resultPopup.contentTxt = contentTxt;
		var scoreTxt = this.game.add.bitmapText(this.game.world.width*0.5, this.game.world.height*0.3 + 15, '00000', { font: '50px grobold', align: 'left' });
		scoreTxt.setText('00000');
		scoreTxt.anchor.setTo(0.5,0);
		this.resultPopup.add(scoreTxt);
		this.resultPopup.scoreTxt = scoreTxt;

		var temp1 = this.resultPopup.create(190, 530, 'gui_star_empty'); temp1.anchor.setTo(0.5, 0.5);
		var temp2 = this.resultPopup.create(320, 580, 'gui_star_empty'); temp2.anchor.setTo(0.5, 0.5);
		var temp3 = this.resultPopup.create(440, 530, 'gui_star_empty'); temp3.anchor.setTo(0.5, 0.5);
		this.resultPopup.star1 = this.resultPopup.create(190, 530, 'gui_star'); this.resultPopup.star1.anchor.setTo(0.5, 0.5); this.resultPopup.star1.scale.setTo(0, 0);
		this.resultPopup.star2 = this.resultPopup.create(320, 580, 'gui_star'); this.resultPopup.star2.anchor.setTo(0.5, 0.5); this.resultPopup.star2.scale.setTo(0, 0);
		this.resultPopup.star3 = this.resultPopup.create(440, 530, 'gui_star'); this.resultPopup.star3.anchor.setTo(0.5, 0.5); this.resultPopup.star3.scale.setTo(0, 0);

		//Tutorial Popup
		this.tutorialPopup = new TutorialPopup(this.game, 0, 0, this);
		this.game.add.existing(this.tutorialPopup);

		/*this.tutorialPopup = null;
		if(this.game.level == 0)
		{
			this.tutorialPopup = this.game.add.group();

			this.tutorialArr = [];
			this.tutorialId = 0;

			this.tutorial1 = this.tutorialPopup.create(0, 0, 'tutorial1'); this.tutorialArr.push(this.tutorial1);
			this.tutorial2 = this.tutorialPopup.create(0, 0, 'tutorial2'); this.tutorial2.visible = false; this.tutorialArr.push(this.tutorial2);
			this.tutorial3 = this.tutorialPopup.create(0, 0, 'tutorial3'); this.tutorial3.visible = false; this.tutorialArr.push(this.tutorial3);
			this.tutorial4 = this.tutorialPopup.create(0, 0, 'tutorial4'); this.tutorial4.visible = false; this.tutorialArr.push(this.tutorial4);
			this.tutorial5 = this.tutorialPopup.create(0, 0, 'tutorial5'); this.tutorial5.visible = false; this.tutorialArr.push(this.tutorial5);
			this.tutorial6 = this.tutorialPopup.create(0, 0, 'tutorial6'); this.tutorial6.visible = false; this.tutorialArr.push(this.tutorial6);

			var prevBtn = this.game.add.button(this.game.world.width*0.25, this.game.world.height*0.88, 'btn_left', this.onTutorialPrev, this, null, 0, 1);
			prevBtn.anchor.setTo(0.5,0.5);
			prevBtn.visible = false;
			this.tutorialPopup.add(prevBtn);
			this.tutorialPopup.prevBtn = prevBtn;
			var playBtn = this.game.add.button(this.game.world.width*0.5, this.game.world.height*0.88, 'btn_play', this.onTutorialPlay, this, null, 0, 1);
			playBtn.anchor.setTo(0.5,0.5);
			playBtn.visible = false;
			this.tutorialPopup.add(playBtn);
			this.tutorialPopup.playBtn = playBtn;
			var nextBtn = this.game.add.button(this.game.world.width*0.75, this.game.world.height*0.88, 'btn_right', this.onTutorialNext, this, null, 0, 1);
			nextBtn.anchor.setTo(0.5,0.5);
			this.tutorialPopup.add(nextBtn);
			this.tutorialPopup.nextBtn = nextBtn;
		}*/


		this.chargeSpeed = SSS.LevelPack[this.game.level].chargeSpeed;
		this.chargeBar = SSS.LevelPack[this.game.level].chargeBar;
		this.timerCount = 0;
		this.life = 3;
		this.score = 0;
	},

	onAudio: function()
	{
		UtilSound.playSound('sfx_button');
		if(this.audioBtn.frame == 0)
		{
			this.audioBtn.frame = 1;
			UtilSound.mute();
		}else
		{
			this.audioBtn.frame = 0;
			UtilSound.unmute();
		}
	},

	loadLevel: function()
	{
		//Enemy
		this.enemyGroup = this.game.add.group();

		this.enemyCount = SSS.LevelPack[this.game.level].enemyCount;

		this.leftEnemyArr = [];
		var dir = Math.floor(Math.random()*2);
		for(var i=0;i<this.enemyCount/2;i++)
		{
			if(dir == 1) i++;
			var enemyId = 0;
			if((this.game.level+1) % 6 == 0) enemyId = Math.floor(Math.random()*3)+4;
			else enemyId = Math.floor(Math.random()*3)+1;

			var enemy = new Enemy(this.game, this.game.world.width*0.2 - 100*(i-1), this.game.world.height*0.5, enemyId);
			enemy.anchor.setTo(0.5,0.5);
			enemy.scale.setTo(0.2*(5-i), 0.2*(5-i));
			//enemy.x = this.game.world.width*0.2 - 200*(i-1);
			this.enemyGroup.addAt(enemy, 0);
			if(dir == 1) i--;

			this.leftEnemyArr.push(enemy);
		}

		this.rightEnemyArr = [];
		for(i=0;i<this.enemyCount/2;i++)
		{
			if(dir == 0) i++;
			var enemyId = 0;
			if((this.game.level+1) % 6 == 0) enemyId = Math.floor(Math.random()*3)+4;
			else enemyId = Math.floor(Math.random()*3)+1;

			var enemy = new Enemy(this.game, this.game.world.width*0.8 + 100*(i-1), this.game.world.height*0.5, enemyId);
			enemy.anchor.setTo(0.5,0.5);
			enemy.scale.setTo(0.2*(5-i), 0.2*(5-i));
			//enemy.x = this.game.world.width*0.8 + 200*(i-1);
			this.enemyGroup.addAt(enemy, 0);
			if(dir == 0) i--;

			this.rightEnemyArr.push(enemy);
		}

		this.activeEnemy = this.retrieveEnemy(dir);
		this.game.add.tween(this.activeEnemy.scale).to({x:1.5, y:1.5}, 500, Phaser.Easing.Linear.Out, true, 0);
		var tween = this.game.add.tween(this.activeEnemy).to({x:this.game.world.width*0.5}, 500, Phaser.Easing.Linear.Out, true, 0);
		this.enemyGroup.bringToTop(this.activeEnemy);
		this.activeEnemy.dash();
		tween.onComplete.add(this.activeEnemy.ready, this.activeEnemy);
	},

	retrieveEnemy: function(dir)
	{
		if(dir == undefined) dir = Math.floor(Math.random()*2);

		if(dir == 0 && this.leftEnemyArr.length == 0) dir = 1;
		else if(dir == 1 && this.rightEnemyArr.length == 0) dir = 0;

		if(dir == 0)
		{
			for(var i=0;i<this.leftEnemyArr.length;i++)
			{
				if(i == 0) continue;
				enemy = this.leftEnemyArr[i];

				this.game.add.tween(enemy.scale).to({x:0.2*(5-i), y:0.2*(5-i)}, 500, Phaser.Easing.Linear.Out, true, 0);
				var tween = this.game.add.tween(enemy).to({x:this.game.world.width*0.2 - 100*(i-1)}, 500, Phaser.Easing.Linear.Out, true, 0);
				enemy.dash();
				tween.onComplete.add(enemy.ready, enemy);
			}

			return this.leftEnemyArr.splice(0,1)[0];
		}
		else
		{
			for(var i=0;i<this.rightEnemyArr.length;i++)
			{
				if(i == 0) continue;
				enemy = this.rightEnemyArr[i];

				this.game.add.tween(enemy.scale).to({x:0.2*(5-i), y:0.2*(5-i)}, 500, Phaser.Easing.Linear.Out, true, 0);
				var tween = this.game.add.tween(enemy).to({x:this.game.world.width*0.8 + 100*(i-1)}, 500, Phaser.Easing.Linear.Out, true, 0);
				enemy.dash();
				tween.onComplete.add(enemy.ready, enemy);
			}

			return this.rightEnemyArr.splice(0,1)[0];
		}

		return null;
	},

	onTimer: function()
	{
		this.tutorialPopup.showTutorial(1);

		if(this.activeEnemy.enemyId == 2 || this.activeEnemy.enemyId == 5) this.tutorialPopup.showTutorial(4);
		else if(this.activeEnemy.enemyId == 0 || this.activeEnemy.enemyId == 3) this.tutorialPopup.showTutorial(2);
		else if(this.activeEnemy.enemyId == 1 || this.activeEnemy.enemyId == 4) this.tutorialPopup.showTutorial(3);

		this.setChargeBar(-this.chargeSpeed);
		if(this.chargeBar <= 0)//Game Over
		{
			this.showResult(false);
		}
	},

	setChargeBar: function(add)
	{
		this.chargeBar += add;
		if(this.chargeBar <= 0)
		{
			this.chargeBar = 0;
		}else if(this.chargeBar >= 100)
		{
			this.chargeBar = 100;
		}

		var tempWidth = this.chargeBar/100*488;
		this.guiBar.crop.width = (73+tempWidth);

		if(this.chargeBar > 50) this.tutorialPopup.showTutorial(5);
	},

	update: function()
	{
		if(this.resultPopup.visible == true || this.tutorialPopup.visible == true || this.pausePopup.visible == true) return;

		this.bgC.x = this.game.world.width*0.5 + UtilAccel.tiltX * 150;
		this.bgC.y = this.game.world.height*0.4 + UtilAccel.tiltY * 150;
		this.bgB.x = this.game.world.width*0.5 + UtilAccel.tiltX * 110;
		this.bgB.y = this.game.world.height*0.4 + UtilAccel.tiltY * 110;
		this.bgA.x = this.game.world.width*0.5 + UtilAccel.tiltX * 70;
		this.bgA.y = this.game.world.height*0.4 + UtilAccel.tiltY * 70;

		this.activeEnemy.x = this.game.world.width*0.5 + UtilAccel.tiltX * 30;
		this.activeEnemy.y = this.game.world.height*0.5 + UtilAccel.tiltY * 30;

		for(var i=0;i<this.leftEnemyArr.length;i++)
		{
			enemy = this.leftEnemyArr[i];

			enemy.x = this.game.world.width*0.2 - 100*(i) + UtilAccel.tiltX * (70 + i*5);
			enemy.y = this.game.world.height*0.5 + UtilAccel.tiltY * (70 + i*5);
		}
		for(var i=0;i<this.rightEnemyArr.length;i++)
		{
			enemy = this.rightEnemyArr[i];

			enemy.x = this.game.world.width*0.8 + 100*(i) + UtilAccel.tiltX * (70 + i*5);
			enemy.y = this.game.world.height*0.5 + UtilAccel.tiltY * (70 + i*5);
		}

		this.timerCount += this.game.time.elapsed;
		if(this.timerCount >= 1000)
		{
			this.timerCount = 0;
			this.onTimer();
		}
	},

	onAttack: function(weapon)
	{
		if(this.resultPopup.visible == true || this.tutorialPopup.visible == true || this.pausePopup.visible == true) return;

		var isCorrect = true;

		if(weapon == this.sword && (this.activeEnemy.enemyId == 2 || this.activeEnemy.enemyId == 5))
		{
			UtilSound.playSound('sfx_sword');
			this.effect.animations.play('sword');
			this.score += this.chargeBar;
			this.activeEnemy.hit();
			this.setChargeBar(10);
		}
		else if(weapon == this.spear && (this.activeEnemy.enemyId == 0 || this.activeEnemy.enemyId == 3))
		{
			UtilSound.playSound('sfx_spear');
			this.effect.animations.play('spear');
			this.score += this.chargeBar;
			this.activeEnemy.hit();
			this.setChargeBar(10);
		}
		else if(weapon == this.staff  && (this.activeEnemy.enemyId == 1 || this.activeEnemy.enemyId == 4))
		{
			UtilSound.playSound('sfx_staff');
			this.effect.animations.play('staff');
			this.score += this.chargeBar;
			this.activeEnemy.hit();
			this.setChargeBar(10);
		}
		else
		{
			this.tutorialPopup.showTutorial(6);
			isCorrect = false;

			this.activeEnemy.attack();

			this.red.alpha = 1;
			this.game.add.tween(this.red).to({alpha:0}, 500, Phaser.Easing.Linear.In, true);

			this.life--;
			this.setChargeBar(-20);
			if(this.life == 2) this.game.add.tween(this.heart1.scale).to({x:0, y:0}, 500, Phaser.Easing.Linear.In, true);
			else if(this.life == 1) this.game.add.tween(this.heart2.scale).to({x:0, y:0}, 500, Phaser.Easing.Linear.In, true);
			else if(this.life == 0)
			{
				this.game.add.tween(this.heart3.scale).to({x:0, y:0}, 500, Phaser.Easing.Linear.In, true);

				this.showResult(false);
				return;
			}
		}

		if(this.score/10000 > 1) this.scoreTxt.setText(''+this.score);
		else if(this.score/1000 > 1) this.scoreTxt.setText('0'+this.score);
		else if(this.score/100 > 1) this.scoreTxt.setText('00'+this.score);
		else if(this.score/10 > 1) this.scoreTxt.setText('000'+this.score);
		else if(this.score > 1) this.scoreTxt.setText('0000'+this.score);

		this.enemyCount--;
		if(this.enemyCount <= 0)
		{
			this.showResult(true);
		}else
		{
			if(isCorrect == false) var tempEnemy = this.activeEnemy;

			this.activeEnemy = this.retrieveEnemy();
			this.game.add.tween(this.activeEnemy.scale).to({x:1.5, y:1.5}, 500, Phaser.Easing.Linear.Out, true, 0);
			var tween = this.game.add.tween(this.activeEnemy).to({x:this.game.world.width*0.5}, 500, Phaser.Easing.Linear.Out, true, 0);
			this.enemyGroup.bringToTop(this.activeEnemy);
			if(isCorrect == false) this.enemyGroup.bringToTop(tempEnemy);
			this.activeEnemy.dash();
			tween.onComplete.add(this.activeEnemy.ready, this.activeEnemy);
		}
	},

	showResult: function(isWin)
	{
		this.resultPopup.visible = true;
		this.resultPopup.y = 960;
		this.game.add.tween(this.resultPopup).to({y:50}, 1000, Phaser.Easing.Exponential.Out, true, 500);

		this.resultPopup.scoreTxt.setText(''+this.score);

		if(isWin)
		{
			this.resultPopup.contentTxt.setText('YOU WIN!\nENEMIES DEFEATED!');
			if(this.game.level >= 18) this.resultPopup.nextBtn.visible = false;

			UtilSave.setHighscore(this.score, 'tempalabs.sss.score.'+this.game.level);

			var levelUnlocked = UtilSave.getAchievement('tempalabs.sss.level');
			if(this.game.level == levelUnlocked) UtilSave.setAchievement((levelUnlocked+1), 'tempalabs.sss.level');

			if(this.life >= 1) this.game.add.tween(this.resultPopup.star1.scale).to({x:1, y:1}, 1000, Phaser.Easing.Back.Out, true, 1000);
			if(this.life >= 2) this.game.add.tween(this.resultPopup.star2.scale).to({x:1, y:1}, 1000, Phaser.Easing.Back.Out, true, 1500);
			if(this.life >= 3) this.game.add.tween(this.resultPopup.star3.scale).to({x:1, y:1}, 1000, Phaser.Easing.Back.Out, true, 2000);

			var starCount = UtilSave.getAchievement('tempalabs.sss.star.'+this.game.level);
			if(this.life > starCount) UtilSave.setAchievement(this.life, 'tempalabs.sss.star.'+this.game.level);
		}else
		{
			if(this.life == 0) this.resultPopup.contentTxt.setText('YOU LOSE!\nNO MORE LIFE!');
			else if(this.chargeBar <= 0) this.resultPopup.contentTxt.setText('YOU LOSE!\nNO MORALE!');
			this.resultPopup.nextBtn.visible = false;
		}
	},

	onPause: function()
	{
		this.pausePopup.visible = !this.pausePopup.visible;
	},

	onResultExit: function()
	{
		UtilSound.stopSound('bgm1');
		UtilSound.stopSound('bgm2');
		UtilSound.playSound('bgm1');
		/*if((this.game.level+1) % 6 == 0)
		{
			UtilSound.stopSound('bgm2');
			UtilSound.playSound('bgm1');
		}*/
		UtilSound.playSound('sfx_button');
		this.game.state.start('title');
	},

	onResultRetry: function()
	{
		UtilSound.playSound('sfx_button');
		this.game.state.start('games');
	},

	onResultNext: function()
	{
		UtilSound.playSound('sfx_button');
		this.game.level++;
		this.game.state.start('games');
	},

	onTutorialPrev: function()
	{
		console.log('onTutorialPrev');
		this.tutorialArr[this.tutorialId].visible = false;
		this.tutorialId--;
		this.tutorialArr[this.tutorialId].visible = true;

		if(this.tutorialId == 0)
		{
			this.tutorialPopup.prevBtn.visible = false;
		}
		this.tutorialPopup.nextBtn.visible = true;
	},

	onTutorialNext: function()
	{
		console.log('onTutorialNext');
		this.tutorialArr[this.tutorialId].visible = false;
		this.tutorialId++;
		this.tutorialArr[this.tutorialId].visible = true;

		if(this.tutorialId == 5)
		{
			this.tutorialPopup.nextBtn.visible = false;
			this.tutorialPopup.playBtn.visible = true;
		}
		this.tutorialPopup.prevBtn.visible = true;
	},

	onTutorialPlay: function()
	{
		console.log('onTutorialPlay');
		this.tutorialPopup.visible = false;
		//this.tutorialPopup.destroy();
		this.tutorialPopup = null;
	}
}