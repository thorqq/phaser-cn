/*! parking | http://automobiles.honda.com/fit/ | 2014-05-28 */
!
function() {
    var a = {
        controls: {},
        states: {},
        music: ["Music_0", "Music_1", "Music_2"],
        currentMusic: -1,
        isSfxMuted: !1,
        isMusicMuted: !1,
        vibrate: !1,
        production: "hondafitchallenge.honda.com" === window.location.host
    };
    navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate || null, function() {
        a.analytics = {
            pageView: function(a) {
                s.pageName = a, s.channel = a, s.prop1 = a, s.prop2 = "", s.prop3 = "", s.eVar1 = a, s.eVar2 = "", s.eVar3 = "", s.eVar4 = "", s.t()
            },
            buttonClick: function(a, b) {
                s.linkTrackVars = "events,prop3,eVar3", s.linkTrackEvents = "event8", s.events = "event8";
                var c = a + " | " + b;
                s.prop3 = c, s.eVar3 = c, s.tl(this, "o", "click"), s.linkTrackVars = "", s.linkTrackEvents = "", s.prop3 = "", s.eVar3 = "", s.events = ""
            },
            socialClick: function(a, b, c) {
                s.linkTrackVars = "events,prop2,eVar2", s.linkTrackEvents = "event7", s.events = "event7";
                var d = b + " | " + c + " | " + a;
                s.prop2 = d, s.eVar2 = d, s.tl(this, "o", "share"), s.linkTrackVars = "", s.linkTrackEvents = "", s.prop2 = "", s.eVar2 = "", s.events = ""
            },
            reportSweepsError: function(a) {
                s.linkTrackVars = "eVar4", s.eVar4 = a, s.tl(this, "o", "click"), s.linkTrackVars = "", s.eVar4 = ""
            },
            reportEvent: function(a) {
                s.linkTrackVars = "events", s.linkTrackEvents = a, s.events = a, s.tl(this, "o", "click"), s.linkTrackVars = "", s.linkTrackEvents = "", s.events = ""
            }
        }
    }(), function() {
        var b = a.Base64 = {
            _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
            encode: function(a) {
                var c, d, e, f, g, h, i, j = "",
                    k = 0;
                for (a = b._utf8_encode(a); k < a.length;) c = a.charCodeAt(k++), d = a.charCodeAt(k++), e = a.charCodeAt(k++), f = c >> 2, g = (3 & c) << 4 | d >> 4, h = (15 & d) << 2 | e >> 6, i = 63 & e, isNaN(d) ? h = i = 64 : isNaN(e) && (i = 64), j = j + b._keyStr.charAt(f) + b._keyStr.charAt(g) + b._keyStr.charAt(h) + b._keyStr.charAt(i);
                return j
            },
            decode: function(a) {
                var c, d, e, f, g, h, i, j = "",
                    k = 0;
                for (a = a.replace(/[^A-Za-z0-9\+\/\=]/g, ""); k < a.length;) f = b._keyStr.indexOf(a.charAt(k++)), g = b._keyStr.indexOf(a.charAt(k++)), h = b._keyStr.indexOf(a.charAt(k++)), i = b._keyStr.indexOf(a.charAt(k++)), c = f << 2 | g >> 4, d = (15 & g) << 4 | h >> 2, e = (3 & h) << 6 | i, j += String.fromCharCode(c), 64 !== h && (j += String.fromCharCode(d)), 64 !== i && (j += String.fromCharCode(e));
                return j = b._utf8_decode(j)
            },
            _utf8_encode: function(a) {
                a = a.replace(/\r\n/g, "\n");
                for (var b = "", c = 0; c < a.length; c++) {
                    var d = a.charCodeAt(c);
                    128 > d ? b += String.fromCharCode(d) : d > 127 && 2048 > d ? (b += String.fromCharCode(d >> 6 | 192), b += String.fromCharCode(63 & d | 128)) : (b += String.fromCharCode(d >> 12 | 224), b += String.fromCharCode(d >> 6 & 63 | 128), b += String.fromCharCode(63 & d | 128))
                }
                return b
            },
            _utf8_decode: function(a) {
                for (var b = "", c = 0, d = 0, e = 0, f = 0; c < a.length;) d = a.charCodeAt(c), 128 > d ? (b += String.fromCharCode(d), c++) : d > 191 && 224 > d ? (e = a.charCodeAt(c + 1), b += String.fromCharCode((31 & d) << 6 | 63 & e), c += 2) : (e = a.charCodeAt(c + 1), f = a.charCodeAt(c + 2), b += String.fromCharCode((15 & d) << 12 | (63 & e) << 6 | 63 & f), c += 3);
                return b
            }
        }
    }(), function() {
        a.fitFacts = {
            pause: ["Brakes: Pause on a dime. The 2015 Fit’s", "optimal brake tuning enhances stopping", "performance and brake balance for", "a smoother ride."],
            level1: ["Rearview camera: A multi-angle rearview", "camera on the 2015 Fit helps you spot", "objects behind you while reversing and", "parallel parking."],
            level2: ["Expanded view drivers mirror: The 2015", "Fit's Expanded View Driver’s Mirror", "gives you a wider look to see vehicles", "in your left blind spot."],
            level3: ["Honda LaneWatch: the available Honda", "LaneWatch helps you see objects in your", "right blind spot with a camera on your", "passenger-side mirror."],
            level4: ["Headlights: Be seen with the newly ", "designed grille with automatic on-off", "headlights & available fog lights that", "give the 2015 Fit a striking stance."],
            level5: ["Honda Fit's Look: The 2015 Fit features", "an aggressive styling with a more modern", "and sporty redesign for a new look", "and feel."]
        }, a.fitFacts.level6 = a.fitFacts.level1, a.fitFacts.level7 = a.fitFacts.level2, a.fitFacts.level8 = a.fitFacts.level3
    }(), function() {
        $(document).on({
            MSPointerUp: function(b) {
                if (a.game && a.game.input) {
                    var c = b.originalEvent;
                    c.identifier = c.pointerId, a.game.input.stopPointer(c)
                }
            },
            pointerup: function(b) {
                if (a.game && a.game.input) {
                    var c = b.originalEvent;
                    c.identifier = c.pointerId, a.game.input.stopPointer(c)
                }
            },
            mouseup: function(b) {
                if (a.game && a.game.input) {
                    var c = b.originalEvent;
                    a.game.input.mousePointer.stop(c)
                }
            }
        })
    }(), a.resize = function() {
        var b = a.game ? a.game.scale : null;
        if (b) {
            b.scaleMode = Phaser.ScaleManager.SHOW_ALL, b.setShowAll(), b.pageAlignHorizontally = !0, b.pageAlignVertically = !0, b.refresh();
            var c = $("#gameContainer>canvas"),
                d = c.width(),
                e = (c.height(), d / 1136),
                f = c.outerHeight(!0) - c.outerHeight(),
                g = f - 10;
            $("#sweeps").css({
                transform: "scale(" + e + "," + e + ")",
                "transform-origin": "top left",
                "margin-left": c.css("margin-left"),
                "margin-top": g
            })
        }
    }, $(window).resize(a.resize), function() {
        var b = 8,
            c = 600,
            d = c * b;
        a.scores = {
            init: function() {
                this.get(0) || this.set(0, 0)
            },
            get: function(b) {
                var c = a.settings.get("scores");
                return c && c.length > b ? c[b] : null
            },
            getTotal: function() {
                var b, c, d = a.settings.get("scores") || [],
                    e = 0;
                for (b = 0, c = d.length; c > b; b++) e += d[b] || 0;
                return e
            },
            set: function(c, d) {
                var e = a.settings.get("scores") || [];
                e[c] = d;
                var f = c + 1;
                d > 0 && b > f && !e[f] && (e[f] = 0), a.settings.set("scores", e)
            },
            getTitle: function() {
                var a = this.getTotal() / d;
                return .2 >= a ? "FENDER BENDER" : .4 >= a ? "TRAINING WHEELS" : .6 >= a ? "LIKE A GLOVE" : .8 >= a ? "PLATINUM" : "PARK-FECTIONIST"
            },
            isLevelLocked: function(a) {
                return null === this.get(a)
            }
        }
    }(), function() {
        var b = !1;
        try {
            b = !! window.localStorage
        } catch (c) {}
        var d = {};
        if (b) try {
            var e = a.Base64.decode(localStorage.getItem("s"));
            d = JSON.parse(e)
        } catch (c) {}
        a.settings = {
            get: function(a) {
                return d[a]
            },
            set: function(c, e) {
                if (d[c] = e, b) {
                    var f = JSON.stringify(d);
                    localStorage.setItem("s", a.Base64.encode(f))
                }
            },
            getMusicMuted: function() {
                return this.get("musicMuted") === !0
            },
            setMusicMuted: function(b) {
                a.isMusicMuted = b, this.set("musicMuted", b === !0)
            },
            getEffectsMuted: function() {
                return this.get("effectsMuted") === !0
            },
            setEffectsMuted: function(b) {
                a.isSfxMuted = b, this.set("effectsMuted", b === !0)
            },
            getVibrationEnabled: function() {
                return navigator.vibration && this.get("vibration") !== !1
            },
            setVibrationEnabled: function(b) {
                b = b && navigator.vibration, a.vibrate = b, this.set("vibration", b === !0)
            }
        }, a.settings.setMusicMuted(a.settings.getMusicMuted()), a.settings.setEffectsMuted(a.settings.getEffectsMuted()), a.settings.getVibrationEnabled(a.settings.getVibrationEnabled())
    }(), function() {
        var a = window.location.href;
        (0 === a.indexOf("http://hondafitchallenge.honda.com") || 0 === a.indexOf("http://staging.hondafitchallenge.honda.com")) && (window.location = a.replace(/^http:/, "https:"))
    }(), function() {
        var b = 5;
        a.sweeps = {
            eventDateString: "7/25/14",
            recordEntry: function() {
                var c = a.settings.get("sweepsEntries") || [];
                c.length >= b && c.shift();
                var d = (new Date).toDateString();
                c.push(d), a.settings.set("sweepsEntries", c)
            },
            canEnter: function() {
                var c = 140271834e4;
                if (Date.now() > c) return !1;
                var d, e, f = a.settings.get("sweepsEntries") || [],
                    g = (new Date).toDateString(),
                    h = 0;
                for (d = 0, e = f.length; e > d; d++) f[d] === g && h++;
                return b > h
            }
        }
    }(), a.EventDispatcher = function() {}, a.EventDispatcher.prototype = {
        constructor: a.EventDispatcher,
        apply: function(b) {
            b.addEventListener = a.EventDispatcher.prototype.addEventListener, b.hasEventListener = a.EventDispatcher.prototype.hasEventListener, b.removeEventListener = a.EventDispatcher.prototype.removeEventListener, b.dispatchEvent = a.EventDispatcher.prototype.dispatchEvent
        },
        addEventListener: function(a, b) {
            void 0 === this._listeners && (this._listeners = {});
            var c = this._listeners;
            void 0 === c[a] && (c[a] = []), -1 === c[a].indexOf(b) && c[a].push(b)
        },
        hasEventListener: function(a, b) {
            if (void 0 === this._listeners) return !1;
            var c = this._listeners;
            return void 0 !== c[a] && -1 !== c[a].indexOf(b) ? !0 : !1
        },
        removeEventListener: function(a, b) {
            if (void 0 !== this._listeners) {
                var c = this._listeners,
                    d = c[a];
                if (void 0 !== d) {
                    var e = d.indexOf(b); - 1 !== e && d.splice(e, 1)
                }
            }
        },
        dispatchEvent: function() {
            var a = [];
            return function(b) {
                if (void 0 !== this._listeners) {
                    var c = this._listeners,
                        d = c[b.type];
                    if (void 0 !== d) {
                        b.target = this;
                        for (var e = d.length, f = 0; e > f; f++) a[f] = d[f];
                        for (f = 0; e > f; f++) a[f].call(this, b)
                    }
                }
            }
        }()
    }, a.ValueManager = function(a, b, c, d, e, f) {
        this._targetValue = 0, this.object = a, this.prop = b, this.min = c || -100, this.max = d || 200, this.decAmt = f || .05, this.incAmt = e || .15
    }, a.ValueManager.prototype = {
        constructor: a.ValueManager,
        set Value(a) {
            this._targetValue = a
        },
        get Value() {
            return this._targetValue
        },
        setNormalizedTarget: function(a) {
            var b = a * (this.max - this.min);
            this._targetValue = this.min + b
        },
        getNormalizedCurrentValue: function() {
            var a = this.object[this.prop];
            return this.min < 0 && (a -= this.min), a / (this.max - this.min)
        },
        increase: function(a) {
            this._targetValue = this.object[this.prop] + a
        },
        decrease: function(a) {
            this._targetValue = this.object[this.prop] - a
        },
        update: function() {
            this.object[this.prop] = 0 === this._targetValue ? a.lerp(this.object[this.prop], this._targetValue, this.decAmt) : a.lerp(this.object[this.prop], this._targetValue, this.incAmt), this.object[this.prop] = a.clamp(this.object[this.prop], this.min, this.max)
        }
    }, function() {
        function b(b, c) {
            Phaser.Button.call(this, b, c.x, c.y, c.key, c.callback, c.callbackContext, c.overFrame, c.outFrame, c.downFrame, c.upFrame);
            var d = new Phaser.BitmapText(b, 0, 0, c.font, c.text, c.size);
            d.align = "center", this.addChild(d), this.input.useHandCursor = !0, a.controls.Utils.centerBitmapText({
                text: d,
                width: this.width,
                height: this.height,
                offsetX: c.textOffsetX,
                offsetY: c.textOffsetY
            })
        }
        b.prototype = Object.create(Phaser.Button.prototype), b.prototype.constructor = b, a.controls.BitmapTextButton = b
    }(), function() {
        function b(a, b) {
            Phaser.Group.call(this, a, b.parent), this.clickArea = new Phaser.Rectangle(0, 0, b.width, b.height), this.callback = b.callback, this.callbackContext = b.callbackContext, this.game.input.onTap.add(this.onTap, this)
        }
        b.prototype = Object.create(Phaser.Group.prototype), b.prototype.constructor = b, b.prototype.onTap = function(a) {
            this.clickArea.contains(a.x, a.y) && this.callback.call(this.callbackContext)
        }, a.controls.ClickableGroup = b
    }(), function() {
        function b(a, b) {
            b = b || {}, Phaser.Group.call(this, a), this.fixedToCamera = !0, this.cameraOffset.setTo(0, 0);
            var c = this.add(new Phaser.Graphics(a, 0, 0));
            c.beginFill(16777215, b.alpha || .7), c.drawRect(0, 0, a.width, a.height), c.endFill()
        }
        b.prototype = Object.create(Phaser.Group.prototype), b.prototype.constructor = b, b.preload = function(b) {
            a.controls.SocialButtons.preload(b), b.spritesheet("backMenuPink", "images/menus/menuMenuPink.png", 176, 36, 2, 0, 4), b.spritesheet("menuButton", "images/menus/menuButton.png", 396, 108), b.spritesheet("toggleSwitch", "images/controls/controlsToggle.png", 186, 64), b.bitmapFont("largepink", "images/fonts/largepink.png", "images/fonts/largepink.xml"), b.bitmapFont("smallwhite", "images/fonts/smallwhite.png", "images/fonts/smallwhite.xml"), b.bitmapFont("medblue", "images/fonts/medblue.png", "images/fonts/medblue.xml"), b.image("fitFact", "images/menus/menuFitFact.png"), b.image("fitLink", "images/menus/menuCheckOutFit.png")
        }, b.prototype.createFitFact = function(b) {
            var c = 200,
                d = this.game.height - c,
                e = this.add(new Phaser.Graphics(this.game, 0, 0));
            e.beginFill(6796507, .9), e.drawRect(0, d, this.game.width, c), e.endFill(), this.add(new Phaser.Image(this.game, 30, d + 13, "fitFact"));
            var f, g, h, i = [];
            if (b) {
                for (f = 0, g = b.length; g > f; f++) h = this.add(new Phaser.BitmapText(this.game, 240, 0, "smallwhite", b[f], 32)), h.updateTransform(), i.push(h);
                a.controls.Utils.centerItemsVertically({
                    items: i,
                    widthProperty: "textWidth",
                    heightProperty: "textHeight",
                    height: c,
                    offsetY: d,
                    paddingY: 2
                })
            }
        }, b.prototype.createMenuButton = function(b) {
            var c = this.add(new Phaser.Button(this.game, 30, 30, "backMenuPink", function() {
                a.analytics.buttonClick(b, "Menu"), this.clickMenu()
            }, this, 1, 0));
            c.input.useHandCursor = !0
        }, b.prototype.createTitle = function(b, c) {
            var d = this.add(new Phaser.BitmapText(this.game, 0, c || 160, "largepink", b, 80));
            a.controls.Utils.centerBitmapText({
                text: d,
                width: this.game.width
            })
        }, b.prototype.clickMenu = function() {
            a.startMenu("levels")
        }, b.prototype.closeDialog = function() {
            this.destroy(!0)
        }, b.prototype.createSocialButtons = function(b) {
            return b = b || {}, b.parent = b.parent || this, new a.controls.SocialButtons(this.game, b)
        }, b.prototype.createButton = function(b, c, d, e) {
            var f = this.add(new a.controls.BitmapTextButton(this.game, {
                key: "menuButton",
                text: d,
                textOffsetX: -12,
                textOffsetY: -12,
                font: "smallwhite",
                size: 28,
                callback: e,
                callbackContext: this,
                outFrame: 0,
                overFrame: 1
            }));
            return f.x = b || 0, f.y = c || 0, f
        }, b.prototype.createSweepsButton = function(b, c, d) {
            if (!a.sweeps.canEnter()) return null;
            var e = this.createButton(c, d, "ENTER SWEEPSTAKES", function() {
                a.game.state.start("sweeps"), a.analytics.buttonClick(b, "Enter Sweepstakes")
            });
            return e
        }, a.controls.DialogGroup = b
    }(), function() {
        function b(b, d, e) {
            c.call(this, b, d, e);
            var f = "Game Won Page";
            a.analytics.pageView(f), this.createMenuButton(f), this.createTitle("CONGRATS");
            var g = this.createSweepsButton(f, 0, 300),
                h = g ? [g] : [];
            h.push(this.createButton(0, 300, "MAIN MENU", function() {
                a.analytics.buttonClick(f, "Main Menu"), a.startMenu("home")
            })), a.controls.Utils.centerItemsHorizontally({
                items: h,
                width: this.game.width,
                paddingX: 20
            }), this.createShareText();
            var i = "I got a new high score of " + a.scores.getTotal() + " in the #HondaFitChallenge. Try to beat that at hondafitchallenge.honda.com NoPurNec. Rules: http://honda.us/1gb2wi9";
            this.createSocialButtons({
                x: (this.game.width - 170) / 2,
                y: this.game.height - 90,
                message: i,
                parentName: "Game Won"
            })
        }
        var c = a.controls.DialogGroup;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.createShareText = function() {
            var b = this.add(new Phaser.BitmapText(this.game, 0, 420, "medblue", "SHARE YOUR SCORE AND", 42));
            a.controls.Utils.centerBitmapText({
                text: b,
                width: this.game.width
            });
            var c = this.add(new Phaser.BitmapText(this.game, 0, 480, "medblue", "CLAIM BRAGGING RIGHTS", 42));
            a.controls.Utils.centerBitmapText({
                text: c,
                width: this.game.width
            })
        }, a.controls.GameWonDialog = b
    }(), function() {
        function b(a, b) {
            Phaser.Image.call(this, a, b.x, b.y, b.trackKey), this.handle = new Phaser.Image(a, 0, this.height / 2, b.handleKey), this.handle.anchor.set(.5, .5), this.addChild(this.handle), this.activePointer = null, this.inputEnabled = !0, this.events.onInputDown.add(this.onInputDown, this), this.events.onInputUp.add(this.onInputUp, this), this.setValue(b.value || .5)
        }
        b.prototype = Object.create(Phaser.Image.prototype), b.prototype.constructor = b, b.prototype.setValue = function(a) {
            var b = this.width - this.handle.width,
                c = b * a,
                d = this.handle.width / 2 + c;
            this.handle.x = d
        }, b.prototype.getInputValue = function() {
            if (!this.activePointer) return null;
            var a = this.width,
                b = this.x,
                c = this.activePointer.worldX - b,
                d = Math.max(0, Math.min(a, c)),
                e = d / a;
            return e
        }, b.prototype.onInputDown = function(a, b) {
            this.activePointer = b
        }, b.prototype.onInputUp = function() {
            this.activePointer = null
        }, a.controls.HorizontalSlider = b
    }(), function() {
        function b(a, b, d) {
            c.call(this, a, d), this.level = b, this.level && this.level.pause()
        }
        var c = a.controls.DialogGroup;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.clickMenu = function() {
            this.level && this.level.resume(), c.prototype.clickMenu.call(this)
        }, b.prototype.closeDialog = function() {
            c.prototype.closeDialog.call(this), this.level && this.level.resume()
        }, a.controls.LevelDialog = b
    }(), function() {
        function b(b, d, e) {
            var f = "Level Lost Page";
            a.analytics.pageView(f), c.call(this, b, d, e);
            var g = a.fitFacts[a.game.state.current];
            g && this.createFitFact(g), this.createMenuButton(f), this.createSocialButtons({
                x: this.game.width - 200,
                y: 30,
                pageName: "Level Lost"
            }), this.createTitle("NICE TRY");
            var h = this.createSweepsButton(f, 0, 300),
                i = h ? [h] : [];
            i.push(this.createButton(0, 300, "TRY AGAIN", function() {
                a.analytics.buttonClick(f, "Try Again"), a.game.state.restart()
            })), a.controls.Utils.centerItemsHorizontally({
                items: i,
                width: this.game.width,
                paddingX: 20
            })
        }
        var c = a.controls.LevelDialog;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, a.controls.LevelFailDialog = b
    }(), function() {
        function b(b, d, e) {
            c.call(this, b, d, e);
            var f = "Game Pause Page";
            a.analytics.pageView(f), this.createFitFact(a.fitFacts.pause), this.createMenuButton(f), this.createTitle("PAUSED");
            var g = this.createButton(0, 300, "CONTINUE", $.proxy(function() {
                a.analytics.buttonClick(f, "Continue"), a.analytics.pageView("Game Page"), this.closeDialog()
            }, this));
            g.x = (this.game.width - g.width) / 2
        }
        var c = a.controls.LevelDialog;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, a.controls.LevelPauseDialog = b
    }(), function() {
        function b(b, d, e) {
            c.call(this, b, d, e);
            var f = "Level Won Page";
            a.analytics.pageView(f);
            var g = a.fitFacts[a.game.state.current];
            g && this.createFitFact(g), this.createMenuButton(f), this.createSocialButtons({
                x: this.game.width - 200,
                y: 30,
                pageName: "Level Won"
            }), this.createPointsText(e.points), this.createTitle("CONGRATS");
            var h = this.createSweepsButton(f, 0, 300),
                i = h ? [h] : [];
            i.push(this.createButton(0, 300, "NEXT LEVEL", function() {
                a.game.state.start(e.nextState), a.analytics.buttonClick(f, "Next Level")
            })), a.controls.Utils.centerItemsHorizontally({
                items: i,
                width: this.game.width,
                paddingX: 20
            })
        }
        var c = a.controls.LevelDialog;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.createPointsText = function(b) {
            var c = this.add(new Phaser.BitmapText(this.game, 0, 100, "medblue", b + " POINTS", 42));
            a.controls.Utils.centerBitmapText({
                text: c,
                width: this.game.width
            })
        }, a.controls.LevelWonDialog = b
    }(), function() {
        function b(a, b) {
            Phaser.Group.call(this, a, b), this.fixedToCamera = !0, this.cameraOffset.setTo(0, 0);
            var c = this.add(new Phaser.Graphics(a, 0, 0));
            c.beginFill(0, 1), c.drawRect(0, 0, a.width, a.height), c.endFill(), this.createUI(), a.load.onFileComplete.add(this.onLoadProgress, this)
        }
        b.prototype = Object.create(Phaser.Group.prototype), b.prototype.constructor = b, b.preload = function(a) {
            a.image("greenHonda", "images/loading/loadingLogo.png"), a.image("loadingTracks", "images/loading/loadingTracks.png"), a.image("loadingCar", "images/loading/loadingFit.png"), a.bitmapFont("bonus", "images/fonts/bonus.png", "images/fonts/bonus.xml"), a.bitmapFont("largepink", "images/fonts/largepink.png", "images/fonts/largepink.xml")
        }, b.prototype.createUI = function() {
            var a = this.add(new Phaser.Image(this.game, this.game.width / 2, 80, "greenHonda"));
            a.anchor.set(.5, 0), this.add(new Phaser.BitmapText(this.game, 300, 350, "bonus", "LOADING", 54)), this.percentageText = this.add(new Phaser.BitmapText(this.game, 700, 350, "largepink", "0", 54)), this.tracks = this.add(new Phaser.Image(this.game, 0, 478, "loadingTracks")), this.trackRect = new Phaser.Rectangle(0, 0, this.tracks.width, this.tracks.height), this.car = this.add(new Phaser.Image(this.game, 0, 460, "loadingCar"))
        }, b.prototype.testSlowLoad = function(a) {
            for (var b = 5, c = 1; b >= c; c++) a.image("preload-test-" + c, "http://thinkpixellab.com/pxloader/slowImage.php?delay=" + c + "&time=" + Date.now())
        }, b.prototype.onLoadProgress = function(b) {
            this.progressTween && this.progressTween.stop();
            var c = b / 100 * 1136;
            this.progressTween = a.game.add.tween(this.car).to({
                x: c
            }, 800, Phaser.Easing.Cubic.InOut, !0), b >= 100 && this.progressTween.onComplete.add(this.onLoadComplete, this)
        }, b.prototype.postUpdate = function() {
            Phaser.Group.prototype.postUpdate.call(this), a.game.world.bringToTop(this);
            var b = this.trackRect.width = this.car.x;
            if (b !== this.lastCarX) {
                this.lastCarX = b, this.tracks.crop(this.trackRect);
                var c = Math.round(b / 1136 * 100);
                c !== this.lastPercentage && (this.percentageText.text = c.toString(), this.lastPercentage = c)
            }
        }, b.prototype.onLoadComplete = function() {
            this.destroy(!0)
        }, a.controls.LoadingScreen = b
    }(), function() {
        function b(b, d, e) {
            e = e || {}, e.alpha = .9, c.call(this, b, d, e), e.showMenuButton && this.createMenuButton("Options Page"), this.createTitle("OPTIONS", 80), this.createOption("MUSIC", 220, !a.isMusicMuted, this.toggleMusic), this.createOption("SOUND FX", 300, !a.isSfxMuted, this.toggleEffects);
            var f = this.createButton(0, 500, "CLOSE", this.closeDialog);
            a.controls.Utils.centerItemsHorizontally({
                items: [f],
                width: this.game.width,
                paddingX: 20
            })
        }
        var c = a.controls.LevelDialog;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.createOption = function(a, b, c, d) {
            this.add(new Phaser.BitmapText(this.game, 300, b + 10, "medblue", a, 36));
            var e = this.add(new Phaser.Button(this.game, 650, b, "toggleSwitch", d, this));
            e.frame = c ? 0 : 1, e.input.useHandCursor = !0
        }, b.prototype.toggleMusic = function(b) {
            var c = !a.settings.getMusicMuted();
            a.settings.setMusicMuted(c), b.frame = c ? 1 : 0
        }, b.prototype.toggleEffects = function(b) {
            var c = !a.settings.getEffectsMuted();
            a.settings.setEffectsMuted(c), b.frame = c ? 1 : 0
        }, b.prototype.toggleVibration = function(b) {
            var c = !a.settings.getVibrationEnabled();
            a.settings.setVibrationEnabled(c), b.frame = a.settings.getVibrationEnabled() ? 0 : 1
        }, a.controls.OptionsDialog = b
    }(), function() {
        function b(b, d) {
            c.call(this, b, d), this.wheel = new Phaser.Image(b, this.width / 2 + (d.wheel.offsetX || 0), this.height / 2 + (d.wheel.offsetY || 0), d.wheel.key), this.wheel.anchor.set(.5, .5), d.wheelShadow && (this.wheelShadow = new Phaser.Image(b, this.wheel.x + (d.wheelShadow.offsetX || 0), this.wheel.y + (d.wheelShadow.offsetY || 0), d.wheelShadow.key), this.wheelShadow.anchor.set(.5, .5), this.addChild(this.wheelShadow)), this.addChild(this.wheel), this.minRotation = d.minRotation || -360 * a.toRADIANS, this.maxRotation = d.maxRotation || 360 * a.toRADIANS, this.activePointer = null, this.inputEnabled = !0, this.events.onInputDown.add(this.onInputDown, this), this.events.onInputUp.add(this.onInputUp, this), this.setValue(d.value || .5)
        }
        var c = a.controls.HorizontalSlider;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.setValue = function(a) {
            if (c.prototype.setValue.call(this, a), this.wheel) {
                var b = (this.width, a * (this.maxRotation - this.minRotation)),
                    d = this.minRotation + b;
                this.wheel.rotation = d, this.wheelShadow && (this.wheelShadow.rotation = d)
            }
        }, a.controls.RotationSlider = b
    }(), function() {
        function b(a, b) {
            Phaser.Group.call(this, a, b.parent), this.x = b.x || 0, this.y = b.y || 0, this.title = b.title || c.title, this.url = b.url || c.url, this.message = b.message || c.message, this.pageName = b.pageName, this.createButtons()
        }
        var c = {
            title: "Honda Fit Challenge",
            url: "http://hondafitchallenge.honda.com/",
            message: "Join me & play the #HondaFitChallenge. Do you have what it takes? Find out at hondafitchallenge.honda.com NoPurNec. Rules: http://honda.us/1gb2wi9"
        };
        b.prototype = Object.create(Phaser.Group.prototype), b.prototype.constructor = b, b.preload = function(a) {
            a.image("fbButton", "images/menus/menuFacebook.png"), a.image("twitterButton", "images/menus/menuTwitter.png")
        }, b.prototype.createButtons = function() {
            var a = this.add(new Phaser.Button(this.game, 0, 4, "twitterButton", this.clickTwitter, this));
            a.input.useHandCursor = !0;
            var b = this.add(new Phaser.Button(this.game, 100, 0, "fbButton", this.clickFacebook, this));
            b.input.useHandCursor = !0
        }, b.prototype.clickFacebook = function() {
            var b = 550,
                c = 300,
                d = screen.width / 2 - b / 2,
                e = screen.height / 3 - c / 2,
                f = "http://www.facebook.com/sharer.php?s=100&p[title]=" + encodeURIComponent(this.title) + "&p[summary]=" + encodeURIComponent(this.message) + "&p[url]=" + encodeURIComponent(this.url);
            window.open(f, "facebook", "height=" + c + ",width=" + b + ",left=" + d + ",top=" + e + ",resizable=1"), a.analytics.socialClick(this.pageName, "facebook", this.message)
        }, b.prototype.clickTwitter = function() {
            var b = 550,
                c = 300,
                d = screen.width / 2 - b / 2,
                e = screen.height / 3 - c / 2,
                f = "http://twitter.com/share?url=/&text=" + encodeURIComponent(this.message) + "&count=none";
            window.open(f, "tweet", "height=" + c + ",width=" + b + ",left=" + d + ",top=" + e + ",resizable=1"), a.analytics.socialClick(this.pageName, "twitter", this.message)
        }, a.controls.SocialButtons = b
    }(), function() {
        function b(b, d) {
            var e = "Confirmation Page";
            a.analytics.pageView(e), d = d || {}, d.alpha = 1, c.call(this, b, d), this.createMenuButton(e), this.createTitle("THANKS", 80);
            var f = this.createButton(0, 300, "CONTINUE PLAYING", function() {
                a.analytics.buttonClick(e, "Continue Playing"), a.startMenu("levels")
            });
            a.controls.Utils.centerItemsHorizontally({
                items: [f],
                width: this.game.width,
                paddingX: 20
            }), this.createLink();
            var g = "I entered for a chance to win a trip to LA on " + a.sweeps.eventDateString + ". Play for your chance at hondafitchallenge.honda.com. Rules http://honda.us/1gb2wi9 NoPurNec";
            this.createSocialButtons({
                x: (this.game.width - 170) / 2,
                y: 200,
                message: g,
                pageName: "Confirmation Page"
            })
        }
        var c = a.controls.DialogGroup;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.createLink = function() {
            var b = this.add(new Phaser.Button(this.game, 0, 440, "fitLink", this.clickFitLink, this));
            b.input.useHandCursor = !0, a.controls.Utils.centerItemsHorizontally({
                items: [b],
                width: this.game.width
            })
        }, b.prototype.clickFitLink = function() {
            window.open("http://automobiles.honda.com/2015-fit/", "_blank")
        }, a.controls.SweepsCompletedDialog = b
    }(), function() {
        function b(a, b) {
            Phaser.Button.call(this, a, b.x, b.y, b.key, b.callback, b.callbackContext, b.overFrame, b.outFrame, b.downFrame, b.upFrame);
            var c = new Phaser.Text(a, this.width / 2, this.height / 2, b.text, {
                font: b.font,
                fill: b.fill
            });
            c.anchor.set(.5), this.addChild(c)
        }
        b.prototype = Object.create(Phaser.Button.prototype), b.prototype.constructor = b, a.controls.TextButton = b
    }(), function() {
        function b(a, b) {
            var c = a.widthProperty || "width";
            return a.widthProperties && (c = a.widthProperties[b]), a.items[b][c]
        }
        function c(a, b) {
            var c = a.heightProperty || "height";
            return a.heightProperties && (c = a.heightProperties[b]), a.items[b][c]
        }
        var d = {
            centerBitmapText: function(a) {
                var b = a.text;
                b.updateTransform(), a.width && (b.x = a.width / 2 - b.textWidth / 2 + (a.offsetX || 0)), a.height && (b.y = a.height / 2 - b.textHeight / 2 + (a.offsetY || 0))
            },
            centerItemsHorizontally: function(a) {
                a.paddingX = a.paddingX || 0, a.paddingY = a.paddingY || 0, a.offsetX = a.offsetX || 0, a.offsetY = a.offsetY || 0;
                var d, e, f = a.items.length,
                    g = (f - 1) * a.paddingX;
                for (d = 0; f > d; d++) g += b(a, d);
                for (a.width = a.width || g, e = (a.width - g) / 2 + a.offsetX, d = 0; f > d; d++) a.items[d].x = e, e += b(a, d) + a.paddingX;
                if (a.height) for (d = 0; f > d; d++) a.items[d].y = a.height / 2 - c(a, d) / 2 + a.offsetY
            },
            centerItemsVertically: function(a) {
                a.paddingX = a.paddingX || 0, a.paddingY = a.paddingY || 0, a.offsetX = a.offsetX || 0, a.offsetY = a.offsetY || 0;
                var d, e, f = a.items.length,
                    g = (f - 1) * a.paddingY;
                for (d = 0; f > d; d++) g += c(a, d);
                for (a.height = a.height || g, e = (a.height - g) / 2 + a.offsetY, d = 0; f > d; d++) a.items[d].y = e, e += c(a, d) + a.paddingY;
                if (a.width) for (d = 0; f > d; d++) a.items[d].x = a.width / 2 - b(a, d) / 2 + a.offsetX
            }
        };
        a.controls.Utils = d
    }(), function() {
        function b(a, b) {
            Phaser.Image.call(this, a, b.x, b.y, b.trackKey), this.handle = new Phaser.Image(a, this.width / 2, 0, b.handleKey), this.handle.anchor.set(.5, .5), this.addChild(this.handle), this.activePointer = null, this.inputEnabled = !0, this.events.onInputDown.add(this.onInputDown, this), this.events.onInputUp.add(this.onInputUp, this), this.setValue(b.value || .5)
        }
        b.prototype = Object.create(Phaser.Image.prototype), b.prototype.constructor = b, b.prototype.setValue = function(a) {
            var b = this.height - this.handle.height,
                c = b * (1 - a),
                d = this.handle.height / 2 + c;
            this.handle.y = d
        }, b.prototype.getInputValue = function() {
            if (!this.activePointer) return null;
            var a = this.height - this.handle.height,
                b = this.y + this.handle.height / 2,
                c = this.activePointer.worldY - b,
                d = Math.max(0, Math.min(a, c)),
                e = 1 - d / a;
            return e
        }, b.prototype.onInputDown = function(a, b) {
            this.activePointer = b
        }, b.prototype.onInputUp = function() {
            this.activePointer = null
        }, a.controls.VerticalSlider = b
    }(), function() {
        function b() {
            Phaser.State.call(this), this.bgStart = {
                x: 0,
                y: 0
            }
        }
        b.prototype = Object.create(Phaser.State.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            this.load.image("menuBg", "images/menus/menuBg.png")
        }, b.prototype.create = function() {
            this.bg = this.game.add.image(0, 0, "menuBg"), this.world.setBounds(this.bgStart.x, this.bgStart.y, this.bgStart.x + this.bg.width, this.bgStart.y + this.bg.height), this.bg.x = this.bgStart.x, this.bg.y = this.bgStart.y
        }, b.prototype.slideTo = function(a, b, c) {
            var d = this.game.add.tween(this.game.camera);
            d.to({
                x: a,
                y: b
            }, c, Phaser.Easing.Cubic.InOut, !0)
        }, b.prototype.moveTo = function(a, b) {
            this.game.camera.x = a, this.game.camera.y = b
        }, a.states.BackgroundSlider = b
    }(), function() {
        function b() {
            Phaser.State.call(this)
        }
        b.prototype = Object.create(Phaser.State.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            a.controls.LoadingScreen.preload(this.load)
        }, b.prototype.create = function() {
            a.resize();
            var b = "home";
            a.production || (b = location.search.substr(1) || "home"), "home" === b || "levels" === b ? a.startMenu(b) : b && a.game.state.start(b)
        }, a.states.BootState = b
    }(), function() {
        function b(a, b) {
            Phaser.Group.call(this, a, a.world, "home"), this.parentState = b
        }
        b.prototype = Object.create(Phaser.Group.prototype), b.prototype.constructor = b, b.preload = function(b) {
            b.image("homeLogo", "images/menus/menuHomeLogo.png"), b.image("homeTitle", "images/menus/menuHomeTitle.png"), b.image("startButton", "images/menus/menuHomeStart.png"), b.image("menuFit", "images/menus/menuFit.png"), b.image("linkBar", "images/controls/controlsBar.png"), b.spritesheet("privacyLink", "images/controls/controlsPrivacy.png", 160, 24, 2, 0, 4), b.spritesheet("termsLink", "images/controls/controlsTerms.png", 256, 24, 2, 0, 4), a.controls.SocialButtons.preload(b)
        }, b.prototype.create = function() {
            this.add(new Phaser.Image(this.game, 100, 70, "homeLogo")), this.add(new Phaser.Image(this.game, 415, 100, "homeTitle")), this.add(new Phaser.Image(this.game, 1020, 385, "menuFit"));
            var b = this.add(new Phaser.Button(this.game, 545, 325, "startButton", this.clickStart, this));
            b.input.useHandCursor = !0, new a.controls.SocialButtons(this.game, {
                x: 30,
                y: this.game.height - 100,
                parent: this,
                pageName: "Home Page"
            });
            var c = this.add(new Phaser.Button(this.game, 0, 15, "optionsButton", this.parentState.clickOptions, this.parentState));
            c.x = this.game.width - c.width, c.input.useHandCursor = !0, this.createLinks()
        }, b.prototype.clickStart = function() {
            a.analytics.buttonClick("Home Page", "Start"), this.game.add.audio("carEngine").play(), this.parentState.slideToGroup("levels")
        }, b.prototype.createLinks = function() {
            var b = this.game.height - 40,
                c = this.add(new Phaser.Button(this.game, 0, b + 3, "privacyLink", this.clickPrivacyLink, this, 1, 0));
            c.input.useHandCursor = !0;
            var d = this.add(new Phaser.Image(this.game, 0, b, "linkBar")),
                e = this.add(new Phaser.Button(this.game, 0, b + 3, "termsLink", this.clickTermsLink, this, 1, 0));
            e.input.useHandCursor = !0, a.controls.Utils.centerItemsHorizontally({
                items: [c, d, e],
                width: this.game.width,
                paddingX: 10
            })
        }, b.prototype.clickPrivacyLink = function() {
            a.analytics.buttonClick("Home Page", "Privacy"), window.open("http://automobiles.honda.com/information/consumer-privacy-policy.aspx", "_blank")
        }, b.prototype.clickTermsLink = function() {
            a.analytics.buttonClick("Home Page", "Terms of Use"), window.open("terms.html", "_blank")
        }, a.states.HomeMenu = b
    }(), function() {
        function b() {
            Phaser.State.call(this), $.extend(this, a.EventDispatcher.prototype)
        }
        b.prototype = Object.create(Phaser.State.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            new a.controls.LoadingScreen(this.game), a.controls.DialogGroup.preload(this.load), this.loadedImages = ["car", "carShadow", "tire", "pivot", "trans32", "gasTrack", "gasHandle", "steeringHandle", "steeringWheel", "steeringShadow", "optionsButton"], this.loadedAudio = ["crash", "start", "lose", "win", "music", "finalWin"], this.loadedPhysics = ["carData"], this.load.image("car", "images/fit.png"), this.load.image("carShadow", "images/fitshadow.png"), this.load.image("tire", "images/tire.png"), this.load.image("pivot", "images/pivotPoint.png"), this.load.image("trans32", "images/trans32.png"), this.load.image("gasTrack", "images/controls/controlsSpeedSlider.png"), this.load.image("gasHandle", "images/controls/controlsSpeedThumb.png"), this.load.image("steeringTrack", "images/controls/controlsSteeringSlider.png"), this.load.image("steeringHandle", "images/controls/controlsSteeringThumb.png"), this.load.image("steeringWheel", "images/controls/controlsSteering.png"), this.load.image("steeringShadow", "images/controls/controlsSteeringShadow.png"), this.load.image("optionsButton", "images/controls/controlsOptions.png"), this.load.image("pauseButton", "images/controls/controlsPause.png"), this.load.image("clockCorner", "images/controls/controlsCorner.png"), this.load.spritesheet("crashIcon", "images/controls/controlsCrash.png", 68, 80), a.controls.SocialButtons.preload(this.load), this.load.physics("carData", "physics/car.json"), this.load.audio("crash", ["audio/sfx/crash.m4a", "audio/sfx/crash.ogg"]), this.load.audio("win", ["audio/sfx/level_win.m4a", "audio/sfx/level_win.ogg"]), this.load.audio("start", ["audio/sfx/level_start.m4a", "audio/sfx/level_start.ogg"]), this.load.audio("lose", ["audio/sfx/level_lose.m4a", "audio/sfx/level_loose.ogg"]), this.load.audio("finalWin", ["audio/sfx/final_level_win.m4a", "audio/sfx/final_level_win.ogg"]), a.currentMusic = 2 === a.currentMusic ? 0 : a.currentMusic + 1, this.load.audio("music", ["audio/music/" + a.music[a.currentMusic] + ".m4a", "audio/music/" + a.music[a.currentMusic] + ".ogg"])
        }, b.prototype.create = function() {
            this.levelPaused = !1, this.rotRange = 15, this.range = 15, this.score = 0, this.isFinalizingPark = !1, this.isParked = !1, this.didLose = !1, this.canDrive = !0, this.lastCollisionPoint = {
                x: this.playerOrientation.x,
                y: this.playerOrientation.y
            }, this.groundLayer = null, this.colliderLayer = null, this.player = null, this.cursors = null, this.speed = 0, this.direction = 0, this.debugText = "", this.collisionManager = new a.CollisionManager(3), this.collisionManager.addEventListener(a.COLLISION, $.proxy(this.handleCollision, this)), this.collisionManager.addEventListener(a.COLLISION_MAX, $.proxy(this.handleCollisionMax, this)), this.cursors = this.game.input.keyboard.createCursorKeys(), this.createWorld(), this.createGasPedal(), this.createSteeringWheel(), this.createOptionsButton(), this.createPauseButton(), this.createTimer(), this.createCrashIcons(), a.analytics.pageView("Game Page")
        }, b.prototype.updateTimer = function() {
            this.didLose = !0, this.game.time.events.remove(this.timerEvent), this.isParked || this.youLose()
        }, b.prototype.createOptionsButton = function() {
            var a = this.optionsButton = this.game.add.button(0, 15, "optionsButton", this.clickOptions, this);
            a.x = this.game.width - a.width, a.input.useHandCursor = !0, a.fixedToCamera = !0, a.cameraOffset.setTo(a.x, a.y)
        }, b.prototype.createPauseButton = function() {
            var a = this.pauseButton = this.game.add.button(960, 20, "pauseButton", this.clickPauseButton, this);
            a.input.useHandCursor = !0, a.fixedToCamera = !0, a.cameraOffset.setTo(a.x, a.y)
        }, b.prototype.clickPauseButton = function() {
            a.analytics.buttonClick("Game Page", "Pause"), new a.controls.LevelPauseDialog(this.game, this)
        }, b.prototype.pause = function() {
            this.levelPaused || (this.music.pause(), this.game.time.events.remove(this.timerEvent), this.game.physics.p2.pause(), this.levelPaused = !0, this.toggleControlVisibility(!this.levelPaused))
        }, b.prototype.resume = function() {
            this.levelPaused && (this.music.resume(), this.timerEvent = this.game.time.events.add(Phaser.Timer.SECOND * Math.floor((this.timer + 1e3) / 1e3), this.updateTimer, this), this.levelPaused = !1, this.toggleControlVisibility(!this.levelPaused), this.game.physics.p2.resume())
        }, b.prototype.toggleControlVisibility = function(a) {
            this.steering.visible = a, this.gasPedal.visible = a, this.pauseButton.visible = a, this.optionsButton.visible = a, this.timerCorner.visible = a, this.timerText.visible = a;
            for (var b = 0, c = this.crashIcons.length; c > b; b++) this.crashIcons[b].visible = a
        }, b.prototype.clickOptions = function() {
            this.levelPaused || (a.analytics.buttonClick("Game Page", "Options"), new a.controls.OptionsDialog(this.game, this, {
                showMenuButton: !0
            }))
        }, b.prototype.createCrashIcons = function() {
            this.crashIcons = [];
            var a, b, c, d = 90,
                e = 8,
                f = 0;
            for (a = 0, b = 3; b > a; a++) c = this.crashIcons[a] = this.game.add.sprite(d, e, "crashIcon", 1), c.fixedToCamera = !0, c.cameraOffset.setTo(c.x, c.y), d += c.width + f
        }, b.prototype.createTimer = function() {
            this.timerCorner = this.game.add.image(0, 0, "clockCorner"), this.timerCorner.fixedToCamera = !0, this.timerText = this.game.add.bitmapText(12, 20, "smallwhite", ":" + Math.floor(this.timer / 1e3), 40), this.timerText.fixedToCamera = !0
        }, b.prototype.createGasPedal = function() {
            this.gasPedal = new a.controls.VerticalSlider(this.game, {
                x: 0,
                y: 0,
                value: this.speedManager.getNormalizedCurrentValue(),
                trackKey: "gasTrack",
                handleKey: "gasHandle"
            });
            var b = this.gasPedal.width,
                c = this.gasPedal.height,
                d = 30,
                e = this.game.width - b,
                f = this.game.height - d - c;
            this.gasPedal.fixedToCamera = !0, this.gasPedal.cameraOffset.setTo(e, f), this.world.add(this.gasPedal)
        }, b.prototype.createSteeringWheel = function() {
            this.steering = new a.controls.RotationSlider(this.game, {
                x: 0,
                y: 0,
                minRotation: -30 * a.toRADIANS,
                maxRotation: 30 * a.toRADIANS,
                value: this.directionManager.getNormalizedCurrentValue(),
                trackKey: "steeringTrack",
                handleKey: "steeringHandle",
                wheel: {
                    key: "steeringWheel",
                    offsetY: 50
                },
                wheelShadow: {
                    key: "steeringShadow",
                    offsetX: 20,
                    offsetY: 20
                }
            });
            var b = (this.steering.width, this.steering.height),
                c = 30,
                d = c,
                e = this.game.height - b;
            this.steering.fixedToCamera = !0, this.steering.cameraOffset.setTo(d, e), this.world.add(this.steering)
        }, b.prototype.resetCar = function() {
            this.player.body.setZeroRotation(), this.player.body.setZeroVelocity(), this.player.x = this.playerOrientation.x, this.player.y = this.playerOrientation.y, this.player.body.angle = this.playerOrientation.z
        }, b.prototype.createWorld = function() {
            this.speedManager = new a.ValueManager(this, "speed", -100, 200, .15, .025), this.directionManager = new a.ValueManager(this, "direction", -30, 30), this.world.setBounds(this.minX, this.minY, this.maxX, this.maxY), this.physics.startSystem(Phaser.Physics.P2JS), this.stage.backgroundColor = "#222222", this.groundLayer = this.game.add.group(), this.groundLayer.z = 0, this.map = this.groundLayer.create(.5 * this.maxX, .5 * this.maxY, "level"), this.game.physics.p2.enableBody(this.map, !1), this.map.body.kinematic = !0, this.map.body.clearShapes(), this.map.z = 0;
            this.map.body.addPhaserPolygon("physicsData", "level");
            this.carLayer = this.game.add.group(), this.carLayer.z = 2, this.player = this.carLayer.create(this.playerOrientation.x, this.playerOrientation.y, "trans32"), this.player.collideWorldBounds = !0, this.physics.p2.enableBody(this.player, !1), this.player.body.clearShapes(), this.player.body.loadPolygon("carData", "car"), this.player.body.angle = this.playerOrientation.z, this.player.body.onBeginContact.add(this.enterCollisionHandler, this), this.player.body.onEndContact.add(this.exitCollisionHandler, this), this.wheels = this.game.add.group(this.player), this.leftWheel = this.wheels.create(12 - .5 * this.player.width, .5 * -this.player.height + 32, "tire"), this.rightWheel = this.wheels.create(82 - (.5 * this.player.width - 1), .5 * -this.player.height + 32, "tire"), this.leftWheel.anchor.setTo(.5, .5), this.rightWheel.anchor.setTo(.5, .5), this.car = this.wheels.create(-34, -60, "car"), this.carShadow = this.groundLayer.create(0, 0, "carShadow"), this.pivot = this.wheels.create(15, 19, "pivot"), this.pivot.anchor.setTo(.5, .5), this.player.pivot.y = .3 * this.player.height, this.player.collideWorldBounds = !0, this.physics.p2.enableBody(this.player, !1), this.player.body.clearShapes(), this.player.body.loadPolygon("carData", "car");
            var b = this.game.add.group();
            b.create(0, 0, "above"), this.crash = this.game.add.audio("crash"), this.startSound = this.game.add.audio("start"), this.loseSound = this.game.add.audio("lose"), this.winSound = this.game.add.audio("win"), this.finalWinSound = this.game.add.audio("finalWin"), this.music = this.game.add.audio("music"), this.music.loop = !0, this.music.volume = a.isMusicMuted ? 0 : 1, this.timer = 1e3 * this.levelTimeout, window.setTimeout(function() {
                this.timerEvent = this.game.time.events.add(Phaser.Timer.SECOND * this.levelTimeout, this.updateTimer, this), this.music.play()
            }.bind(this), 1250), this.camera.follow(this.player, Phaser.Camera.FOLLOW_TOPDOWN_TIGHT), a.isSfxMuted || this.startSound.play()
        }, b.prototype.enterCollisionHandler = function(b) {
            this.canDrive && (this.lastCollisionPoint.x = this.player.x, this.lastCollisionPoint.y = this.player.y, this.player.body.moveForward(this.speedManager.Value < 0 ? 1500 : -1500), this.canDrive = !1, this.player.body.fixedRotation = !0, window.setTimeout(this.resetCanDrive.bind(this), 1500), this.collisionManager.addCollision(b), this.debugText = "can vibrate? " + a.vibrate, a.vibrate && window.navigator.vibrate(100), a.isSfxMuted || this.crash.play())
        }, b.prototype.exitCollisionHandler = function() {}, b.prototype.resetCanDrive = function() {
            this.player.body.fixedRotation = !1, this.canDrive = !0
        }, b.prototype.update = function() {
            var b = 0 === this.timer ? 0 : Math.floor(this.timer / 1e3);
            if (b = Math.max(0, b), this.timerText.setText(b.toString()), this.music.volume = a.isMusicMuted ? 0 : 1, !this.levelPaused) {
                this.game.time.events.length > 0 && (this.timer = this.game.time.events.duration), this.updateInput(), this.directionManager.update(), this.speedManager.update(), this.updateDriveControls();
                var c = Math.abs(this.speed / this.speedManager.max);
                this.isParked || (this.speed < 0 ? this.player.body.rotateLeft(this.direction * c) : this.player.body.rotateRight(this.direction * c), this.player.body.moveForward(this.speed), this.leftWheel.rotation = this.direction * a.toRADIANS, this.rightWheel.rotation = this.direction * a.toRADIANS, this.carShadow.angle = this.player.angle, this.carShadow.x = this.car.world.x, this.carShadow.y = this.car.world.y, this.carShadow.x += 7, this.carShadow.y += 7), this.canDrive ? this.checkParking() : this.player.body.setZeroVelocity()
            }
        }, b.prototype.updateInput = function() {
            var a = 10,
                b = 7;
            this.player.body.setZeroVelocity();
            var c = this.steering.getInputValue();
            null !== c && this.canDrive ? this.directionManager.setNormalizedTarget(c) : this.cursors.left.isDown && this.canDrive ? this.directionManager.decrease(a) : this.cursors.right.isDown && this.canDrive ? this.directionManager.increase(a) : (this.player.body.setZeroRotation(), this.directionManager.Value = 0);
            var d = this.gasPedal.getInputValue();
            null !== d && this.canDrive ? this.speedManager.setNormalizedTarget(d) : this.cursors.up.isDown && this.canDrive ? this.speedManager.increase(this.speedManager.Value < 0 ? 3 * b : b) : this.cursors.down.isDown && this.canDrive ? this.speedManager.decrease(this.speedManager.Value > 0 ? 3 * b : b) : this.speedManager.Value = 0
        }, b.prototype.updateDriveControls = function() {
            var a = this.speedManager.getNormalizedCurrentValue();
            this.gasPedal.setValue(a);
            var b = this.directionManager.getNormalizedCurrentValue();
            this.steering.setValue(b)
        }, b.prototype.checkParking = function() {
            var b, c, d;
            b = Math.abs(Math.floor(a.getDistance(this.parking, {
                x: this.pivot.world.x,
                y: this.pivot.world.y
            }))), c = Math.abs(this.player.angle), d = Math.abs(c - this.parking.z), d = d > 90 ? 90 - (d - 90) : d, this.isParked || (!this.isFinalizingPark && b <= this.range && d < this.rotRange ? (this.isFinalizingPark = !0, window.setTimeout(this.finalizePark.bind(this), 1e3)) : !this.isFinalizingPark)
        }, b.prototype.finalizePark = function() {
            var b = 100 * (this.collisionManager.maxCollisions - this.collisionManager.count),
                c = Math.abs(Math.floor(a.getDistance(this.parking, {
                    x: this.pivot.world.x,
                    y: this.pivot.world.y
                }))),
                d = 10 * Math.abs(this.range - c),
                e = Math.abs(Math.abs(this.player.angle) - this.parking.z);
            e = e > 90 ? 90 - (e - 90) : e;
            var f = 10 * Math.round(this.rotRange - e);
            if (c > this.range || e >= this.rotRange || this.didLose) return void(this.isFinalizingPark = !1);
            this.game.time.events.remove(this.timerEvent), this.isParked = !0, console.log("scoring: collisionValue: " + b + ", distanceValue: " + d + ", rotValue: " + f), this.music.stop(), "level8" === this.game.state.current ? a.isSfxMuted || this.finalWinSound.play() : a.isSfxMuted || this.winSound.play(), this.score = b + d + f;
            var g = a.scores.get(this.levelIndex);
            this.score > g && a.scores.set(this.levelIndex, this.score), this.onWin(this.score)
        }, b.prototype.onWin = function(b) {
            this.timerText.visible = !1, new a.controls.LevelWonDialog(this.game, this, {
                points: b,
                nextState: "level" + (this.levelIndex + 2)
            })
        }, b.prototype.shutdown = function() {
            this.music.stop(), this.winSound.stop(), this.finalWinSound.stop();
            for (var a in this.loadedImages) this.game.cache.removeImage(this.loadedImages[a]);
            for (var b in this.loadedAudio) this.game.cache.removeSound(this.loadedAudio[b]);
            for (var c in this.loadedPhysics) this.game.cache.removePhysics(this.loadedPhysics[c])
        }, b.prototype.render = function() {}, b.prototype.handleCollision = function() {
            var a = this.collisionManager.count - 1,
                b = this.crashIcons[a];
            b && (b.frame = 0)
        }, b.prototype.handleCollisionMax = function() {
            this.youLose()
        }, b.prototype.youLose = function() {
            this.music.stop(), this.timerText.visible = !1, a.isSfxMuted || this.loseSound.play(), new a.controls.LevelFailDialog(this.game, this, {})
        }, a.states.Level = b
    }(), function() {
        function b() {
            c.call(this), this.minX = 0, this.minY = 0, this.maxX = 1136, this.maxY = 1920, this.levelIndex = 0, this.levelTimeout = 60
        }
        var c = a.states.Level;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            c.prototype.preload.call(this), this.loadedImages.push("level"), this.loadedImages.push("above"), this.loadedPhysics.push("physicsData"), this.load.image("above", "images/levels/level1/above.png"), this.load.image("level", "images/levels/level1/below.png"), this.load.physics("physicsData", "physics/level1.json")
        }, b.prototype.create = function() {
            this.parking = {
                x: 962,
                y: 592,
                z: 0
            }, this.playerOrientation = {
                x: 752,
                y: 1687,
                z: 0
            }, c.prototype.create.call(this)
        }, b.prototype.update = function() {
            c.prototype.update.call(this)
        }, b.prototype.render = function() {
            c.prototype.render.call(this)
        }, a.states.Level1 = b
    }(), function() {
        function b() {
            c.call(this), this.minX = 0, this.minY = 0, this.maxX = 1136, this.maxY = 1920, this.levelIndex = 1, this.levelTimeout = 60
        }
        var c = a.states.Level;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            c.prototype.preload.call(this), this.loadedImages.push("level"), this.loadedImages.push("above"), this.loadedPhysics.push("physicsData"), this.load.image("above", "images/levels/level2/above.png"), this.load.image("level", "images/levels/level2/below.png"), this.load.physics("physicsData", "physics/level2.json")
        }, b.prototype.create = function() {
            this.parking = {
                x: 154,
                y: 571,
                z: 0
            }, this.playerOrientation = {
                x: 749,
                y: 1661,
                z: 0
            }, c.prototype.create.call(this)
        }, b.prototype.update = function() {
            c.prototype.update.call(this)
        }, b.prototype.render = function() {
            c.prototype.render.call(this)
        }, a.states.Level2 = b
    }(), function() {
        function b() {
            c.call(this), this.minX = 0, this.minY = 0, this.maxX = 1136, this.maxY = 1920, this.levelIndex = 2, this.levelTimeout = 60
        }
        var c = a.states.Level;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            c.prototype.preload.call(this), this.loadedImages.push("level"), this.loadedImages.push("above"), this.loadedPhysics.push("physicsData"), this.load.image("above", "images/levels/level3/above.png"), this.load.image("level", "images/levels/level3/below.png"), this.load.physics("physicsData", "physics/level3.json")
        }, b.prototype.create = function() {
            this.parking = {
                x: 630,
                y: 387,
                z: 0
            }, this.playerOrientation = {
                x: 263,
                y: 1663,
                z: 90
            }, c.prototype.create.call(this)
        }, b.prototype.update = function() {
            c.prototype.update.call(this)
        }, b.prototype.render = function() {
            c.prototype.render.call(this)
        }, a.states.Level3 = b
    }(), function() {
        function b() {
            c.call(this), this.minX = 0, this.minY = 0, this.maxX = 1748, this.maxY = 2080, this.levelIndex = 3, this.levelTimeout = 90
        }
        var c = a.states.Level;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            c.prototype.preload.call(this), this.loadedImages.push("level"), this.loadedImages.push("above"), this.loadedPhysics.push("physicsData"), this.load.image("above", "images/levels/level4/above.png"), this.load.image("level", "images/levels/level4/below.png"), this.load.physics("physicsData", "physics/level4.json")
        }, b.prototype.create = function() {
            this.parking = {
                x: 1050,
                y: 408,
                z: 0
            }, this.playerOrientation = {
                x: 409,
                y: 329,
                z: 180
            }, c.prototype.create.call(this)
        }, b.prototype.update = function() {
            c.prototype.update.call(this)
        }, b.prototype.render = function() {
            c.prototype.render.call(this)
        }, a.states.Level4 = b
    }(), function() {
        function b() {
            c.call(this), this.minX = 0, this.minY = 0, this.maxX = 1752, this.maxY = 1920, this.levelIndex = 4, this.levelTimeout = 60
        }
        var c = a.states.Level;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            c.prototype.preload.call(this), this.loadedImages.push("level"), this.loadedImages.push("above"), this.loadedPhysics.push("physicsData"), this.load.image("above", "images/levels/level5/above.png"), this.load.image("level", "images/levels/level5/below.png"), this.load.physics("physicsData", "physics/level5.json")
        }, b.prototype.create = function() {
            this.parking = {
                x: 534,
                y: 772,
                z: 0
            }, this.playerOrientation = {
                x: 1485,
                y: 1550,
                z: 0
            }, c.prototype.create.call(this)
        }, b.prototype.update = function() {
            c.prototype.update.call(this)
        }, b.prototype.render = function() {
            c.prototype.render.call(this)
        }, a.states.Level5 = b
    }(), function() {
        function b() {
            c.call(this), this.minX = 0, this.minY = 0, this.maxX = 1752, this.maxY = 1920, this.levelIndex = 5, this.levelTimeout = 60
        }
        var c = a.states.Level;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            c.prototype.preload.call(this), this.loadedImages.push("level"), this.loadedImages.push("above"), this.loadedPhysics.push("physicsData"), this.load.image("above", "images/levels/level6/above.png"), this.load.image("level", "images/levels/level6/below.png"), this.load.physics("physicsData", "physics/level6.json")
        }, b.prototype.create = function() {
            this.parking = {
                x: 934,
                y: 1080,
                z: 0
            }, this.playerOrientation = {
                x: 236,
                y: 1699,
                z: 0
            }, c.prototype.create.call(this)
        }, b.prototype.update = function() {
            c.prototype.update.call(this)
        }, b.prototype.render = function() {
            c.prototype.render.call(this)
        }, a.states.Level6 = b
    }(), function() {
        function b() {
            c.call(this), this.minX = 0, this.minY = 0, this.maxX = 1920, this.maxY = 2480, this.levelIndex = 6, this.levelTimeout = 90
        }
        var c = a.states.Level;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            c.prototype.preload.call(this), this.loadedImages.push("level"), this.loadedImages.push("above"), this.loadedPhysics.push("physicsData"), this.load.image("above", "images/levels/level7/above.png"), this.load.image("level", "images/levels/level7/below.png"), this.load.physics("physicsData", "physics/level7.json")
        }, b.prototype.create = function() {
            this.parking = {
                x: 947,
                y: 585,
                z: 90
            }, this.playerOrientation = {
                x: 1475,
                y: 1649,
                z: 180
            }, c.prototype.create.call(this)
        }, b.prototype.update = function() {
            c.prototype.update.call(this)
        }, b.prototype.render = function() {
            c.prototype.render.call(this)
        }, a.states.Level7 = b
    }(), function() {
        function b() {
            c.call(this), this.minX = 0, this.minY = 0, this.maxX = 2500, this.maxY = 1712, this.levelIndex = 7, this.levelTimeout = 60
        }
        var c = a.states.Level;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            c.prototype.preload.call(this), this.loadedImages.push("level"), this.loadedImages.push("above"), this.loadedPhysics.push("physicsData"), this.load.image("above", "images/levels/level8/above.png"), this.load.image("level", "images/levels/level8/below.png"), this.load.physics("physicsData", "physics/level8.json")
        }, b.prototype.create = function() {
            this.parking = {
                x: 2358,
                y: 844,
                z: 0
            }, this.playerOrientation = {
                x: 272,
                y: 425,
                z: 90
            }, c.prototype.create.call(this)
        }, b.prototype.onWin = function(b) {
            this.timerText.visible = !1, new a.controls.GameWonDialog(this.game, this, {
                points: b
            })
        }, b.prototype.update = function() {
            c.prototype.update.call(this)
        }, b.prototype.render = function() {
            c.prototype.render.call(this)
        }, a.states.Level8 = b
    }(), function() {
        function b(a, b) {
            Phaser.Group.call(this, a, a.world, "levels"), this.parentState = b
        }
        b.prototype = Object.create(Phaser.Group.prototype), b.prototype.constructor = b, b.preload = function(a) {
            a.image("levelMedal", "images/menus/menuLevelsMedal.png"), a.spritesheet("levelButton", "images/menus/menuLevelButton.png", 152, 148), a.spritesheet("backMenuBlue", "images/menus/menuMenuBlue.png", 176, 36, 2, 0, 4), a.bitmapFont("levelnums", "images/fonts/levelnums.png", "images/fonts/levelnums.xml"), a.bitmapFont("smallwhite", "images/fonts/smallwhite.png", "images/fonts/smallwhite.xml"), a.bitmapFont("bonus", "images/fonts/bonus.png", "images/fonts/bonus.xml")
        }, b.prototype.create = function() {
            this.createBackButton(), this.createScores(), this.addRow(230, 0, 4), this.addRow(384, 4, 4);
            var a = this.add(new Phaser.Button(this.game, 0, 15, "optionsButton", this.parentState.clickOptions, this.parentState));
            a.x = this.game.width - a.width, a.input.useHandCursor = !0
        }, b.prototype.createBackButton = function() {
            var a = this.add(new Phaser.Button(this.game, 30, 30, "backMenuBlue", this.clickBackHome, this, 1, 0));
            a.input.useHandCursor = !0
        }, b.prototype.createScores = function() {
            var b = this.add(new Phaser.BitmapText(this.game, 0, 90, "smallwhite", a.scores.getTotal() + " Bonus Points", 28));
            a.controls.Utils.centerItemsHorizontally({
                items: [b],
                width: this.game.width,
                widthProperty: "textWidth"
            });
            var c = this.add(new Phaser.Image(this.game, 0, 120, "levelMedal")),
                d = this.add(new Phaser.BitmapText(this.game, 0, 125, "bonus", a.scores.getTitle(), 52));
            d.updateTransform(), a.controls.Utils.centerItemsHorizontally({
                items: [c, d],
                widthProperties: ["width", "textWidth"],
                width: this.game.width,
                paddingX: 30
            })
        }, b.prototype.addRow = function(b, c, d) {
            var e, f, g, h, i, j, k = function(a) {
                    return function() {
                        this.clickLevel(a)
                    }
                },
                l = [];
            for (e = 0; d > e; e++, c++) f = a.scores.get(c), h = f > 0 ? 0 : 0 === f ? 1 : 2, j = a.scores.isLevelLocked(c), g = new a.controls.BitmapTextButton(this.game, {
                x: 0,
                y: b,
                key: "levelButton",
                text: (c + 1).toString(),
                font: "levelnums",
                size: 54,
                textOffsetX: -10,
                textOffsetY: -25,
                callback: j ? $.noop : k(c),
                callbackContext: this,
                outFrame: h
            }), j && (g.input.useHandCursor = !1), this.add(g), l.push(g), f > 0 && (i = new Phaser.BitmapText(this.game, 0, 88, "smallwhite", "+" + f, 24), g.addChild(i), i.updateTransform(), i.x = 60 - i.textWidth / 2);
            a.controls.Utils.centerItemsHorizontally({
                items: l,
                width: this.game.width,
                paddingX: 40,
                offsetX: 12.5
            })
        }, b.prototype.clickLevel = function(b) {
            var c = b + 1;
            this.game.state.start("level" + c), a.analytics.buttonClick("Level Menu", "Level " + c)
        }, b.prototype.clickBackHome = function() {
            a.analytics.buttonClick("Level Menu", "Back"), this.parentState.slideToGroup("home")
        }, a.states.LevelMenu = b
    }(), function() {
        function b() {
            c.call(this), this.bgPositions = {
                home: {
                    x: 0,
                    y: 0
                },
                levels: {
                    x: 1136,
                    y: 0
                }
            }
        }
        var c = a.states.BackgroundSlider;
        b.prototype = Object.create(c.prototype), b.prototype.constructor = b, b.prototype.init = function(a) {
            this.menuGroup = a || "home"
        }, b.prototype.preload = function() {
            c.prototype.preload.call(this), new a.controls.LoadingScreen(this.game), this.load.audio("music", ["audio/music/IntroMusic.m4a", "audio/music/IntroMusic.ogg"]), this.load.audio("carEngine", ["audio/sfx/CarEngine.m4a", "audio/sfx/CarEngine.ogg"]), this.load.image("optionsButton", "images/controls/controlsOptions.png"), a.states.HomeMenu.preload(this.load), a.states.LevelMenu.preload(this.load), a.controls.DialogGroup.preload(this.load)
        }, b.prototype.create = function() {
            c.prototype.create.call(this), this.moveToGroup(this.menuGroup), this.homeMenu = this.world.add(new a.states.HomeMenu(this.game, this)), this.homeMenu.create(), this.levelsMenu = this.world.add(new a.states.LevelMenu(this.game, this)), this.levelsMenu.x = this.bgPositions.levels.x, this.levelsMenu.create(), this.music = this.game.add.audio("music"), this.music.loop = !0, this.music.volume = a.isMusicMuted ? 0 : 1, this.music.play()
        }, b.prototype.update = function() {
            this.music.volume = a.isMusicMuted ? 0 : 1
        }, b.prototype.shutdown = function() {
            this.music.stop()
        }, b.prototype.slideToGroup = function(a, b) {
            this.menuGroup = a || "home";
            var c = this.bgPositions[this.menuGroup];
            this.slideTo(c.x, c.y, b), this.trackTransition(a)
        }, b.prototype.moveToGroup = function(a) {
            this.menuGroup = a || "home";
            var b = this.bgPositions[this.menuGroup];
            this.moveTo(b.x, b.y), this.trackTransition(a)
        }, b.prototype.trackTransition = function(b) {
            var c = "levels" === b ? "Level Menu" : "Home Page";
            a.analytics.pageView(c)
        }, b.prototype.clickOptions = function() {
            if (!this.optionsOpen) {
                var b = "levels" === this.menuGroup ? "Level Menu" : "Home Page";
                a.analytics.buttonClick(b, "Options"), new a.controls.OptionsDialog(this.game, this, {
                    showMenuButton: !1
                })
            }
        }, b.prototype.pause = function() {
            this.optionsOpen = !0
        }, b.prototype.resume = function() {
            this.optionsOpen = !1
        }, a.states.MenuMap = b
    }(), function() {
        function b() {
            this.pageName = "Sweepstakes Entry Form", Phaser.State.call(this), this.$sweeps = $("#sweeps"), this.$sweepsError = $("#sweepsError"), this.$fitNewsCheckbox = $("#sendInfo").click($.proxy(this.onFitNewsClick, this)), this.$termsCheckbox = $("#terms").click($.proxy(this.onTermsClick, this)), $("#privacyLink").on("click", $.proxy(function() {
                a.analytics.buttonClick(this.pageName, "Privacy")
            }, this)), $("#termsLink").on("click", $.proxy(function() {
                a.analytics.buttonClick(this.pageName, "Official Sweepstakes Rules")
            }, this)), $("#sweepsEventDate").text(a.sweeps.eventDateString), $("#sweepsCancel").click($.proxy(this.onCancel, this)), this.$submit = $("#sweepsSubmit").click($.proxy(this.onSubmitClick, this)), $.verify.addRules({
                checked: function(a) {
                    var b = a.field.is(":checked");
                    return b ? !0 : "Checkbox is not checked"
                }
            }), this.$form = $("#sweepsForm").verify({
                prompt: $.proxy(this.onValidate, this)
            })
        }
        var c;
        c = a.production ? "https://eshopping.services.honda.com/Custom/QuoteRequest.ashx" : "https://Staging.eshopping.services.honda.com/Custom/QuoteRequest.ashx", c += "?appid=hondafitchallenge.honda.com&method=SubmitQuoteRequest&output=json", b.prototype = Object.create(Phaser.State.prototype), b.prototype.constructor = b, b.prototype.preload = function() {
            this.backgroundColor = 16777215, a.controls.DialogGroup.preload(this.load)
        }, b.prototype.create = function() {
            a.resize(), this.errors = {}, this.updateErrors(), this.resetSubmit(), this.$sweeps.show(), a.analytics.pageView(this.pageName)
        }, b.prototype.onValidate = function(a, b) {
            var c = a.attr("id"),
                d = a.attr("placeholder");
            b ? this.errors[c] = "terms" === c ? "You must agree to the Official Sweepstakes Rules" : "Please provide a valid " + d : delete this.errors[c], this.updateErrors()
        }, b.prototype.updateErrors = function() {
            var a = this.getFirstError() || "";
            this.$sweepsError.text(a).toggle( !! a)
        }, b.prototype.getFirstError = function() {
            for (var a in this.errors) {
                var b = this.errors[a];
                if (b) return b
            }
            return null
        }, b.prototype.shutdown = function() {
            this.$sweeps.hide()
        }, b.prototype.onTermsClick = function() {
            var b = this.$termsCheckbox.prop("checked");
            b && a.analytics.reportEvent("event5")
        }, b.prototype.onFitNewsClick = function() {
            var b = this.$fitNewsCheckbox.prop("checked");
            b && a.analytics.reportEvent("event3")
        }, b.prototype.onCancel = function() {
            a.analytics.buttonClick(this.pageName, "Cancel"), a.startMenu("levels")
        }, b.prototype.onSubmitClick = function() {
            this.submitting || (a.analytics.buttonClick(this.pageName, "Submit"), this.$form.validate($.proxy(this.onValidationSuccess, this)))
        }, b.prototype.onValidationSuccess = function() {
            var a = this.getFirstError();
            if (!this.submitting && !a) {
                this.submitting = !0, this.$submit.text("Submitting...").addClass("disabled");
                var b = JSON.parse(JSON.stringify(d)),
                    e = b.MIT[0].Input,
                    f = e.Required;
                f.FirstName = $("#firstName").val(), f.LastName = $("#lastName").val(), f.AddressLine1 = $("#address").val(), f.City = $("#city").val(), f.State = $("#state").val(), f.ZipCode = $("#zip").val(), f.Email = $("#email").val();
                var g = $("#phone").val() || "";
                g = g.replace(/[^\d]/g, ""), e.Optional.Phones.Phone[0].PhoneNumber = g;
                var h = this.$fitNewsCheckbox.prop("checked");
                e.Optional.Tracking.CampaignName = h ? "76" : "";
                var i = JSON.stringify(b);
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: c,
                    data: i,
                    success: $.proxy(this.onSubmitSuccess, this),
                    error: $.proxy(this.onSubmitError, this)
                })
            }
        }, b.prototype.onSubmitSuccess = function() {
            this.$sweeps.hide(), new a.controls.SweepsCompletedDialog(this.game), a.sweeps.recordEntry(), a.analytics.reportEvent("event9")
        }, b.prototype.onSubmitError = function(b) {
            var c = b.responseJSON;
            if (!c) try {
                c = JSON.parse(b.responseText)
            } catch (d) {}
            var e = "Oops, we encountered a problem submitting your entry. Please try again.";
            if (c && c.errors) {
                var f = c.errors.error || {};
                $.isArray(f) && f.length > 0 && (f = f[0]);
                var g = parseInt(f["@error_number"], 10) || 0;
                switch (g) {
                case 22124:
                    e = "Please correct your phone number";
                    break;
                case 22126:
                case 1102:
                    e = "Please correct your address.";
                    break;
                case 22115:
                    e = "Please correct your email address."
                }
            }
            this.$sweepsError.text(e).show(), this.resetSubmit(), a.analytics.reportSweepsError(e)
        }, b.prototype.resetSubmit = function() {
            this.submitting = !1, this.$submit.text("Submit").removeClass("disabled")
        };
        var d = {
            MIT: [{
                "@id": "1",
                Input: {
                    Required: {
                        SourceCD: "eshopping",
                        ProductDivisionCD: "A",
                        FirstName: "",
                        LastName: "",
                        AddressLine1: "",
                        City: "",
                        State: "",
                        ZipCode: "",
                        Email: "",
                        ContactMethod: "Any",
                        ContactTime: "Anytime",
                        Provider: "FIT PARKING GAME BLITZ AHM",
                        AddressScrubbing: "true",
                        ValidateOnly: "false",
                        Models: {
                            Model: [{
                                "@id": "1",
                                ModelYear: "2015",
                                ModelMake: "Honda",
                                ModelMarketingName: "Fit"
                            }]
                        },
                        Dealers: {
                            Dealer: [{
                                "@id": "1",
                                DealerID: "000000"
                            }]
                        }
                    },
                    Optional: {
                        IsDealerized: "false",
                        Tracking: {
                            CampaignName: ""
                        },
                        Phones: {
                            Phone: [{
                                "@id": "1",
                                PhoneNumber: "",
                                PhoneType: "Home"
                            }]
                        },
                        TransactionCD: "SS"
                    }
                }
            }]
        };
        a.states.SweepsForm = b
    }(), a.CollisionManager = function(b, c) {
        this.maxCollisions = b || 3, this.count = 0, this.delay = c || 500, $.extend(this, a.EventDispatcher.prototype)
    }, a.COLLISION_MAX = "collisionMax", a.COLLISION = "collision", a.CollisionManager.prototype = {
        constructor: a.CollisionManager,
        reset: function() {
            this.count = 0, this.resetFlag()
        },
        canAcceptCollision: !0,
        addCollision: function(b) {
            this.canAcceptCollision && (this.canAcceptCollision = !1, this.count++, this.dispatchEvent({
                type: a.COLLISION,
                body: b
            }), this.count >= this.maxCollisions ? this.dispatchEvent({
                type: a.COLLISION_MAX,
                body: b
            }) : window.setTimeout(this.resetFlag.bind(this), this.delay))
        },
        resetFlag: function() {
            this.canAcceptCollision = !0
        }
    }, function() {
        window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame, Modernizr.Detectizr.detect({
            addAllFeaturesAsClass: !0,
            detectDevice: !0,
            detectOS: !0,
            detectBrowser: !0,
            detectScreen: !1,
            detectPlugins: !1
        });
        var b = Modernizr.Detectizr.device || {},
            c = "ie" === b.browser && 9 === parseInt(b.browserVersion, 10),
            d = Modernizr.canvas && Modernizr.audio && Modernizr.video && Modernizr.csstransforms && !c;
        return d ? void $(document).ready(function() {
            a.scores.init();
            var b = 1136,
                c = 640,
                d = Phaser.CANVAS;
            a.game = new Phaser.Game(b, c, d, "gameContainer", null, !1, !0), a.game.state.add("boot", new a.states.BootState), a.game.state.add("menu", new a.states.MenuMap), a.game.state.add("sweeps", new a.states.SweepsForm), a.game.state.add("level1", new a.states.Level1), a.game.state.add("level2", new a.states.Level2), a.game.state.add("level3", new a.states.Level3), a.game.state.add("level4", new a.states.Level4), a.game.state.add("level5", new a.states.Level5), a.game.state.add("level6", new a.states.Level6), a.game.state.add("level7", new a.states.Level7), a.game.state.add("level8", new a.states.Level8), a.startMenu = function(b) {
                a.game.state.start("menu", !0, !1, b)
            }, a.game.state.start("boot");
            var e = Modernizr.mobile && window.innerWidth / window.innerHeight < 1;
            e && ($("#portrait").show(), $("#portraitClick").click(function() {
                $("#portrait").fadeOut(200)
            }))
        }) : void $("#upgrade").show()
    }(), function() {
        a.toDEGREES = 180 / Math.PI, a.toRADIANS = Math.PI / 180, a.lerp = function(a, b, c) {
            return a + (b - a) * c
        }, a.clamp = function(a, b, c) {
            return Math.min(Math.max(a, b), c)
        }, a.getDistance = function(a, b) {
            var c = b.x - a.x,
                d = b.y - a.y;
            return Math.sqrt(c * c + d * d)
        }, a.getRandomInRange = function(a, b) {
            return a % 1 === 0 && b % 1 === 0 ? Math.floor(Math.random() * (b - (a - 1)) + a) : Math.random() * (b - a) + a
        }
    }()
}();