// Copyright © 2015 John Watson
// All rights reserved

var _EXAMPLEWIDTH = 848;
var _EXAMPLEHEIGHT = 450;

var _PHASERVERSION = "2.2.2";

var mechanics = null;
var game = null;
var currentExample = null;

function loadExamples() {

    $.get('examples.json')
        .success(function(data) {
            mechanics = data;

            var hash = window.location.hash || '';
            var slug = '';
            var example = 0;
            var total = 0;
            if (hash !== '') {
                var parts = hash.split('-');
                slug = parts[0].substr(1);
                example = Math.floor(parts[1]) - 1;
            }
            
            for(var i = 0; i < mechanics.length; i++) {
                var m = mechanics[i];
                if (m.slug === slug) {
                    currentExample = m.examples[example];
                    
                    loadExample(currentExample);
                    break;
                }
            }
        });

}

function loadExample(example) {




    $('#example-container').empty();
    $('#example-container').append($('<div id="game"></div>'));






    if (example !== undefined) {
        $.getScript('source/' + example.script)
            .success(function(script) {
                // Replace _EXAMPLEWIDTH and _EXAMPLEHEIGHT so the examples can be
                // copied and run with no modifications necessary.
                script = script.replace('_EXAMPLEWIDTH', _EXAMPLEWIDTH);
                script = script.replace('_EXAMPLEHEIGHT', _EXAMPLEHEIGHT);
                script = "// This example uses the Phaser " + _PHASERVERSION + " framework\n\n" + script;

                setTimeout(windowResize, 1000);
                setTimeout(checkRenderer, 2000);
                setTimeout(showFps, 2000);
            })
    }

    return false;
}

function showFps() {
    if (game === undefined || game === null) return;
    
    game.time.advancedTiming = true;
    var fpsText = game.add.text(
        20, 20, '', { font: '16px Arial', fill: '#ffffff' }
    );
    fpsText.setShadow(1, 1, '#4488cc', 0);
    var fpsTimer = game.time.create();
    fpsTimer.loop(3000, function() {
        if (game.time.fps !== 0) {
            fpsText.text = game.time.fps + ' FPS';
        }
    });
    fpsTimer.start();
}

function windowResize() {

    if (game) {
        game.scale.maxWidth = _EXAMPLEWIDTH;
        game.scale.maxHeight = _EXAMPLEHEIGHT;
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.refresh();

        if (game.physics.p2 !== null) {
            // Do some magic on the P2 physics world when the game resizes
            var left = false;
            var right = false;
            var top = false;
            var bottom = false;
            if (game.physics.p2.walls.left) left = true;
            if (game.physics.p2.walls.right) right = true;
            if (game.physics.p2.walls.top) top = true;
            if (game.physics.p2.walls.bottom) bottom = true;
            game.physics.p2.setBoundsToWorld(left, right, top, bottom, false)
        }
    }
}

function checkRenderer() {
    $('#webgl-warning').hide();
    if (game && currentExample && currentExample.webgl && game.renderer instanceof PIXI.CanvasRenderer) {
        $('#webgl-warning').show();
    }
}

$(document).ready(
    function() {
        loadExamples();

        windowResize();

        // Disable scrolling to top when page is loaded on an Android device.
        // The intent of scrollTop in Phaser is to hide the navigation bar.
        Phaser.ScaleManager.prototype.scrollTop = function() {};
    }
);
