window.onload = function() {
	
	var game = new Phaser.Game(480,320,Phaser.CANVAS,"",{preload:onPreload, create:onCreate, update:onUpdate});                

	var playerSpeed = 150;
	var player;
	var platformGroup;

	function onPreload() {
		game.load.image("platform180","platform180.png");
		game.load.image("platform120","platform120.png");
		game.load.image("player","player.png");
		game.load.image("ground","ground.png");
	}

	function onCreate() {
		platformgroup = game.add.group();
		game.physics.startSystem(Phaser.Physics.ARCADE);
		player = game.add.sprite(240, 0, "player");
		player.anchor.setTo(0.5);
		game.physics.enable(player, Phaser.Physics.ARCADE);
		game.physics.arcade.gravity.y = playerSpeed;
		addPlatform(240,60,"platform180");  
		addPlatform(340,140,"platform120");  
		addPlatform(140,140,"platform120");
		addPlatform(420,220,"platform120"); 
		addPlatform(60,220,"platform120");
		addPlatform(100,316,"ground");
		addPlatform(380,316,"ground");
		game.input.onDown.add(changeDir, this);	
	}
	
	function addPlatform(posX,posY,asset){
		platform = game.add.sprite(posX,posY,asset)
		platform.anchor.setTo(0.5);
		game.physics.enable(platform, Phaser.Physics.ARCADE);
		platform.body.allowGravity = false;
		platform.body.immovable = true;
		platformgroup.add(platform);	
	}
	
	function onUpdate() {
		player.body.velocity.y = Math.abs(playerSpeed);
		player.body.velocity.x = 0;
		player.body
		game.physics.arcade.collide(player, platformgroup, movePlayer);
		if(player.y>320){
			player.y = 0
		}
		if(player.x<12){
			player.x=12;
			playerSpeed*=-1
		}
		if(player.x>468){
			player.x=468;
			playerSpeed*=-1
		}

	}
	
	function movePlayer(){     
		player.body.velocity.x = playerSpeed
	}
	
	function changeDir(){
		playerSpeed *= -1;
	}
	
}