// Copyright © 2014 John Watson <john@watson-net.com> All rights reserved
var Asteroid = function(e) {
        Phaser.Sprite.call(this, e, 0, 0, e.cache.getBitmapData("asteroid")), this.FUEL_DRAIN_PER_SECOND = 60, this.FUEL_SIZE_RATIO = .35, this.TRACTOR_GRAVITY = .5, this.initAsteroid()
    };
Asteroid.prototype = Object.create(Phaser.Sprite.prototype), Asteroid.prototype.constructor = Asteroid, Asteroid.prototype.initAsteroid = function() {
    this.x = this.game.rnd.integerInRange(0, G.arena.width), this.y = this.game.rnd.integerInRange(0, G.arena.height), this.fuel = this.game.rnd.integerInRange(20, 60), this.radius = this.fuel * this.FUEL_SIZE_RATIO, this.game.physics.enable(this, Phaser.Physics.ARCADE), this.anchor.setTo(.5, .5), this.body.maxVelocity.setTo(100, 100), this.body.velocity.setTo(this.game.rnd.integerInRange(-25, 25), this.game.rnd.integerInRange(-25, 25)), this.body.angularVelocity = 8 * this.game.rnd.integerInRange(-90, 90) / this.radius, this.alpha = 0, this.game.add.tween(this).to({
        alpha: 1
    }, 750, Phaser.Easing.Sinusoidal.In, !0)
}, Asteroid.prototype.update = function() {
    this.body.x < 0 && (this.body.x = 0, this.body.velocity.x = -this.body.velocity.x, G.highlightBounce(this)), this.body.x > G.arena.width - 2 * this.radius && (this.body.x = G.arena.width - 2 * this.radius, this.body.velocity.x = -this.body.velocity.x, G.highlightBounce(this)), this.body.y < 0 && (this.body.y = 0, this.body.velocity.y = -this.body.velocity.y, G.highlightBounce(this)), this.body.y > G.arena.height - 2 * this.radius && (this.body.y = G.arena.height - 2 * this.radius, this.body.velocity.y = -this.body.velocity.y, G.highlightBounce(this)), this.radius <= 6 && (this.fuel = 0), this.radius = this.fuel * this.FUEL_SIZE_RATIO, this.scale.setTo(this.radius / 32, this.radius / 32)
};
var BootState = function() {};
BootState.prototype.preload = function() {
    this.game.load.image("preloader-icon", "assets/gfx/preloader-icon.png"), this.game.load.image("preloader-bg", "assets/gfx/preloader-bg.png"), this.game.load.image("preloader-fg", "assets/gfx/preloader-fg.png")
}, BootState.prototype.create = function() {
    this.game.device.desktop, this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL, this.game.scale.maxWidth = G.width, this.game.scale.maxHeight = G.height, this.game.scale.pageAlignHorizontally = !0, this.game.scale.pageAlignVertically = !0, this.game.scale.setScreenSize(), this.game.stage.backgroundColor = G.backgroundColor, this.game.state.start("preloader"), this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.LEFT, Phaser.Keyboard.RIGHT, Phaser.Keyboard.UP, Phaser.Keyboard.DOWN, Phaser.Keyboard.SPACEBAR]), G.generateTextures(), G.game.canvas.setAttribute("tabindex", "1"), G.game.canvas.focus()
}, BootState.prototype.update = function() {}, window.onload = function() {
    var e = new Phaser.Game(G.width, G.height, Phaser.AUTO, "game");
    G.game = e, e.state.add("boot", BootState, !0), e.state.add("preloader", PreloadState, !1), e.state.add("menu", MenuState, !1), e.state.add("game", GameState, !1), e.state.add("end", EndState, !1), e.state.add("screenshot", ScreenshotState, !1)
};
var EndState = function() {
        this.name = "end"
    };
EndState.prototype.create = function() {
    G.setupStage(), this.addTitles(), this.addButtons(), G.playMusic(), G.fadeIn(1e3, G.backgroundColor)
}, EndState.prototype.addTitles = function() {
    var e = "GAME OVER\nSCORE " + G.score.toLocaleString() + "\nFUEL/MINUTE " + G.getFuelPerMinute() + "\nLEVEL " + (G.level + 1),
        t = this.game.add.text(400, 150, e, {
            font: "40px jupiter",
            fill: "#ff0000",
            align: "center"
        });
    t.setShadow(3, 3, 0, 0), t.x = this.game.width / 2 - t.getBounds().width / 2
}, EndState.prototype.addButtons = function() {
    var e = this.game.add.text(0, 0, "[SPACE] TO START OVER", {
        font: "40px jupiter",
        fill: "#ffffff",
        align: "center"
    });
    e.setShadow(3, 3, 0, 0), e.x = this.game.width / 2 - e.getBounds().width / 2, e.y = this.game.height - 78, this.game.add.tween(e).to({
        alpha: .2
    }, 1e3, Phaser.Easing.Sinusoidal.In, !0, 0, Number.POSITIVE_INFINITY, !0)
}, EndState.prototype.update = function() {
    this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.game.state.start("game")
};
var Enemy = function(e) {
        Phaser.Sprite.call(this, e, 0, 0, e.cache.getBitmapData("enemy")), this.STATE_IDLE = 1, this.STATE_HOMING = 2, this.MAX_SPEED = 40, this.THRUST = 30, this.DETECTION_DISTANCE = G.arena.width / 8, this.THRUST_INTERVAL = 2e3, this.thrust_elapsed = 0, this.homing_elapsed = 0, this.total_elapsed = 0, this.radius = 5, this.demo = !1, this.alive = !0, this.scannerBitmap = this.game.add.bitmapData(2 * this.DETECTION_DISTANCE, 2 * this.DETECTION_DISTANCE), this.scannerSprite = this.game.add.sprite(0, 0, this.scannerBitmap), this.scannerSprite.anchor.setTo(.5, .5), this.addChild(this.scannerSprite), this.initEnemy()
    };
Enemy.prototype = Object.create(Phaser.Sprite.prototype), Enemy.prototype.constructor = Enemy, Enemy.prototype.initEnemy = function() {
    for (var e = 0; 5 > e; e++) {
        this.x = this.game.rnd.integerInRange(50, G.arena.width - 50), this.y = this.game.rnd.integerInRange(50, G.arena.height - 50);
        var t = this.game.math.distance(this.x, this.y, G.player.x, G.player.y);
        if (t > this.DETECTION_DISTANCE) break
    }
    this.rotationOffset = this.game.rnd.realInRange(0, 2 * Math.PI), this.game.physics.enable(this, Phaser.Physics.ARCADE), this.body.maxVelocity.setTo(this.MAX_SPEED, this.MAX_SPEED), this.body.drag.setTo(this.THRUST, this.THRUST), this.anchor.setTo(.5, .5), this.state = this.STATE_IDLE, this.alpha = 0, this.game.add.tween(this).to({
        alpha: 1
    }, 750, Phaser.Easing.Sinusoidal.In, !0)
}, Enemy.prototype.update = function() {
    if (this.updateScannerSprite(), this.thrust_elapsed += this.game.time.elapsed, this.homing_elapsed += this.game.time.elapsed, this.total_elapsed += this.game.time.elapsed, this.rotation = Math.sin(this.total_elapsed / 1e3) * Math.PI + this.rotationOffset, this.demo !== !0) {
        var e = this.game.math.distance(this.x, this.y, G.player.x, G.player.y);
        if (G.player.alive && e <= this.DETECTION_DISTANCE) {
            this.homing_elapsed > 800 * (e / this.DETECTION_DISTANCE) + 50 && (G.sfx.homing.isPlaying && G.sfx.homing.stop(), G.sfx.homing.play(), this.homing_elapsed = 0), this.state = this.STATE_HOMING;
            var t = G.player.x - this.x,
                a = G.player.y - this.y,
                i = Math.max(Math.abs(t), Math.abs(a));
            this.body.acceleration.x = t / i * this.THRUST * this.DETECTION_DISTANCE / e, this.body.acceleration.y = a / i * this.THRUST * this.DETECTION_DISTANCE / e
        } else this.state == this.STATE_HOMING && (G.sfx.scanning.isPlaying || G.sfx.scanning.play()), this.state = this.STATE_IDLE, this.game.math.chanceRoll(6e3) && (G.sfx.scanning.isPlaying || G.sfx.scanning.play()), this.thrust_elapsed > this.THRUST_INTERVAL && (this.body.acceleration.x = this.game.rnd.integerInRange(-this.THRUST / 5, this.THRUST / 5), this.body.acceleration.y = this.game.rnd.integerInRange(-this.THRUST / 5, this.THRUST / 5), this.thrust_elapsed = 0);
        this.body.x < 0 && (this.body.x = 0, this.body.velocity.x = -this.body.velocity.x), this.body.x > G.arena.width - 2 * this.radius && (this.body.x = G.arena.width - 2 * this.radius, this.body.velocity.x = -this.body.velocity.x), this.body.y < 0 && (this.body.y = 0, this.body.velocity.y = -this.body.velocity.y), this.body.y > G.arena.height - 2 * this.radius && (this.body.y = G.arena.height - 2 * this.radius, this.body.velocity.y = -this.body.velocity.y)
    }
}, Enemy.prototype.updateScannerSprite = function() {
    var e = this.scannerBitmap.context;
    e.save(), e.clearRect(0, 0, 2 * this.DETECTION_DISTANCE, 2 * this.DETECTION_DISTANCE);
    var t = this.scannerBitmap.width / 2,
        a = this.scannerBitmap.height / 2;
    if (e.translate(t, a), this.state != this.STATE_IDLE) {
        e.strokeStyle = "rgba(255, 0, 0, " + this.game.rnd.realInRange(.5, 1) + ")", e.beginPath(), e.arc(0, 0, this.DETECTION_DISTANCE, 0, 2 * Math.PI), e.stroke();
        var i, s, o, r, h = Math.sin(this.total_elapsed / 1e3),
            n = "1 1 5";
        for (e.save(), e.fillStyle = "rgba(255, 0, 0, " + this.game.rnd.realInRange(.2, .7) + ")", i = 0; 1e3 > i; i++) s = this.game.rnd.realInRange(0, 2 * Math.PI), r = this.game.rnd.integerInRange(.75 * this.DETECTION_DISTANCE, this.DETECTION_DISTANCE), e.rotate(s), e.fillRect(r, 0, 1, 1);
        if (e.restore(), e.font = '18px "8thcargo"', e.fillStyle = "rgba(255, 0, 0, " + this.game.rnd.realInRange(.2, .7) + ")", h >= 0) for (i = 0; 40 > i; i++) s = this.game.rnd.realInRange(0, 2 * Math.PI), o = n.substr(this.game.rnd.integerInRange(0, n.length - 1), 1), r = 40 + 10 * this.game.rnd.integerInRange(0, 2), e.rotate(s), e.fillText(o, r, 0);
        else for (s = 2 * Math.PI / 10, i = 0; 10 > i; i++) e.rotate(s), e.fillText(n, 40, 0)
    }
    e.strokeStyle = "rgba(255, 0, 0, 0.3)", e.beginPath(), e.arc(0, 0, this.DETECTION_DISTANCE * Math.abs(Math.sin(this.total_elapsed / 1e3 * 3)), 0, 2 * Math.PI), e.stroke(), e.beginPath(), e.arc(0, 0, this.DETECTION_DISTANCE * Math.abs(Math.sin(this.total_elapsed / 1e3 * 18)), 0, 2 * Math.PI), e.stroke(), e.beginPath(), e.arc(0, 0, this.DETECTION_DISTANCE * Math.abs(Math.sin(this.total_elapsed / 1e3 * 32)), 0, 2 * Math.PI), e.stroke(), e.restore(), this.scannerBitmap.dirty = !0
}, ExhaustParticle = function(e, t, a) {
    Phaser.Particle.call(this, e, t, a, e.cache.getBitmapData("exhaust"))
}, ExhaustParticle.prototype = Object.create(Phaser.Particle.prototype), ExhaustParticle.prototype.constructor = ExhaustParticle, ExhaustParticle.prototype.onEmit = function() {
    var e = Math.max(Math.abs(G.player.body.acceleration.x), Math.abs(G.player.body.acceleration.y));
    this.x = G.player.x - G.player.radius * G.player.body.acceleration.x / e, this.y = G.player.y - G.player.radius * G.player.body.acceleration.y / e, this.body.velocity.x = 1.5 * -G.player.body.acceleration.x + G.player.body.velocity.x + this.game.rnd.integerInRange(-G.player.THRUST / 10, G.player.THRUST / 10), this.body.velocity.y = 1.5 * -G.player.body.acceleration.y + G.player.body.velocity.y + this.game.rnd.integerInRange(-G.player.THRUST / 10, G.player.THRUST / 10)
}, ExplosionParticle = function(e, t, a) {
    Phaser.Particle.call(this, e, t, a, e.cache.getBitmapData("debris"))
}, ExplosionParticle.prototype = Object.create(Phaser.Particle.prototype), ExplosionParticle.prototype.constructor = ExplosionParticle, ExplosionParticle.prototype.onEmit = function() {
    this.body.velocity.x += G.player.body.velocity.x, this.body.velocity.y += G.player.body.velocity.y
};
var G = {
    game: null,
    width: 800,
    height: 600,
    arena: {
        width: 800,
        height: 500
    },
    sfx: {},
    message: null,
    tutorial: {},
    level: 0,
    score: 0,
    fuel: {
        level: 0,
        total: 0,
        goal: 0,
        graph: []
    },
    underAttack: !1,
    gameTime: 0,
    levels: [{
        enemies: 0,
        fuel: 100
    }, {
        enemies: 1,
        fuel: 200
    }, {
        enemies: 2,
        fuel: 250
    }, {
        enemies: 3,
        fuel: 300
    }, {
        enemies: 4,
        fuel: 350
    }, {
        enemies: 5,
        fuel: 350
    }, {
        enemies: 6,
        fuel: 350
    }, {
        enemies: 7,
        fuel: 350
    }, {
        enemies: 8,
        fuel: 350
    }, {
        enemies: 9,
        fuel: 350
    }, {
        enemies: 10,
        fuel: 500
    }, {
        enemies: 11,
        fuel: 500
    }, {
        enemies: 12,
        fuel: 500
    }, {
        enemies: 13,
        fuel: 500
    }, {
        enemies: 14,
        fuel: 500
    }, {
        enemies: 15,
        fuel: 500
    }]
};
G.setupStage = function() {
    G.game.stage.backgroundColor = 0;
    var e = new Starfield(this.game);
    e.alpha = .75, G.game.world.setBounds(-100, -100, this.game.width + 200, this.game.height + 200)
}, G.addRectangle = function(e) {
    var t = G.game.add.graphics(0, 0);
    return t.beginFill(e, 1), t.drawRect(0, 0, G.game.width, G.game.height), t.endFill(), t
}, G.fadeIn = function(e, t, a) {
    void 0 === a && (a = 0), void 0 === t && (t = 0), void 0 === e && (e = 500);
    var i = G.addRectangle(t);
    i.alpha = 1, G.game.add.tween(i).to({
        alpha: 0
    }, e, Phaser.Easing.Quadratic.In, !0, a)
}, G.fadeOut = function(e, t, a) {
    void 0 === a && (a = 0), void 0 === t && (t = 0), void 0 === e && (e = 500);
    var i = G.addRectangle(t);
    i.alpha = 0, G.game.add.tween(i).to({
        alpha: 1
    }, e, Phaser.Easing.Quadratic.In, !0, a)
}, G.showTutorial = function(e, t) {
    void 0 === G.tutorial[e] && 0 === G.message.getQueueLength() && (G.tutorial[e] = !0, G.message.add(t))
}, G.shake = function(e, t) {
    var a = 1e3 / 60,
        i = Math.ceil(t / a),
        s = function() {
            G.game.add.tween(G.game.camera).to({
                x: G.game.rnd.integerInRange(-e, e),
                y: G.game.rnd.integerInRange(-e, e)
            }, a, Phaser.Easing.Sinusoidal.InOut, !0), e = .9 * e
        },
        o = function() {
            G.game.camera.x = 0, G.game.camera.y = 0
        };
    G.game.time.events.repeat(a, i, s), G.game.time.events.add(a * i + 100, o)
}, G.drawPolygon = function(e, t, a, i, s) {
    if (!(3 > s)) {
        var o = 2 * Math.PI / s;
        e.beginPath(), e.moveTo(t + i, a);
        for (var r = 1; s > r; r++) e.lineTo(t + i * Math.cos(o * r), a + i * Math.sin(o * r));
        e.closePath()
    }
}, G.generateTextures = function() {
    var e, t = this.game.add.bitmapData(24, 24, "ship", !0);
    e = t.context, e.clearRect(0, 0, t.width, t.height), e.strokeStyle = "rgb(255, 255, 255)", e.lineWidth = 2, G.drawPolygon(e, t.width / 2, t.height / 2, 10, 8), e.stroke();
    var a = this.game.add.bitmapData(64, 64, "asteroid", !0);
    e = a.context, e.strokeStyle = "rgb(255, 255, 255)", e.fillStyle = "rgba(255, 255, 255, 0.3)", e.lineWidth = 4, G.drawPolygon(e, a.width / 2, a.height / 2, 30, 5), e.stroke();
    var i = this.game.add.bitmapData(12, 12, "enemy", !0);
    e = i.context, e.strokeStyle = "rgb(255, 0, 0)", e.fillStyle = "rgba(255, 0, 0, 0.3)", e.lineWidth = 2, G.drawPolygon(e, i.width / 2, i.height / 2, 5, 3), e.stroke(), e.fill();
    var s = this.game.add.bitmapData(48, 2, "highlight", !0);
    e = s.context, e.strokeStyle = "rgb(100, 200, 255)", e.lineWidth = 2, e.moveTo(0, 0), e.lineTo(48, 0), e.stroke();
    var o = this.game.add.bitmapData(2, 2, "debris", !0);
    e = o.context, e.fillStyle = "rgb(255, 255, 255)", e.fillRect(0, 0, 2, 2);
    var r = this.game.add.bitmapData(10, 10, "exhaust", !0);
    e = r.context, e.strokeStyle = "rgb(255, 255, 255)", e.lineWidth = 2, e.arc(5, 5, 4, 0, 2 * Math.PI), e.stroke();
    var h = this.game.add.bitmapData(500, 1, "preloader-fg", !0);
    e = h.context, e.fillStyle = "rgba(255, 0, 0, 1.0)", e.fillRect(0, 0, 500, 1);
    var n = this.game.add.bitmapData(500, 1, "preloader-bg", !0);
    e = n.context, e.fillStyle = "rgba(255, 0, 0, 0.25)", e.fillRect(0, 0, 500, 1)
}, G.highlightBounce = function(e) {
    void 0 === G.wallBounceHighlights && (G.wallBounceHighlights = G.game.add.group());
    var t = 0,
        a = e.x,
        i = e.y;
    20 > a && (a = 1, t = 90), a > G.arena.width - 20 && (a = G.arena.width - 1, t = 90), 20 > i && (i = 1, t = 0), i > G.arena.height - 20 && (i = G.arena.height - 1, t = 0);
    var s = G.wallBounceHighlights.getFirstDead();
    null === s && (s = G.game.add.sprite(a, i, G.game.cache.getBitmapData("highlight")), s.anchor.setTo(.5, .5), G.wallBounceHighlights.add(s)), s.reset(a, i), s.revive(), s.scale.setTo(1, 1), s.alpha = 1, s.angle = t;
    var o = G.game.add.tween(s);
    o.onComplete.add(function() {
        this.kill()
    }, s), o.to({
        alpha: 0
    }, 1e3, Phaser.Easing.Sinusoidal.Out, !0), G.game.add.tween(s.scale).to({
        x: 0
    }, 1e3, Phaser.Easing.Cubic.Out, !0)
}, G.playMusic = function() {
    G.game.time.events.add(3e3, G.playMusic), void 0 !== G.sfx.music && G.sfx.music.isPlaying || (G.sfx.music = G.sfx["song" + G.game.rnd.integerInRange(1, 3)], G.sfx.music.play())
}, G.addPoints = function(e) {
    if (G.fuel.total += e, G.fuel.level += e, e > 0) {
        var t = 100;
        G.bonusx10.on = !1, G.underAttack && (t = 10 * t, G.bonusx10.x = G.player.x, G.bonusx10.y = G.player.y - G.player.radius, G.bonusx10.bonusText = "x10", G.bonusx10.tint = 255 * G.game.rnd.integerInRange(0, 100) * 255 + 255 + G.game.rnd.integerInRange(0, 100), G.bonusx10.on = !0), G.score += e * (G.level + 1) * t
    }
    var a = Math.floor(G.gameTime / 1e3);
    void 0 === G.fuel.graph[a] ? G.fuel.graph[a] = e : G.fuel.graph[a] += e
}, G.addBonus = function(e) {
    e *= G.level + 1, G.underAttack && (e = 10 * e), G.score += e, G.bonus1k.x = G.player.x, G.bonus1k.y = G.player.y - G.player.radius - 10, G.bonus1k.bonusText = "+" + e, G.bonus1k.tint = 16711680, G.bonus1k.emitParticle()
}, G.nextLevel = function() {
    G.level++, G.fuel.level = 0, G.fuel.goal = void 0 !== G.levels[G.level] ? G.levels[G.level].fuel : 1e3, G.enemyGroup.add(new Enemy(G.game)), G.sfx.levelup.play(), G.message.add("LEVEL " + (G.level + 1))
}, G.getFuelPerMinute = function() {
    var e = G.gameTime / 1e3 / 60,
        t = this.fuel.total / e;
    return 0 === e && (t = 0), t = Math.floor(t), t > 999 && (t = 999), t
}, G.drawStatic = function(e, t, a, i, s) {
    e.save(), e.fillStyle = "rgba(255, 255, 255, " + G.game.rnd.realInRange(.1, .2) + ")";
    for (var o = 0; i * s / 20 > o; o++) e.fillRect(t + G.game.rnd.integerInRange(0, i), a + G.game.rnd.integerInRange(0, s), 2, 1);
    e.restore()
}, G.drawGauge = function(e, t, a, i, s, o) {
    e.save();
    var r = o / s * 2 * Math.PI * .75 + .75 * Math.PI,
        h = Math.cos(r) * i,
        n = Math.sin(r) * i;
    e.strokeStyle = "rgba(255, 255, 255, " + G.game.rnd.realInRange(.4, .6) + ")", e.lineWidth = 2, e.beginPath(), e.moveTo(t, a), e.lineTo(t + h, a + n), e.stroke(), e.restore()
};
var GameState = function() {
        this.name = "game"
    };
GameState.prototype.create = function() {
    G.setupStage(), G.player = new Player(this.game, 400, 250), G.player.revive(), this.game.add.existing(G.player), this.asteroidGroup = this.game.add.group();
    for (var e = 0; 20 > e; e++) this.createAsteroid();
    G.enemyGroup = this.game.add.group(), this.tractorBitmap = this.game.add.bitmapData(G.arena.width, G.arena.height), this.tractorBeams = this.game.add.image(0, 0, this.tractorBitmap), this.explosion = this.game.add.emitter(0, 0, 16), this.explosion.gravity = 0, this.explosion.particleClass = ExplosionParticle, this.explosion.makeParticles(), this.explosion.setScale(1, 6, 1, 1), this.explosion.setXSpeed(-75, 75), this.explosion.setYSpeed(-75, 75), this.explosion.setRotation(-1e3, 1e3), this.explosion.setAlpha(1, 0, 2e3, Phaser.Easing.Cubic.In), G.bonus1k = this.game.add.emitter(0, 0, 10), G.bonus1k.gravity = 0, G.bonus1k.particleClass = TextParticle, G.bonus1k.makeParticles(), G.bonus1k.setXSpeed(0, 0), G.bonus1k.setYSpeed(-30, -30), G.bonus1k.setRotation(0, 0), G.bonus1k.setAlpha(1, 0, 2e3, Phaser.Easing.Cubic.In), G.bonusx10 = this.game.add.emitter(0, 0, 100), G.bonusx10.gravity = 0, G.bonusx10.particleClass = TextParticle, G.bonusx10.makeParticles(), G.bonusx10.setXSpeed(0, 0), G.bonusx10.setYSpeed(-100, -100), G.bonusx10.setRotation(0, 0), G.bonusx10.setAlpha(1, 0, 1500, Phaser.Easing.Cubic.In), G.bonusx10.start(!1, 1500, 160, 1), G.bonusx10.on = !1, this.selfDestructTimer = this.game.add.text(0, 0, "", {
        font: '16px "jupiter"',
        fill: "#ff0000"
    }), this.selfDestructTimer.visible = !1, this.hud = this.game.add.group(), this.hud.bg = this.game.add.image(0, G.arena.height, "hud-background", 0, this.hud), this.hud.levelText = this.game.add.text(418, G.arena.height + 80, "", {
        font: '16px "jupiter"',
        fill: "#64c8ff"
    }, this.hud), this.hud.randomText = this.game.add.text(352, G.arena.height + 80, "", {
        font: '16px "jupiter"',
        fill: "#64c8ff"
    }, this.hud), this.hud.scoreText = this.game.add.text(535, G.arena.height + 46, "", {
        font: '30px "jupiter"',
        fill: "#64c8ff"
    }, this.hud), this.hud.fpmText = this.game.add.text(223, G.arena.height + 80, "", {
        font: '16px "jupiter"',
        fill: "#64c8ff"
    }, this.hud), this.hud.fpsText = this.game.add.text(292, G.arena.height + 45, "", {
        font: '16px "jupiter"',
        fill: "#64c8ff"
    }, this.hud), this.hud.scannerEnemy = new Enemy(this.game), this.hud.add(this.hud.scannerEnemy), this.hud.overlayBitmap = this.game.add.bitmapData(G.arena.width, 100), this.hud.overlaySprite = this.game.add.sprite(0, G.arena.height, this.hud.overlayBitmap, 0, this.hud), this.resetGame(), this.game.time.advancedTiming = !0
}, GameState.prototype.resetGame = function() {
    G.playMusic(), G.level = 0, G.score = 0, G.fuel.level = 0, G.fuel.total = 0, G.fuel.goal = G.levels[G.level].fuel, G.fuel.graph = [], G.underAttack = !1, G.gameTime = 0, delete G.wallBounceHighlights, G.message = new Message(this.game, null), G.message.add("Welcome, space miner!"), G.message.add("Use W,A,S,D or ARROWS to move"), G.message.add("Watch your fuel"), G.message.add("Avoid mines"), G.message.add("Collect fuel from asteroids"), G.message.add("Collect EXTRA fuel to advance"), G.enemyGroup.callAll("kill"), this.self_destruct = 0
}, GameState.prototype.update = function() {
    this.collectFuel(), G.gameTime += this.game.time.elapsed, G.message.update(), G.underAttack = !1, G.enemyGroup.forEachAlive(function(e) {
        if (!G.underAttack) {
            var t = this.game.math.distance(e.x, e.y, G.player.x, G.player.y);
            t <= e.DETECTION_DISTANCE && (G.underAttack = !0)
        }
    }, this), this.hud.levelText.alpha = this.game.rnd.realInRange(.4, .6), this.hud.levelText.setText("LVL" + (G.level + 1)), this.hud.randomText.alpha = this.game.rnd.realInRange(.4, .6), this.hud.randomText.setText(this.game.rnd.integerInRange(1e3, 9999) / 10 + ""), this.hud.scoreText.alpha = this.game.rnd.realInRange(.4, .6), this.hud.scoreText.setText(G.score.toLocaleString()), this.hud.scoreText.x = 610 - this.hud.scoreText.getBounds().width / 2, this.hud.fpmText.alpha = this.game.rnd.realInRange(.4, .6), this.hud.fpmText.setText("FPM " + G.getFuelPerMinute()), this.hud.fpsText.alpha = this.game.rnd.realInRange(.4, .6), this.hud.fpsText.setText(this.game.time.fps + ""), this.hud.overlayBitmap.context.clearRect(0, 0, G.arena.width, 100), this.hud.overlayBitmap.dirty = !0;
    for (var e = 51, t = 80, a = 0, i = Math.floor(G.gameTime / 1e3), s = i - e; i > s; s++) void 0 !== G.fuel.graph[s] && (a = Math.max(G.fuel.graph[s], a));
    0 === a && (a = 1);
    var o = 3;
    this.hud.overlayBitmap.context.fillStyle = "rgba(100, 200, 255, " + this.game.rnd.realInRange(.4, .6) + ")";
    for (var r = 0; e > r; r++) s = i - r, void 0 !== G.fuel.graph[s] ? this.hud.overlayBitmap.context.fillRect(15 + r * (o + 1), 9, o, G.fuel.graph[s] / a * t + 4) : this.hud.overlayBitmap.context.fillRect(15 + r * (o + 1), 9, o, 1);
    G.drawStatic(this.hud.overlayBitmap.context, 7, 8, 215, 86), G.drawStatic(this.hud.overlayBitmap.context, 528, 40, 164, 48);
    var h = 65,
        n = G.fuel.level / G.levels[G.level].fuel * h;
    this.hud.overlayBitmap.context.fillStyle = "rgba(100, 255, 200, " + this.game.rnd.realInRange(.4, .6) + ")", this.hud.overlayBitmap.context.fillRect(226, 10 + h - n, 40, n), G.drawGauge(this.hud.overlayBitmap.context, 298, 23, 10, G.player.body.maxVelocity.x, Math.abs(G.player.body.velocity.x)), G.drawGauge(this.hud.overlayBitmap.context, 501, 23, 10, G.player.body.maxVelocity.y, Math.abs(G.player.body.velocity.y)), G.drawGauge(this.hud.overlayBitmap.context, 310, 76, 10, G.player.THRUST, Math.abs(G.player.body.acceleration.x)), G.drawGauge(this.hud.overlayBitmap.context, 492, 76, 10, G.player.THRUST, Math.abs(G.player.body.acceleration.y)), this.hud.scannerEnemy.revive(), this.hud.scannerEnemy.x = G.arena.width / 2, this.hud.scannerEnemy.y = G.arena.height + 45, this.hud.scannerEnemy.state = this.hud.scannerEnemy.STATE_HOMING, this.hud.scannerEnemy.demo = !0, this.hud.scannerEnemy.scale.setTo(.6, .6), this.hud.scannerEnemy.visible = G.underAttack ? !0 : !1, G.underAttack ? (G.sfx.detected.isPlaying || G.sfx.detected.play(), this.hud.setAll("tint", 16711680)) : (G.sfx.detected.isPlaying && G.sfx.detected.stop(), this.hud.setAll("tint", 16777215)), G.player.alive && this.game.input.keyboard.isDown(Phaser.Keyboard.ZERO) && this.selfDestruct(), G.fuel.level > G.fuel.goal && G.nextLevel(), this.self_destruct > 0 && (G.sfx.selfdestruct.isPlaying || G.sfx.selfdestruct.play(), this.self_destruct = this.self_destruct - this.game.time.elapsed, this.selfDestructTimer.setText(this.self_destruct / 1e3 + ""), this.selfDestructTimer.visible = !0, this.selfDestructTimer.x = G.player.x + G.player.radius + 5, this.selfDestructTimer.y = G.player.y - 8, this.self_destruct <= 0 && (this.self_destruct = 0, this.selfDestructTimer.visible = !1, this.explode()))
}, GameState.prototype.collectFuel = function() {
    if (G.player.alive) {
        var e = this.tractorBitmap.context;
        e.clearRect(0, 0, this.tractorBitmap.width, this.tractorBitmap.height), e.lineWidth = 2, this.asteroidGroup.forEachAlive(function(t) {
            if (this.game.math.distance(G.player.x, G.player.y, t.x, t.y) < 4 * G.player.radius) {
                G.sfx.tractorbeam.isPlaying || G.sfx.tractorbeam.play(), e.beginPath();
                var a = this.game.rnd.realInRange(0, 1),
                    i = this.game.rnd.integerInRange(0, 255);
                e.strokeStyle = "rgba(" + i + ", 255, " + i + ", " + a + ")", e.moveTo(t.x, t.y), e.lineTo(G.player.x, G.player.y), e.stroke();
                var s = t.x - G.player.x,
                    o = t.y - G.player.y,
                    r = Math.max(Math.abs(s), Math.abs(o));
                G.player.body.velocity.x += s / r * t.TRACTOR_GRAVITY, G.player.body.velocity.y += o / r * t.TRACTOR_GRAVITY, G.player.addFuel(t.FUEL_DRAIN_PER_SECOND * this.game.time.elapsed / 1e3), t.fuel -= t.FUEL_DRAIN_PER_SECOND * this.game.time.elapsed / 1e3, t.fuel <= 0 && (G.sfx.tractorbeam.isPlaying && G.sfx.tractorbeam.stop(), G.sfx.ding.play(), G.addBonus(1e3), t.kill(), this.createAsteroid())
            }
        }, this), G.enemyGroup.forEachAlive(function(e) {
            var t = this.game.math.distance(e.x, e.y, G.player.x, G.player.y);
            t <= e.radius + G.player.radius && (e.kill(), this.explode())
        }, this), this.tractorBitmap.dirty = !0
    }
}, GameState.prototype.createAsteroid = function() {
    var e = this.asteroidGroup.getFirstDead();
    return (void 0 === e || null === e) && (e = new Asteroid(this.game), this.asteroidGroup.add(e)), e.initAsteroid(), e.revive(), e
}, GameState.prototype.selfDestruct = function() {
    this.self_destruct = 3e3
}, GameState.prototype.explode = function() {
    this.explosion.x = G.player.x, this.explosion.y = G.player.y, this.explosion.start(!0, 2e3, 0, 100), G.sfx.explosion.play(), G.player.kill(), G.fadeOut(3e3, G.backgroundColor), this.game.time.events.add(3e3, function() {
        this.game.state.start("end")
    }, this)
}, Phaser.Filter.Glow = function(e) {
    Phaser.Filter.call(this, e), this.fragmentSrc = ["precision lowp float;", "varying vec2 vTextureCoord;", "varying vec4 vColor;", "uniform sampler2D uSampler;", "void main() {", "vec4 sum = vec4(0);", "vec2 texcoord = vTextureCoord;", "for(int xx = -4; xx <= 4; xx++) {", "for(int yy = -3; yy <= 3; yy++) {", "float dist = sqrt(float(xx*xx) + float(yy*yy));", "float factor = 0.0;", "if (dist == 0.0) {", "factor = 3.0;", "} else {", "factor = 3.0/abs(float(dist));", "}", "sum += texture2D(uSampler, texcoord + vec2(xx, yy) * 0.002) * factor;", "}", "}", "gl_FragColor = sum * 0.025 + texture2D(uSampler, texcoord);", "}"]
}, Phaser.Filter.Glow.prototype = Object.create(Phaser.Filter.prototype), Phaser.Filter.Glow.prototype.constructor = Phaser.Filter.Glow;
var MenuState = function() {
        this.name = "menu"
    };
MenuState.prototype.create = function() {
    G.setupStage(), this.asteroidGroup = this.game.add.group();
    for (var e = 0; 20 > e; e++) asteroid = new Asteroid(this.game), this.asteroidGroup.add(asteroid), asteroid.initAsteroid(), asteroid.revive();
    G.player = {
        x: -1e3,
        y: -1e3
    }, G.enemyGroup = this.game.add.group(), G.enemyGroup.add(new Enemy(G.game)), G.enemyGroup.add(new Enemy(G.game)), G.enemyGroup.add(new Enemy(G.game));
    var t = new Enemy(G.game);
    G.enemyGroup.add(t), t.demo = !0, t.x = G.arena.width - 150, t.y = G.arena.height - 100, t.state = t.STATE_HOMING, t.scale.setTo(8, 8), t.smoothed = !1, this.game.add.image(0, this.game.height - 100, "hud-background").tint = 16711680, this.game.add.image(0, 0, "menu-background"), this.addTitles(), this.addButtons(), G.playMusic(), G.fadeIn(1e3, G.backgroundColor)
}, MenuState.prototype.addTitles = function() {
    var e = '"Eins Eins Funf (115)"\n\nProgramming, art, music, sound,\nand design by John Watson\nflagrantdisregard.com\n\nSource @ \ngithub.com/jotson/115\n\n(c) 2014 John Watson\nLicensed under the MIT license',
        t = this.game.add.text(400, 40, e, {
            font: "30px jupiter",
            fill: "#64c8ff",
            align: "right"
        });
    t.setShadow(3, 3, 0, 0)
}, MenuState.prototype.addButtons = function() {
    var e = this.game.add.text(0, 0, "[SPACE] TO START", {
        font: "40px jupiter",
        fill: "#ffffff",
        align: "center"
    });
    e.setShadow(3, 3, 0, 0), e.x = this.game.width / 2 - e.getBounds().width / 2, e.y = this.game.height - 78, this.game.add.tween(e).to({
        alpha: .2
    }, 1e3, Phaser.Easing.Sinusoidal.In, !0, 0, Number.POSITIVE_INFINITY, !0)
}, MenuState.prototype.update = function() {
    this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.game.state.start("game")
};
var Message = function(e, t) {
        this.game = e, this.parent = t, this.messages = [], this.messageTimer = 0
    };
Message.prototype.add = function(e) {
    this.messages.push(e)
}, Message.prototype.update = function() {
    if (this.messageTimer -= G.game.time.elapsed, 0 !== this.messages.length && this.messageTimer <= 0) {
        var e = this.messages.shift(),
            t = 80 * e.length;
        1e3 > t && (t = 1500);
        var a = this.game.add.text(10, -250, e, {
            font: '40px "jupiter"',
            fill: "#ff0000"
        });
        a.fixedToCamera = !0, a.updateTransform(), a.cameraOffset.x = this.game.width / 2 - a.getBounds().width / 2, this.game.add.tween(a).to({
            alpha: .3
        }, 250, Phaser.Easing.Cubic.In, !0, t + 350);
        var i = this.game.add.tween(a.cameraOffset).to({
            y: 10
        }, 250, Phaser.Easing.Elastic.Out, !1, 100).to({
            y: this.game.camera.height
        }, 250, Phaser.Easing.Cubic.In, !1, t);
        i._lastChild.onComplete.add(function() {
            this.destroy()
        }, a), i.start(), this.messageTimer = t + 300
    }
}, Message.prototype.getQueueLength = function() {
    return this.messages.length
};
var Player = function(e, t, a) {
        Phaser.Sprite.call(this, e, t, a, e.cache.getBitmapData("ship")), this.THRUST = 60, this.STARTING_FUEL = 100, this.MAX_FUEL = 100, this.FUEL_BURN_PER_SECOND = 25, this.SEGMENTS = 8, this.radius = 12, this.fuel = this.STARTING_FUEL, this.fuelBitmap = this.game.add.bitmapData(2 * this.radius, 2 * this.radius), this.fuelSprite = this.game.add.image(0, 0, this.fuelBitmap), this.fuelSprite.anchor.setTo(.5, .5), this.addChild(this.fuelSprite), this.updateTexture(), this.game.physics.enable(this, Phaser.Physics.ARCADE), this.anchor.setTo(.5, .5), this.body.maxVelocity.setTo(100, 100), this.body.drag.setTo(this.THRUST / 10, this.THRUST / 10), this.exhaust = this.game.add.emitter(0, 0, 10), this.exhaust.gravity = 0, this.exhaust.particleClass = ExhaustParticle, this.exhaust.makeParticles(), this.exhaust.setScale(.5, 1, .5, 1, 500), this.exhaust.setAlpha(1, 0, 500, Phaser.Easing.Cubic.In), this.exhaust.start(!1, 500, 100, 1), this.exhaust.on = !1
    };
Player.prototype = Object.create(Phaser.Sprite.prototype), Player.prototype.constructor = Player, Player.prototype.update = function() {
    this.body.acceleration.setTo(0, 0), G.player.alive && this.fuel > 0 && ((this.game.input.keyboard.isDown(Phaser.Keyboard.A) || this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) && (this.body.acceleration.x = -this.THRUST), (this.game.input.keyboard.isDown(Phaser.Keyboard.D) || this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) && (this.body.acceleration.x = this.THRUST), (this.game.input.keyboard.isDown(Phaser.Keyboard.W) || this.game.input.keyboard.isDown(Phaser.Keyboard.UP)) && (this.body.acceleration.y = -this.THRUST), (this.game.input.keyboard.isDown(Phaser.Keyboard.S) || this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN)) && (this.body.acceleration.y = this.THRUST)), 0 !== this.body.acceleration.x || 0 !== this.body.acceleration.y ? (this.addFuel(-this.FUEL_BURN_PER_SECOND * this.game.time.elapsed / 1e3), this.fuel < 0 && (this.fuel = 0), G.sfx.thrust.isPlaying || G.sfx.thrust.play(), this.exhaust.on = !0, this.exhaust.x = this.x, this.exhaust.y = this.y, 0 !== this.body.acceleration.x && this.body.acceleration.x / Math.abs(this.body.acceleration.x) != this.body.velocity.x / Math.abs(this.body.velocity.x) && G.shake(.01 * this.body.speed, 10), 0 !== this.body.acceleration.y && this.body.acceleration.y / Math.abs(this.body.acceleration.y) != this.body.velocity.y / Math.abs(this.body.velocity.y) && G.shake(.01 * this.body.speed, 10)) : (G.sfx.thrust.isPlaying && G.sfx.thrust.stop(), this.exhaust.on = !1), this.body.x < 0 && (this.body.x = 0, this.body.velocity.x = -this.body.velocity.x, G.sfx.bounce.play(), G.highlightBounce(this), G.shake(1, 250)), this.body.x > G.arena.width - 2 * this.radius && (this.body.x = G.arena.width - 2 * this.radius, this.body.velocity.x = -this.body.velocity.x, G.sfx.bounce.play(), G.highlightBounce(this), G.shake(1, 250)), this.body.y < 0 && (this.body.y = 0, this.body.velocity.y = -this.body.velocity.y, G.sfx.bounce.play(), G.highlightBounce(this), G.shake(1, 250)), this.body.y > G.arena.height - 2 * this.radius && (this.body.y = G.arena.height - 2 * this.radius, this.body.velocity.y = -this.body.velocity.y, G.sfx.bounce.play(), G.highlightBounce(this), G.shake(1, 250))
}, Player.prototype.addFuel = function(e) {
    e > 0 && this.fuel >= this.MAX_FUEL ? (G.addPoints(e), G.sfx.beep.isPlaying || G.sfx.beep.play()) : this.fuel = this.fuel + e, this.fuel <= 0 && (this.fuel = 0, G.sfx.outoffuel.isPlaying || G.sfx.outoffuel.play()), this.updateTexture()
}, Player.prototype.updateTexture = function() {
    var e = this.fuelBitmap.context;
    e.clearRect(0, 0, this.fuelBitmap.width, this.fuelBitmap.height);
    var t = this.fuelBitmap.width / 2,
        a = this.fuelBitmap.height / 2,
        i = -Math.PI / 2,
        s = -Math.PI / 2 + 2 * Math.PI * this.fuel / this.MAX_FUEL;
    this.fuel > .5 * this.MAX_FUEL ? e.fillStyle = "rgb(0, 255, 0)" : this.fuel > .25 * this.MAX_FUEL ? e.fillStyle = "rgb(255, 255, 0)" : this.fuel > 0 * this.MAX_FUEL && (e.fillStyle = "rgb(255, 0, 0)"), e.strokeStyle = e.fillStyle, e.beginPath(), this.fuel > 0 ? (e.moveTo(t, a), e.arc(t, a, this.radius - 5, i, s, !1), e.fill()) : (e.arc(t, a, this.radius / 3, 0, 2 * Math.PI, !1), e.stroke()), this.fuelBitmap.dirty = !0
};
var DEBUG_PRELOADER = !1;
DEBUG_PRELOADER && (Phaser.Loader.prototype.originalNextFile = Phaser.Loader.prototype.nextFile, Phaser.Loader.prototype.nextFile = function(e, t) {
    var a = this;
    window.setTimeout(function() {
        Phaser.Loader.prototype.originalNextFile.call(a, e, t)
    }, 100)
});
var PreloadState = function() {};
PreloadState.prototype.preload = function() {
    preloadIcon = this.game.add.sprite(0, 0, this.game.cache.getBitmapData("enemy")), preloadIcon.y = this.game.height / 2 - preloadIcon.height - 20, preloadIcon.x = this.game.width / 2 - preloadIcon.width / 2, preloadIcon.anchor.setTo(.5, .5), this.game.add.tween(preloadIcon).to({
        angle: 360
    }, 2e3, Phaser.Easing.Linear.None, !0, 0, Number.POSITIVE_INFINITY), preloadBg = this.game.add.sprite(0, 0, this.game.cache.getBitmapData("preloader-bg")), preloadBg.y = this.game.height / 2 - preloadBg.height / 2, preloadBg.x = this.game.width / 2 - preloadBg.width / 2, preloadFg = this.game.add.sprite(0, 0, this.game.cache.getBitmapData("preloader-fg")), preloadFg.y = this.game.height / 2 - preloadFg.height / 2, preloadFg.x = this.game.width / 2 - preloadFg.width / 2, this.game.load.setPreloadSprite(preloadFg), this.game.load.onFileComplete.add(this.fileLoaded, this), this.game.load.image("menu-background", "assets/gfx/start.png"), this.game.load.image("hud-background", "assets/gfx/hud.png"), this.game.load.audio("beep", ["assets/sfx/beep.ogg", "assets/sfx/beep.mp3"]), this.game.load.audio("bounce", ["assets/sfx/bounce.ogg", "assets/sfx/bounce.mp3"]), this.game.load.audio("detected", ["assets/sfx/detected.ogg", "assets/sfx/detected.mp3"]), this.game.load.audio("ding", ["assets/sfx/ding.ogg", "assets/sfx/ding.mp3"]), this.game.load.audio("explosion", ["assets/sfx/explosion.ogg", "assets/sfx/explosion.mp3"]), this.game.load.audio("homing", ["assets/sfx/homing.ogg", "assets/sfx/homing.mp3"]), this.game.load.audio("levelup", ["assets/sfx/levelup.ogg", "assets/sfx/levelup.mp3"]), this.game.load.audio("outoffuel", ["assets/sfx/out-of-fuel.ogg", "assets/sfx/out-of-fuel.mp3"]), this.game.load.audio("scanning", ["assets/sfx/scanning.ogg", "assets/sfx/scanning.mp3"]), this.game.load.audio("selfdestruct", ["assets/sfx/self-destruct.ogg", "assets/sfx/self-destruct.mp3"]), this.game.load.audio("thrust", ["assets/sfx/thrust.ogg", "assets/sfx/thrust.mp3"]), this.game.load.audio("tractorbeam", ["assets/sfx/tractor-beam.ogg", "assets/sfx/tractor-beam.mp3"]), this.game.load.audio("song1", ["assets/sfx/song1.ogg", "assets/sfx/song1.mp3"]), this.game.load.audio("song2", ["assets/sfx/song2.ogg", "assets/sfx/song2.mp3"]), this.game.load.audio("song3", ["assets/sfx/song3.ogg", "assets/sfx/song3.mp3"])
}, PreloadState.prototype.create = function() {
    G.setupStage(), G.sfx.song1 = this.game.add.sound("song1", .3, !1), G.sfx.song2 = this.game.add.sound("song2", .3, !1), G.sfx.song3 = this.game.add.sound("song3", .3, !1), G.sfx.beep = this.game.add.sound("beep", 1), G.sfx.bounce = this.game.add.sound("bounce", .6), G.sfx.detected = this.game.add.sound("detected", 1), G.sfx.ding = this.game.add.sound("ding", .6), G.sfx.explosion = this.game.add.sound("explosion", 1), G.sfx.homing = this.game.add.sound("homing", 1), G.sfx.levelup = this.game.add.sound("levelup", .6), G.sfx.outoffuel = this.game.add.sound("outoffuel", 1), G.sfx.scanning = this.game.add.sound("scanning", 1), G.sfx.selfdestruct = this.game.add.sound("selfdestruct", 1), G.sfx.thrust = this.game.add.sound("thrust", 1), G.sfx.tractorbeam = this.game.add.sound("tractorbeam", .25), this.game.add.text(0, 0, ".", {
        font: '16px "jupiter"',
        fill: "#000000"
    }), this.game.add.text(0, 0, ".", {
        font: '16px "8thcargo"',
        fill: "#000000"
    }), G.fadeOut(1e3, G.backgroundColor), this.game.time.events.add(1e3, function() {
        this.game.state.start("menu")
    }, this)
}, PreloadState.prototype.update = function() {}, PreloadState.prototype.fileLoaded = function() {}, Phaser.Filter.Scanline = function(e) {
    Phaser.Filter.call(this, e), this.fragmentSrc = ["precision lowp float;", "varying vec2 vTextureCoord;", "varying vec4 vColor;", "uniform sampler2D uSampler;", "uniform vec2 resolution;", "void main() {", "float brightness = clamp(sin(vTextureCoord.y * resolution.y * 5.0) + 0.5, 0.75, 1.0);", "gl_FragColor = texture2D(uSampler, vTextureCoord);", "gl_FragColor.rgb = mix(gl_FragColor.rgb, brightness * gl_FragColor.rgb, 1.0);", "}"]
}, Phaser.Filter.Scanline.prototype = Object.create(Phaser.Filter.prototype), Phaser.Filter.Scanline.prototype.constructor = Phaser.Filter.Scanline;
var ScreenshotState = function() {
        this.name = "screenshot"
    };
ScreenshotState.prototype.create = function() {
    G.setupStage()
}, ScreenshotState.prototype.update = function() {};
var Starfield = function(e) {
        Phaser.Group.call(this, e), this.makeStarfield(3e3, 1, .4, 6), this.makeStarfield(800, 2, .4, 12), this.makeStarfield(400, 2, .6, 18), this.makeStarfield(200, 3, .8, 24), this.filters = [this.game.add.filter("Glow")]
    };
Starfield.prototype = Object.create(Phaser.Group.prototype), Starfield.prototype.constructor = Starfield, Starfield.prototype.update = function() {}, Starfield.prototype.makeStarfield = function(e, t, a, i) {
    var s = this.game.add.bitmapData(2 * this.game.width, this.game.height),
        o = s.context;
    o.fillStyle = "rgba(255, 255, 255, " + a + ")";
    for (var r = 0; e > r; r++) {
        var h = this.game.rnd.integerInRange(0, 2 * this.game.width),
            n = this.game.rnd.integerInRange(0, this.game.height);
        o.fillRect(h, n, t, t)
    }
    var l = this.game.add.tileSprite(0, 0, 2 * this.game.width, this.game.height, s, 0, this);
    return this.game.add.tween(l.tilePosition).to({
        x: 2 * -this.game.width
    }, 2 * this.game.width / i * 1e3, Phaser.Easing.Linear.None, !0, 0, Number.POSITIVE_INFINITY), l
}, TextParticle = function(e, t, a) {
    Phaser.Particle.call(this, e, t, a), this.textSprite = this.game.add.text(0, 0, "", {
        font: '16px "jupiter"',
        fill: "#ffffff"
    }), this.addChild(this.textSprite)
}, TextParticle.prototype = Object.create(Phaser.Particle.prototype), TextParticle.prototype.constructor = TextParticle, TextParticle.prototype.onEmit = function() {
    this.textSprite.setText(this.parent.bonusText), this.textSprite.tint = this.parent.tint, this.x -= this.getBounds().width / 2
};