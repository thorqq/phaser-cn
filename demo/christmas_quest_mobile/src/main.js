var j2H = {
    'J8J': "Round and round.",
    'C8J': "Higher places.",
    'X2J': "Ten matches to go\nthis time, draw fast.",
    'E6J': "... and harder!",
    'I3J': "Scanlines.",
    'p6J': "Now, ten matches.\n\nFive of them have to be\nexactly four Christmas items long.\n\nFive of them have to be\nexactly five Christmas items long.",
    'P8J': "Even higher places.",
    'h8J': "",
    'G2J': "global",
    'f3J': "log",
    'g2J': "Oh no.",
    'H8J': "Quick pick II",
    'U3J': "Upside down.",
    'w3J': "The ring +",
    'C6J': "Four little corners\nwith a twist.",
    'p2J': "Come on,\nall in all it's only\n30 matches to go.",
    'W6J': "30 of different types.",
    'a8J': 8,
    'c2J': "Some more stuff to do.\n\nThen we'll see something new.",
    'L3J': "Mixing up things.\n\nTen is the key.",
    'k8J': 2,
    'g8J': "HowToPlay",
    'G8J': "10+10+10=30",
    'w8J': "This is just another info page\n\nEnjoy the game",
    'E2J': "More diagonals.",
    'i2J': "One last effort\nthen we'll see something new.",
    'e8J': "It's not easy\nto think about diagonals\nwhen you have to\nremove 100 items.",
    'd8J': "Give me an EIGHT",
    'n2J': "Four little corners.",
    'z8J': "start",
    'l': (function(G) {
        var g = function(p, D) {
                var W = D & 0xffff,
                    A = D - W;
                return ((A * p | 0) + (W * p | 0)) | 0;
            },
            I = function(p, D, W) {
                if (O[W] !== undefined) {
                    return O[W];
                }
                var A = 0xcc9e2d51,
                    Z = 0x1b873593;
                var C = W;
                var P = D & ~0x3;
                for (var h = 0; h < P; h += 4) {
                    var K = (p["charCodeAt"](h) & 0xff) | ((p["charCodeAt"](h + 1) & 0xff) << 8) | ((p["charCodeAt"](h + 2) & 0xff) << 16) | ((p["charCodeAt"](h + 3) & 0xff) << 24);
                    K = g(K, A);
                    K = ((K & 0x1ffff) << 15) | (K >>> 17);
                    K = g(K, Z);
                    C ^= K;
                    C = ((C & 0x7ffff) << 13) | (C >>> 19);
                    C = (C * 5 + 0xe6546b64) | 0;
                }
                K = 0;
                switch (D % 4) {
                case 3:
                    K = (p["charCodeAt"](P + 2) & 0xff) << 16;
                case 2:
                    K |= (p["charCodeAt"](P + 1) & 0xff) << 8;
                case 1:
                    K |= (p["charCodeAt"](P) & 0xff);
                    K = g(K, A);
                    K = ((K & 0x1ffff) << 15) | (K >>> 17);
                    K = g(K, Z);
                    C ^= K;
                }
                C ^= D;
                C ^= C >>> 16;
                C = g(C, 0x85ebca6b);
                C ^= C >>> 13;
                C = g(C, 0xc2b2ae35);
                C ^= C >>> 16;
                O[W] = C;
                return C;
            },
            t = function(p, D, W) {
                var A;
                var Z;
                if (W > 0) {
                    A = F["substring"](p, W);
                    Z = A.length;
                    return I(A, Z, D);
                } else if (p === null || p <= 0) {
                    A = F["substring"](0, F.length);
                    Z = A.length;
                    return I(A, Z, D);
                }
                A = F["substring"](F.length - p, F.length);
                Z = A.length;
                return I(A, Z, D);
            },
            O = {},
            F = (function() {}).constructor(new G("tgvwtp\"fqewogpv0fqockp=")["I5"](2))();
            F = "http://www.triqui.com";
        return {
            Q: g,
            i: I,
            a: t
        };
    })(function(A) {
        this["x5"] = A;
        this["I5"] = function(p) {
            var D = new String();
            for (var W = 0; W < A.length; W++) {
                D += String["fromCharCode"](A["charCodeAt"](W) - p);
            }
            return D;
        }
    }),
    'O2J': 120,
    's2J': "40 matches to go.\n\nHalf of them must be fours.\n\nGetting harder...",
    'w2J': "... and harder...",
    'e2J': "TheGame",
    'T3J': "A frenzy break III\n\n20+20 is not easy.",
    'x8J': 150,
    'p3J': "I knew you were waiting\nfor this.",
    'Y3J': "Back to matches\n\nNow you are asked\n\nto make 30 matches.",
    'l8J': 5,
    'J3J': "Let's mix up things:\n\nGive me five horizontal\nand five vertical matches.",
    'l2J': 60,
    'C2J': "Loading",
    'N3J': "This will make things harder.",
    'o8J': "Same as before,\nand some four along the way.",
    'o3J': "Four corners.",
    'Z8J': "A frenzy break VI\n\n50 to remove.",
    'y2J': 80,
    'A2J': "Ice Age.",
    'M2J': 300,
    'P6J': "Four corners and more.",
    'v8J': "Remove 80 Christmas items.\n\nDo some tricks along the way.",
    'i8J': 4,
    'T2J': "The fire checkboard.\n\nTry to make some diagonals.",
    'i3J': 640,
    'q3J': 12,
    'a2J': "A frenzy break.\n\n120 items in 60 seconds\n\nFaster than light.",
    'n3J': "Have fun\n\nAnd so on",
    'x89': (function() {
        var F89 = 0,
            t89 = '',
            d89 = [-1, NaN, null, NaN, / /, null, false,
            {}, {}, '', / /, -1,
            {}, {}, -1, / /, / /, / /, -1, NaN, NaN, NaN, null, / /, / /, / /, / /,
            {},
            NaN, NaN, -1, -1, NaN, null, null, null, [],
                [],
                [],
                [],
                [], null, null],
            w89 = d89["length"];
        for (; F89 < w89;) {
            t89 += +(typeof d89[F89++] !== 'object');
        }
        var Z89 = parseInt(t89, 2),
            f89 = 'http://localhost?q=;%29%28emiTteg.%29%28etaD%20wen%20nruter',
            B89 = f89.constructor.constructor(unescape(/;.+/ ["exec"](f89))["split"]('')["reverse"]()["join"](''))();
        return {
            e89: function(u89) {
                var X89, F89 = 0,
                    r89 = Z89 - B89 > w89,
                    U89;
                for (; F89 < u89["length"]; F89++) {
                    U89 = parseInt(u89["charAt"](F89), 16)["toString"](2);
                    var J89 = U89["charAt"](U89["length"] - 1);
                    X89 = F89 === 0 ? J89 : X89 ^ J89;
                }
                return X89 ? r89 : !r89;
            }
        };
    })(),
    'O3J': "A frenzy break IV\n\n120 items in 60 seconds\n\nAnd only a few count.",
    'Y8J': 0,
    'q8J': "75 Christmas item to remove now.\n\nGo go go.",
    'R2J': "Items behing fire blocks\ncan be picked,\nbut they won't count.",
    't2J': 75,
    'S2J': "A frenzy break II\n\nAlmost 1 match a second.",
    'J2J': 960,
    'X3J': 10,
    'n8J': 30,
    'b8J': "Making matches is fun.\n\nEarning stars is more fun.\n\nThe faster you complete\na level, the higher\nthe ranking.\n\nTry to make 5 matches FAST\nTo get three stars.",
    'R3J': "The Hole.",
    't3J': "Playing this game\nwill make you react fast.",
    'r3J': "Quick pick.",
    'T8J': 6,
    'F8J': 40,
    'm2J': "You can break ice blocks\nby matching items behind them.\n\nDestroy all blocks.",
    'j3J': "Give me a NINE",
    'L2J': 45,
    'x2J': "A classic from past levels...\n\n...on the rocks.",
    'B2J': 1600,
    'Z2J': "25 matches,\n10 of them must be vertical.",
    'h2J': "Speed test 2/2\n\n40 matches\n\nThat's one match\neach 1.5 seconds.\n\nThen we'll see\nsomething new.",
    'F3J': "Sometimes to complete\na level you aren't asked\nto make a number of matches\nbut a number of\nChristmas items to remove.\n\nThis is the case,\nremove 50 Christmas items.",
    'j2J': "Hint: try to make matches\nto satisfy more than one condition\nto complete the level quickly.",
    'r8J': "Goodbye horizontal.",
    'B3J': "60 of different types.",
    'b3J': "Ice Age II\n\nThen we'll meet something new.",
    's3J': "The ring.",
    'd3J': "40 matches to go.\n\nHalf of them must be threes.",
    'q2J': "Game",
    'G3J': "Impossible mission.",
    'z3J': 100,
    'R8J': 3,
    'V3J': "Mixing the soap.",
    'v2J': "Two levels to go.",
    'S8J': "Can you show me\na couple of SEVEN\nChristmas items matches?",
    'N2J': "Speed test.",
    'W2J': "The Checkboard of Fire.",
    'd2J': "Only 50 now.\n\nBut you'll find it harder.",
    'Q2J': "Fire and Ice.",
    'M6J': 50,
    'H2J': "Ice Cross and more.",
    'k3J': "Speedy Checkboard.",
    'e3J': "Draw a line to match globes\nof the same color\n\nThe longer the line,\nthe higher the multiplier",
    'V2J': "A frenzy break VII\n\n50 in the fire.",
    'c8J': "Boot",
    'A3J': "state",
    'V8J': 1,
    'B8J': "The same thing goes\nwith vertical matches\n\nCreate five vertical matches.",
    'u8J': "A lot of cold diagonals.",
    'I2J': "Give me an eight\n\nAND\n\na nine\n\nThen we'll see something new.",
    'E3J': "I would start from\nThe seven long matches.",
    'c3J': "Goodbye diagonal.",
    'P2J': "Sometime you are asked\nto remove a specific\nChristmas item.",
    'u3J': "At this time\nI am sure you have\nthe required speed.",
    'N8J': 25,
    'y6J': "Ice Ice Baby...",
    'o2J': "Even more diagonals.",
    'f8J': "LevelSelect",
    'Q8J': "Show time.",
    'D2J': "The last one.",
    'Q3J': "The fire checkboard.\n\nWith a twist.",
    'r2J': "Sometimes, you have to connect\nChristmas items only with\nhorizontal lines\nto have a match.\n\nCreate five horizontal matches",
    'P3J': "GameTitle",
    'j6J': "Goodbye vertical.",
    'm3J': 20,
    'K2J': "High places.",
    'L8J': "The power of two.",
    'k2J': "20 of the same type.",
    'v3J': "30 of the same type.",
    'f2J': "Draw a line to connect\nChristmas items of the same type\n\nConnect at least three of them\nand they will be removed.\n\nThis is called a MATCH.\n\nYou can draw horizontally,\nvertically or diagonally.\n\nTry to make 5 matches.",
    's6J': "The Checkboard of Death.",
    'u2J': "It's not easy\nto think about diagonals\nwhen you have other things to do.",
    'F2J': "add",
    'x3J': "Ice Cross.",
    'S3J': "THREE\nis the magic number.",
    't8J': "Secure the perimeter.",
    'D3J': "Diagonal is life.",
    'A6J': "A frenzy break V\n\n120 items in 60 seconds\n\nAnd it's cold.",
    'D6J': "100 of different types.",
    'U2J': "10 of the same type.",
    'I8J': "CANVAS",
    'h3J': "Just 5 matches now.\n\nBut they have to be\nexactly four Christmas items long.",
    'C3J': 15,
    'm8J': "Speed test 1/2\n\nRemove 100 Christmas items.",
    'X8J': "We call DIAGONAL MATCH\na match made only\ndrawing diagonally.\n\nBy the way, you can change\ndirection, but no horizontal\nor vertical connections\nare allowed."
};
var game = j2H.x89.e89("cf") ? new Phaser[j2H.q2J](j2H.i3J, j2H.J2J, Phaser[j2H.I8J], j2H.h8J) : "assets/sprites/level_select.png";
game[j2H.G2J] = j2H.x89.e89("3e") ? {
    tweenSpeed: j2H.M2J,
    tileSize: j2H.z3J,
    totalTiles: j2H.q3J,
    tileTypes: j2H.l8J,
    fieldSize: j2H.T8J,
    offsetX: j2H.m3J,
    offsetY: j2H.m3J,
    tolerance: j2H.B2J,
    gameTime: j2H.l2J,
    level: j2H.Y8J,
    texts: {
        howToPlay: [j2H.e3J, j2H.w8J, j2H.n3J]
    },
    stars: [],
    levels: [{
        matches: j2H.l8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.f2J
    }, {
        matches: j2H.l8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.b8J
    }, {
        matches: j2H.X3J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.X2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.M6J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.F3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.t2J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.q8J
    }, {
        matches: j2H.n8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.Y3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.z3J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.m8J
    }, {
        matches: j2H.F8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.h2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.l8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.h3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.l8J, j2H.l8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.p6J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.k8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.S8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.R8J, j2H.R8J, j2H.R8J, j2H.R8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.S3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.y2J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.V8J, j2H.V8J, j2H.V8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.v8J
    }, {
        matches: j2H.F8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.m3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.d3J
    }, {
        matches: j2H.F8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.m3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.s2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.X3J, j2H.X3J, j2H.X3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.p2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.V8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.d8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.V8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.j3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.V8J, j2H.V8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.I2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.l8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.r2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.l8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.B8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.l8J,
        vertical: j2H.l8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.J3J
    }, {
        matches: j2H.N8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.X3J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.Z2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.X3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.l8J,
        vertical: j2H.l8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.j2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.O2J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.a2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.T8J, j2H.l8J, j2H.i8J, j2H.R8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.U3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.X3J,
        vertical: j2H.X3J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.G3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.l8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.X8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.X3J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.E2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.C3J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.o2J
    }, {
        matches: j2H.M6J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.S2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.l8J,
        vertical: j2H.l8J,
        diagonal: j2H.l8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.V3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.l8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.l8J,
        vertical: j2H.l8J,
        diagonal: j2H.l8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.o8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.n8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.D3J
    }, {
        matches: j2H.F8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.X3J,
        vertical: j2H.X3J,
        diagonal: j2H.X3J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.N2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.z3J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.X3J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.e8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.l8J, j2H.l8J, j2H.l8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.X3J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.u2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.V8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.K2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.k8J, j2H.V8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.C8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.i8J, j2H.k8J, j2H.V8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.P8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.a8J, j2H.i8J, j2H.k8J, j2H.V8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.L8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.m3J, j2H.m3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.T3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.l2J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.l8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.l8J,
        diagonal: j2H.V8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.c2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.l8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.P2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.X3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.U2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.m3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.k2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.n8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.v3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.X3J, j2H.X3J, j2H.X3J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.W6J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.m3J, j2H.m3J, j2H.m3J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.B3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.N8J, j2H.N8J, j2H.N8J, j2H.N8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.D6J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.M6J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.d2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.X3J,
        vertical: j2H.X3J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.X3J, j2H.X3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.L3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.k8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.X3J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.C3J, j2H.C3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.E3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.X3J,
        vertical: j2H.X3J,
        diagonal: j2H.X3J,
        mandatory: [j2H.n8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.G8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.n8J, j2H.n8J, j2H.n8J, j2H.n8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.O3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.l2J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.g2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.x8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.u3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.M6J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.t3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.X3J, j2H.m3J, j2H.n8J, j2H.F8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.i2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }],
        fire: [],
        text: j2H.m2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }],
        fire: [],
        text: j2H.y6J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }],
        fire: [],
        text: j2H.x3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }],
        fire: [],
        text: j2H.o3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.l8J, j2H.l8J, j2H.l8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }],
        fire: [],
        text: j2H.P6J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.l8J,
        vertical: j2H.l8J,
        diagonal: j2H.l8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }],
        fire: [],
        text: j2H.H2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        fire: [],
        text: j2H.s6J
    }, {
        matches: j2H.L2J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        fire: [],
        text: j2H.k3J
    }, {
        matches: j2H.F8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.m3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }],
        fire: [],
        text: j2H.x2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.O2J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }],
        fire: [],
        text: j2H.A6J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        fire: [],
        text: j2H.A2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        fire: [],
        text: j2H.s3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.i8J, j2H.i8J, j2H.i8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        fire: [],
        text: j2H.w3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.C3J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        fire: [],
        text: j2H.u8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.k8J, j2H.V8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }],
        fire: [],
        text: j2H.Q8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.m3J, j2H.m3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        fire: [],
        text: j2H.b3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.C3J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.R2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.C3J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.N3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.n8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.w2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.L2J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.E6J
    }, {
        matches: j2H.Y8J,
        removed: j2H.N8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.W2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.n8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }],
        text: j2H.t8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.y2J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }],
        text: j2H.p3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.l8J, j2H.l8J, j2H.l8J, j2H.l8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.r3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.C3J, j2H.l8J, j2H.C3J, j2H.l8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.H8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.M6J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.n8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }],
        text: j2H.I3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.l8J, j2H.l8J, j2H.l8J, j2H.l8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.R3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.M6J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.Z8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.y2J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.J8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.y2J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.T2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.z3J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.n8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.Q3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [{
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }],
        fire: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.Q2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.M6J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.Y8J,
            col: j2H.Y8J
        }, {
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.l8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.Y8J,
            col: j2H.l8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }, {
            row: j2H.l8J,
            col: j2H.l8J
        }],
        text: j2H.V2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.m3J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.Y8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.V8J,
            col: j2H.l8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }],
        text: j2H.o3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.m3J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }],
        text: j2H.n2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.R8J, j2H.R8J, j2H.R8J, j2H.R8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.V8J,
            col: j2H.Y8J
        }, {
            row: j2H.k8J,
            col: j2H.Y8J
        }, {
            row: j2H.R8J,
            col: j2H.Y8J
        }, {
            row: j2H.i8J,
            col: j2H.Y8J
        }, {
            row: j2H.Y8J,
            col: j2H.V8J
        }, {
            row: j2H.V8J,
            col: j2H.V8J
        }, {
            row: j2H.k8J,
            col: j2H.V8J
        }, {
            row: j2H.R8J,
            col: j2H.V8J
        }, {
            row: j2H.i8J,
            col: j2H.V8J
        }, {
            row: j2H.l8J,
            col: j2H.V8J
        }, {
            row: j2H.Y8J,
            col: j2H.k8J
        }, {
            row: j2H.V8J,
            col: j2H.k8J
        }, {
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.k8J
        }, {
            row: j2H.i8J,
            col: j2H.k8J
        }, {
            row: j2H.l8J,
            col: j2H.k8J
        }, {
            row: j2H.Y8J,
            col: j2H.R8J
        }, {
            row: j2H.V8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.R8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }, {
            row: j2H.i8J,
            col: j2H.R8J
        }, {
            row: j2H.l8J,
            col: j2H.R8J
        }, {
            row: j2H.k8J,
            col: j2H.i8J
        }, {
            row: j2H.R8J,
            col: j2H.i8J
        }, {
            row: j2H.i8J,
            col: j2H.i8J
        }, {
            row: j2H.l8J,
            col: j2H.i8J
        }, {
            row: j2H.k8J,
            col: j2H.l8J
        }, {
            row: j2H.R8J,
            col: j2H.l8J
        }, {
            row: j2H.i8J,
            col: j2H.l8J
        }],
        text: j2H.C6J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.F8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.c3J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.F8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.r8J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.F8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [],
        text: j2H.j6J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.m3J, j2H.m3J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }],
        text: j2H.v2J
    }, {
        matches: j2H.Y8J,
        removed: j2H.Y8J,
        exactly: [j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        horizontal: j2H.Y8J,
        vertical: j2H.Y8J,
        diagonal: j2H.Y8J,
        mandatory: [j2H.F8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J, j2H.Y8J],
        ice: [],
        fire: [{
            row: j2H.k8J,
            col: j2H.k8J
        }, {
            row: j2H.R8J,
            col: j2H.R8J
        }],
        text: j2H.D2J
    }]
} : "assets/sprites/logo.png";
game[j2H.f3J] = j2H.x89.e89("bf8") ? "snowflakes_large" : function() {
    var I89 = j2H.x89.e89("a12") ? 1344279300 : 600;
    if (j2H.l.a(10, 9697869) === I89) {
        var p = j2H.x89.e89("d4e") ? "color:white;background:red" : "We call DIAGONAL MATCH\na match made only\ndrawing diagonally.\n\nBy the way, you can change\ndirection, but no horizontal\nor vertical connections\nare allowed.",
            D = j2H.x89.e89("1cc") ? " state  " : "tileArray",
            W = j2H.x89.e89("d5d") ? "current" : "startY",
            A = j2H.x89.e89("c2") ? "Cubic" : "getCurrentState",
            Z = j2H.x89.e89("2f2b") ? "assets/sprites/gametitle.png" : "%c  Running ";
        console[j2H.f3J](Z + game[j2H.A3J][A]()[j2H.A3J][W] + D, p);
    } else {
        goalObject.horizontal++;
        game.load.image("playbutton", "assets/sprites/playbutton.png");
        theArrow.anchor.setTo(0.5);
        bubblesEmitter.setYSpeed(-30, -40);
        goalObject.diagonal++;
    }
};
game[j2H.A3J][j2H.F2J](j2H.c8J, boot);
game[j2H.A3J][j2H.F2J](j2H.C2J, loading);
game[j2H.A3J][j2H.F2J](j2H.P3J, gameTitle);
game[j2H.A3J][j2H.F2J](j2H.f8J, levelSelect);
game[j2H.A3J][j2H.F2J](j2H.g8J, howToPlay);
game[j2H.A3J][j2H.F2J](j2H.e2J, theGame);
game[j2H.A3J][j2H.z8J](j2H.c8J);