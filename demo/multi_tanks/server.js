var express = require('express')
  , app = express(app)
  , server = require('http').createServer(app);

// 从当前目录使用静态文件
app.use(express.static(__dirname));

// 用于保存客户端数据
var clients = {};
  
//获取EurecaServer类
var EurecaServer = require('eureca.io').EurecaServer;

//创建一个EurecaServer实例
var eurecaServer = new EurecaServer({allow:['setId', 'spawnEnemy', 'kill', 'updateState']});

//把eureca.io附着在http服务器上
eurecaServer.attach(server);



//响应客户端连接
eurecaServer.onConnect(function (conn) {    
    console.log('New Client id=%s ', conn.id, conn.remoteAddress);
	
	//getClient函数返回客户端的一个代理，通过他可以调用客户独断的函数
    var remote = eurecaServer.getClient(conn.id);    
	
	//注册客户端
	clients[conn.id] = {id:conn.id, remote:remote}
	
	//调用客户端的setId方法
	remote.setId(conn.id);	
});

//响应客户端断开
eurecaServer.onDisconnect(function (conn) {    
    console.log('Client disconnected ', conn.id);
	
	var removeId = clients[conn.id].id;
	
	delete clients[conn.id];
	
	for (var c in clients)
	{
		var remote = clients[c].remote;
		
		//调用客户端的kill函数
		remote.kill(conn.id);
	}	
});

//把所有坦克的位置发送给所有的客户端
eurecaServer.exports.handshake = function()
{
	for (var c in clients)
	{
		var remote = clients[c].remote;
		for (var cc in clients)
		{		
			//发送最近一次的位置
			var x = clients[cc].laststate ? clients[cc].laststate.x:  0;
			var y = clients[cc].laststate ? clients[cc].laststate.y:  0;

			remote.spawnEnemy(clients[cc].id, x, y);		
		}
	}
}


//将坦克状态发送到所有客户端
eurecaServer.exports.handleKeys = function (keys) {
	var conn = this.connection;
	var updatedClient = clients[conn.id];
	
	for (var c in clients)
	{
		var remote = clients[c].remote;
		remote.updateState(updatedClient.id, keys);
		//console.log('handleKeys: ', conn.id, Math.round(keys.x), Math.round(keys.y));
		
		//保存最近一次状态
		clients[c].laststate = keys;
	}
}
server.listen(8000);