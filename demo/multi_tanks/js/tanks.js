//地面
var land;
//当前坦克的id
var myId=0;
//当前玩家
var player;
//坦克列表
var tanksList;
//启动logo
var logo;
//光标键，控制坦克前进和左右转
var cursors;

var fireRate = 100;
var nextFire = 0;

var ready = false;
var eurecaServer;

//这个函数用来处理客户端与服务器的通讯
var eurecaClientSetup = function() {
	//创建一个 eureca.io 客户端实例
	var eurecaClient = new Eureca.Client();
	
	eurecaClient.ready(function (proxy) {		
		eurecaServer = proxy;
	});
	
	//定义在 exports 命名空间下的函数可以被服务器端调用
	//设置id，创建游戏
	eurecaClient.exports.setId = function(id) 
	{
		//设置id
		myId = id;
		console.log("my id: " + id);
		//Phaser创建游戏，必须要在设置id的后面
		create();
		//与服务器握手，获取其他所有坦克的位置
		eurecaServer.handshake();
		//ready变成true之后，Phaser的主循环更新函数才开始起作用
		ready = true;
	}	
	
	//在游戏中清除一个坦克
	eurecaClient.exports.kill = function(id)
	{	
		if (tanksList[id]) {
			tanksList[id].kill();
			console.log('killing ', id, tanksList[id]);
		}
	}	
	
	//生成一个新坦克
	eurecaClient.exports.spawnEnemy = function(i, x, y)
	{
		if (i == myId) {
		    return; //this is me
		}
		
		console.log("new enemy: ", i, x, y);
		var tnk = new Tank(i, game, true, x, y);
		tanksList[i] = tnk;
	}
	
	//用于服务端更新某个坦克的状态（运动角度速度、位置、炮塔角度等）
	eurecaClient.exports.updateState = function(id, state)
	{
		if (tanksList[id])  {
			tanksList[id].cursor = state;
			if(id != myId) {
			    tanksList[id].tank.x = state.x;
			    tanksList[id].tank.y = state.y;
		    }   
			tanksList[id].tank.angle = state.angle;
			tanksList[id].turret.rotation = state.rot;
			tanksList[id].update();
			console.log('pos2: ', id, state.x, state.y);			
		}
	}
}

var info_pos = 0;
Tank = function (index, game, is_enemy, xx, yy) {
	this.cursor = {
		left:false,
		right:false,
		up:false,
		fire:false		
	}
	
	this.input = {
		left:false,
		right:false,
		up:false,
		fire:false
	}

    var x = xx;
    var y = yy;
    
    this.info = game.add.text(0, 0, 'debug');
    this.info.font = "Courier";
    this.info.fontSize = 14;
    this.info.fill = "#ffffff";
    this.info.lineSpacing = 4;
    this.info.fixedToCamera = true;
    this.info.cameraOffset.setTo(16, 16 + info_pos);
    info_pos = info_pos + 16;
    
    this.old_x = x;
    this.old_y = y;

    this.game = game;
    this.health = 30;
    this.bullets = game.add.group();
    this.bullets.enableBody = true;
    this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
    this.bullets.createMultiple(20, 'bullet', 0, false);
    this.bullets.setAll('anchor.x', 0.5);
    this.bullets.setAll('anchor.y', 0.5);
    this.bullets.setAll('outOfBoundsKill', true);
    this.bullets.setAll('checkWorldBounds', true);	
	
	this.currentSpeed =0;
    this.fireRate = 500;
    this.nextFire = 0;
    this.alive = true;

    //自己与对方坦克的颜色不同
    if(is_enemy) {
        this.shadow = game.add.sprite(x, y, 'enemy', 'shadow');
        this.tank = game.add.sprite(x, y, 'enemy', 'tank1');
        this.turret = game.add.sprite(x, y, 'enemy', 'turret');
    } else {
        this.shadow = game.add.sprite(x, y, 'tank', 'shadow');
        this.tank = game.add.sprite(x, y, 'tank', 'tank1');
        this.turret = game.add.sprite(x, y, 'tank', 'turret');
    }

    this.shadow.anchor.set(0.5);
    this.tank.anchor.set(0.5);
    this.turret.anchor.set(0.3, 0.5);

    game.physics.enable(this.tank, Phaser.Physics.ARCADE);
    this.tank.id = index;
    this.tank.body.immovable = false;
    this.tank.body.collideWorldBounds = true;
    this.tank.body.bounce.setTo(0, 0);
    
    game.physics.arcade.velocityFromRotation(this.tank.rotation, 0, this.tank.body.velocity);
};

//坦克的状态通过 eurecaClient.exports.updateState 上传到服务器
Tank.prototype.update = function() {
	
	var inputChanged = (
		this.cursor.left != this.input.left ||
		this.cursor.right != this.input.right ||
		this.cursor.up != this.input.up ||
		this.cursor.fire != this.input.fire
	);
	
	
	//上传坦克状态
	if (this.tank.id == myId)
	{
	    if(inputChanged || this.old_x != this.tank.x || this.old_y != this.tank.y) {
    		// 把最新的状态传给服务器
    		this.input.x = this.tank.x;
    		this.input.y = this.tank.y;
    		this.input.angle = this.tank.angle;
    		this.input.rot = this.turret.rotation;
    		
    	    this.old_x = this.tank.x;
    		this.old_y = this.tank.y;
    		eurecaServer.handleKeys(this.input);
    	}
	}

    this.info.text = this.tank.id + ' : \tx=' + Math.round(this.tank.x) + ', \ty=' + Math.round(this.tank.y)  + ', \tangle=' + Math.round(this.tank.angle);


	//左右光标键控制坦克转弯
    if (this.cursor.left)
    {
        this.tank.angle -= 1;
    }
    else if (this.cursor.right)
    {
        this.tank.angle += 1;
    }	
    
    //控制前进的速度
    if (this.cursor.up)
    {
        //  The speed we'll travel at
        this.currentSpeed = 300;
    }
    else
    {
        if (this.currentSpeed > 0)
        {
            this.currentSpeed -= 4;
        }
    }
    
    //开火
    if (this.cursor.fire)
    {	
		this.fire({x:this.cursor.tx, y:this.cursor.ty});
    }
		
    if (this.currentSpeed > 0)
    {
        game.physics.arcade.velocityFromRotation(this.tank.rotation, this.currentSpeed, this.tank.body.velocity);
    }	
	else
	{
		game.physics.arcade.velocityFromRotation(this.tank.rotation, 0, this.tank.body.velocity);
	}
	
    this.shadow.x = this.tank.x;
    this.shadow.y = this.tank.y;
    this.shadow.rotation = this.tank.rotation;

    this.turret.x = this.tank.x;
    this.turret.y = this.tank.y;
};

//开火
Tank.prototype.fire = function(target) {
		if (!this.alive) return;
        if (this.game.time.now > this.nextFire && this.bullets.countDead() > 0)
        {
            this.nextFire = this.game.time.now + this.fireRate;
            var bullet = this.bullets.getFirstDead();
            bullet.reset(this.turret.x, this.turret.y);

			bullet.rotation = this.game.physics.arcade.moveToObject(bullet, target, 500);
        }
}

//被消灭，或者下线
Tank.prototype.kill = function() {
	this.alive = false;
	this.tank.kill();
	this.turret.kill();
	this.shadow.kill();
	this.info.destroy();
}

//创建game
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'phaser-example', { preload: preload, create: eurecaClientSetup, update: update, render: render});

//预加载
function preload () {

    game.load.atlas('tank', 'assets/tanks.png', 'assets/tanks.json');
    game.load.atlas('enemy', 'assets/enemy-tanks.png', 'assets/tanks.json');
    game.load.image('logo', 'assets/logo.png');
    game.load.image('bullet', 'assets/bullet.png');
    game.load.image('earth', 'assets/scorched_earth.png');
    game.load.spritesheet('kaboom', 'assets/explosion.png', 64, 64, 23);
    
}

//创建游戏
function create () {

    //游戏失去焦点后，游戏继续执行
    game.stage.disableVisibilityChange = false;
    
    //  游戏世界 2000 x 2000 大小
    game.world.setBounds(-1000, -1000, 2000, 2000);
	game.stage.disableVisibilityChange  = true;
	
    //  滚动瓦片背景
    land = game.add.tileSprite(0, 0, 800, 600, 'earth');
    land.fixedToCamera = true;
    
    //坦克列表
    tanksList = {};
	
	//玩家自己的坦克
	player = new Tank(myId, game, false, 0, 0);
	tanksList[myId] = player;
	player.tank.x=0;
	player.tank.y=0;

    //  爆炸池
    var explosions = game.add.group();

    for (var i = 0; i < 10; i++)
    {
        var explosionAnimation = explosions.create(0, 0, 'kaboom', [0], false);
        explosionAnimation.anchor.setTo(0.5, 0.5);
        explosionAnimation.animations.add('kaboom');
    }

    //当前坦克放最上层
    player.tank.bringToTop();
    player.turret.bringToTop();
		
	//显示logo
    logo = game.add.sprite(0, 200, 'logo');
    logo.fixedToCamera = true;
    
    game.input.onDown.add(removeLogo, this);

    game.camera.follow(player.tank);
    game.camera.deadzone = new Phaser.Rectangle(150, 150, 500, 300);
    game.camera.focusOnXY(0, 0);

    //响应键盘光标键
    cursors = game.input.keyboard.createCursorKeys();
	
	//去掉logo
	setTimeout(removeLogo, 1000);

}

//去掉logo
function removeLogo () {
    game.input.onDown.remove(removeLogo, this);
    logo.kill();
}

function update () {
	//如果客户端没有准备好，直接返回
	if (!ready) return;
	
	//获取坦克状态
	player.input.left = cursors.left.isDown;
	player.input.right = cursors.right.isDown;
	player.input.up = cursors.up.isDown;
	player.input.fire = game.input.activePointer.isDown;
	player.input.tx = game.input.x+ game.camera.x;
	player.input.ty = game.input.y+ game.camera.y;
	
	//炮台角度
	player.turret.rotation = game.physics.arcade.angleToPointer(player.turret);	
    
    land.tilePosition.x = -game.camera.x;
    land.tilePosition.y = -game.camera.y;

    //处理坦克之间碰撞事件、炮弹射中坦克事件
    for (var i in tanksList)
    {
		if (!tanksList[i]) continue;
		
		//必须要加这句，否则不会产生碰撞效果
		if(player.tank.id != tanksList[i].tank.id) 
		{
		    game.physics.arcade.collide(player.tank, tanksList[i].tank);
		}
		
		var curBullets = tanksList[i].bullets;
		var curTank = tanksList[i].tank;
		for (var j in tanksList)
		{
		    //暂不处理射中坦克事件
			if (!tanksList[j]) continue;
			if (j!=i) 
			{
			
				var targetTank = tanksList[j].tank;
				
				game.physics.arcade.overlap(curBullets, targetTank, bulletHitPlayer, null, this);
			
			}
			if (tanksList[j].alive)
			{
				tanksList[j].update();
			}			
		}
    }
}

function bulletHitPlayer (tank, bullet) {

    bullet.kill();
}

function render() {

    //game.debug.cameraInfo(game.camera, 32, 32);
}
