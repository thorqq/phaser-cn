﻿
var Utils = {};
Utils.GlobalSettings = {
    muted: false,
    lastNickname: '',
    bestLapTime: 0,
    lastLapTime: 0,
    lastTrackId: 0,
    firstTime: true,
    isMobile: false
};
Utils.MersenneTwister = function(seed) {
    'use strict';
    if (typeof seed === 'undefined') {
        seed = new Date().getTime();
    }
    this.MAX_INT = 4294967296.0;
    this.N = 624;
    this.M = 397;
    this.UPPER_MARK = 0x80000000;
    this.LOWER_MASK = 0x7fffffff;
    this.MATRIX_A = 0x9908b0df;
    this.mt = new Array(this.N);
    this.mti = this.N + 1;
    this.seed(seed);
};
Utils.MersenneTwister.prototype = {
    seed: function(seed) {
        var s;
        this.mt[0] = seed >>> 0;
        for (this.mti = 1; this.mti < this.N; this.mti++) {
            s = this.mt[this.mti - 1] ^ (this.mt[this.mti - 1] >>> 30);
            this.mt[this.mti] = (((((s & 0xffff0000) >>> 16) * 1812433253) << 16) + (s & 0x0000ffff) * 1812433253) + this.mti;
            this.mt[this.mti] >>>= 0;
        }
    },
    seedArray: function(vector) {
        var i = 1,
            j = 0,
            k = this.N > vector.length ? this.N : vector.length,
            s;
        this.seed(19650218);
        for (; k > 0; k--) {
            s = this.mt[i - 1] ^ (this.mt[i - 1] >>> 30);
            this.mt[i] = (this.mt[i] ^ (((((s & 0xffff0000) >>> 16) * 1664525) << 16) + ((s & 0x0000ffff) * 1664525))) + vector[j] + j;
            this.mt[i] >>>= 0;
            i++;
            j++;
            if (i >= this.N) {
                this.mt[0] = this.mt[this.N - 1];
                i = 1;
            }
            if (j >= vector.length) {
                j = 0;
            }
        }
        for (k = this.N - 1; k; k--) {
            s = this.mt[i - 1] ^ (this.mt[i - 1] >>> 30);
            this.mt[i] = (this.mt[i] ^ (((((s & 0xffff0000) >>> 16) * 1566083941) << 16) + (s & 0x0000ffff) * 1566083941)) - i;
            this.mt[i] >>>= 0;
            i++;
            if (i >= this.N) {
                this.mt[0] = this.mt[this.N - 1];
                i = 1;
            }
        }
        this.mt[0] = 0x80000000;
    },
    int: function() {
        var y, kk, mag01 = new Array(0, this.MATRIX_A);
        if (this.mti >= this.N) {
            if (this.mti === this.N + 1) {
                this.seed(5489);
            }
            for (kk = 0; kk < this.N - this.M; kk++) {
                y = (this.mt[kk] & this.UPPER_MARK) | (this.mt[kk + 1] & this.LOWER_MASK);
                this.mt[kk] = this.mt[kk + this.M] ^ (y >>> 1) ^ mag01[y & 1];
            }
            for (; kk < this.N - 1; kk++) {
                y = (this.mt[kk] & this.UPPER_MARK) | (this.mt[kk + 1] & this.LOWER_MASK);
                this.mt[kk] = this.mt[kk + (this.M - this.N)] ^ (y >>> 1) ^ mag01[y & 1];
            }
            y = (this.mt[this.N - 1] & this.UPPER_MARK) | (this.mt[0] & this.LOWER_MASK);
            this.mt[this.N - 1] = this.mt[this.M - 1] ^ (y >>> 1) ^ mag01[y & 1];
            this.mti = 0;
        }
        y = this.mt[this.mti++];
        y ^= (y >>> 11);
        y ^= (y << 7) & 0x9d2c5680;
        y ^= (y << 15) & 0xefc60000;
        y ^= (y >>> 18);
        return y >>> 0;
    },
    int31: function() {
        return this.int() >>> 1;
    },
    real: function() {
        return this.int() * (1.0 / (this.MAX_INT - 1));
    },
    realInRange: function(min, max) {
        min = min || 0;
        max = max || 0;
        return this.real() * (max - min) + min;
    },
    integerInRange: function(min, max) {
        return Math.floor(this.realInRange(min, max));
    },
    realx: function() {
        return (this.int() + 0.5) * (1.0 / this.MAX_INT);
    },
    rnd: function() {
        return this.int() * (1.0 / this.MAX_INT);
    },
    rndHiRes: function() {
        var a = this.int() >>> 5,
            b = this.int() >>> 6;
        return (a * 67108864.0 + b) * (1.0 / 9007199254740992.0);
    }
};﻿
var DataStorage = {};
DataStorage.Database = function(syscookie) {
    this.systemcookie = syscookie;
};
DataStorage.Database.prototype = {
    save: function(key, value, dateOffset) {
        var date = new Date();
        date.setTime(date.getTime() + ((dateOffset ? dateOffset : 365 * 10) * 24 * 60 * 60 * 1000));
        document.cookie = this.systemcookie + "~" + key + "=" + value + "; expires=" + date.toUTCString() + "; path=/";
    },
    load: function(key) {
        var nameeq = this.systemcookie + "~" + key + "=";
        var ca = document.cookie.split(";");
        var rt;
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameeq) == 0) {
                rt = c.substring(nameeq.length, c.length);
                return rt;
            }
        }
        return null;
    }
};﻿
var Procedural = {};
Procedural.NameGen = function(seed) {
    this.vocals = ["a", "e", "i", "o", "u", "ei", "ai", "ou", "j", "ji", "y", "oi", "au", "oo"];
    this.startConsonants = ["b", "c", "d", "f", "g", "h", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z", "ch", "bl", "br", "fl", "gl", "gr", "kl", "pr", "st", "sh", "th"];
    this.endConsonants = ["b", "d", "f", "g", "h", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "z", "ch", "gh", "nn", "st", "sh", "th", "tt", "ss", "pf", "nt"];
    this.nameInstructions = ["cvdvd", "cvd", "vdvd", "cvcvcvd"];
    this.consonants = 'bcdfghjklmnpqrstvwxyz'.split('');
    this.vowels = 'aeiou'.split('');
    this.seed = seed;
    this.rng = new Utils.MersenneTwister(seed);
};
Procedural.NameGen.prototype = {
    setFields: function(vocals, startConsonants, endConsonants) {
        this.vocals = vocals;
        this.startConsonants = startConsonants;
        this.endConsonants = endConsonants;
    },
    setFieldsNI: function(vocals, startConsonants, endConsonants, nameInstructions) {
        this.setFields(vocals, startConsonants, endConsonants);
        this.nameInstructions = nameInstructions;
    },
    setSeed: function(s) {
        this.seed = s;
        this.rng.seed(this.seed);
    },
    getName: function() {
        return this.getNameByInstructions(this.getRandomElementFrom(this.nameInstructions));
    },
    getWord: function(length) {
        var i, word = '',
            length = parseInt(length, 10);
        var randConsonant = null;
        var randVowel = null;
        for (i = 0; i < length / 2; i++) {
            randConsonant = this.consonants[this.randomFromInterval(0, this.consonants.length)];
            randVowel = this.vowels[this.randomFromInterval(0, this.vowels.length)];
            word += (i === 0) ? randConsonant.toUpperCase() : randConsonant;
            word += i * 2 < length - 1 ? randVowel : '';
        }
        return word;
    },
    getNameByInstructions: function(nameInstructions) {
        var nameBuilder = [];
        var l = this.nameInstructions.length;
        var x = null;
        for (var i = 0; i < l; i++) {
            x = nameInstructions.charAt(i);
            switch (x) {
            case 'v':
                nameBuilder.push(this.getRandomElementFrom(this.vocals));
                break;
            case 'c':
                nameBuilder.push(this.getRandomElementFrom(this.startConsonants));
                break;
            case 'd':
                nameBuilder.push(this.getRandomElementFrom(this.endConsonants));
                break;
            }
        }
        var result = nameBuilder.join('');
        result = result.charAt(0).toUpperCase() + result.substring(1, result.length);
        return result;
    },
    randomFromInterval: function(from, to) {
        return this.rng.integerInRange(from, to);
    },
    getRandomElementFrom: function(v) {
        return v[this.randomFromInterval(0, v.length - 1)];
    }
};﻿Utils.MathUtils = function() {};
Utils.MathUtils.prototype = {
    timestamp: function() {
        return new Date().getTime();
    },
    toInt: function(obj, def) {
        if (obj !== null) {
            var x = parseInt(obj, 10);
            if (!isNaN(x)) return x;
        }
        return this.toInt(def, 0);
    },
    toFloat: function(obj, def) {
        if (obj !== null) {
            var x = parseFloat(obj);
            if (!isNaN(x)) return x;
        }
        return this.toFloat(def, 0.0);
    },
    limit: function(value, min, max) {
        return Math.max(min, Math.min(value, max));
    },
    randomInt: function(min, max) {
        return Math.round(this.interpolate(min, max, Math.random()));
    },
    randomChoice: function(options) {
        return options[this.randomInt(0, options.length - 1)];
    },
    percentRemaining: function(n, total) {
        return (n % total) / total;
    },
    accelerate: function(v, accel, dt) {
        return v + (accel * dt);
    },
    interpolate: function(a, b, percent) {
        return a + (b - a) * percent
    },
    easeIn: function(a, b, percent) {
        return a + (b - a) * Math.pow(percent, 2);
    },
    easeOut: function(a, b, percent) {
        return a + (b - a) * (1 - Math.pow(1 - percent, 2));
    },
    easeInOut: function(a, b, percent) {
        return a + (b - a) * ((-Math.cos(percent * Math.PI) / 2) + 0.5);
    },
    exponentialFog: function(distance, density) {
        return 1 / (Math.pow(Math.E, (distance * distance * density)));
    },
    increase: function(start, increment, max) {
        var result = start + increment;
        while (result >= max)
        result -= max;
        while (result < 0)
        result += max;
        return result;
    },
    project: function(p, cameraX, cameraY, cameraZ, cameraDepth, width, height, roadWidth) {
        p.camera.x = (p.world.x || 0) - cameraX;
        p.camera.y = (p.world.y || 0) - cameraY;
        p.camera.z = (p.world.z || 0) - cameraZ;
        p.screen.scale = cameraDepth / p.camera.z;
        p.screen.x = Math.round((width / 2) + (p.screen.scale * p.camera.x * width / 2));
        p.screen.y = Math.round((height / 2) - (p.screen.scale * p.camera.y * height / 2));
        p.screen.w = Math.round((p.screen.scale * roadWidth * width / 2));
    },
    overlap: function(x1, w1, x2, w2, percent) {
        var half = (percent || 1) / 2;
        var min1 = x1 - (w1 * half);
        var max1 = x1 + (w1 * half);
        var min2 = x2 - (w2 * half);
        var max2 = x2 + (w2 * half);
        return !((max1 < min2) || (min1 > max2));
    }
};﻿Utils.RenderHelpers = function(game) {
    this.game = game;
    this.spriteScale = 0.3 * (1 / 80);
};
Utils.RenderHelpers.prototype = {
    polygon: function(ctx, x1, y1, x2, y2, x3, y3, x4, y4, color) {
        ctx.fillStyle = color;
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.lineTo(x3, y3);
        ctx.lineTo(x4, y4);
        ctx.closePath();
        ctx.fill();
    },
    segment: function(ctx, width, lanes, x1, y1, w1, x2, y2, w2, fog) {
        var r1 = this.rumbleWidth(w1, lanes),
            r2 = this.rumbleWidth(w2, lanes),
            l1 = this.laneMarkerWidth(w1, lanes),
            l2 = this.laneMarkerWidth(w2, lanes),
            lanew1, lanew2, lanex1, lanex2, lane;
        ctx.fillStyle = '#FBFEFE';
        ctx.fillRect(0, y2, width, y1 - y2);
        this.polygon(ctx, x1 - w1 - r1, y1, x1 - w1, y1, x2 - w2, y2, x2 - w2 - r2, y2, '#222222');
        this.polygon(ctx, x1 + w1 + r1, y1, x1 + w1, y1, x2 + w2, y2, x2 + w2 + r2, y2, '#222222');
        this.polygon(ctx, x1 - w1, y1, x1 + w1, y1, x2 + w2, y2, x2 - w2, y2, '#111111');
        lanew1 = w1 * 2 / lanes;
        lanew2 = w2 * 2 / lanes;
        lanex1 = x1 - w1 + lanew1;
        lanex2 = x2 - w2 + lanew2;
        for (lane = 1; lane < lanes; lanex1 += lanew1, lanex2 += lanew2, lane++)
        this.polygon(ctx, lanex1 - l1 / 2, y1, lanex1 + l1 / 2, y1, lanex2 + l2 / 2, y2, lanex2 - l2 / 2, y2, '#333333');
        this.fog(ctx, 0, y1, width, y2 - y1, fog);
    },
    fog: function(ctx, x, y, width, height, fog) {
        if (fog < 1) {
            ctx.globalAlpha = (1 - fog);
            ctx.fillStyle = '#FBFEFE';
            ctx.fillRect(x, y, width, height);
            ctx.globalAlpha = 1;
        }
    },
    sprite: function(ctx, width, height, resolution, roadWidth, sprites, scale, destX, destY, offsetX, offsetY, clipY) {
        var destW = (sprites.width * scale * width / 2) * (this.spriteScale * roadWidth);
        var destH = (sprites.height * scale * width / 2) * (this.spriteScale * roadWidth);
        destX = destX + (destW * (offsetX || 0));
        destY = destY + (destH * (offsetY || 0));
        var clipH = clipY ? Math.max(0, destY + destH - clipY) : 0;
        if (clipH < destH) {
            ctx.drawImage(sprites, sprites.x, sprites.y, sprites.width, sprites.height - (sprites.height * clipH / destH), destX, destY, destW, destH - clipH);
        }
    },
    player: function(spr, ctx, width, height, resolution, roadWidth, sprites, speedPercent, scale, destX, destY, steer, updown, mathUtils) {
        var bounce = (1.5 * Math.random() * speedPercent * resolution) * mathUtils.randomChoice([-1, 1]);
        if (steer < 0) spr.animations.play('flyleft', 10, true);
        else if (steer > 0) spr.animations.play('flyright', 10, true);
        else spr.animations.play('flystraight', 10, true);
        spr.position.x = destX;
        spr.position.y = destY + bounce - 130;
        spr.scale.x = 3.7;
        spr.scale.y = 2;
    },
    background: function(ctx, background, width, height, rotation, offset) {
        rotation = rotation || 0;
        offset = offset || 0;
        var imageW = background.width / 2;
        var imageH = background.height;
        var sourceX = background.x + Math.floor(background.width * rotation);
        var sourceY = background.y
        var sourceW = Math.min(imageW, background.x + background.width - sourceX);
        var sourceH = imageH;
        var destX = 0;
        var destY = offset;
        var destW = Math.floor(width * (sourceW / imageW));
        var destH = height;
        ctx.drawImage(background, sourceX, sourceY, sourceW, sourceH, destX, destY, destW, destH);
        if (sourceW < imageW) ctx.drawImage(background, background.x, sourceY, imageW - sourceW, sourceH, destW - 1, destY, width - destW, destH);
    },
    rumbleWidth: function(projectedRoadWidth, lanes) {
        return projectedRoadWidth / Math.max(6, 2 * lanes);
    },
    laneMarkerWidth: function(projectedRoadWidth, lanes) {
        return projectedRoadWidth / Math.max(32, 8 * lanes);
    }
};
Phaser.Plugin.SpriteRendererPlugin = function(game, parent) {
    Phaser.Plugin.call(this, game, parent);
    this.sprites = [];
};
Phaser.Plugin.SpriteRendererPlugin.prototype = Object.create(Phaser.Plugin.prototype);
Phaser.Plugin.SpriteRendererPlugin.prototype.constructor = Phaser.Plugin.SpriteRendererPlugin;
Phaser.Plugin.SpriteRendererPlugin.prototype.addSprite = function(sprite) {
    sprite.parent = this.game.stage._stage;
    sprite.exists = true;
    sprite.visible = true;
    sprite.alive = true;
    this.sprites.push(sprite);
};
Phaser.Plugin.SpriteRendererPlugin.prototype.update = function() {
    if (this.sprites) {
        var i = 0;
        var sprite;
        for (i = 0; i < this.sprites.length; i++) {
            sprite = this.sprites[i];
            if (sprite) {
                if (sprite instanceof Phaser.Sprite) {
                    sprite.preUpdate();
                    sprite.updateCache();
                }
                sprite.updateTransform();
            }
        }
    }
};
Phaser.Plugin.SpriteRendererPlugin.prototype.postRender = function() {
    if (this.sprites) {
        var i = 0;
        var sprite;
        for (i = 0; i < this.sprites.length; i++) {
            sprite = this.sprites[i];
            if (sprite) {
                this.game.renderer.renderDisplayObject(sprite);
            }
        }
    }
};
Phaser.Plugin.SpriteRendererPlugin.prototype.destroy = function() {
    if (this.sprites) {
        var i = 0;
        var sprite;
        for (i = 0; i < this.sprites.length; i++) {
            sprite = this.sprites[i];
            if (sprite) {
                if (sprite instanceof Phaser.Sprite) {
                    sprite.destroy();
                }
                sprite = null;
            }
        }
        this.sprites = null;
    }
};﻿
var Sim = {};
Sim.RacingSim = function(game, state) {
    this.game = game;
    this.state = state;
    this.fps = 60;
    this.step = 1 / this.fps;
    this.width = 1024;
    this.height = 672;
    this.centrifugal = 0.3;
    this.offRoadDecel = 0.99;
    this.skySpeed = 0.001;
    this.hillSpeed = 0.002;
    this.treeSpeed = 0.003;
    this.skyOffset = 0;
    this.hillOffset = 0;
    this.treeOffset = 0;
    this.segments = [];
    this.cars = [];
    this.background = null;
    this.sprites = null;
    this.resolution = null;
    this.roadWidth = 2000;
    this.segmentLength = 200;
    this.rumbleLength = 3;
    this.trackLength = null;
    this.lanes = 2;
    this.fieldOfView = 100;
    this.cameraHeight = 1000;
    this.cameraDepth = null;
    this.drawDistance = 300;
    this.playerX = 0;
    this.playerZ = null;
    this.fogDensity = 2;
    this.position = 0;
    this.speed = 0;
    this.maxSpeed = (this.segmentLength / this.step) / 2;
    this.maxBoostSpeed = this.maxSpeed * 2;
    this.currentMaxSpeed = this.maxSpeed;
    this.accel = this.maxSpeed / 5;
    this.breaking = -this.maxSpeed;
    this.decel = -this.maxSpeed / 5;
    this.offRoadDecel = -this.maxSpeed / 2;
    this.offRoadLimit = this.maxSpeed / 4;
    this.totalCars = 200;
    this.currentLapTime = 0;
    this.lastLapTime = null;
    this.keyLeft = false;
    this.keyRight = false;
    this.gemCount = 0;
    this.maxGems = 0;
    this.gameOver = false;
    this.raceOver = false;
    this.raceOverTime = 0;
    this.showTutorial = false;
    this.tutorialStage = 0;
    this.tutorialTimer = 0;
    this.leftButtonDown = false;
    this.rightButtonDown = false;
    this.upButtonDown = false;
    this.downButtonDown = false;
    this.mathUtils = new Utils.MathUtils();
    this.renderHelpers = new Utils.RenderHelpers(this.game);
};
Sim.RacingSim.prototype = {
    init: function() {
        this.cameraDepth = 1 / Math.tan((this.fieldOfView / 2) * Math.PI / 180);
        this.playerZ = (this.cameraHeight * this.cameraDepth);
        this.resolution = this.height / 480;
        this.spriteRenderer = new Phaser.Plugin.SpriteRendererPlugin(this.game, null);
        this.game.plugins.add(this.spriteRenderer);
        this.player = new Phaser.Sprite(this.game, -200, -200, 'dragon', 0);
        this.player.visible = false;
        this.player.animations.add('flystraight', [0, 1, 2, 3]);
        this.player.animations.add('flyleft', [4, 5, 6, 7]);
        this.player.animations.add('flyright', [8, 9, 10, 11]);
        this.player.anchor.setTo(0.5, 0.5);
        this.spriteRenderer.addSprite(this.player);
        this.speedText = new Phaser.BitmapText(this.game, 10, 70, '', {
            font: '32px Desyrel',
            align: 'left'
        });
        this.spriteRenderer.addSprite(this.speedText);
        this.gemStats = new Phaser.Sprite(this.game, this.game.width / 2 - 100, 15, 'gemstat', 0);
        this.spriteRenderer.addSprite(this.gemStats);
        this.gemText = new Phaser.BitmapText(this.game, this.game.width / 2 - 40, 15, '', {
            font: '32px Desyrel',
            align: 'center'
        });
        this.spriteRenderer.addSprite(this.gemText);
        this.timeText = new Phaser.BitmapText(this.game, this.game.width - 210, 15, 'Time: 0', {
            font: '32px Desyrel',
            align: 'left'
        });
        this.spriteRenderer.addSprite(this.timeText);
        this.bestText = new Phaser.BitmapText(this.game, this.game.width - 210, 60, 'Best: ???', {
            font: '32px Desyrel',
            align: 'left'
        });
        this.spriteRenderer.addSprite(this.bestText);
        this.exitButton = this.game.add.button(5, 10, 'systembuttons2', this.exitClick, this, 1, 0, 1);
        this.spriteRenderer.addSprite(this.exitButton);
        this.retryButton = this.game.add.button(60, 10, 'systembuttons2', this.retryClick, this, 3, 2, 3);
        this.spriteRenderer.addSprite(this.retryButton);
        this.soundButton = null;
        if (!Utils.GlobalSettings.muted) {
            this.soundButton = this.game.add.button(115, 10, 'systembuttons2', this.soundClick, this, 5, 4, 5);
        } else {
            this.soundButton = this.game.add.button(115, 10, 'systembuttons2', this.soundClick, this, 7, 6, 7);
        }
        this.spriteRenderer.addSprite(this.soundButton);
        if (Utils.GlobalSettings.firstTime) {
            this.showTutorial = true;
            this.backing = new Phaser.Sprite(this.game, this.game.width / 2, 200, 'backing', 0);
            this.backing.anchor.setTo(0.5, 0.5);
            this.backing.alpha = 0.5;
            this.backing.width = 500;
            this.backing.height = 200;
            this.spriteRenderer.addSprite(this.backing);
            this.tutorialText = new Phaser.BitmapText(this.game, 290, 110, 'Press UP to accelerate.\nUse LEFT and RIGHT to steer.\nStay on the SkyRoad!', {
                font: '32px Desyrel',
                align: 'center'
            });
            this.spriteRenderer.addSprite(this.tutorialText);
            if (Utils.GlobalSettings.isMobile) {
                this.tutorialText.setText('Touch the LEFT and RIGHT\nsides of the screen to steer.\nStay on the SkyRoad!');
            }
        }
        if (Utils.GlobalSettings.isMobile) {
            this.leftButton = new Phaser.Sprite(this.game, 20, this.game.height - 200, 'controls', 1);
            this.leftButton.alpha = 0.3;
            this.spriteRenderer.addSprite(this.leftButton);
            this.rightButton = new Phaser.Sprite(this.game, this.game.width - 80, this.game.height - 200, 'controls', 0);
            this.rightButton.alpha = 0.3;
            this.spriteRenderer.addSprite(this.rightButton);
            this.upButtonDown = false;
            this.game.input.onDown.add(this.touchDown, this);
            this.game.input.onUp.add(this.touchUp, this);
        }
    },
    retryClick: function() {
        this.gameOver = true;
        this.state.destroy();
        this.game.state.start('gamescreen');
    },
    soundClick: function() {
        Utils.GlobalSettings.muted = !Utils.GlobalSettings.muted;
        if (!Utils.GlobalSettings.muted) {
            this.soundButton.setFrames(5, 4, 5);
            this.state.bgm.restart('', 0, 0.1, true);
        } else {
            this.soundButton.setFrames(7, 6, 7);
            this.state.bgm.pause();
        }
    },
    touchDown: function() {
        this.upButtonDown = true;
        this.leftButtonDown = false;
        this.rightButtonDown = false;
        if (this.game.input.y > 100) {
            if (this.game.input.x <= this.game.width / 2) {
                this.leftButtonDown = true;
            } else if (this.game.input.x > this.game.width / 2) {
                this.rightButtonDown = true;
            }
        }
    },
    touchUp: function() {
        this.leftButtonDown = false;
        this.rightButtonDown = false;
    },
    exitClick: function() {
        this.gameOver = true;
        this.state.destroy();
        this.game.state.start('mainmenu');
    },
    update: function() {
        if (this.gameOver) {
            return;
        }
        var dt = this.game.time.elapsed / 1000;
        var n, car, carW, sprite, spriteW;
        var playerSegment = this.findSegment(this.position + this.playerZ);
        var playerW = 80 * this.renderHelpers.spriteScale;
        var speedPercent = this.speed / this.maxBoostSpeed;
        var dx = dt * 2 * speedPercent;
        var startPosition = this.position;
        this.updateCars(dt, playerSegment, playerW);
        this.position = this.mathUtils.increase(this.position, dt * this.speed, this.trackLength);
        if (!this.raceOver) {
            if (this.leftButtonDown || this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
                this.playerX = this.playerX - dx;
                this.keyLeft = true;
                this.keyRight = false;
            } else if (this.rightButtonDown || this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                this.playerX = this.playerX + dx;
                this.keyRight = true;
                this.keyLeft = false;
            } else {
                this.keyLeft = false;
                this.keyRight = false;
                this.rightButtonDown = false;
                this.leftButtonDown = false;
            }
        }
        this.playerX = this.playerX - (dx * speedPercent * playerSegment.curve * this.centrifugal);
        if (!this.raceOver && (this.upButtonDown || this.game.input.keyboard.isDown(Phaser.Keyboard.UP))) this.speed = this.mathUtils.accelerate(this.speed, this.accel, dt);
        else if (!this.raceOver && (this.downButtonDown || this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN))) this.speed = this.mathUtils.accelerate(this.speed, this.breaking, dt);
        else this.speed = this.mathUtils.accelerate(this.speed, this.decel, dt);
        if ((this.playerX < -1) || (this.playerX > 1)) {
            if (this.speed > this.offRoadLimit) this.speed = this.mathUtils.accelerate(this.speed, this.offRoadDecel, dt);
        }
        for (n = 0; n < playerSegment.sprites.length; n++) {
            sprite = playerSegment.sprites[n];
            if (sprite.canCollide) {
                spriteW = this.game.cache.getImage(sprite.image).width * this.renderHelpers.spriteScale;
                if (this.mathUtils.overlap(this.playerX, playerW, sprite.offset + spriteW / 2 * (sprite.offset > 0 ? 1 : -1), spriteW)) {
                    this.speed = this.maxSpeed / 5;
                    this.position = this.mathUtils.increase(playerSegment.p1.world.z, -this.playerZ, this.trackLength);
                    break;
                }
            } else {
                break;
            }
        }
        for (n = 0; n < playerSegment.cars.length; n++) {
            car = playerSegment.cars[n];
            carW = this.game.cache.getImage(car.sprite.image).width * this.renderHelpers.spriteScale;
            if (this.speed > car.speed) {
                if (this.mathUtils.overlap(this.playerX, playerW, car.offset, carW, 0.8)) {
                    if (car.sprite.canCollide) {
                        if (car.speed < 1) {
                            this.speed = 2000;
                        } else this.speed = car.speed * (car.speed / this.speed);
                        this.position = this.mathUtils.increase(car.z, -this.playerZ, this.trackLength);
                        break;
                    } else {
                        this.handleObjectCollision(n, playerSegment.cars);
                        break;
                    }
                }
            }
        }
        this.playerX = this.mathUtils.limit(this.playerX, -3, 3);
        this.speed = this.mathUtils.limit(this.speed, 0, this.currentMaxSpeed);
        this.speedText.setText(Math.ceil(this.speed / 100) + ' mph');
        this.skyOffset = this.mathUtils.increase(this.skyOffset, this.skySpeed * playerSegment.curve * (this.position - startPosition) / this.segmentLength, 1);
        this.hillOffset = this.mathUtils.increase(this.hillOffset, this.hillSpeed * playerSegment.curve * (this.position - startPosition) / this.segmentLength, 1);
        this.treeOffset = this.mathUtils.increase(this.treeOffset, this.treeSpeed * playerSegment.curve * (this.position - startPosition) / this.segmentLength, 1);
        if (!this.raceOver) {
            if (this.position > this.playerZ) {
                if (this.currentLapTime && (startPosition < this.playerZ)) {
                    this.lastLapTime = this.currentLapTime;
                    console.log('finish: ' + this.lastLapTime);
                    this.raceOver = true;
                    Utils.GlobalSettings.lastLapTime = Math.round(this.currentLapTime * 100) / 100;
                } else {
                    this.currentLapTime += dt;
                    this.timeText.setText('Time: ' + Math.round(this.currentLapTime * 100) / 100);
                }
            }
        } else {
            this.raceOverTime += dt;
            if (this.speed <= 0 || this.raceOverTime >= 3) {
                this.raceOverTime = 0;
                this.gameOver = true;
                this.state.onGameOver();
            }
        }
        if (this.showTutorial) {
            this.tutorialTimer += dt;
            if (this.tutorialTimer >= 6) {
                this.tutorialTimer = 0;
                this.tutorialStage++;
                if (this.tutorialStage >= 3) {
                    this.showTutorial = false;
                    this.backing.visible = false;
                    this.tutorialText.visible = false;
                } else if (this.tutorialStage === 1) {
                    this.tutorialText.setText('Collect the Gems to speed up.\nBlue - Fast\nRed - Faster\nGold - Fastest');
                } else if (this.tutorialStage === 2) {
                    this.tutorialText.setText('Try to get the fastest time to\nbe the Track Boss!');
                }
            }
        }
    },
    updateCars: function(dt, playerSegment, playerW) {
        var n, car, oldSegment, newSegment;
        for (n = 0; n < this.cars.length; n++) {
            car = this.cars[n];
            oldSegment = this.findSegment(car.z);
            car.offset = car.offset + this.updateCarOffset(car, oldSegment, playerSegment, playerW);
            if (this.currentLapTime > 0) car.z = this.mathUtils.increase(car.z, dt * car.speed, this.trackLength);
            car.percent = this.mathUtils.percentRemaining(car.z, this.segmentLength);
            newSegment = this.findSegment(car.z);
            if (oldSegment != newSegment) {
                index = oldSegment.cars.indexOf(car);
                oldSegment.cars.splice(index, 1);
                newSegment.cars.push(car);
            }
        }
    },
    updateCarOffset: function(car, carSegment, playerSegment, playerW) {
        var i, j, dir, segment, otherCar, otherCarW, lookahead = 20,
            carW = this.game.cache.getImage(car.sprite.image).width * 0.3 * (1 / 80);
        if ((carSegment.index - playerSegment.index) > this.drawDistance) return 0;
        for (i = 1; i < lookahead; i++) {
            segment = this.segments[(carSegment.index + i) % this.segments.length];
            if ((segment === playerSegment) && (car.speed > this.speed) && (this.mathUtils.overlap(this.playerX, playerW, car.offset, carW, 1.2))) {
                if (this.playerX > 0.5) dir = -1;
                else if (this.playerX < -0.5) dir = 1;
                else dir = (car.offset > this.playerX) ? 1 : -1;
                return dir * 1 / i * (car.speed - this.speed) / this.maxSpeed;
            }
            for (j = 0; j < segment.cars.length; j++) {
                otherCar = segment.cars[j];
                if (otherCar.sprite.canCollide) {
                    otherCarW = this.game.cache.getImage(otherCar.sprite.image).width * this.renderHelpers.spriteScale;
                    if ((car.speed > otherCar.speed) && this.mathUtils.overlap(car.offset, carW, otherCar.offset, otherCarW, 1.2)) {
                        if (otherCar.offset > 0.5) dir = -1;
                        else if (otherCar.offset < -0.5) dir = 1;
                        else dir = (car.offset > otherCar.offset) ? 1 : -1;
                        return dir / i * (car.speed - otherCar.speed) / this.maxSpeed;
                    }
                } else {
                    break;
                }
            }
        }
        if (car.offset < -1.5) return 0.1;
        else if (car.offset > 1.5) return -0.1;
        else return 0;
    },
    handleObjectCollision: function(pos, arr) {
        if (arr) {
            var obj = arr[pos];
            if (obj) {
                switch (obj.sprite.type) {
                case 0:
                    this.currentMaxSpeed += 5;
                    if (this.currentMaxSpeed > this.maxBoostSpeed) this.currentMaxSpeed = this.maxBoostSpeed;
                    this.speed += 5;
                    if (this.speed > this.maxBoostSpeed) this.speed = this.maxBoostSpeed;
                    this.gemCount++;
                    this.gemText.setText(this.gemCount + '/' + this.maxGems);
                    arr.splice(pos, 1);
                    if (!Utils.GlobalSettings.muted) {
                        this.state.dripSnd.play();
                    }
                    break;
                case 1:
                    this.currentMaxSpeed += 10;
                    if (this.currentMaxSpeed > this.maxBoostSpeed) this.currentMaxSpeed = this.maxBoostSpeed;
                    this.speed += 10;
                    if (this.speed > this.maxBoostSpeed) this.speed = this.maxBoostSpeed;
                    this.gemCount++;
                    this.gemText.setText(this.gemCount + '/' + this.maxGems);
                    arr.splice(pos, 1);
                    if (!Utils.GlobalSettings.muted) {
                        this.state.dripSnd.play();
                    }
                    break;
                case 2:
                    this.currentMaxSpeed += 15;
                    if (this.currentMaxSpeed > this.maxBoostSpeed) this.currentMaxSpeed = this.maxBoostSpeed;
                    this.speed += 15;
                    if (this.speed > this.maxBoostSpeed) this.speed = this.maxBoostSpeed;
                    this.gemCount++;
                    this.gemText.setText(this.gemCount + '/' + this.maxGems);
                    arr.splice(pos, 1);
                    if (!Utils.GlobalSettings.muted) {
                        this.state.dripSnd.play();
                    }
                    break;
                }
            }
        }
    },
    render: function() {
        if (this.gameOver) {
            return;
        }
        this.game.context.setTransform(1, 0, 0, 1, 0, 0);
        var baseSegment = this.findSegment(this.position);
        var basePercent = this.mathUtils.percentRemaining(this.position, this.segmentLength);
        var playerSegment = this.findSegment(this.position + this.playerZ);
        var playerPercent = this.mathUtils.percentRemaining(this.position + this.playerZ, this.segmentLength);
        var playerY = this.mathUtils.interpolate(playerSegment.p1.world.y, playerSegment.p2.world.y, playerPercent);
        var maxy = this.height;
        var x = 0;
        var dx = -(baseSegment.curve * basePercent);
        this.renderHelpers.background(this.game.context, this.game.cache.getImage('sky'), this.width, this.height, this.skyOffset, this.resolution * this.skySpeed * playerY);
        this.renderHelpers.background(this.game.context, this.game.cache.getImage('clouds'), this.width, this.height, this.skyOffset, this.resolution * this.hillSpeed * playerY);
        this.renderHelpers.background(this.game.context, this.game.cache.getImage('clouds2'), this.width, this.height, this.skyOffset, this.resolution * this.treeSpeed * playerY);
        var n, i, segment, car, sprite, spriteScale, spriteX, spriteY, index;
        for (n = 0; n < this.drawDistance; n += 1) {
            index = (baseSegment.index + n) % this.segments.length;
            segment = this.segments[index];
            segment.looped = segment.index < baseSegment.index;
            segment.fog = this.mathUtils.exponentialFog(n / this.drawDistance, this.fogDensity);
            segment.clip = maxy;
            this.mathUtils.project(segment.p1, (this.playerX * this.roadWidth) - x, playerY + this.cameraHeight, this.position - (segment.looped ? this.trackLength : 0), this.cameraDepth, this.width, this.height, this.roadWidth);
            this.mathUtils.project(segment.p2, (this.playerX * this.roadWidth) - x - dx, playerY + this.cameraHeight, this.position - (segment.looped ? this.trackLength : 0), this.cameraDepth, this.width, this.height, this.roadWidth);
            x = x + dx;
            dx = dx + segment.curve;
            if ((segment.p1.camera.z <= this.cameraDepth) || (segment.p2.screen.y >= segment.p1.screen.y) || (segment.p2.screen.y >= maxy)) continue;
            this.renderHelpers.segment(this.game.context, this.width, this.lanes, segment.p1.screen.x, segment.p1.screen.y, segment.p1.screen.w, segment.p2.screen.x, segment.p2.screen.y, segment.p2.screen.w, segment.fog);
            maxy = segment.p1.screen.y;
        }
        for (n = (this.drawDistance - 1); n > 0; n--) {
            segment = this.segments[(baseSegment.index + n) % this.segments.length];
            for (i = 0; i < segment.cars.length; i++) {
                car = segment.cars[i];
                sprite = car.sprite;
                spriteScale = this.mathUtils.interpolate(segment.p1.screen.scale, segment.p2.screen.scale, car.percent);
                spriteX = this.mathUtils.interpolate(segment.p1.screen.x, segment.p2.screen.x, car.percent) + (spriteScale * car.offset * this.roadWidth * this.width / 2);
                spriteY = this.mathUtils.interpolate(segment.p1.screen.y, segment.p2.screen.y, car.percent);
                if (sprite.numFrames > 1) {
                    sprite.frameTimer += this.game.time.elapsed;
                    if (sprite.frameTimer > sprite.frameSpeed) {
                        sprite.frameTimer = 0;
                        sprite.currentFrame++;
                        if (sprite.currentFrame >= sprite.numFrames) {
                            sprite.currentFrame = 0;
                        }
                    }
                    sprite.image = sprite.baseImage + '_' + sprite.currentFrame;
                } else {
                    sprite.image = sprite.baseImage;
                }
                this.renderHelpers.sprite(this.game.context, this.width, this.height, this.resolution, this.roadWidth, this.game.cache.getImage(sprite.image), spriteScale, spriteX, spriteY, -0.5, -1, segment.clip);
            }
            for (i = 0; i < segment.sprites.length; i++) {
                sprite = segment.sprites[i];
                spriteScale = segment.p1.screen.scale;
                spriteX = segment.p1.screen.x + (spriteScale * sprite.offset * this.roadWidth * this.width / 2);
                spriteY = segment.p1.screen.y;
                if (sprite.numFrames > 1) {
                    sprite.frameTimer += this.game.time.elapsed;
                    if (sprite.frameTimer > sprite.frameSpeed) {
                        sprite.frameTimer = 0;
                        sprite.currentFrame++;
                        if (sprite.currentFrame >= sprite.numFrames) {
                            sprite.currentFrame = 0;
                        }
                    }
                    sprite.image = sprite.baseImage + '_' + sprite.currentFrame;
                } else {
                    sprite.image = sprite.baseImage;
                }
                this.renderHelpers.sprite(this.game.context, this.width, this.height, this.resolution, this.roadWidth, this.game.cache.getImage(sprite.image), spriteScale, spriteX, spriteY, (sprite.offset < 0 ? -1 : 0), -1, segment.clip);
            }
            if (segment == playerSegment) {
                this.renderHelpers.player(this.player, this.game.context, this.width, this.height, this.resolution, this.roadWidth, this.game.cache.getImage('player'), this.speed / this.maxSpeed, this.cameraDepth / this.playerZ, this.width / 2, (this.height / 2) - (this.cameraDepth / this.playerZ * this.mathUtils.interpolate(playerSegment.p1.camera.y, playerSegment.p2.camera.y, playerPercent) * this.height / 2), this.speed * (this.keyLeft ? -1 : this.keyRight ? 1 : 0), playerSegment.p2.world.y - playerSegment.p1.world.y, this.mathUtils);
            }
        }
    },
    findSegment: function(z) {
        return this.segments[Math.floor(z / this.segmentLength) % this.segments.length];
    },
    destroy: function() {
        this.cars = null;
        this.background = null;
        this.sprites = null;
        this.resolution = null;
        this.mathUtils = null;
        this.renderHelpers = null;
        if (this.spriteRenderer) {
            this.spriteRenderer.destroy();
            this.spriteRenderer = null;
        }
        this.player = null;
        this.speedText = null;
        this.gemStats = null;
        this.gemText = null;
        this.timeText = null;
        if (Utils.GlobalSettings.isMobile) {
            this.game.input.onDown.removeAll();
            this.game.input.onUp.removeAll();
        }
    }
};﻿Sim.TrackManager = function(game, state, sim) {
    this.game = game;
    this.state = state;
    this.sim = sim;
    this.rng = new Utils.MersenneTwister(0);
};
Sim.TrackManager.prototype = {
    lastY: function() {
        return (this.sim.segments.length == 0) ? 0 : this.sim.segments[this.sim.segments.length - 1].p2.world.y;
    },
    addSegment: function(curve, y) {
        var n = this.sim.segments.length;
        this.sim.segments.push({
            index: n,
            p1: {
                world: {
                    y: this.lastY(),
                    z: n * this.sim.segmentLength
                },
                camera: {},
                screen: {}
            },
            p2: {
                world: {
                    y: y,
                    z: (n + 1) * this.sim.segmentLength
                },
                camera: {},
                screen: {}
            },
            curve: curve,
            sprites: [],
            cars: []
        });
    },
    addRoad: function(enter, hold, leave, curve, y) {
        var startY = this.lastY();
        var endY = startY + (this.sim.mathUtils.toInt(y, 0) * this.sim.segmentLength);
        var n, total = enter + hold + leave;
        for (n = 0; n < enter; n++)
        this.addSegment(this.sim.mathUtils.easeIn(0, curve, n / enter), this.sim.mathUtils.easeInOut(startY, endY, n / total));
        for (n = 0; n < hold; n++)
        this.addSegment(curve, this.sim.mathUtils.easeInOut(startY, endY, (enter + n) / total));
        for (n = 0; n < leave; n++)
        this.addSegment(this.sim.mathUtils.easeInOut(curve, 0, n / leave), this.sim.mathUtils.easeInOut(startY, endY, (enter + hold + n) / total));
    },
    ROAD: {
        LENGTH: {
            NONE: 0,
            SHORT: 25,
            MEDIUM: 50,
            LONG: 100
        },
        HILL: {
            NONE: 0,
            LOW: 20,
            MEDIUM: 40,
            HIGH: 60
        },
        CURVE: {
            NONE: 0,
            EASY: 2,
            MEDIUM: 4,
            HARD: 6
        }
    },
    TRACKTYPE: {
        STRAIGHT: 0,
        HILL: 1,
        CURVE: 2,
        LOWROLLINGHILLS: 3,
        SCURVES: 4,
        BUMPS: 5,
        DOWNHILLTOEND: 6,
        CUSTOM: 7
    },
    addStraight: function(num) {
        num = num || this.ROAD.LENGTH.MEDIUM;
        this.addRoad(num, num, num, 0, 0);
    },
    addHill: function(num, height) {
        num = num || this.ROAD.LENGTH.MEDIUM;
        height = height || this.ROAD.HILL.MEDIUM;
        this.addRoad(num, num, num, 0, height);
    },
    addCurve: function(num, curve, height) {
        num = num || this.ROAD.LENGTH.MEDIUM;
        curve = curve || this.ROAD.CURVE.MEDIUM;
        height = height || this.ROAD.HILL.NONE;
        this.addRoad(num, num, num, curve, height);
    },
    addLowRollingHills: function(num, height) {
        num = num || this.ROAD.LENGTH.SHORT;
        height = height || this.ROAD.HILL.LOW;
        this.addRoad(num, num, num, 0, height / 2);
        this.addRoad(num, num, num, 0, -height);
        this.addRoad(num, num, num, this.ROAD.CURVE.EASY, height);
        this.addRoad(num, num, num, 0, 0);
        this.addRoad(num, num, num, -this.ROAD.CURVE.EASY, height / 2);
        this.addRoad(num, num, num, 0, 0);
    },
    addSCurves: function(num) {
        num = num || this.ROAD.LENGTH.MEDIUM;
        this.addRoad(num, num, num, -this.ROAD.CURVE.EASY, this.ROAD.HILL.NONE);
        this.addRoad(num, num, num, this.ROAD.CURVE.MEDIUM, this.ROAD.HILL.MEDIUM);
        this.addRoad(num, num, num, this.ROAD.CURVE.EASY, -this.ROAD.HILL.LOW);
        this.addRoad(num, num, num, -this.ROAD.CURVE.EASY, this.ROAD.HILL.MEDIUM);
        this.addRoad(num, num, num, -this.ROAD.CURVE.MEDIUM, -this.ROAD.HILL.MEDIUM);
    },
    addBumps: function() {
        this.addRoad(10, 10, 10, 0, 5);
        this.addRoad(10, 10, 10, 0, -2);
        this.addRoad(10, 10, 10, 0, -5);
        this.addRoad(10, 10, 10, 0, 8);
        this.addRoad(10, 10, 10, 0, 5);
        this.addRoad(10, 10, 10, 0, -7);
        this.addRoad(10, 10, 10, 0, 5);
        this.addRoad(10, 10, 10, 0, -2);
    },
    addDownhillToEnd: function(num) {
        num = num || 200;
        this.addRoad(num, num, num, -this.ROAD.CURVE.EASY, -this.lastY() / this.sim.segmentLength);
    },
    addBillboard: function(image, z, offset, type, canCollide, numFrames, frameSpeed) {
        numFrames = numFrames || 0;
        frameSpeed = frameSpeed || 0;
        var img = image;
        if (numFrames > 1) {
            img = image + '_0';
        }
        var segment = this.sim.findSegment(z);
        segment.sprites.push({
            baseImage: image,
            image: img,
            offset: offset,
            type: type,
            canCollide: canCollide,
            currentFrame: 0,
            numFrames: numFrames,
            frameTimer: 0,
            frameSpeed: frameSpeed
        });
    },
    addObject: function(image, z, offset, speed, type, canCollide, numFrames, frameSpeed) {
        numFrames = numFrames || 0;
        frameSpeed = frameSpeed || 0;
        var sprite = {
            baseImage: image,
            image: '',
            type: type,
            canCollide: canCollide,
            currentFrame: 0,
            numFrames: numFrames,
            frameTimer: 0,
            frameSpeed: frameSpeed
        }
        if (numFrames > 1) {
            sprite.image = image + '_0';
        } else {
            sprite.image = image;
        }
        var vehicle = {
            offset: offset,
            z: z,
            sprite: sprite,
            speed: speed
        };
        var segment = this.sim.findSegment(vehicle.z);
        segment.cars.push(vehicle);
        this.sim.cars.push(vehicle);
    },
    getTrackPart: function(type, enterLength, holdLength, leaveLength, curve, height) {
        enterLength = enterLength || this.ROAD.LENGTH.MEDIUM;
        holdLength = holdLength || enterLength;
        leaveLength = leaveLength || enterLength;
        curve = curve || this.ROAD.CURVE.MEDIUM;
        height = height || this.ROAD.HILL.NONE;
        switch (type) {
        case 0:
            this.addStraight(enterLength);
            break;
        case 1:
            this.addHill(enterLength, height);
            break;
        case 2:
            this.addCurve(enterLength, curve, height);
            break;
        case 3:
            this.addLowRollingHills(enterLength, height)
            break;
        case 4:
            this.addSCurves(enterLength);
            break;
        case 5:
            this.addBumps();
            break;
        case 6:
            this.addDownhillToEnd(enterLength);
            break;
        case 7:
            this.addRoad(enterLength, holdLength, leaveLength, curve, height);
            break;
        }
    },
    loadTrack: function(data) {
        var line = data.split('\n');
        if (line) {
            var i, j, part, thingType, canCollide, type, loopCount, startingZ, zOffset, image, speed, offset, numFrames, frameSpeed, seed, zMinOffset, zMaxOffset, offsetMin, offsetMax;
            this.sim.gemCount = 0;
            this.sim.maxGems = 0;
            for (i = 0; i < line.length; i++) {
                part = line[i].split(',');
                if (part && part.length > 0) {
                    thingType = parseInt(part[0]);
                    switch (thingType) {
                    case 0:
                        if (part.length === 7) {
                            this.getTrackPart(parseInt(part[1]), parseInt(part[2]), parseInt(part[3]), parseInt(part[4]), parseInt(part[5]), parseInt(part[6]));
                        }
                        break;
                    case 1:
                        if (part.length >= 6) {
                            image = part[1];
                            z = parseInt(part[2]);
                            offset = parseFloat(part[3]);
                            type = parseInt(part[4]);
                            if (parseInt(part[5]) === 0) canCollide = false;
                            else canCollide = true;
                            if (part.length === 6) {
                                this.addBillboard(image, z, offset, type, canCollide);
                            } else if (part.length === 8) {
                                numFrames = parseInt(part[6]);
                                frameSpeed = parseInt(part[7]);
                                this.addBillboard(image, z, offset, type, canCollide, numFrames, frameSpeed);
                            }
                        }
                        break;
                    case 2:
                        if (part.length >= 7) {
                            image = part[1];
                            z = parseInt(part[2]);
                            offset = parseFloat(part[3]);
                            speed = parseInt(part[4]);
                            type = parseInt(part[5]);
                            if (type === 0 || type === 1 || type === 2) {
                                this.sim.maxGems += 1;
                            }
                            if (parseInt(part[6]) === 0) canCollide = false;
                            else canCollide = true;
                            if (part.length === 7) {
                                this.addObject(image, z, offset, speed, type, canCollide);
                            } else if (part.length === 9) {
                                numFrames = parseInt(part[7]);
                                frameSpeed = parseInt(part[8]);
                                this.addObject(image, z, offset, speed, type, canCollide, numFrames, frameSpeed);
                            }
                        }
                        break;
                    case 3:
                        if (part.length >= 7) {
                            loopCount = parseInt(part[1]);
                            startingZ = parseInt(part[2]);
                            zOffset = parseInt(part[3]);
                            image = part[4];
                            offset = parseFloat(part[5]);
                            if (parseInt(part[6]) === 0) canCollide = false;
                            else canCollide = true;
                            if (part.length === 7) {
                                for (j = 0; j < loopCount; j++) {
                                    this.addBillboard(image, startingZ + (j * zOffset), offset, -1, canCollide);
                                }
                            } else if (part.length === 9) {
                                numFrames = parseInt(part[7]);
                                frameSpeed = parseInt(part[8]);
                                for (j = 0; j < loopCount; j++) {
                                    this.addBillboard(image, startingZ + (j * zOffset), offset, -1, canCollide, numFrames, frameSpeed);
                                }
                            }
                        }
                        break;
                    case 4:
                        if (part.length >= 9) {
                            loopCount = parseInt(part[1]);
                            startingZ = parseInt(part[2]);
                            zOffset = parseInt(part[3]);
                            image = part[4];
                            offset = parseFloat(part[5]);
                            speed = parseInt(part[6]);
                            type = parseInt(part[7]);
                            if (type === 0 || type === 1 || type === 2) {
                                this.sim.maxGems += loopCount;
                            }
                            if (parseInt(part[8]) === 0) canCollide = false;
                            else canCollide = true;
                            if (part.length === 9) {
                                for (j = 0; j < loopCount; j++) {
                                    this.addObject(image, startingZ + (j * zOffset), offset, speed, type, canCollide);
                                }
                            } else if (part.length === 11) {
                                numFrames = parseInt(part[9]);
                                frameSpeed = parseInt(part[10]);
                                for (j = 0; j < loopCount; j++) {
                                    this.addObject(image, startingZ + (j * zOffset), offset, speed, type, canCollide, numFrames, frameSpeed);
                                }
                            }
                        }
                        break;
                    case 5:
                        if (part.length >= 7) {
                            loopCount = parseInt(part[1]);
                            startingZ = parseInt(part[2]);
                            zOffset = parseInt(part[3]);
                            image = part[4];
                            offset = parseFloat(part[5]);
                            if (parseInt(part[6]) === 0) canCollide = false;
                            else canCollide = true;
                            if (part.length === 7) {
                                for (j = 0; j < loopCount; j++) {
                                    this.addBillboard(image, startingZ + (j * zOffset), offset, -1, canCollide);
                                    this.addBillboard(image, startingZ + (j * zOffset), -offset, -1, canCollide);
                                }
                            } else if (part.length === 9) {
                                numFrames = parseInt(part[7]);
                                frameSpeed = parseInt(part[8]);
                                for (j = 0; j < loopCount; j++) {
                                    this.addBillboard(image, startingZ + (j * zOffset), offset, -1, canCollide, numFrames, frameSpeed);
                                    this.addBillboard(image, startingZ + (j * zOffset), -offset, -1, canCollide, numFrames, frameSpeed);
                                }
                            }
                        }
                        break;
                    case 6:
                        if (part.length >= 9) {
                            loopCount = parseInt(part[1]);
                            startingZ = parseInt(part[2]);
                            zOffset = parseInt(part[3]);
                            image = part[4];
                            offset = parseFloat(part[5]);
                            speed = parseInt(part[6]);
                            type = parseInt(part[7]);
                            if (type === 0 || type === 1 || type === 2) {
                                this.sim.maxGems += loopCount * 2;
                            }
                            if (parseInt(part[8]) === 0) canCollide = false;
                            else canCollide = true;
                            if (part.length === 9) {
                                for (j = 0; j < loopCount; j++) {
                                    this.addObject(image, startingZ + (j * zOffset), offset, speed, type, canCollide);
                                    this.addObject(image, startingZ + (j * zOffset), -offset, speed, type, canCollide);
                                }
                            } else if (part.length === 11) {
                                numFrames = parseInt(part[9]);
                                frameSpeed = parseInt(part[10]);
                                for (j = 0; j < loopCount; j++) {
                                    this.addObject(image, startingZ + (j * zOffset), offset, speed, type, canCollide, numFrames, frameSpeed);
                                    this.addObject(image, startingZ + (j * zOffset), -offset, speed, type, canCollide, numFrames, frameSpeed);
                                }
                            }
                        }
                        break;
                    case 7:
                        if (part.length === 10) {
                            seed = parseInt(part[1]);
                            this.rng.seed(seed);
                            loopCount = parseInt(part[2]);
                            startingZ = parseInt(part[3]);
                            zMinOffset = parseInt(part[4]);
                            zMaxOffset = parseInt(part[5]);
                            image = part[6].split('#');
                            offsetMin = parseFloat(part[7]);
                            offsetMax = parseFloat(part[8]);
                            if (parseInt(part[9]) === 0) canCollide = false;
                            else canCollide = true;
                            for (j = 0; j < loopCount; j++) {
                                this.addBillboard(image[this.rng.integerInRange(0, image.length)], startingZ + (j * this.rng.integerInRange(zMinOffset, zMaxOffset)), this.rng.realInRange(offsetMin, offsetMax), -1, canCollide);
                            }
                        }
                        break;
                    case 8:
                        if (part.length === 12) {
                            seed = parseInt(part[1]);
                            this.rng.seed(seed);
                            loopCount = parseInt(part[2]);
                            startingZ = parseInt(part[3]);
                            zMinOffset = parseInt(part[4]);
                            zMaxOffset = parseInt(part[5]);
                            image = part[6].split('#');
                            offsetMin = parseFloat(part[7]);
                            offsetMax = parseFloat(part[8]);
                            speed = parseInt(part[9]);
                            type = parseInt(part[10]);
                            if (type === 0 || type === 1 || type === 2) {
                                this.sim.maxGems += loopCount;
                            }
                            if (parseInt(part[11]) === 0) canCollide = false;
                            else canCollide = true;
                            for (j = 0; j < loopCount; j++) {
                                this.addObject(image[this.rng.integerInRange(0, image.length)], startingZ + (j * this.rng.integerInRange(zMinOffset, zMaxOffset)), this.rng.realInRange(offsetMin, offsetMax), speed, type, canCollide);
                            }
                        }
                        break;
                    case 9:
                        if (part.length === 10) {
                            seed = parseInt(part[1]);
                            this.rng.seed(seed);
                            loopCount = parseInt(part[2]);
                            startingZ = parseInt(part[3]);
                            zMinOffset = parseInt(part[4]);
                            zMaxOffset = parseInt(part[5]);
                            image = part[6].split('#');
                            offsetMin = parseFloat(part[7]);
                            offsetMax = parseFloat(part[8]);
                            if (parseInt(part[9]) === 0) canCollide = false;
                            else canCollide = true;
                            for (j = 0; j < loopCount; j++) {
                                this.addBillboard(image[this.rng.integerInRange(0, image.length)], startingZ + (j * this.rng.integerInRange(zMinOffset, zMaxOffset)), this.rng.realInRange(offsetMin, offsetMax), -1, canCollide);
                                this.addBillboard(image[this.rng.integerInRange(0, image.length)], startingZ + (j * this.rng.integerInRange(zMinOffset, zMaxOffset)), -this.rng.realInRange(offsetMin, offsetMax), -1, canCollide);
                            }
                        }
                        break;
                    case 10:
                        if (part.length === 12) {
                            seed = parseInt(part[1]);
                            this.rng.seed(seed);
                            loopCount = parseInt(part[2]);
                            startingZ = parseInt(part[3]);
                            zMinOffset = parseInt(part[4]);
                            zMaxOffset = parseInt(part[5]);
                            image = part[6].split('#');
                            offsetMin = parseFloat(part[7]);
                            offsetMax = parseFloat(part[8]);
                            speed = parseInt(part[9]);
                            type = parseInt(part[10]);
                            if (type === 0 || type === 1 || type === 2) {
                                this.sim.maxGems += loopCount;
                            }
                            if (parseInt(part[11]) === 0) canCollide = false;
                            else canCollide = true;
                            for (j = 0; j < loopCount; j++) {
                                this.addObject(image[this.rng.integerInRange(0, image.length)], startingZ + (j * this.rng.integerInRange(zMinOffset, zMaxOffset)), this.rng.realInRange(offsetMin, offsetMax), speed, type, canCollide);
                                this.addObject(image[this.rng.integerInRange(0, image.length)], startingZ + (j * this.rng.integerInRange(zMinOffset, zMaxOffset)), -this.rng.realInRange(offsetMin, offsetMax), speed, type, canCollide);
                            }
                        }
                        break;
                    }
                }
            }
        }
    },
    resetRoad: function() {
        this.sim.segments = [];
        this.sim.cars = [];
        var data = this.game.cache.getText('track' + Utils.GlobalSettings.lastTrackId);
        this.loadTrack(data);
        this.sim.gemText.setText(this.sim.gemCount + '/' + this.sim.maxGems);
        this.addObject('finishline', this.sim.findSegment(this.sim.playerZ).index, 0, 0.1, -1, false, 3, 100);
        this.sim.trackLength = this.sim.segments.length * this.sim.segmentLength;
        this.state.onLoaded();
    },
};﻿
var UI = {};
UI.SubmitScoreForm = function(game, state) {
    this.game = game;
    this.state = state;
    this.letterArray = [];
    this.backing = null;
    this.bestText = null;
    this.bestLabel = null;
    this.nicknameText = null;
    this.nicknameLabel = null;
    this.ship = null;
    this.shipLabel = null;
    this.timeText = null;
    this.timeLabel = null;
    this.headingText = null;
    this.submitButton = null;
    this.submitLabel = null;
    this.messageLabel = null;
    this.group = null;
    this.exitButton = null;
};
UI.SubmitScoreForm.prototype = {
    init: function() {
        this.group = this.game.add.group();
        this.backing = this.game.add.sprite(100, 0, 'backing');
        this.backing.alpha = 0;
        this.backing.width = this.game.width - 200;
        this.backing.height = this.game.height - 100;
        this.group.add(this.backing);
        var tweenBacking = this.game.add.tween(this.backing).to({
            alpha: 0.8
        }, 1000, Phaser.Easing.Sinusoidal.Out, true);
        this.headingText = this.game.add.bitmapText(180, 10, 'Track Finished!', {
            font: '60px Desyrel',
            align: 'center'
        });
        this.group.add(this.headingText);
        this.submitButton = this.game.add.button(800, 50, 'systembuttons', this.submitPressed, this, 1, 0, 1);
        this.submitLabel = this.game.add.text(655, 85, 'Submit time', {
            font: "14pt Courier",
            fill: "#00ff00",
            stroke: "#00ff00",
            strokeThickness: 1
        });
        this.group.add(this.submitButton);
        this.group.add(this.submitLabel);
        this.exitButton = this.game.add.button(110, 10, 'systembuttons2', this.exitClick, this, 1, 0, 1);
        this.group.add(this.exitButton);
        this.retryButton = this.game.add.button(110, 70, 'systembuttons2', this.retryClick, this, 3, 2, 3);
        this.group.add(this.retryButton);
        this.nicknameText = this.game.add.bitmapText(180, 170, this.state.nickname, {
            font: '64px Desyrel',
            align: 'center'
        });
        this.nicknameLabel = this.game.add.text(180, 150, 'Enter your Nickname:', {
            font: "14pt Courier",
            fill: "#ffffff",
            stroke: "#ffffff",
            strokeThickness: 1
        });
        this.group.add(this.nicknameText);
        this.group.add(this.nicknameLabel);
        this.bestNameText = this.game.add.bitmapText(480, 180, '', {
            font: '20px Desyrel',
            align: 'center'
        });
        this.bestText = this.game.add.text(480, 200, '' + Utils.GlobalSettings.bestLapTime, {
            font: "44pt Impact",
            fill: "#ffffff",
            stroke: "#ff0000",
            strokeThickness: 2
        });
        this.bestLabel = this.game.add.text(480, 150, 'Track Boss:', {
            font: "14pt Courier",
            fill: "#ffffff",
            stroke: "#ffffff",
            strokeThickness: 1
        });
        this.group.add(this.bestNameText);
        this.group.add(this.bestText);
        this.group.add(this.bestLabel);
        this.timeText = this.game.add.text(700, 200, '' + Utils.GlobalSettings.lastLapTime, {
            font: "44pt Impact",
            fill: "#ffffff",
            stroke: "#00ff00",
            strokeThickness: 2
        });
        this.timeLabel = this.game.add.text(700, 150, 'Your Time:', {
            font: "14pt Courier",
            fill: "#ffffff",
            stroke: "#ffffff",
            strokeThickness: 1
        });
        this.group.add(this.timeText);
        this.group.add(this.timeLabel);
        this.messageLabel = this.game.add.text(180, 110, '', {
            font: "16pt Courier",
            fill: "#ff0000",
            stroke: "#ff0000",
            strokeThickness: 1
        });
        this.messageLabel.visible = false;
        this.group.add(this.messageLabel);
        var offs = 0;
        var offsi = 9;
        var xp = 180;
        var yp = 320;
        for (var i = 0; i < offsi; i++) {
            var letterButton = this.game.add.button(xp + (i * 75), yp, 'letters', this.letterPressed.bind(this, i), this, i, i, i);
            this.letterArray.push(letterButton);
            letterButton = this.game.add.button(xp + (i * 75), yp + 75, 'letters', this.letterPressed.bind(this, i + offsi), this, i + offsi, i + offsi, i + offsi);
            this.letterArray.push(letterButton);
            if (i + (offsi * 2) < 27) {
                letterButton = this.game.add.button(xp + (i * 75), yp + (75 * 2), 'letters', this.letterPressed.bind(this, i + (offsi * 2)), this, i + (offsi * 2), i + (offsi * 2), i + (offsi * 2));
                this.letterArray.push(letterButton);
            }
        }
        this.trackBossBg = this.game.add.sprite(this.game.width / 2, this.game.height / 2, 'trackboss');
        this.trackBossBg.anchor.setTo(0.5, 0.5);
        this.trackBossBg.visible = false;
        this.okButton = this.game.add.button(this.game.width / 2 - 49, this.game.height - 100, 'ok', this.okClick, this, 1, 0, 2);
        this.okButton.visible = false;
        this.getTrackBoss();
    },
    okClick: function() {
        if (this.okButton.visible && this.okButton.alpha === 1) {
            if (!Utils.GlobalSettings.muted) {
                this.state.clickSnd.play();
            }
            this.okButton.alpha = 0.5;
            this.destroy();
            this.state.destroy();
            this.game.state.start('mainmenu');
        }
    },
    exitClick: function() {
        if (this.exitButton.alpha === 1) {
            if (!Utils.GlobalSettings.muted) {
                this.state.clickSnd.play();
            }
            this.exitButton.alpha = 0.5;
            this.destroy();
            this.state.destroy();
            this.game.state.start('mainmenu');
        }
    },
    retryClick: function() {
        if (this.retryButton.alpha === 1) {
            if (!Utils.GlobalSettings.muted) {
                this.state.clickSnd.play();
            }
            this.retryButton.alpha = 0.5;
            this.destroy();
            this.state.destroy();
            this.game.state.start('gamescreen');
        }
    },
    submitPressed: function() {
        if (this.submitButton.alpha === 1) {
            if (!Utils.GlobalSettings.muted) {
                this.state.clickSnd.play();
            }
            if (this.state.nickname.length > 0) {
                this.messageLabel.visible = true;
                this.messageLabel.content = 'Submitting your time...';
                this.submitButton.alpha = 0.5;
                this.submitLabel.alpha = 0.5;
                this.submitScore();
            } else {
                this.messageLabel.visible = true;
                this.messageLabel.content = 'Please enter a nickname!';
                this.submitButton.alpha = 1;
                this.submitLabel.alpha = 1;
            }
        }
    },
    getTrackBoss: function() {
        var request = $.ajax({
            url: "http://www.gamepyong.com/skylegend/php/skylegend.php",
            type: "POST",
            data: {
                action: 'getbossscore',
                track: Utils.GlobalSettings.lastTrackId
            }
        });
        request.done(function(msg) {
            if (this.bestNameText && this.bestText && msg && msg.length > 0) {
                var part = msg.split(',');
                if (part && part.length === 2) {
                    this.bestNameText.setText(part[0]);
                    this.bestText.content = part[1];
                }
            }
        }.bind(this));
        request.fail(function(jqXHR, textStatus) {}.bind(this));
    },
    submitScore: function() {
        Utils.GlobalSettings.lastNickname = this.state.nickname;
        var request = $.ajax({
            url: "http://www.gamepyong.com/skylegend/php/skylegend.php",
            type: "POST",
            data: {
                action: 'score',
                nickname: this.state.nickname,
                track: Utils.GlobalSettings.lastTrackId,
                time: Utils.GlobalSettings.lastLapTime
            }
        });
        request.done(function(msg) {
            if (msg === 'success') {
                this.messageLabel.content = 'Time submitted!';
                this.destroy();
                this.state.destroy();
                this.game.state.start('mainmenu');
            } else if (msg === 'trackboss') {
                this.group.visible = false;
                if (this.letterArray) {
                    for (var i = 0; i < this.letterArray.length; i++) {
                        this.letterArray[i].kill();
                    }
                }
                this.trackBossBg.visible = true;
                this.okButton.visible = true;
            } else {
                this.submitButton.alpha = 1;
                this.submitLabel.alpha = 1;
                this.messageLabel.visible = true;
                this.messageLabel.content = 'Time was not submitted. Please try again.';
            }
        }.bind(this));
        request.fail(function(jqXHR, textStatus) {
            this.submitButton.alpha = 1;
            this.submitLabel.alpha = 1;
            this.messageLabel.visible = true;
            this.messageLabel.content = 'Time was not submitted. Please try again.';
        }.bind(this));
    },
    letterPressed: function(letterNum) {
        if (this.submitButton.alpha === 1) {
            if (!Utils.GlobalSettings.muted) {
                this.state.dripSnd.play();
            }
            var letter = '';
            switch (letterNum) {
            case 0:
                letter = 'a';
                break;
            case 1:
                letter = 'b';
                break;
            case 2:
                letter = 'c';
                break;
            case 3:
                letter = 'd';
                break;
            case 4:
                letter = 'e';
                break;
            case 5:
                letter = 'f';
                break;
            case 6:
                letter = 'g';
                break;
            case 7:
                letter = 'h';
                break;
            case 8:
                letter = 'i';
                break;
            case 9:
                letter = 'j';
                break;
            case 10:
                letter = 'k';
                break;
            case 11:
                letter = 'l';
                break;
            case 12:
                letter = 'm';
                break;
            case 13:
                letter = 'n';
                break;
            case 14:
                letter = 'o';
                break;
            case 15:
                letter = 'p';
                break;
            case 16:
                letter = 'q';
                break;
            case 17:
                letter = 'r';
                break;
            case 18:
                letter = 's';
                break;
            case 19:
                letter = 't';
                break;
            case 20:
                letter = 'u';
                break;
            case 21:
                letter = 'v';
                break;
            case 22:
                letter = 'w';
                break;
            case 23:
                letter = 'x';
                break;
            case 24:
                letter = 'y';
                break;
            case 25:
                letter = 'z';
                break;
            }
            if (letterNum === 26) {
                if (this.state.nickname.length > 0) {
                    this.state.nickname = this.state.nickname.substring(0, this.state.nickname.length - 1);
                    this.nicknameText.setText(this.state.nickname);
                }
            } else {
                if (this.state.nickname.length < 5) {
                    this.state.nickname += letter;
                    this.nicknameText.setText(this.state.nickname);
                }
            }
        }
    },
    destroy: function() {
        if (this.letterArray) {
            for (var i = 0; i < this.letterArray.length; i++) {
                this.letterArray[i].kill();
            }
            this.letterArray = null;
        }
        this.backing.destroy();
        this.nicknameText = null;
        this.nicknameLabel.destroy();
        this.bestNameText = null;
        this.bestText = null;
        this.bestLabel.destroy();
        this.timeText = null;
        this.timeLabel.destroy();
        this.headingText = null;
        this.submitButton.kill();
        this.submitLabel.destroy();
        this.messageLabel.destroy();
        this.exitButton.kill();
        this.exitButton = null;
        this.trackBossBg.destroy();
        this.okButton.kill();
        this.group.destroy();
        this.group = null;
        console.log('destroy submitscoreform');
    }
};﻿
var Screen = {};
Screen.Boot = function(game) {
    this.game = game;
};
Screen.Boot.prototype = {
    setupScaling: function() {
        this.game.stage.scaleMode = Phaser.StageScaleMode.SHOW_ALL;
        this.game.stage.disableVisibilityChange = false;
        this.game.input.maxPointers = 1;
        this.game.stage.disablePauseScreen = false;
        this.game.stage.scale.pageAlignHorizontally = true;
        this.game.stage.scale.pageAlignVertically = true;
        this.game.stage.scale.minWidth = 480;
        this.game.stage.scale.minHeight = 320;
        this.game.stage.scale.maxWidth = 1024;
        this.game.stage.scale.maxHeight = 672;
        this.game.stage.scale.refresh();
    },
    setupDesktop: function() {
        $('#game').css('position', 'absolute');
        $('#game').css('overflow', 'hidden');
        $('#game').css('left', '50%');
        $('#game').css('top', '50%');
        $('#game').css('width', '800px');
        $('#game').css('height', '600px');
        $('#game').css('margin-left', '-400px');
        $('#game').css('margin-top', '-300px');
    },
    setupMobile: function() {
        if (this.game.device.android && this.game.device.chrome == false) {
            this.game.stage.scaleMode = Phaser.StageScaleMode.EXACT_FIT;
            this.game.stage.scale.maxIterations = 1;
        }
    },
    preload: function() {
        this.game.load.image('loaderFull', 'assets/loadingFull.png');
        this.game.load.image('loaderEmpty', 'assets/loadingEmpty.png');
    },
    create: function() {
        this.setupScaling();
        if (this.game.device.desktop) {
            Utils.GlobalSettings.isMobile = false;
        } else {
            this.setupMobile();
            Utils.GlobalSettings.isMobile = true;
        }
        this.game.state.start('preloader');
    }
};﻿Screen.Preloader = function(game) {
    this.game = game;
};
Screen.Preloader.prototype = {
    loaded: false,
    preload: function() {
        this.loaderEmpty = this.game.add.sprite(0, 0, 'loaderEmpty');
        this.loaderEmpty.x = this.game.world.centerX - this.loaderEmpty.width / 2;
        this.loaderEmpty.y = this.game.world.centerY - this.loaderEmpty.height / 2;
        this.loaderEmpty.name = 'loaderEmpty';
        this.loaderFull = this.game.add.sprite(0, 0, 'loaderFull');
        this.loaderFull.x = this.game.world.centerX - this.loaderFull.width / 2;
        this.loaderFull.y = this.game.world.centerY - this.loaderFull.height / 2;
        this.loaderFull.name = 'loaderFull';
        this.game.load.image('logo', 'assets/logo.png');
        this.game.load.image('phaserlogo', 'assets/phaserlogo.png');
        this.game.load.image('clouds', 'assets/clouds.png');
        this.game.load.image('sky', 'assets/sky.png');
        this.game.load.image('sky2', 'assets/sky2.jpg');
        this.game.load.image('gemstat', 'assets/gemstat.png');
        this.game.load.image('clouds2', 'assets/clouds2.png');
        this.game.load.image('plant', 'assets/plant.png');
        this.game.load.image('plantpurple', 'assets/plantPurple.png');
        this.game.load.image('rock', 'assets/rock.png');
        this.game.load.image('hill_large', 'assets/hill_large.png');
        this.game.load.image('hill_small', 'assets/hill_small.png');
        this.game.load.image('fence', 'assets/fence.png');
        this.game.load.image('fence2', 'assets/fenceBroken.png');
        this.game.load.image('box', 'assets/box.png');
        this.game.load.image('box2', 'assets/boxAlt.png');
        this.game.load.image('box3', 'assets/boxExplosive.png');
        this.game.load.image('bush', 'assets/bush.png');
        this.game.load.image('cactus', 'assets/cactus.png');
        this.game.load.image('backing', 'assets/backing.png');
        this.game.load.image('flag', 'assets/flag.jpg');
        this.game.load.image('trackboss', 'assets/trackboss.png');
        this.game.load.image('finishline_0', 'assets/finishline_0.png');
        this.game.load.image('finishline_1', 'assets/finishline_1.png');
        this.game.load.image('finishline_2', 'assets/finishline_2.png');
        this.game.load.image('npc1_0', 'assets/npc1_0.png');
        this.game.load.image('npc1_1', 'assets/npc1_1.png');
        this.game.load.image('npc2_0', 'assets/npc2_0.png');
        this.game.load.image('npc2_1', 'assets/npc2_1.png');
        this.game.load.image('npc3_0', 'assets/npc3_0.png');
        this.game.load.image('npc3_1', 'assets/npc3_1.png');
        this.game.load.image('npc4_0', 'assets/npc4_0.png');
        this.game.load.image('npc4_1', 'assets/npc4_1.png');
        this.game.load.image('npc5_0', 'assets/npc5_0.png');
        this.game.load.image('npc5_1', 'assets/npc5_1.png');
        this.game.load.image('npc6_0', 'assets/npc6_0.png');
        this.game.load.image('npc6_1', 'assets/npc6_1.png');
        this.game.load.image('npc7_0', 'assets/npc7_0.png');
        this.game.load.image('npc7_1', 'assets/npc7_1.png');
        this.game.load.image('npc8_0', 'assets/npc8_0.png');
        this.game.load.image('npc8_1', 'assets/npc8_1.png');
        this.game.load.image('billboard1', 'assets/billboard1.png');
        this.game.load.image('billboard2', 'assets/billboard2.png');
        this.game.load.image('billboard3', 'assets/billboard3.png');
        this.game.load.image('billboard4', 'assets/billboard4.png');
        this.game.load.image('billboard5', 'assets/billboard5.png');
        this.game.load.image('billboard6', 'assets/billboard6.png');
        this.game.load.image('billboard7', 'assets/billboard7.png');
        this.game.load.image('billboard8', 'assets/billboard8.png');
        this.game.load.image('billboard9', 'assets/billboard9.png');
        this.game.load.image('billboard10', 'assets/billboard10.png');
        this.game.load.image('billboard11', 'assets/billboard11.png');
        this.game.load.image('billboard12', 'assets/billboard12.png');
        this.game.load.image('billboard13', 'assets/billboard13.png');
        this.game.load.image('tree1', 'assets/tree1.png');
        this.game.load.image('tree2', 'assets/tree2.png');
        this.game.load.image('tree3', 'assets/tree3.png');
        this.game.load.image('tree4', 'assets/tree4.png');
        this.game.load.image('tree5', 'assets/tree5.png');
        this.game.load.image('tree6', 'assets/tree6.png');
        this.game.load.image('tree7', 'assets/tree7.png');
        this.game.load.image('gem1_0', 'assets/gem1_0.png');
        this.game.load.image('gem1_1', 'assets/gem1_1.png');
        this.game.load.image('gem1_2', 'assets/gem1_2.png');
        this.game.load.image('gem2_0', 'assets/gem2_0.png');
        this.game.load.image('gem2_1', 'assets/gem2_1.png');
        this.game.load.image('gem2_2', 'assets/gem2_2.png');
        this.game.load.image('gem3_0', 'assets/gem3_0.png');
        this.game.load.image('gem3_1', 'assets/gem3_1.png');
        this.game.load.image('gem3_2', 'assets/gem3_2.png');
        this.game.load.spritesheet('ok', 'assets/ok.png', 98, 41);
        this.game.load.spritesheet('cancel', 'assets/cancel.png', 98, 41);
        this.game.load.spritesheet('systembuttons', 'assets/systembuttons.png', 64, 64);
        this.game.load.spritesheet('systembuttons2', 'assets/systembuttons2.png', 50, 50);
        this.game.load.spritesheet('trackbuttons', 'assets/trackbuttons.png', 150, 150);
        this.game.load.spritesheet('dragon', 'assets/dragon.png', 128, 128);
        this.game.load.spritesheet('letters', 'assets/letters.png', 64, 64);
        this.game.load.spritesheet('controls', 'assets/controls.png', 64, 64);
        this.game.load.bitmapFont('desyrel', 'assets/fonts/desyrel.png', 'assets/fonts/desyrel.xml');
        this.game.load.audio('gp', ['assets/audio/gp.ogg']);
        this.game.load.audio('click', ['assets/audio/click.ogg']);
        this.game.load.audio('drip', ['assets/audio/drip.ogg']);
        this.game.load.audio('bgm1', ['assets/audio/bgm1.ogg']);
        this.game.load.audio('bgm2', ['assets/audio/bgm2.ogg']);
        for (var i = 0; i < 5; i++) {
            this.game.load.text('track' + i, 'assets/tracks/track' + i + '.txt');
        }
        this.game.load.onFileComplete.add(this.fileLoaded, this);
        this.game.load.setPreloadSprite(this.loaderFull);
    },
    fileLoaded: function(progress, cacheID, success, filesLoaded, totalFiles) {
        if (filesLoaded === totalFiles && !this.loaded) {
            this.loaded = true;
            this.game.state.start('splashscreen');
        }
    },
    update: function() {}
};﻿Screen.SplashScreen = function(game) {
    this.game = game;
};
Screen.SplashScreen.prototype = {
    create: function() {
        this.gp = this.game.add.audio('gp');
        this.gp.play();
        this.phaserLogo = this.game.add.sprite(0, 0, 'phaserlogo');
        this.phaserLogo.x = -15;
        this.phaserLogo.y = -30;
        this.phaserLogo.anchor.setTo(0.5, 0.5);
        this.phaserLogo.body.angularVelocity = 130;
        this.phaserLogo.collideWorldBounds = true;
        this.phaserLogo.body.velocity.x = 400;
        this.phaserLogo.body.velocity.y = 300;
        this.logo = this.game.add.sprite(0, 0, 'logo');
        this.logo.x = this.game.world.centerX - this.logo.width / 2;
        this.logo.y = this.game.world.centerY - this.logo.height / 2;
        this.logo.scale.x = 20;
        this.logo.scale.y = 20;
        this.tween = this.game.add.tween(this.logo.scale).to({
            x: 1,
            y: 1
        }, 2000, Phaser.Easing.Sinusoidal.Out, true);
        this.tween2 = this.game.add.tween(this.logo).to({
            alpha: 0
        }, 1000);
        this.tween.chain(this.tween2);
        this.tween2.onComplete.add(this.done.bind(this));
    },
    done: function() {
        this.game.state.start('mainmenu');
    },
    update: function() {},
    destroy: function() {
        if (this.gp) {
            this.gp.stop();
            this.gp = null;
        }
        if (this.phaserLogo) {
            this.phaserLogo.kill();
            this.phaserLogo = null;
        }
        if (this.logo) {
            this.logo.kill();
            this.logo = null;
        }
        if (this.tween) {
            this.tween.stop();
            this.tween = null;
        }
        if (this.tween2) {
            this.tween2.onComplete.removeAll();
            this.tween2.stop();
            this.tween2 = null;
        }
    }
};﻿Screen.MainMenu = function(game) {
    this.game = game;
};
Screen.MainMenu.prototype = {
    create: function() {
        this.database = new DataStorage.Database('gamepyong_skyleg');
        this.setup();
    },
    supportsLocalStorage: function() {
        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
            return false;
        }
    },
    setup: function() {
        this.clickSnd = this.game.add.audio('click');
        this.bgm = this.game.add.audio('bgm1');
        if (!Utils.GlobalSettings.muted && !Utils.GlobalSettings.isMobile) {
            this.bgm.play('', 0, 0.2, true);
        }
        this.bg = this.game.add.sprite(0, 0, 'sky2');
        this.titleLabel1 = this.game.add.bitmapText(10, -200, 'Sky', {
            font: '100px Desyrel',
            align: 'left'
        });
        this.titleLabel2 = this.game.add.bitmapText(40, -380, 'Legend', {
            font: '100px Desyrel',
            align: 'left'
        });
        this.dragon = this.game.add.sprite(-200, 600, 'dragon');
        this.dragon.animations.add('flystraight', [0, 1, 2, 3]);
        this.dragon.play('flystraight', 10, true);
        var tween1 = this.game.add.tween(this.titleLabel1).to({
            y: 10
        }, 1000, Phaser.Easing.Back.Out, true);
        var tween2 = this.game.add.tween(this.titleLabel2).to({
            y: 80
        }, 1000, Phaser.Easing.Back.Out, true);
        var tween3 = this.game.add.tween(this.dragon).to({
            x: 200,
            y: 10
        }, 1000, Phaser.Easing.Sinusoidal.Out, true);
        this.playButton = this.game.add.button(this.game.width / 2 - 32, this.game.height / 2 - 32, 'systembuttons', this.playClick, this, 3, 2, 3);
        this.playLabel = this.game.add.bitmapText(this.game.width / 2 - 60, this.game.height / 2 + 40, 'Play', {
            font: '50px Desyrel',
            align: 'left'
        });
        this.credits = ["Programming - Hsaka", "Made with - Phaser [phaser.io]", "Graphics - J. W. Bjerk (eleazzaar)\n[www.jwbjerk.com/art]", "Graphics - prdatur\n[http://opengameart.org/content/basic-gems-icon-set-remix]", "Graphics - GameArtForge\n[http://opengameart.org/content/letter-blocks-set-01]", "Graphics - Alucard\n[http://opengameart.org/content/monsters-2d-pack-no-2]", "Graphics - Barbara Rivera, Casper Nilsson, Johann CHARLOT\nChris Phillips, Lanea Zimmerman", "Graphics - Daniel Cook\n[http://www.lostgarden.com/]", "Music - Przemysław Sikorski\n[http://soundcloud.com/rezoner/]"];
        this.creditIndex = 0;
        this.creditText = this.game.add.bitmapText(0, 0, this.credits[this.creditIndex], {
            font: '24px Desyrel',
            align: 'left'
        });
        this.creditText.x = this.game.width + 1000;
        this.creditText.y = this.game.height - 60;
        this.creditTween = this.game.add.tween(this.creditText).to({
            x: 0
        }, 6000, Phaser.Easing.Sinusoidal.Out, true).loop();
        this.creditTween.onComplete.add(this.creditDone.bind(this));
        this.soundButton = null;
        if (!Utils.GlobalSettings.muted) {
            this.soundButton = this.game.add.button(this.game.width - 60, 10, 'systembuttons2', this.soundClick, this, 5, 4, 5);
        } else {
            this.soundButton = this.game.add.button(this.game.width - 60, 10, 'systembuttons2', this.soundClick, this, 7, 6, 7);
        }
        this.setupTrackButtons();
        if (this.supportsLocalStorage()) {
            if (localStorage["skyleg.firstTime"] && localStorage["skyleg.firstTime"] === '1') {
                Utils.GlobalSettings.firstTime = false;
            }
        } else {
            var data = this.database.load('firstTime');
            if (data === '1') {
                Utils.GlobalSettings.firstTime = false;
            }
        }
    },
    setupTrackButtons: function() {
        this.trackPage = 0;
        this.trackMaxPages = 0;
        this.trackGroup = this.game.add.group();
        this.trackButtonsLabel = this.game.add.bitmapText(this.game.width - 480, 120, 'select a track:', {
            font: '60px Desyrel',
            align: 'left'
        });
        this.trackGroup.add(this.trackButtonsLabel);
        this.backing = this.game.add.sprite(0, 220, 'backing');
        this.backing.alpha = 0.5;
        this.backing.width = this.game.width;
        this.backing.height = 240;
        this.trackGroup.add(this.backing);
        this.nameGen = new Procedural.NameGen(0);
        this.trackbuttons = [];
        this.trackLabels = [];
        this.trackNames = [];
        this.trackBossNames = [];
        this.trackBossTimes = [];
        for (var i = 0; i < 5; i++) {
            this.trackbuttons.push(this.game.add.button(40 + (i * 200), 250, 'trackbuttons', this.trackButtonClick.bind(this, i), this, 1, 0, 1));
            this.trackGroup.add(this.trackbuttons[i]);
            this.trackLabels.push(this.game.add.text(115 + (i * 200), 325, '', {
                font: "44pt Impact",
                fill: "#ffffff",
                stroke: "#000000",
                strokeThickness: 4,
                align: 'center'
            }));
            this.trackLabels[i].anchor.setTo(0.5, 0.5);
            this.trackGroup.add(this.trackLabels[i]);
            this.trackNames.push(this.game.add.text(115 + (i * 200), 235, '', {
                font: "14pt Impact",
                fill: "#00ff00",
                stroke: "#000000",
                strokeThickness: 0
            }));
            this.trackNames[i].anchor.setTo(0.5, 0.5);
            this.trackGroup.add(this.trackNames[i]);
            this.trackBossNames.push(this.game.add.bitmapText(110 + (i * 200), 415, 'unknown', {
                font: '32px Desyrel',
                align: 'center'
            }));
            this.trackBossNames[i].anchor.setTo(0.5, 0.5);
            this.trackGroup.add(this.trackBossNames[i]);
            this.trackBossTimes.push(this.game.add.text(110 + (i * 200), 440, '???', {
                font: "14pt Impact",
                fill: "#ff0000",
                stroke: "#000000",
                strokeThickness: 0
            }));
            this.trackBossTimes[i].anchor.setTo(0.5, 0.5);
            this.trackGroup.add(this.trackBossTimes[i]);
        }
        this.trackPagePrev = this.game.add.button(10, 470, 'systembuttons', this.trackPrevClick, this, 7, 6, 7);
        this.trackGroup.add(this.trackPagePrev);
        this.trackPageNext = this.game.add.button(this.game.width - 74, 470, 'systembuttons', this.trackNextClick, this, 5, 4, 5);
        this.trackGroup.add(this.trackPageNext);
        this.trackPageLabel = this.game.add.bitmapText(this.game.width / 2, 500, (this.trackPage + 1) + '/' + (this.trackMaxPages + 1), {
            font: '45px Desyrel',
            align: 'center'
        });
        this.trackPageLabel.anchor.setTo(0.5, 0.5);
        this.trackGroup.add(this.trackPageLabel);
        this.trackGroup.visible = false;
    },
    trackPrevClick: function() {
        if (!Utils.GlobalSettings.muted) {
            this.clickSnd.play();
        }
        if (this.trackPage > 0) {
            this.trackPage -= 1;
            this.trackPageLabel.setText((this.trackPage + 1) + '/' + (this.trackMaxPages + 1));
            this.getTrackPage(this.trackPage);
        }
    },
    trackNextClick: function() {
        if (!Utils.GlobalSettings.muted) {
            this.clickSnd.play();
        }
        if (this.trackPage < this.trackMaxPages) {
            this.trackPage += 1;
            this.trackPageLabel.setText((this.trackPage + 1) + '/' + (this.trackMaxPages + 1));
            this.getTrackPage(this.trackPage);
        }
    },
    trackButtonClick: function(button) {
        if (!Utils.GlobalSettings.muted) {
            this.clickSnd.play();
        }
        var track = (this.trackPage * 5) + button;
        Utils.GlobalSettings.lastTrackId = track;
        this.destroy();
        this.game.state.start('gamescreen');
    },
    getTrackPage: function(page) {
        this.nameGen.setSeed(page);
        var request = $.ajax({
            url: "http://www.gamepyong.com/skylegend/php/skylegend.php",
            type: "POST",
            data: {
                action: 'gettrackpage',
                page: page
            }
        });
        request.done(function(msg) {
            if (this.trackLabels && this.trackNames && this.trackBossNames && this.trackBossTimes && msg && msg.length > 0) {
                var part1 = msg.split('#');
                if (part1 && part1.length === 5) {
                    var part2, name, time;
                    for (var i = 0; i < part1.length; i++) {
                        part2 = part1[i].split(',');
                        if (part2 && part2.length === 2) {
                            name = part2[0];
                            time = part2[1];
                            if (name.length === 0) {
                                name = 'unknown';
                            }
                            if (time.length === 0) {
                                time = '???';
                            }
                            this.trackLabels[i].content = '' + ((page * 5) + (i + 1));
                            this.trackNames[i].content = '' + this.nameGen.getWord(6);
                            this.trackBossNames[i].setText(name);
                            this.trackBossTimes[i].content = time;
                        }
                    }
                }
            }
        }.bind(this));
        request.fail(function(jqXHR, textStatus) {
            if (this.trackLabels && this.trackNames && this.trackBossNames && this.trackBossTimes) {
                for (var i = 0; i < 5; i++) {
                    this.trackLabels[i].content = '' + ((page * 5) + (i + 1));
                    this.trackNames[i].content = '' + this.nameGen.getWord(6);
                    this.trackBossNames[i].setText('unknown');
                    this.trackBossTimes[i].content = '???';
                }
            }
        }.bind(this));
    },
    playClick: function() {
        if (this.playButton.alpha === 1) {
            if (!Utils.GlobalSettings.muted) {
                this.clickSnd.play();
            }
            this.playButton.alpha = 0.5;
            this.playButton.visible = false;
            this.playLabel.visible = false;
            this.trackGroup.visible = true;
            this.getTrackPage(this.trackPage);
        }
    },
    soundClick: function() {
        Utils.GlobalSettings.muted = !Utils.GlobalSettings.muted;
        if (!Utils.GlobalSettings.muted) {
            this.clickSnd.play();
            this.soundButton.setFrames(5, 4, 5);
            this.bgm.restart('', 0, 0.1, true);
        } else {
            this.soundButton.setFrames(7, 6, 7);
            this.bgm.pause();
        }
    },
    creditDone: function() {
        if (this.credits) {
            this.creditIndex++;
            if (this.creditIndex >= this.credits.length) this.creditIndex = 0;
            this.creditText.setText(this.credits[this.creditIndex]);
            this.creditText.x = this.game.width + 1000;
            this.creditText.y = this.game.height - 60;
        }
    },
    update: function() {},
    destroy: function() {
        this.credits = null;
        this.creditText = null;
        if (this.creditTween) {
            this.creditTween.onComplete.removeAll();
            this.creditTween.stop();
            this.creditTween = null;
        }
        this.soundButton = null;
        if (this.clickSnd) {
            this.clickSnd.stop();
            this.clickSnd = null;
        }
        if (this.bg) {
            this.bg.destroy();
            this.bg = null;
        }
        this.titleLabel1 = null;
        this.titleLabel2 = null;
        if (this.dragon) {
            this.dragon.destroy();
            this.dragon = null;
        }
        this.playButton = null;
        this.playLabel = null;
        this.trackButtonsLabel = null;
        this.backing.destroy();
        this.backing = null;
        this.nameGen = null;
        this.trackbuttons = null;
        this.trackLabels = null;
        this.trackNames = null;
        this.trackBossNames = null;
        this.trackBossTimes = null;
        this.trackPagePrev = null;
        this.trackPageNext = null;
        this.trackPageLabel = null;
        this.trackGroup.destroy();
        this.trackGroup = null;
        if (this.bgm) {
            this.bgm.stop();
            this.bgm = null;
        }
        console.log('destroy mainmenu');
    }
};﻿Screen.GameScreen = function(game) {
    this.game = game;
    this.loading = false;
};
Screen.GameScreen.prototype = {
    create: function() {
        this.loading = true;
        this.bgm = this.game.add.audio('bgm2');
        this.dripSnd = this.game.add.audio('drip');
        if (!Utils.GlobalSettings.muted) {
            this.bgm.play('', 0, 0.2, true);
        }
        this.database = new DataStorage.Database('gamepyong_skyleg');
        if (this.supportsLocalStorage()) {
            localStorage["skyleg.firstTime"] = 1;
        } else {
            this.database.save('firstTime', '1', null);
        }
        this.loadingText = this.game.add.bitmapText(10, 60, 'Loading..', {
            font: '60px Desyrel',
            align: 'left'
        });
        this.racingSim = new Sim.RacingSim(this.game, this);
        this.racingSim.init();
        this.trackManager = new Sim.TrackManager(this.game, this, this.racingSim);
        this.trackManager.resetRoad();
        this.getTrackBoss();
    },
    supportsLocalStorage: function() {
        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
            return false;
        }
    },
    getTrackBoss: function() {
        var request = $.ajax({
            url: "http://www.gamepyong.com/skylegend/php/skylegend.php",
            type: "POST",
            data: {
                action: 'getbossscore',
                track: Utils.GlobalSettings.lastTrackId
            }
        });
        request.done(function(msg) {
            if (this.racingSim && msg && msg.length > 0) {
                var part = msg.split(',');
                if (part && part.length === 2) {
                    this.racingSim.bestText.setText('Best: ' + part[1] + '\n[' + part[0] + ']');
                }
            }
        }.bind(this));
        request.fail(function(jqXHR, textStatus) {}.bind(this));
    },
    onLoaded: function() {
        this.loading = false;
        this.loadingText.visible = false;
    },
    onGameOver: function() {
        this.destroy();
        this.game.state.start('scorescreen');
    },
    destroy: function() {
        if (this.racingSim) {
            this.racingSim.destroy();
            this.racingSim = null;
        }
        this.trackManager = null;
        this.loadingText = null;
        if (this.bgm) {
            this.bgm.stop();
            this.bgm = null;
        }
        if (this.dripSnd) {
            this.dripSnd.stop();
            this.dripSnd = null;
        }
        console.log('destroy gamescreen');
    },
    update: function() {
        if (!this.loading && this.racingSim) {
            this.racingSim.update();
        }
    },
    render: function() {
        if (!this.loading && this.racingSim) {
            this.racingSim.render();
        }
    }
};﻿Screen.ScoreScreen = function(game) {
    this.game = game;
};
Screen.ScoreScreen.prototype = {
    create: function() {
        this.dripSnd = this.game.add.audio('drip');
        this.clickSnd = this.game.add.audio('click');
        this.nickname = Utils.GlobalSettings.lastNickname;
        this.bg = this.game.add.sprite(0, 0, 'flag');
        this.scoreForm = new UI.SubmitScoreForm(this.game, this);
        this.scoreForm.init();
    },
    destroy: function() {
        console.log('destroy scorescreen');
    },
    update: function() {},
};