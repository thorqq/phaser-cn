﻿
var Utils = {};
Utils.GlobalSettings = {
    namespace: 'ints',
    width: 640,
    height: 960,
    aspectX: 1,
    aspectY: 1,
    muted: false,
    lastNickname: '',
    lastRound: 0,
    firstTime: true,
    isMobile: false,
    continuing: false,
    text: null
};
Utils.BackgroundMusic = {
    load: function(game) {}
};
Utils.MersenneTwister = function(seed) {
    'use strict';
    if (typeof seed === 'undefined') {
        seed = new Date().getTime();
    }
    this.MAX_INT = 4294967296.0;
    this.N = 624;
    this.M = 397;
    this.UPPER_MARK = 0x80000000;
    this.LOWER_MASK = 0x7fffffff;
    this.MATRIX_A = 0x9908b0df;
    this.mt = new Array(this.N);
    this.mti = this.N + 1;
    this.seed(seed);
};
Utils.MersenneTwister.prototype = {
    seed: function(seed) {
        var s;
        this.mt[0] = seed >>> 0;
        for (this.mti = 1; this.mti < this.N; this.mti++) {
            s = this.mt[this.mti - 1] ^ (this.mt[this.mti - 1] >>> 30);
            this.mt[this.mti] = (((((s & 0xffff0000) >>> 16) * 1812433253) << 16) + (s & 0x0000ffff) * 1812433253) + this.mti;
            this.mt[this.mti] >>>= 0;
        }
    },
    seedArray: function(vector) {
        var i = 1,
            j = 0,
            k = this.N > vector.length ? this.N : vector.length,
            s;
        this.seed(19650218);
        for (; k > 0; k--) {
            s = this.mt[i - 1] ^ (this.mt[i - 1] >>> 30);
            this.mt[i] = (this.mt[i] ^ (((((s & 0xffff0000) >>> 16) * 1664525) << 16) + ((s & 0x0000ffff) * 1664525))) + vector[j] + j;
            this.mt[i] >>>= 0;
            i++;
            j++;
            if (i >= this.N) {
                this.mt[0] = this.mt[this.N - 1];
                i = 1;
            }
            if (j >= vector.length) {
                j = 0;
            }
        }
        for (k = this.N - 1; k; k--) {
            s = this.mt[i - 1] ^ (this.mt[i - 1] >>> 30);
            this.mt[i] = (this.mt[i] ^ (((((s & 0xffff0000) >>> 16) * 1566083941) << 16) + (s & 0x0000ffff) * 1566083941)) - i;
            this.mt[i] >>>= 0;
            i++;
            if (i >= this.N) {
                this.mt[0] = this.mt[this.N - 1];
                i = 1;
            }
        }
        this.mt[0] = 0x80000000;
    },
    int: function() {
        var y, kk, mag01 = new Array(0, this.MATRIX_A);
        if (this.mti >= this.N) {
            if (this.mti === this.N + 1) {
                this.seed(5489);
            }
            for (kk = 0; kk < this.N - this.M; kk++) {
                y = (this.mt[kk] & this.UPPER_MARK) | (this.mt[kk + 1] & this.LOWER_MASK);
                this.mt[kk] = this.mt[kk + this.M] ^ (y >>> 1) ^ mag01[y & 1];
            }
            for (; kk < this.N - 1; kk++) {
                y = (this.mt[kk] & this.UPPER_MARK) | (this.mt[kk + 1] & this.LOWER_MASK);
                this.mt[kk] = this.mt[kk + (this.M - this.N)] ^ (y >>> 1) ^ mag01[y & 1];
            }
            y = (this.mt[this.N - 1] & this.UPPER_MARK) | (this.mt[0] & this.LOWER_MASK);
            this.mt[this.N - 1] = this.mt[this.M - 1] ^ (y >>> 1) ^ mag01[y & 1];
            this.mti = 0;
        }
        y = this.mt[this.mti++];
        y ^= (y >>> 11);
        y ^= (y << 7) & 0x9d2c5680;
        y ^= (y << 15) & 0xefc60000;
        y ^= (y >>> 18);
        return y >>> 0;
    },
    int31: function() {
        return this.int() >>> 1;
    },
    real: function() {
        return this.int() * (1.0 / (this.MAX_INT - 1));
    },
    realInRange: function(min, max) {
        min = min || 0;
        max = max || 0;
        return this.real() * (max - min) + min;
    },
    integerInRange: function(min, max) {
        return Math.floor(this.realInRange(min, max));
    },
    realx: function() {
        return (this.int() + 0.5) * (1.0 / this.MAX_INT);
    },
    rnd: function() {
        return this.int() * (1.0 / this.MAX_INT);
    },
    rndHiRes: function() {
        var a = this.int() >>> 5,
            b = this.int() >>> 6;
        return (a * 67108864.0 + b) * (1.0 / 9007199254740992.0);
    }
};﻿
var DataStorage = {};
DataStorage.Database = function(syscookie) {
    this.systemcookie = syscookie;
};
DataStorage.Database.prototype = {
    save: function(key, value, dateOffset) {
        var date = new Date();
        date.setTime(date.getTime() + ((dateOffset ? dateOffset : 365 * 10) * 24 * 60 * 60 * 1000));
        document.cookie = this.systemcookie + "~" + key + "=" + value + "; expires=" + date.toUTCString() + "; path=/";
    },
    load: function(key) {
        var nameeq = this.systemcookie + "~" + key + "=";
        var ca = document.cookie.split(";");
        var rt;
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameeq) == 0) {
                rt = c.substring(nameeq.length, c.length);
                return rt;
            }
        }
        return null;
    }
};
DataStorage.supportsLocalStorage = function() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
};
DataStorage.GlobalSettings = {
    database: new DataStorage.Database('gamepyong_' + Utils.GlobalSettings.namespace)
};﻿ (function(cocoonjsphaser) {
    cocoonjsphaser.utils = {
        fixDOMParser: function() {
            window.DOMParser = DOMishParser;
        }
    };

    function DOMishParser() {}
    DOMishParser.prototype.parseFromString = function(data) {
        return new DOMishObject(JSON.parse(data));
    };

    function DOMishAttributes() {}
    DOMishAttributes.prototype.getNamedItem = function(name) {
        return {
            nodeValue: this[name] || null
        };
    };

    function makeDOMishObject(data) {
        return new DOMishObject(data);
    }

    function DOMishObject(data) {
        this.attributes = this.convertContent(data);
        this.length = Object.keys(this.attributes).length;
    }
    DOMishObject.prototype.documentElement = document;
    DOMishObject.prototype.convertContent = function(obj) {
        var attributes = new DOMishAttributes(),
            prop;
        for (prop in obj) {
            if (obj[prop] !== null && typeof obj[prop] === 'object') {
                attributes[prop] = Array.isArray(obj[prop]) ? obj[prop].map(makeDOMishObject) : new DOMishObject(obj[prop]);
            } else {
                attributes[prop] = obj[prop];
            }
        }
        return attributes;
    };
    DOMishObject.prototype.getElementsByTagName = function(name) {
        return this.attributes[name] ? Array.isArray(this.attributes[name]) ? this.attributes[name] : [this.attributes[name]] : [];
    };
    DOMishObject.prototype.getAttribute = function(name) {
        return this.attributes.getNamedItem(name).nodeValue;
    };
}(window.cocoonjsphaser = window.cocoonjsphaser || {}));﻿Utils.AdManager = {
    bannerParams: {
        "bannerAdUnit": "fa1dce6ae938471eb279c897728decbf",
        "refresh": 20
    },
    banner: null,
    alignTop: false,
    loaded: false,
    visible: true,
    load: function() {
        try {
            this.banner = CocoonJS.Ad.createBanner(this.bannerParams);
            if (this.banner) {
                this.banner.onBannerShown.addEventListener(function() {}.bind(this));
                this.banner.onBannerHidden.addEventListener(function() {}.bind(this));
                this.banner.onBannerReady.addEventListener(function(width, height) {
                    this.loaded = true;
                    var dips = window.devicePixelRatio;
                    var x = (window.innerWidth * dips) / 2 - width / 2;
                    var y = 0;
                    if (!this.alignTop) {
                        y = (window.innerHeight * dips) - height;
                    }
                    var rect = new CocoonJS.Ad.Rectangle(x, y, width, height);
                    this.banner.setRectangle(rect);
                    if (this.visible) {
                        this.banner.showBanner();
                    }
                }.bind(this));
                this.banner.refreshBanner();
            }
        } catch (e) {
            console.log(e);
        }
    },
    show: function() {
        this.visible = true;
        if (this.banner) {
            this.banner.showBanner();
        }
    },
    hide: function() {
        this.visible = false;
        if (this.banner) {
            this.banner.hideBanner();
        }
    }
};﻿
var UI = {};
UI.SubmitNameForm = function(game, state) {
    this.game = game;
    this.state = state;
    this.letterArray = [];
    this.backing = null;
    this.nicknameText = null;
    this.headingText = null;
};
UI.SubmitNameForm.prototype = {
    init: function() {
        this.backing = this.game.add.sprite(0, 0, 'backing');
        this.backing.alpha = 0;
        this.backing.width = Utils.GlobalSettings.width;
        this.backing.height = Utils.GlobalSettings.height;
        var tweenBacking = this.game.add.tween(this.backing).to({
            alpha: 0.8
        }, 1000, Phaser.Easing.Sinusoidal.Out, true);
        this.headingText = this.game.add.bitmapText(10, 300, 'mecha', 'Please enter your nickname:', 50);
        this.headingText.updateTransform();
        this.headingText.position.x = Utils.GlobalSettings.width / 2 - this.headingText.textWidth / 2;
        this.nicknameText = this.game.add.bitmapText(40, 400, 'mecha', Utils.GlobalSettings.lastNickname, 70);
        this.nicknameText.updateTransform();
        this.nicknameText.position.x = Utils.GlobalSettings.width / 2 - this.nicknameText.textWidth / 2;
        this.goButton = this.game.add.button(Utils.GlobalSettings.width - 140, 400, 'go', null, this, 0, 0, 0);
        this.goButton.visible = false;
        if (Utils.GlobalSettings.lastNickname.trim().length > 0) this.goButton.visible = true;
        this.goButton.events.onInputDown.addOnce(this.goClick.bind(this));
        var offs = 0;
        var offsi = 9;
        var xp = 10;
        var yp = 550;
        for (var i = 0; i < offsi; i++) {
            var letterButton = this.game.add.button(xp + (i * 70), yp, 'letters', this.letterPressed.bind(this, i), this, i, i, i);
            this.letterArray.push(letterButton);
            letterButton = this.game.add.button(xp + (i * 70), yp + 75, 'letters', this.letterPressed.bind(this, i + offsi), this, i + offsi, i + offsi, i + offsi);
            this.letterArray.push(letterButton);
            if (i + (offsi * 2) < 27) {
                letterButton = this.game.add.button(xp + (i * 70), yp + (75 * 2), 'letters', this.letterPressed.bind(this, i + (offsi * 2)), this, i + (offsi * 2), i + (offsi * 2), i + (offsi * 2));
                this.letterArray.push(letterButton);
            }
        }
    },
    letterPressed: function(letterNum) {
        if (!Utils.GlobalSettings.muted) {
            this.state.dripSnd.play();
        }
        var letter = '';
        switch (letterNum) {
        case 0:
            letter = 'a';
            break;
        case 1:
            letter = 'b';
            break;
        case 2:
            letter = 'c';
            break;
        case 3:
            letter = 'd';
            break;
        case 4:
            letter = 'e';
            break;
        case 5:
            letter = 'f';
            break;
        case 6:
            letter = 'g';
            break;
        case 7:
            letter = 'h';
            break;
        case 8:
            letter = 'i';
            break;
        case 9:
            letter = 'j';
            break;
        case 10:
            letter = 'k';
            break;
        case 11:
            letter = 'l';
            break;
        case 12:
            letter = 'm';
            break;
        case 13:
            letter = 'n';
            break;
        case 14:
            letter = 'o';
            break;
        case 15:
            letter = 'p';
            break;
        case 16:
            letter = 'q';
            break;
        case 17:
            letter = 'r';
            break;
        case 18:
            letter = 's';
            break;
        case 19:
            letter = 't';
            break;
        case 20:
            letter = 'u';
            break;
        case 21:
            letter = 'v';
            break;
        case 22:
            letter = 'w';
            break;
        case 23:
            letter = 'x';
            break;
        case 24:
            letter = 'y';
            break;
        case 25:
            letter = 'z';
            break;
        }
        if (letterNum === 26) {
            if (Utils.GlobalSettings.lastNickname.length > 0) {
                Utils.GlobalSettings.lastNickname = Utils.GlobalSettings.lastNickname.substring(0, Utils.GlobalSettings.lastNickname.length - 1);
                this.nicknameText.setText(Utils.GlobalSettings.lastNickname);
                this.nicknameText.updateTransform();
                this.nicknameText.position.x = Utils.GlobalSettings.width / 2 - this.nicknameText.textWidth / 2;
            }
        } else {
            if (Utils.GlobalSettings.lastNickname.length < 5) {
                Utils.GlobalSettings.lastNickname += letter;
                this.nicknameText.setText(Utils.GlobalSettings.lastNickname);
                this.nicknameText.updateTransform();
                this.nicknameText.position.x = Utils.GlobalSettings.width / 2 - this.nicknameText.textWidth / 2;
            }
        }
        if (this.nicknameText.text.trim().length > 0) this.goButton.visible = true;
        else this.goButton.visible = false;
    },
    goClick: function() {
        if (!Utils.GlobalSettings.muted) {
            this.state.clickSnd.play();
        }
        this.goButton.events.onInputDown.removeAll();
        this.game.state.start('gamescreen');
    },
    destroy: function() {
        if (this.letterArray) {
            for (var i = 0; i < this.letterArray.length; i++) {
                if (this.letterArray[i]) {
                    this.letterArray[i].destroy();
                    this.letterArray[i] = null;
                }
            }
            this.letterArray = null;
        }
        if (this.backing) {
            this.backing.destroy();
            this.backing = null;
        }
        this.nicknameText = null;
        this.headingText = null;
        if (this.goButton) {
            this.goButton.kill();
            this.goButton = null;
        }
        console.log('destroy submitscoreform');
    }
};﻿
var Screen = {};
Screen.Boot = function(game) {
    this.game = game;
};
Screen.Boot.prototype = {
    getRatio: function(type, w, h) {
        var width = navigator.isCocoonJS ? window.innerWidth : w,
            height = navigator.isCocoonJS ? window.innerHeight : h;
        var dips = window.devicePixelRatio;
        width = width * dips;
        height = height * dips;
        var scaleX = width / w,
            scaleY = height / h,
            result = {
                x: 1,
                y: 1
            };
        switch (type) {
        case 'all':
            result.x = scaleX > scaleY ? scaleY : scaleX;
            result.y = scaleX > scaleY ? scaleY : scaleX;
            break;
        case 'fit':
            result.x = scaleX > scaleY ? scaleX : scaleY;
            result.y = scaleX > scaleY ? scaleX : scaleY;
            break;
        case 'fill':
            result.x = scaleX;
            result.y = scaleY;
            break;
        }
        return result;
    },
    setupScaling: function() {
        if (navigator.isCocoonJS) {
            var ratio = this.getRatio('fill', Utils.GlobalSettings.width, Utils.GlobalSettings.height);
            this.game.world.scale.x = ratio.x;
            this.game.world.scale.y = ratio.y;
            this.game.world.updateTransform();
            this.game.world.setBounds(0, 0, Utils.GlobalSettings.width, Utils.GlobalSettings.height);
            this.game.world.camera.bounds.setTo(0, 0, Utils.GlobalSettings.width, Utils.GlobalSettings.height);
        } else {
            this.game.stage.disableVisibilityChange = true;
            if (this.game.device.desktop) {
                this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.game.scale.pageAlignHorizontally = true;
                this.game.scale.pageAlignVertically = true;
                this.game.scale.setScreenSize(true);
            } else {
                this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.game.scale.pageAlignHorizontally = true;
                this.game.scale.pageAlignVertically = true;
                this.game.scale.forceOrientation(false, true, 'orientation');
                this.game.scale.hasResized.add(this.gameResized, this);
                this.game.scale.setScreenSize(true);
            }
            this.game.world.setBounds(0, 0, Utils.GlobalSettings.width, Utils.GlobalSettings.height);
        }
    },
    gameResized: function(width, height) {},
    preload: function() {
        this.game.load.image('loaderFull', 'assets/loadingFull.png');
        this.game.load.image('loaderEmpty', 'assets/loadingEmpty.png');
        this.game.load.image('orientation', 'assets/orientation.jpg');
    },
    create: function() {
        this.setupScaling();
        if (this.game.device.desktop) {
            Utils.GlobalSettings.isMobile = false;
        } else {
            Utils.GlobalSettings.isMobile = true;
        }
        this.game.state.start('preloader');
    }
};﻿Screen.Preloader = function(game) {
    this.game = game;
};
Screen.Preloader.prototype = {
    loaded: false,
    ready: false,
    preload: function() {
        this.loaderEmpty = this.game.add.sprite(0, 0, 'loaderEmpty');
        this.loaderEmpty.x = Utils.GlobalSettings.width / 2 - this.loaderEmpty.width / 2;
        this.loaderEmpty.y = Utils.GlobalSettings.height / 2 - this.loaderEmpty.height / 2;
        this.loaderFull = this.game.add.sprite(0, 0, 'loaderFull');
        this.loaderFull.x = Utils.GlobalSettings.width / 2 - this.loaderFull.width / 2;
        this.loaderFull.y = Utils.GlobalSettings.height / 2 - this.loaderFull.height / 2;
        this.game.load.image('logo', 'assets/logo.png');
        this.game.load.image('phaserlogo', 'assets/phaserlogo.png');
        this.game.load.image('backing', 'assets/backing.png');
        this.game.load.image('add', 'assets/add.png');
        this.game.load.image('minus', 'assets/minus.png');
        this.game.load.image('backspace', 'assets/backspace.png');
        this.game.load.image('clear', 'assets/clear.png');
        this.game.load.image('check', 'assets/check.png');
        this.game.load.image('bar', 'assets/bar.png');
        this.game.load.image('bg', 'assets/bg.png');
        this.game.load.image('bg2', 'assets/bg2.png');
        this.game.load.image('start', 'assets/start.png');
        this.game.load.image('go', 'assets/go.png');
        this.game.load.image('tutorial', 'assets/tutorial.png');
        this.game.load.image('panel', 'assets/panel.png');
        this.game.load.image('next', 'assets/next.png');
        this.game.load.spritesheet('systembuttons', 'assets/systembuttons.png', 64, 64);
        this.game.load.spritesheet('systembuttons2', 'assets/systembuttons2.png', 50, 50);
        this.game.load.spritesheet('numbers', 'assets/nums.png', 128, 128);
        this.game.load.spritesheet('title', 'assets/title.png', 128, 128);
        this.game.load.spritesheet('letters', 'assets/letters.png', 64, 64);
        this.game.load.spritesheet('arrow', 'assets/arrow.png', 81, 64);
        this.game.load.audio('gp', ['assets/audio/gp.ogg']);
        this.game.load.audio('click', ['assets/audio/click.ogg']);
        this.game.load.audio('drip', ['assets/audio/drip.ogg']);
        this.game.load.audio('complete', ['assets/audio/complete.ogg']);
        this.game.load.audio('tap', ['assets/audio/zipclick.ogg']);
        this.game.load.bitmapFont('desyrel', 'assets/fonts/desyrel.png', 'assets/fonts/desyrel' + (navigator.isCocoonJS ? '.json' : '.xml'));
        this.game.load.bitmapFont('mecha', 'assets/fonts/mecha_0.png', 'assets/fonts/mecha' + (navigator.isCocoonJS ? '.json' : '.xml'));
        this.game.load.bitmapFont('calculator', 'assets/fonts/calculator_0.png', 'assets/fonts/calculator' + (navigator.isCocoonJS ? '.json' : '.xml'));
        this.game.load.text('text', 'assets/text.json');
        this.game.load.onFileComplete.add(this.fileLoaded, this);
        this.game.load.setPreloadSprite(this.loaderFull);
        this.game.add.sprite(0, 0, '');
    },
    fileLoaded: function(progress, cacheID, success, filesLoaded, totalFiles) {
        if (filesLoaded === totalFiles && !this.loaded) {
            this.loaded = true;
        }
    },
    update: function() {
        if (!this.ready && this.loaded) {
            this.ready = true;
            Utils.GlobalSettings.text = JSON.parse(this.game.cache.getText('text'));
            Utils.BackgroundMusic.load(this.game);
            this.game.state.start('splashscreen');
        }
    },
    shutdown: function() {
        if (this.loaderEmpty) {
            this.loaderEmpty.destroy();
            this.loaderEmpty = null;
        }
        if (this.loaderFull) {
            this.loaderFull.destroy();
            this.loaderFull = null;
        }
    }
};﻿Screen.SplashScreen = function(game) {
    this.game = game;
};
Screen.SplashScreen.prototype = {
    create: function() {
        this.gp = this.game.add.audio('gp');
        this.gp.play();
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.physics.arcade.gravity.y = 300;
        this.phaserLogo = this.game.add.sprite(0, 0, 'phaserlogo');
        this.phaserLogo.x = 0;
        this.phaserLogo.y = 0;
        this.phaserLogo.anchor.setTo(0.5, 0.5);
        this.game.physics.enable(this.phaserLogo, Phaser.Physics.ARCADE);
        this.phaserLogo.body.acceleration.x = 200;
        this.phaserLogo.body.angularVelocity = 10;
        this.phaserLogo.body.angularAcceleration = 200;
        this.phaserLogo.body.collideWorldBounds = false;
        this.logo = this.game.add.sprite(0, 0, 'logo');
        this.logo.x = Utils.GlobalSettings.width / 2 - this.logo.width / 2;
        this.logo.y = Utils.GlobalSettings.height / 2 - this.logo.height / 2;
        this.logo.scale.x = 20;
        this.logo.scale.y = 20;
        this.tween = this.game.add.tween(this.logo.scale).to({
            x: 1,
            y: 1
        }, 2000, Phaser.Easing.Sinusoidal.Out, true);
        this.tween2 = this.game.add.tween(this.logo).to({
            alpha: 0
        }, 1000);
        this.tween.chain(this.tween2);
        this.tween2.onComplete.add(this.done.bind(this));
        this.game.add.sprite(0, 0, '');
    },
    done: function() {
        if (!Utils.GlobalSettings.muted) {}
        this.game.state.start('mainmenu');
    },
    update: function() {},
    shutdown: function() {
        if (this.gp) {
            this.gp.stop();
            this.gp = null;
        }
        if (this.phaserLogo) {
            this.phaserLogo.kill();
            this.phaserLogo = null;
        }
        if (this.logo) {
            this.logo.kill();
            this.logo = null;
        }
        if (this.tween) {
            this.tween.stop();
            this.tween = null;
        }
        if (this.tween2) {
            this.tween2.onComplete.removeAll();
            this.tween2.stop();
            this.tween2 = null;
        }
    }
};﻿Screen.MainMenu = function(game) {
    this.game = game;
};
Screen.MainMenu.prototype = {
    create: function() {
        this.setup();
    },
    setup: function() {
        if (DataStorage.supportsLocalStorage()) {
            if (localStorage[Utils.GlobalSettings.namespace + ".firstTime"] && localStorage[Utils.GlobalSettings.namespace + ".firstTime"] === '1') {
                Utils.GlobalSettings.firstTime = false;
            }
        } else {
            var data = DataStorage.GlobalSettings.database.load('firstTime');
            if (data === '1') {
                Utils.GlobalSettings.firstTime = false;
            }
        }
        this.loadGame();
        if (!Utils.GlobalSettings.lastNickname) {
            Utils.GlobalSettings.lastNickname = '';
        }
        if (!Utils.GlobalSettings.lastRound) {
            Utils.GlobalSettings.lastRound = 0;
        }
        this.clickSnd = this.game.add.audio('click');
        this.dripSnd = this.game.add.audio('drip');
        this.bg = this.game.add.image(0, 0, 'bg2');
        this.game.physics.arcade.gravity.y = 0;
        this.emitter = this.game.add.emitter(Utils.GlobalSettings.width / 2, -32, 50);
        this.emitter.makeParticles('numbers', [0, 1, 2, 3, 4, 5, 6, 7, 8]);
        this.emitter.maxParticleScale = 0.6;
        this.emitter.minParticleScale = 0.2;
        this.emitter.setYSpeed(20, 100);
        this.emitter.gravity = 0;
        this.emitter.width = Utils.GlobalSettings.width * 1.5;
        this.emitter.minRotation = 0;
        this.emitter.maxRotation = 40;
        this.emitter.start(false, 15000, 800);
        this.roundSelected = Utils.GlobalSettings.lastRound;
        this.title = [];
        for (var i = 0; i < 4; i++) {
            this.title.push(this.game.add.sprite(64 + (128 * i), -128, 'title'));
            this.title[i].frame = i;
            this.game.add.tween(this.title[i]).to({
                y: 150
            }, 1000, Phaser.Easing.Bounce.Out, true, i * 200);
        }
        this.startButton = this.game.add.button(Utils.GlobalSettings.width / 2 - 124, Utils.GlobalSettings.height - 250, 'start', null, this, 0, 0, 0);
        this.startButton.events.onInputDown.addOnce(this.playClick.bind(this));
        this.currentRoundLabel = this.game.add.bitmapText(Utils.GlobalSettings.width / 2 - 100, Utils.GlobalSettings.height - 190, 'mecha', 'Current Round: ' + (Utils.GlobalSettings.lastRound + 1), 34);
        this.currentRoundLabel.updateTransform();
        this.currentRoundLabel.position.x = Utils.GlobalSettings.width / 2 - this.currentRoundLabel.textWidth / 2;
        this.credits = ["Code - Hsaka", "Made with - Phaser [phaser.io]", "UI Graphics - Kenney [www.kenney.nl]", "UI Graphics - GameArtForge"];
        this.creditIndex = 0;
        this.creditText = this.game.add.bitmapText(0, 0, 'mecha', this.credits[this.creditIndex], 40);
        this.creditText.x = Utils.GlobalSettings.width + 1000;
        this.creditText.y = Utils.GlobalSettings.height - 100;
        this.creditTween = this.game.add.tween(this.creditText).to({
            x: 0
        }, 2000, Phaser.Easing.Sinusoidal.Out, true).to({
            y: Utils.GlobalSettings.height - 70
        }, 1000, Phaser.Easing.Sinusoidal.In).to({
            y: Utils.GlobalSettings.height + 100
        }, 500, Phaser.Easing.Back.In).loop();
        this.creditTween._lastChild.onComplete.add(this.creditDone.bind(this));
        this.nameForm = new UI.SubmitNameForm(this.game, this);
        this.soundButton = null;
        if (!Utils.GlobalSettings.muted) {
            this.soundButton = this.game.add.button(Utils.GlobalSettings.width - 60, 120, 'systembuttons2', null, this, 5, 4, 5);
        } else {
            this.soundButton = this.game.add.button(Utils.GlobalSettings.width - 60, 120, 'systembuttons2', null, this, 7, 6, 7);
        }
        this.soundButton.events.onInputDown.addOnce(this.soundClick.bind(this));
        this.setupLeaderboard();
        this.stopMusicOnLoad = false;
        if (navigator.isCocoonJS) {
            if (Utils.AdManager.loaded) {
                Utils.AdManager.alignTop = true;
                Utils.AdManager.show();
            } else {
                Utils.AdManager.alignTop = true;
                Utils.AdManager.load();
            }
            CocoonJS.App.setAppShouldFinishCallback(this.exit.bind(this));
            CocoonJS.App.onTextDialogFinished.addEventListener(this.confirmDialog.bind(this));
            CocoonJS.App.onTextDialogCancelled.addEventListener(this.cancelDialog.bind(this));
        }
        this.game.add.sprite(0, 0, '');
    },
    loadGame: function() {
        if (DataStorage.supportsLocalStorage()) {
            this.loadGameLocalStorage();
        } else {
            this.loadGameCookie();
        }
    },
    loadGameCookie: function() {
        var data = DataStorage.GlobalSettings.database.load('save');
        if (data && data.length > 0) {
            var loadedData = data.split('#');
            if (loadedData && loadedData.length === 2) {
                Utils.GlobalSettings.lastNickname = loadedData[0];
                Utils.GlobalSettings.lastRound = parseInt(loadedData[1]);
            }
        }
    },
    loadGameLocalStorage: function() {
        var loadedData = [];
        loadedData.push(localStorage[Utils.GlobalSettings.namespace + ".nickname"]);
        loadedData.push(localStorage[Utils.GlobalSettings.namespace + ".round"]);
        if (loadedData && loadedData.length === 2) {
            Utils.GlobalSettings.lastNickname = loadedData[0];
            Utils.GlobalSettings.lastRound = parseInt(loadedData[1]);
        }
    },
    exit: function() {
        return true;
    },
    setupLeaderboard: function() {
        this.leaderboardGroup = this.game.add.group();
        this.backing = this.game.add.image(Utils.GlobalSettings.width / 2 - 170, Utils.GlobalSettings.height - 610, 'backing');
        this.backing.alpha = 0;
        this.backing.width = 340;
        this.backing.height = 310;
        this.leaderboardGroup.add(this.backing);
        var tweenBacking = this.game.add.tween(this.backing).to({
            alpha: 0.6
        }, 1000, Phaser.Easing.Sinusoidal.Out, true);
        this.roundLabel = this.game.add.bitmapText(Utils.GlobalSettings.width / 2 - 100, Utils.GlobalSettings.height - 670, 'mecha', 'Round: ' + (this.roundSelected + 1), 34);
        this.roundLabel.updateTransform();
        this.roundLabel.position.x = Utils.GlobalSettings.width / 2 - this.roundLabel.textWidth / 2;
        this.nameLabel = this.game.add.bitmapText(Utils.GlobalSettings.width / 2 - 110, Utils.GlobalSettings.height - 640, 'mecha', 'Name', 34);
        this.scoreLabel = this.game.add.bitmapText(Utils.GlobalSettings.width / 2 + 50, Utils.GlobalSettings.height - 640, 'mecha', 'Time', 34);
        this.leaderboardGroup.add(this.nameLabel);
        this.leaderboardGroup.add(this.scoreLabel);
        this.updateButton = this.game.add.button(Utils.GlobalSettings.width - 170, Utils.GlobalSettings.height - 660, 'systembuttons2', this.updateClick, this, 3, 2, 3);
        this.leaderboardGroup.add(this.updateButton);
        this.updateTween = this.game.add.tween(this.updateButton).to({
            alpha: 1
        }, 5000, Phaser.Easing.Sinusoidal.Out, false);
        this.leftArrowButton = this.game.add.button(Utils.GlobalSettings.width / 2 - 260, Utils.GlobalSettings.height - 500, 'arrow', this.leftArrowClick, this, 0, 0, 0);
        this.rightArrowButton = this.game.add.button(Utils.GlobalSettings.width / 2 + 179, Utils.GlobalSettings.height - 500, 'arrow', this.rightArrowClick, this, 1, 1, 1);
        this.rightArrowButton.visible = false;
        if (this.roundSelected === 0) {
            this.leftArrowButton.visible = false;
        }
        this.canClickArrow = true;
        this.names = [];
        this.scores = [];
        for (var i = 0; i < 10; i++) {
            this.names.push(this.game.add.bitmapText(Utils.GlobalSettings.width / 2 - 100, Utils.GlobalSettings.height - 600 + (i * 30), 'mecha', '', 24));
            this.scores.push(this.game.add.bitmapText(Utils.GlobalSettings.width / 2 + 60, Utils.GlobalSettings.height - 600 + (i * 30), 'mecha', '', 24));
            this.leaderboardGroup.add(this.names[i]);
            this.leaderboardGroup.add(this.scores[i]);
        }
        this.leaderboardGroup.visible = true;
        this.updateLeaderboard();
    },
    leftArrowClick: function() {
        if (this.roundSelected > 0 && this.canClickArrow) {
            this.canClickArrow = false;
            this.roundSelected--;
            if (this.roundSelected === 0) {
                this.leftArrowButton.visible = false;
            }
            this.rightArrowButton.visible = true;
            this.roundLabel.setText('Round: ' + (this.roundSelected + 1));
            this.roundLabel.updateTransform();
            this.roundLabel.position.x = Utils.GlobalSettings.width / 2 - this.roundLabel.textWidth / 2;
            if (!Utils.GlobalSettings.muted) {
                this.clickSnd.play();
            }
            for (var i = 0; i < 10; i++) {
                this.names[i].setText('');
                this.scores[i].setText('');
                this.names[i].tint = 0xFFFFFF;
                this.scores[i].tint = 0xFFFFFF;
            }
            this.updateLeaderboard();
        }
    },
    rightArrowClick: function() {
        if (this.roundSelected < Utils.GlobalSettings.lastRound && this.canClickArrow) {
            this.canClickArrow = false;
            this.roundSelected++;
            if (this.roundSelected === Utils.GlobalSettings.lastRound) {
                this.rightArrowButton.visible = false;
            }
            this.leftArrowButton.visible = true;
            this.roundLabel.setText('Round: ' + (this.roundSelected + 1));
            this.roundLabel.updateTransform();
            this.roundLabel.position.x = Utils.GlobalSettings.width / 2 - this.roundLabel.textWidth / 2;
            if (!Utils.GlobalSettings.muted) {
                this.clickSnd.play();
            }
            for (var i = 0; i < 10; i++) {
                this.names[i].setText('');
                this.scores[i].setText('');
                this.names[i].tint = 0xFFFFFF;
                this.scores[i].tint = 0xFFFFFF;
            }
            this.updateLeaderboard();
        }
    },
    updateClick: function() {
        if (this.updateButton.alpha === 1) {
            if (!Utils.GlobalSettings.muted) {
                this.clickSnd.play();
            }
            this.updateButton.alpha = 0;
            this.updateTween.start();
            this.updateLeaderboard();
        }
    },
    updateLeaderboard: function() {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("POST", "http://www.ttafo.com/ints/php/ints.php", true);
        var parameters = "action=gethighscores&round=" + this.roundSelected;
        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                var msg = xmlHttp.responseText;
                if (msg && msg.length > 0 && this.names && this.scores && this.names.length === 10 && this.scores.length === 10) {
                    var data = msg.split('#');
                    if (data && data.length > 0) {
                        var part = null;
                        for (var i = 0; i < data.length; i++) {
                            part = data[i].split(',');
                            if (part && part.length === 3) {
                                if (part[0] === Utils.GlobalSettings.lastNickname) {
                                    this.names[i].tint = 0xFF0000;
                                    this.scores[i].tint = 0xFF0000;
                                }
                                this.names[i].setText((i + 1) + '.  ' + part[0]);
                                this.scores[i].setText('' + part[2]);
                            }
                        }
                    }
                }
                this.canClickArrow = true;
            }
        }.bind(this);
        xmlHttp.ontimeout = function() {
            this.canClickArrow = true;
        }.bind(this);
        xmlHttp.send(parameters);
    },
    playClick: function() {
        if (!Utils.GlobalSettings.muted) {
            this.clickSnd.play();
        }
        this.startButton.events.onInputDown.removeAll();
        this.startButton.events.onInputDown.addOnce(this.playClick.bind(this));
        this.startButton.visible = false;
        if (Utils.GlobalSettings.lastNickname.trim().length === 0) {
            if (navigator.isCocoonJS) {
                CocoonJS.App.showTextDialog("Ints", "Please enter your Nickname:", Utils.GlobalSettings.lastNickname, CocoonJS.App.KeyboardType.TEXT, "Cancel", "Ok");
            } else {
                this.nameForm.init();
            }
        } else {
            this.game.state.start('gamescreen');
        }
    },
    confirmDialog: function(text) {
        Utils.GlobalSettings.lastNickname = text;
        this.game.state.start('gamescreen');
    },
    cancelDialog: function() {
        this.startButton.visible = true;
    },
    soundClick: function() {
        Utils.GlobalSettings.muted = !Utils.GlobalSettings.muted;
        if (!Utils.GlobalSettings.muted) {
            this.clickSnd.play();
            this.soundButton.setFrames(5, 4, 5);
        } else {
            this.soundButton.setFrames(7, 6, 7);
        }
        this.soundButton.events.onInputDown.removeAll();
        this.soundButton.events.onInputDown.addOnce(this.soundClick.bind(this));
    },
    creditDone: function() {
        if (this.credits) {
            this.creditIndex++;
            if (this.creditIndex >= this.credits.length) this.creditIndex = 0;
            this.creditText.setText(this.credits[this.creditIndex]);
            this.creditText.x = Utils.GlobalSettings.width + 1000;
            this.creditText.y = Utils.GlobalSettings.height - 100;
        }
    },
    update: function() {},
    shutdown: function() {
        if (this.bg) {
            this.bg.destroy();
            this.bg = null;
        }
        if (this.emitter) {
            this.emitter.destroy();
            this.emitter = null;
        }
        if (this.startButton) {
            this.startButton.destroy();
            this.startButton = null;
        }
        if (this.currentRoundLabel) {
            this.currentRoundLabel.destroy();
            this.currentRoundLabel = null;
        }
        if (this.nameForm) {
            this.nameForm.destroy();
            this.nameForm = null;
        }
        if (navigator.isCocoonJS) {
            CocoonJS.App.onTextDialogFinished.removeEventListener(CocoonJS.App.onTextDialogFinished.listeners[0]);
            CocoonJS.App.onTextDialogCancelled.removeEventListener(CocoonJS.App.onTextDialogCancelled.listeners[0]);
        }
        this.credits = null;
        this.creditText = null;
        if (this.creditTween) {
            this.creditTween.onComplete.removeAll();
            this.creditTween.stop();
            this.creditTween = null;
        }
        if (this.clickSnd) {
            this.clickSnd.stop();
            this.clickSnd = null;
        }
        if (this.dripSnd) {
            this.dripSnd.stop();
            this.dripSnd = null;
        }
        if (this.soundButton) {
            this.soundButton.destroy();
            this.soundButton = null;
        }
        if (this.nameLabel) {
            this.nameLabel.destroy();
            this.nameLabel = null;
        }
        if (this.scoreLabel) {
            this.scoreLabel.destroy();
            this.scoreLabel = null;
        }
        if (this.updateButton) {
            this.updateButton.kill();
            this.updateButton = null;
        }
        if (this.updateTween) {
            this.updateTween.stop();
            this.updateTween = null;
        }
        if (this.roundLabel) {
            this.roundLabel.destroy();
            this.roundLabel = null;
        }
        if (this.leftArrowButton) {
            this.leftArrowButton.destroy();
            this.leftArrowButton = null;
        }
        if (this.rightArrowButton) {
            this.rightArrowButton.destroy();
            this.rightArrowButton = null;
        }
        for (var i = 0; i < this.names.length; i++) {
            this.names[i] = null;
            this.scores[i] = null;
        }
        this.names = null;
        this.scores = null;
        console.log('destroy mainmenu');
    }
};﻿Screen.GameScreen = function(game) {
    this.game = game;
};
Screen.GameScreen.prototype = {
    create: function() {
        if (DataStorage.supportsLocalStorage()) {
            localStorage[Utils.GlobalSettings.namespace + ".firstTime"] = 1;
        } else {
            DataStorage.GlobalSettings.database.save('firstTime', '1', null);
        }
        if (Utils.GlobalSettings.firstTime) {
            this.saveGame();
        }
        this.loadGame();
        if (!Utils.GlobalSettings.lastNickname) {
            Utils.GlobalSettings.lastNickname = '';
        }
        if (!Utils.GlobalSettings.lastRound) {
            Utils.GlobalSettings.lastRound = 0;
        }
        this.clickSnd = this.game.add.audio('click');
        this.tapSnd = this.game.add.audio('tap');
        this.completeSnd = this.game.add.audio('complete');
        this.dripSnd = this.game.add.audio('drip');
        this.gpSnd = this.game.add.audio('gp');
        this.juicy = this.game.plugins.add(new Phaser.Plugin.Juicy(this.game));
        this.bg = this.game.add.image(0, 0, 'bg');
        this.bar = this.game.add.image(0, 220, 'bar');
        this.check = this.game.add.image(Utils.GlobalSettings.width / 2, 250, 'check');
        this.check.visible = false;
        this.check.scale.setTo(0, 0);
        this.check.anchor.setTo(0.5, 0.5);
        this.checkTween = this.game.add.tween(this.check.scale).to({
            x: 1,
            y: 1
        }, 250, Phaser.Easing.Sinusoidal.Out).to({
            x: 0,
            y: 0
        }, 250, Phaser.Easing.Back.In);
        this.checkTween._lastChild.onComplete.add(this.checkTweenDone.bind(this));
        this.numbers = [];
        this.numButton = [];
        this.numButton.push(this.game.add.sprite(25, Utils.GlobalSettings.height - 400, 'numbers'));
        this.numButton.push(this.game.add.sprite(128 + 50, Utils.GlobalSettings.height - 400, 'numbers'));
        this.numButton.push(this.game.add.sprite(256 + 75, Utils.GlobalSettings.height - 400, 'numbers'));
        this.numButton.push(this.game.add.sprite(384 + 100, Utils.GlobalSettings.height - 400, 'numbers'));
        this.addButton = this.game.add.image(128 + 50, Utils.GlobalSettings.height - 200, 'add');
        this.addButton.inputEnabled = true;
        this.addButton.events.onInputDown.addOnce(this.addClick.bind(this));
        this.minusButton = this.game.add.image(256 + 75, Utils.GlobalSettings.height - 200, 'minus');
        this.minusButton.inputEnabled = true;
        this.minusButton.events.onInputDown.addOnce(this.minusClick.bind(this));
        this.backspaceButton = this.game.add.image(384 + 100 + 32, Utils.GlobalSettings.height - 200 + 32, 'backspace');
        this.backspaceButton.inputEnabled = true;
        this.backspaceButton.events.onInputDown.addOnce(this.backspaceClick.bind(this));
        this.clearButton = this.game.add.image(25 + 32, Utils.GlobalSettings.height - 200 + 32, 'clear');
        this.clearButton.inputEnabled = true;
        this.clearButton.events.onInputDown.addOnce(this.clearClick.bind(this));
        this.round = Utils.GlobalSettings.lastRound;
        this.game.rnd.sow([this.round]);
        for (var i = 0; i < this.numButton.length; i++) {
            this.numbers.push(this.game.rnd.integerInRange(1, 9));
            this.numButton[i].frame = this.numbers[i] - 1;
            this.numButton[i].inputEnabled = true;
            this.numButton[i].events.onInputDown.addOnce(this.numClick.bind(this, i));
        }
        this.answerText = this.game.add.bitmapText(Utils.GlobalSettings.width - 180, 200, 'calculator', '=77', 150);
        this.equationText = this.game.add.bitmapText(30, 200, 'calculator', '', 150);
        this.resultText = this.game.add.bitmapText(50, 340, 'calculator', '0', 75);
        this.roundText = this.game.add.bitmapText(10, 10, 'calculator', 'ROUND: ' + (this.round + 1), 60);
        this.bestText = this.game.add.bitmapText(10, 65, 'calculator', 'BEST: --.--', 60);
        this.timerText = this.game.add.bitmapText(10, 120, 'calculator', 'TIME: 0', 60);
        this.equation = '';
        this.lastWasOperation = true;
        this.prevOperation = -1;
        this.currentResult = 0;
        this.numbersClicked = 0;
        this.operationsClicked = 0;
        this.history = [];
        this.active = true;
        this.timer = 0;
        this.timerStart = false;
        this.blinkTimer = 0;
        this.roundOver = false;
        this.tutorial = this.game.add.image(0, 8, 'tutorial');
        this.tutorial.y = -Utils.GlobalSettings.height;
        this.tutorialTween = this.game.add.tween(this.tutorial).to({
            y: 8
        }, 500, Phaser.Easing.Back.Out);
        if (Utils.GlobalSettings.firstTime) {
            this.tutorialTween.start();
        } else {
            this.tutorial.visible = false;
        }
        this.soundButton = null;
        if (!Utils.GlobalSettings.muted) {
            this.soundButton = this.game.add.button(Utils.GlobalSettings.width - 120, 5, 'systembuttons2', null, this, 5, 4, 5);
        } else {
            this.soundButton = this.game.add.button(Utils.GlobalSettings.width - 120, 5, 'systembuttons2', null, this, 7, 6, 7);
        }
        this.soundButton.events.onInputDown.addOnce(this.soundClick.bind(this));
        this.stopMusicOnLoad = false;
        this.exitButton = this.game.add.button(Utils.GlobalSettings.width - 60, 5, 'systembuttons2', null, this, 1, 0, 1);
        this.exitButton.events.onInputDown.addOnce(this.exitClick.bind(this));
        if (navigator.isCocoonJS) {
            CocoonJS.App.setAppShouldFinishCallback(this.exitClick.bind(this));
            Utils.AdManager.hide();
        }
        this.setupScorePanel();
        this.getBestTime();
        this.game.add.sprite(0, 0, '');
        this.answers = [];
        this.currentAnswerIndex = 0;
        this.getCombinations(this.numbers);
        this.answerText.setText('=' + this.answers[this.currentAnswerIndex]);
    },
    checkTweenDone: function() {
        if (this.currentAnswerIndex >= this.answers.length) {
            if (!Utils.GlobalSettings.muted) {
                this.completeSnd.play();
            }
            this.roundOver = true;
            this.blinkTimer = 0;
            for (var i = 0; i < 10; i++) {
                this.names[i].setText('');
                this.scores[i].setText('');
                this.names[i].tint = 0xFFFFFF;
                this.scores[i].tint = 0xFFFFFF;
            }
            this.submitTime();
            this.round++;
            Utils.GlobalSettings.lastRound = this.round;
            this.addButton.alpha = 0.5;
            this.minusButton.alpha = 0.5;
            this.backspaceButton.alpha = 0.5;
            this.clearButton.alpha = 0.5;
            this.saveGame();
            this.finalTimeLabel.setText('Final Time: ' + (Math.round(this.timer) / 1000));
            this.finalTimeLabel.updateTransform();
            this.finalTimeLabel.position.x = Utils.GlobalSettings.width / 2 - this.finalTimeLabel.textWidth / 2;
            this.leaderboardGroup.visible = true;
            this.leaderboardGroup.x = -Utils.GlobalSettings.width;
            this.leaderboardTween.start();
        } else {
            if (!Utils.GlobalSettings.muted) {
                this.gpSnd.play();
            }
            this.answerText.setText('=' + this.answers[this.currentAnswerIndex]);
            this.reset();
        }
    },
    nextClick: function() {
        if (!Utils.GlobalSettings.muted) {
            this.clickSnd.play();
        }
        this.leaderboardGroup.visible = false;
        this.numbers = [];
        this.game.rnd.sow([this.round]);
        for (var i = 0; i < this.numButton.length; i++) {
            this.numbers.push(this.game.rnd.integerInRange(1, 9));
            this.numButton[i].frame = this.numbers[i] - 1;
        }
        this.answers = [];
        this.currentAnswerIndex = 0;
        this.getCombinations(this.numbers);
        this.timer = 0;
        this.timerStart = false;
        this.answerText.setText('=' + this.answers[this.currentAnswerIndex]);
        this.reset();
        this.roundText.setText('ROUND: ' + (this.round + 1));
        this.bestText.setText('BEST: --.--');
        this.timerText.setText('TIME: 0');
        this.getBestTime();
        this.nextButton.events.onInputDown.removeAll();
        this.nextButton.events.onInputDown.addOnce(this.nextClick.bind(this));
    },
    getBestTime: function() {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("POST", "http://www.ttafo.com/ints/php/ints.php", true);
        var parameters = "action=getbest&round=" + Utils.GlobalSettings.lastRound;
        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                var msg = xmlHttp.responseText;
                if (msg && msg.length > 0) {
                    var data = msg.split('#');
                    if (data && data.length > 0) {
                        var part = null;
                        for (var i = 0; i < data.length; i++) {
                            part = data[i].split(',');
                            if (part && part.length === 3) {
                                this.bestText.setText('BEST: ' + part[2] + ' [' + part[0] + ']');
                            }
                        }
                    }
                }
            }
        }.bind(this);
        xmlHttp.ontimeout = function() {}.bind(this);
        xmlHttp.send(parameters);
    },
    submitTime: function() {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("POST", "http://www.ttafo.com/ints/php/ints.php", true);
        var parameters = "action=score&nickname=" + Utils.GlobalSettings.lastNickname + "&round=" + Utils.GlobalSettings.lastRound + "&time=" + (Math.round(this.timer) / 1000);
        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                var msg = xmlHttp.responseText;
                if (msg && msg.length > 0 && this.names && this.scores && this.names.length === 10 && this.scores.length === 10) {
                    var data = msg.split('#');
                    if (data && data.length > 0) {
                        var part = null;
                        for (var i = 0; i < data.length; i++) {
                            part = data[i].split(',');
                            if (part && part.length === 3) {
                                if (part[0] === Utils.GlobalSettings.lastNickname) {
                                    this.names[i].tint = 0xFF0000;
                                    this.scores[i].tint = 0xFF0000;
                                }
                                this.names[i].setText((i + 1) + '.  ' + part[0]);
                                this.scores[i].setText('' + part[2]);
                            }
                        }
                    }
                }
            }
        }.bind(this);
        xmlHttp.ontimeout = function() {}.bind(this);
        xmlHttp.send(parameters);
    },
    numClick: function(i) {
        if (this.active && this.lastWasOperation && this.numButton[i].alpha === 1 && this.numbersClicked < 4) {
            if (!Utils.GlobalSettings.muted) {
                this.tapSnd.play();
            }
            this.juicy.jelly(this.numButton[i], 0.2, Math.random() * 100);
            if (this.tutorial.visible) {
                this.tutorial.visible = false;
            }
            this.lastWasOperation = false;
            this.equation += '' + this.numbers[i];
            this.equationText.setText(this.equation);
            this.numButton[i].alpha = 0.5;
            this.timerStart = true;
            if (this.prevOperation === -1) {
                this.currentResult = this.numbers[i];
            } else if (this.prevOperation === 0) {
                this.currentResult = this.currentResult + this.numbers[i];
            } else if (this.prevOperation === 1) {
                this.currentResult = this.currentResult - this.numbers[i];
            }
            this.numbersClicked++;
            this.history.push(i);
            this.resultText.setText('' + this.currentResult);
            if (this.currentResult === this.answers[this.currentAnswerIndex]) {
                this.active = false;
                this.nextAnswer();
            }
        }
        this.numButton[i].events.onInputDown.removeAll();
        this.numButton[i].events.onInputDown.addOnce(this.numClick.bind(this, i));
    },
    nextAnswer: function() {
        this.currentAnswerIndex++;
        this.juicy.shake();
        this.check.visible = true;
        this.check.scale.setTo(0, 0);
        this.checkTween.start();
    },
    addClick: function() {
        if (this.active && !this.lastWasOperation && this.operationsClicked < 3) {
            if (!Utils.GlobalSettings.muted) {
                this.tapSnd.play();
            }
            this.juicy.jelly(this.addButton, 0.2, Math.random() * 100);
            this.equation += '+';
            this.equationText.setText(this.equation);
            this.lastWasOperation = true;
            this.prevOperation = 0;
            this.operationsClicked++;
            this.history.push('+');
        }
        this.addButton.events.onInputDown.removeAll();
        this.addButton.events.onInputDown.addOnce(this.addClick.bind(this));
    },
    minusClick: function() {
        if (this.active && !this.lastWasOperation && this.operationsClicked < 3) {
            if (!Utils.GlobalSettings.muted) {
                this.tapSnd.play();
            }
            this.juicy.jelly(this.minusButton, 0.2, Math.random() * 100);
            this.equation += '-';
            this.equationText.setText(this.equation);
            this.lastWasOperation = true;
            this.prevOperation = 1;
            this.operationsClicked++;
            this.history.push('-');
        }
        this.minusButton.events.onInputDown.removeAll();
        this.minusButton.events.onInputDown.addOnce(this.minusClick.bind(this));
    },
    reset: function() {
        this.equation = '';
        this.lastWasOperation = true;
        this.prevOperation = -1;
        this.currentResult = 0;
        this.numbersClicked = 0;
        this.operationsClicked = 0;
        this.history = [];
        this.active = true;
        this.roundOver = false;
        this.blinkTimer = 0;
        this.numButton[0].alpha = 1;
        this.numButton[1].alpha = 1;
        this.numButton[2].alpha = 1;
        this.numButton[3].alpha = 1;
        this.addButton.alpha = 1;
        this.minusButton.alpha = 1;
        this.backspaceButton.alpha = 1;
        this.clearButton.alpha = 1;
        this.equationText.setText(this.equation);
        this.resultText.setText('' + this.currentResult);
    },
    clearClick: function() {
        if (this.active && (this.operationsClicked > 0 || this.numbersClicked > 0)) {
            if (!Utils.GlobalSettings.muted) {
                this.clickSnd.play();
            }
            this.juicy.jelly(this.clearButton, 0.1, Math.random() * 100);
            this.reset();
        }
        this.clearButton.events.onInputDown.removeAll();
        this.clearButton.events.onInputDown.addOnce(this.clearClick.bind(this));
    },
    backspaceClick: function() {
        if (this.active && (this.operationsClicked > 0 || this.numbersClicked > 0)) {
            if (!Utils.GlobalSettings.muted) {
                this.clickSnd.play();
            }
            this.juicy.jelly(this.backspaceButton, 0.1, Math.random() * 100);
            var last = this.history.pop();
            if (last === '-' || last === '+') {
                this.operationsClicked--;
            } else {
                this.numbersClicked--;
                this.numButton[last].alpha = 1;
            }
            this.equation = '';
            this.currentResult = 0;
            this.prevOperation = -1;
            this.lastWasOperation = false;
            if (this.history.length > 0) {
                for (var i = 0; i < this.history.length; i++) {
                    var type = this.history[i];
                    if (type === '-') {
                        this.equation += '-';
                        this.prevOperation = 1;
                        this.lastWasOperation = true;
                    } else if (type === '+') {
                        this.equation += '+';
                        this.prevOperation = 0;
                        this.lastWasOperation = true;
                    } else {
                        this.equation += '' + this.numbers[type];
                        this.lastWasOperation = false;
                        if (this.prevOperation === -1) {
                            this.currentResult = this.numbers[type];
                        } else if (this.prevOperation === 0) {
                            this.currentResult = this.currentResult + this.numbers[type];
                        } else if (this.prevOperation === 1) {
                            this.currentResult = this.currentResult - this.numbers[type];
                        }
                    }
                }
            } else {
                this.lastWasOperation = true;
            }
            this.equationText.setText(this.equation);
            this.resultText.setText('' + this.currentResult);
        }
        this.backspaceButton.events.onInputDown.removeAll();
        this.backspaceButton.events.onInputDown.addOnce(this.backspaceClick.bind(this));
    },
    exitClick: function() {
        if (!Utils.GlobalSettings.muted) {
            this.clickSnd.play();
        }
        this.exitButton.events.onInputDown.removeAll();
        this.soundButton.events.onInputDown.removeAll();
        this.exitButton.visible = false;
        this.soundButton.visible = false;
        this.game.state.start('mainmenu');
        return false;
    },
    addAnswer: function(num) {
        if (num != 0 && num != this.numbers[0] && num != this.numbers[1] && num != this.numbers[2] && num != this.numbers[3]) {
            var exists = false;
            for (var i = 0; i < this.answers.length; i++) {
                if (this.answers[i] === num) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                this.answers.push(num);
            }
        }
    },
    getCombinations: function(nums) {
        var numopps = 2;
        for (var j = 0; j < nums.length; j++) {
            var start = nums[j];
            for (var x = 0; x < numopps; x++) {
                for (var i = 0; i < nums.length; i++) {
                    if (i === j) continue;
                    var result = 0;
                    if (x === 0) {
                        result = start + nums[i];
                    } else if (x === 1) {
                        if (nums[i] > start) {
                            result = nums[i] - start;
                        } else {
                            result = start - nums[i];
                        }
                    }
                    this.addAnswer(result);
                    for (var y = 0; y < numopps; y++) {
                        for (var k = 0; k < nums.length; k++) {
                            if (k === i || k === j) continue;
                            var result2 = 0;
                            if (y === 0) {
                                result2 = result + nums[k];
                            } else if (y === 1) {
                                if (nums[k] > result) {
                                    result2 = nums[k] - result;
                                } else {
                                    result2 = result - nums[k];
                                }
                            }
                            this.addAnswer(result2);
                            for (var z = 0; z < numopps; z++) {
                                for (var l = 0; l < nums.length; l++) {
                                    if (l === k || l === j || l === i) continue;
                                    var result3 = 0;
                                    if (z === 0) {
                                        result3 = result2 + nums[l];
                                    } else if (z === 1) {
                                        if (nums[l] > result2) {
                                            result3 = nums[l] - result2;
                                        } else {
                                            result3 = result2 - nums[l];
                                        }
                                    }
                                    this.addAnswer(result3);
                                }
                            }
                        }
                    }
                }
            }
        }
        this.answers.sort(function(a, b) {
            return a - b
        });
    },
    setupScorePanel: function() {
        this.leaderboardGroup = this.game.add.group();
        this.panel = this.game.add.image(Utils.GlobalSettings.width / 2 - 200, Utils.GlobalSettings.height / 2 - 300, 'panel');
        this.leaderboardGroup.add(this.panel);
        this.backing = this.game.add.image(Utils.GlobalSettings.width / 2 - 170, Utils.GlobalSettings.height - 640, 'backing');
        this.backing.alpha = 0.6;
        this.backing.width = 340;
        this.backing.height = 310;
        this.leaderboardGroup.add(this.backing);
        this.finalTimeLabel = this.game.add.bitmapText(Utils.GlobalSettings.width / 2 - 100, Utils.GlobalSettings.height - 750, 'mecha', 'Final Time: ' + (Math.round(this.timer) / 1000), 50);
        this.leaderboardGroup.add(this.finalTimeLabel);
        this.finalTimeLabel.updateTransform();
        this.finalTimeLabel.position.x = Utils.GlobalSettings.width / 2 - this.finalTimeLabel.textWidth / 2;
        this.nameLabel = this.game.add.bitmapText(Utils.GlobalSettings.width / 2 - 110, Utils.GlobalSettings.height - 670, 'mecha', 'Name', 34);
        this.scoreLabel = this.game.add.bitmapText(Utils.GlobalSettings.width / 2 + 50, Utils.GlobalSettings.height - 670, 'mecha', 'Time', 34);
        this.leaderboardGroup.add(this.nameLabel);
        this.leaderboardGroup.add(this.scoreLabel);
        this.nextButton = this.game.add.image(Utils.GlobalSettings.width / 2 - 75, Utils.GlobalSettings.height - 320, 'next');
        this.nextButton.inputEnabled = true;
        this.nextButton.events.onInputDown.addOnce(this.nextClick.bind(this));
        this.leaderboardGroup.add(this.nextButton);
        this.names = [];
        this.scores = [];
        for (var i = 0; i < 10; i++) {
            this.names.push(this.game.add.bitmapText(Utils.GlobalSettings.width / 2 - 100, Utils.GlobalSettings.height - 630 + (i * 30), 'mecha', '', 24));
            this.scores.push(this.game.add.bitmapText(Utils.GlobalSettings.width / 2 + 60, Utils.GlobalSettings.height - 630 + (i * 30), 'mecha', '', 24));
            this.leaderboardGroup.add(this.names[i]);
            this.leaderboardGroup.add(this.scores[i]);
        }
        this.leaderboardGroup.visible = false;
        this.leaderboardGroup.x = -Utils.GlobalSettings.width;
        this.leaderboardTween = this.game.add.tween(this.leaderboardGroup).to({
            x: 0
        }, 1000, Phaser.Easing.Back.Out);
    },
    soundClick: function() {
        Utils.GlobalSettings.muted = !Utils.GlobalSettings.muted;
        if (!Utils.GlobalSettings.muted) {
            this.clickSnd.play();
            this.soundButton.setFrames(5, 4, 5);
        } else {
            this.soundButton.setFrames(7, 6, 7);
        }
        this.soundButton.events.onInputDown.removeAll();
        this.soundButton.events.onInputDown.addOnce(this.soundClick.bind(this));
    },
    saveGame: function() {
        if (DataStorage.supportsLocalStorage()) {
            this.saveGameLocalStorage();
        } else {
            this.saveGameCookie();
        }
    },
    loadGame: function() {
        if (DataStorage.supportsLocalStorage()) {
            this.loadGameLocalStorage();
        } else {
            this.loadGameCookie();
        }
    },
    saveGameLocalStorage: function() {
        localStorage[Utils.GlobalSettings.namespace + ".nickname"] = Utils.GlobalSettings.lastNickname;
        localStorage[Utils.GlobalSettings.namespace + ".round"] = Utils.GlobalSettings.lastRound;
    },
    saveGameCookie: function() {
        DataStorage.GlobalSettings.database.save('save', Utils.GlobalSettings.lastNickname + '#' + Utils.GlobalSettings.lastRound, null);
    },
    loadGameCookie: function() {
        var data = DataStorage.GlobalSettings.database.load('save');
        if (data && data.length > 0) {
            var loadedData = data.split('#');
            if (loadedData && loadedData.length === 2) {
                Utils.GlobalSettings.lastNickname = loadedData[0];
                Utils.GlobalSettings.lastRound = parseInt(loadedData[1]);
            }
        }
    },
    loadGameLocalStorage: function() {
        var loadedData = [];
        loadedData.push(localStorage[Utils.GlobalSettings.namespace + ".nickname"]);
        loadedData.push(localStorage[Utils.GlobalSettings.namespace + ".round"]);
        if (loadedData && loadedData.length === 2) {
            Utils.GlobalSettings.lastNickname = loadedData[0];
            Utils.GlobalSettings.lastRound = parseInt(loadedData[1]);
        }
    },
    update: function() {
        if (this.active && this.timerStart) {
            this.timer += this.game.time.elapsed;
            this.timerText.setText('TIME: ' + Math.round(this.timer) / 1000);
        } else if (this.roundOver) {
            this.blinkTimer += this.game.time.elapsed;
            if (this.blinkTimer >= 0 && this.blinkTimer < 500) {
                this.timerText.setText('TIME: ');
            } else if (this.blinkTimer >= 500 && this.blinkTimer <= 1500) {
                this.timerText.setText('TIME: ' + Math.round(this.timer) / 1000);
            } else {
                this.blinkTimer = 0;
            }
        }
    },
    shutdown: function() {
        if (this.clickSnd) {
            this.clickSnd.stop();
            this.clickSnd = null;
        }
        if (this.dripSnd) {
            this.dripSnd.stop();
            this.dripSnd = null;
        }
        if (this.tapSnd) {
            this.tapSnd.stop();
            this.tapSnd = null;
        }
        if (this.completeSnd) {
            this.completeSnd.stop();
            this.completeSnd = null;
        }
        if (this.gpSnd) {
            this.gpSnd.stop();
            this.gpSnd = null;
        }
        this.juicy = null;
        if (this.bg) {
            this.bg.destroy();
            this.bg = null;
        }
        if (this.bar) {
            this.bar.destroy();
            this.bar = null;
        }
        if (this.check) {
            this.check.destroy();
            this.check = null;
        }
        if (this.checkTween) {
            this.checkTween.onComplete.removeAll();
            this.checkTween.stop();
            this.checkTween = null;
        }
        this.numbers = null;
        this.numButton = null;
        if (this.addButton) {
            this.addButton.destroy();
            this.addButton = null;
        }
        if (this.minusButton) {
            this.minusButton.destroy();
            this.minusButton = null;
        }
        if (this.backspaceButton) {
            this.backspaceButton.destroy();
            this.backspaceButton = null;
        }
        if (this.clearButton) {
            this.clearButton.destroy();
            this.clearButton = null;
        }
        if (this.answerText) {
            this.answerText.destroy();
            this.answerText = null;
        }
        if (this.equationText) {
            this.equationText.destroy();
            this.equationText = null;
        }
        if (this.resultText) {
            this.resultText.destroy();
            this.resultText = null;
        }
        if (this.roundText) {
            this.roundText.destroy();
            this.roundText = null;
        }
        if (this.bestText) {
            this.bestText.destroy();
            this.bestText = null;
        }
        if (this.timerText) {
            this.timerText.destroy();
            this.timerText = null;
        }
        this.equation = null;
        this.history = null;
        if (this.tutorial) {
            this.tutorial.destroy();
            this.tutorial = null;
        }
        this.tutorialTween = null;
        if (this.soundButton) {
            this.soundButton.destroy();
            this.soundButton = null;
        }
        if (this.exitButton) {
            this.exitButton.destroy();
            this.exitButton = null;
        }
        if (this.leaderboardGroup) {
            this.leaderboardGroup.destroy();
            this.leaderboardGroup = null;
        }
        if (this.panel) {
            this.panel.destroy();
            this.panel = null;
        }
        if (this.backing) {
            this.backing.destroy();
            this.backing = null;
        }
        if (this.finalTimeLabel) {
            this.finalTimeLabel.destroy();
            this.finalTimeLabel = null;
        }
        if (this.nameLabel) {
            this.nameLabel.destroy();
            this.nameLabel = null;
        }
        if (this.scoreLabel) {
            this.scoreLabel.destroy();
            this.scoreLabel = null;
        }
        if (this.nextButton) {
            this.nextButton.destroy();
            this.nextButton = null;
        }
        this.names = null;
        this.scores = null;
        this.leaderboardTween = null;
        this.answers = null;
        console.log('destroy gamescreen');
    }
};