var game = new Phaser.Game(1, 1, Phaser.AUTO, 'stage', { preload: this.preload, create: this.create, update: this.update });

var cursors;
var vship;

function preload(){

    // set the background of the canvas to white
    game.stage.backgroundColor = '0xdddddd';

    // load the ship
    game.load.image('ship', 'ship.png');

    
}


function create(){

    game.input.maxPointers = 2;

    // create the ship sprite
    vship = game.add.sprite(0, 0, 'ship');

    // hide the Phaser sprite.  It's only here as a proxy sprite.  You could possibly just create
    // a empty sprite and move that too.
    vship.visible = false;

    
    //  and its physics settings.  Used to control the ship movement
    game.physics.enable(vship, Phaser.Physics.ARCADE);

    // slow the ship down
    vship.body.drag.set(500);

    // don't let the ship go too fast
    vship.body.maxVelocity.set(500);

    //  Game input
    cursors = game.input.keyboard.createCursorKeys();
    game.input.keyboard.addKeyCapture([Phaser.Keyboard.SPACEBAR]);

}

function update(){

    // When the up arrow is pressed we move the ship
    if (cursors.up.isDown) {
        game.physics.arcade.accelerationFromRotation(vship.rotation, 500, vship.body.acceleration);
    }
    else {
        // otherwise we don't move it
        vship.body.acceleration.set(0);
    }

    // counter clockwise rotatation
    if (cursors.left.isDown) {
        vship.body.angularVelocity = -300;
    }
    else if (cursors.right.isDown) {
        //clockwise rotataion
        vship.body.angularVelocity = 300;
    }
    else {
        vship.body.angularVelocity = 0;
    }


    // We wrap the ship position based on the inner window width and height
    // Because the demo on updatestage.com is in an iframe I need to use outerWindow.
    
    if (vship.x < 0) {                 
        vship.x = window.innerWidth;              
    } else if (vship.x > window.innerWidth) { 
        vship.x = 0;
    }

    if (vship.y < 0) {                 
        vship.y = window.innerHeight;              
    } else if (vship.y > window.innerHeight) { 
        vship.y = 0;
    }

    // move the div
    // get the reference to the ship div sprite
    var ship = document.getElementById('sgShip2');


    // for this examle to work in wordpress I need to get the parent of the ship div and set it's offset
    // for the ship to move correctly around the screen
    // Commnet this out and use the two lines following this one if you're having issues.
    var ol = ship.parentElement.offsetWidth;
    var ot = ship.parentElement.offsetTop;

    // Uncomment is you're not running the code in a parent div
    /*
    var ol = 0;
    var ot = 0;
    */

    // set it's location in pixels
    //var xx = (vship.x - ol).toString() + 'px';
    var xx = vship.x.toString() + 'px';
    var yy = (vship.y - ot).toString() + 'px';

    // set the actual position
    ship.style.top = yy;
    ship.style.left = xx;

    // Rotate the ship div.
    // might need to add a different transfor for different browsers
    ship.style.msTransform = 'rotate(' + vship.angle + 'deg)';
    ship.style.transform = 'rotate(' + vship.angle + 'deg)'; 
    ship.style.webkitTransform = 'rotate(' + vship.angle + 'deg)'; 
    ship.style.mozTransform = 'rotate(' + vship.angle + 'deg)'; 
    ship.style.oTransform = 'rotate(' + vship.angle + 'deg)'; 
}