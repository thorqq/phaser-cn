window.onload = function() {
	
	var game = new Phaser.Game(320, 480, Phaser.CANVAS, "", {preload: onPreload, create: onCreate});                

	// 地图
	var map;
	// 保存触摸的开始位置
	var startX;
	var startY;
	// 用于多点触摸
	var moveIndex;
	// 地图滚动速度。数字越大，滚动速度越快。如果1，则效果就像你在拖拽地图
	var mapSpeed = 1;
	// 组，包含地图和塔
     var mapGroup;
     // 塔
     var candidateTown;

	// 预加载图形
	function onPreload() {
		game.load.image("map", "map.jpg");
        game.load.image("town", "town.png");
	}

	// 进入全屏模式
	function goFullScreen(){
		game.scale.pageAlignHorizontally = true;
		game.scale.pageAlignVertically = true;
		game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		game.scale.setScreenSize(true);
	}

	// 创建游戏
	function onCreate() {
		goFullScreen();
        mapGroup = game.add.group();
        // 放置地图
		map = game.add.image(0, 0, "map");
        mapGroup.add(map);
        // 随机放置塔
        for(var i = 0;i < 10; i++){
           var town = game.add.image(game.rnd.between(50, map.width - 50), game.rnd.between(50, map.height - 50), "town");
           town.anchor.setTo(0.5);
           // 每一个塔都要启用输入监听，监听按下和放开事件
           town.inputEnabled = true;
           town.events.onInputDown.add(selectTown, this);
           town.events.onInputUp.add(confirmTown, this);
           mapGroup.add(town);
        }
        // 确定地图的中心
        mapGroup.x = (game.width - map.width) / 2;
        mapGroup.y = (game.height - map.height) / 2;
        // 监听触摸事件
		game.input.onDown.add(fingerOnMap, this);		
	}
	
	// 用户点击或者触摸地图
	function fingerOnMap(){
		// 保存起始位置
		startX = game.input.worldX;
		startY = game.input.worldY;
		mapGroup.saveX = mapGroup.x;
		mapGroup.saveY = mapGroup.y;
		// 更新监听器
		game.input.onDown.remove(fingerOnMap);
     	game.input.onUp.add(stopMap);
     	moveIndex = game.input.addMoveCallback(dragMap, this)   
	}
	
	// 用户拖拽地图。根据手指的移动和设置的速度来移动地图
	function dragMap(){
		var currentX = game.input.worldX;
		var currentY = game.input.worldY;
		var deltaX = startX - currentX;
		var deltaY = startY - currentY; 
        if(deltaX * deltaX + deltaY * deltaY > 25){
            candidateTown = null;
        }
		mapGroup.x = mapGroup.saveX - deltaX * mapSpeed;
		mapGroup.y = mapGroup.saveY - deltaY * mapSpeed;
		// 这里用来限制地图的移动
        if(mapGroup.x < - map.width + game.width){
           mapGroup.x = - map.width + game.width;
        }
        if(mapGroup.x > 0){
           mapGroup.x = 0;
        }
        if(mapGroup.y < - map.height + game.height){
           mapGroup.y = - map.height + game.height;
        }
        if(mapGroup.y > 0){
           mapGroup.y = 0;
        }
	}
	
	// 用户停止触摸地图
	function stopMap(){
		game.input.onDown.add(fingerOnMap);
     	game.input.onUp.remove(stopMap);
     	game.input.deleteMoveCallback(moveIndex);
	}
     
     // 用户把手指或者鼠标按下塔
     function selectTown(sprite, pointer) {
          candidateTown = sprite;
     }
     
     // 用户停止触摸塔
     function confirmTown(sprite, pointer) {
          if(candidateTown == sprite){
               alert("Town selected");
          }
     }
         
}