var GlobezGame = GlobezGame || {};

GlobezGame.GameTitle = function(){
	startGame = false;
};

GlobezGame.GameTitle.prototype = {
  	create: function(){
  		console.log("%cStarting game title", "color:white; background:red");
		this.add.image(0,0,"background");
          //
		var gameTitleSeaLife = this.add.image(160,70,"gametitle_sealife");
          gameTitleSeaLife.anchor.setTo(0.5,0.5);
		//
		var gameTitleVs = this.add.image(190,120,"gametitle_vs");
          gameTitleVs.anchor.setTo(0.5,0.5);
		//
		var gameTitleMines = this.add.image(160,160,"gametitle_mines");
          gameTitleMines.anchor.setTo(0.5,0.5);
		//
  		var playButton = this.add.button(160,320,"playbutton",this.playTheGame,this)
		playButton.anchor.setTo(0.5,0.5);
	},
	playTheGame: function(){
		if(!startGame){
			startGame = true
			alert("Start the game!!");	
		}
	}
}