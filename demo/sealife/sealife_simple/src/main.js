var GlobezGame = GlobezGame || {};
GlobezGame.gameOptions = {
	gameWidth:320,
	gameHeight:480
}
GlobezGame.game = new Phaser.Game(GlobezGame.gameOptions.gameWidth,GlobezGame.gameOptions.gameHeight, Phaser.CANVAS,"");
GlobezGame.game.state.add("Boot", GlobezGame.Boot);
GlobezGame.game.state.add("Preload", GlobezGame.Preload);
GlobezGame.game.state.add("GameTitle", GlobezGame.GameTitle);
GlobezGame.game.state.start("Boot"); 