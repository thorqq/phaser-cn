var GlobezGame = GlobezGame || {};

GlobezGame.Boot = function(){};
  
GlobezGame.Boot.prototype = {
	preload: function(){ 
		console.log("%cStarting Fish Vs Mines", "color:white; background:red");
        this.load.image("loading","assets/sprites/loading.png");
		this.load.image("logo","assets/sprites/logo.png"); 
	},
  	create: function(){
  		this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.scale.pageAlignHorizontally = true;
		this.scale.pageAlignVertically = true;
		this.scale.setScreenSize(true);
		this.physics.startSystem(Phaser.Physics.ARCADE);
		this.state.start("Preload");
	}
}