var GlobezGame = GlobezGame || {};

GlobezGame.Preload = function(){};

GlobezGame.Preload.prototype = {
	preload: function(){ 
		console.log("%cPreloading assets", "color:white; background:red")
        var loadingBar = this.add.sprite(160,340,"loading");
        loadingBar.anchor.setTo(0.5,0.5);
        this.load.setPreloadSprite(loadingBar);
        var logo = this.add.sprite(160,240,"logo");
        logo.anchor.setTo(0.5,0.5);
		this.load.image("background","assets/sprites/background.png");
		this.load.image("playbutton","assets/sprites/playbutton.png");
		this.load.image("gametitle_sealife","assets/sprites/gametitle_sealife.png");
        this.load.image("gametitle_vs","assets/sprites/gametitle_vs.png");
        this.load.image("gametitle_mines","assets/sprites/gametitle_mines.png");
		this.load.image("blackfade","assets/sprites/blackfade.png");
        this.load.image("bubble","assets/sprites/bubble.png");
	},
  	create: function(){
		this.state.start("GameTitle");
	}
}