var GlobezGame = GlobezGame || {};

GlobezGame.GameTitle = function(){
	startGame = false;
};

GlobezGame.GameTitle.prototype = {
  	create: function(){
  		console.log("%cStarting game title", "color:white; background:red");
		this.add.image(0,0,"background");
		
        //泡泡"发射器"
        var bubblesEmitter = this.add.emitter(160, 500, 50);
        bubblesEmitter.makeParticles("bubble");
        bubblesEmitter.maxParticleScale = 0.6;
        bubblesEmitter.minParticleScale = 0.2;
        bubblesEmitter.setYSpeed(-30, -40);
        bubblesEmitter.setXSpeed(-3, 3);
        bubblesEmitter.gravity = 0;
        bubblesEmitter.width = 320;
        bubblesEmitter.minRotation = 0;
        bubblesEmitter.maxRotation = 40;
        bubblesEmitter.flow(15000, 2000)
        
		//sealife 旋转补间动画
		var gameTitleSeaLife = this.add.image(160,70,"gametitle_sealife");
        gameTitleSeaLife.anchor.setTo(0.5,0.5);
        gameTitleSeaLife.angle = (2+Math.random()*5)*(Math.random()>0.5?1:-1);
        var seaLifeTween = this.add.tween(gameTitleSeaLife);
		seaLifeTween.to({
			angle: -gameTitleSeaLife.angle
		},5000+Math.random()*5000,Phaser.Easing.Linear.None,true,0,1000,true);
		
		//vs 旋转补间动画
		var gameTitleVs = this.add.image(190,120,"gametitle_vs");
        gameTitleVs.anchor.setTo(0.5,0.5);
        gameTitleVs.angle = (2+Math.random()*5)*(Math.random()>0.5?1:-1);
        var vsTween = this.add.tween(gameTitleVs);
		vsTween.to({
			angle: -gameTitleVs.angle
		},5000+Math.random()*5000,Phaser.Easing.Linear.None,true,0,1000,true);
		
		//mines 旋转补间动画
		var gameTitleMines = this.add.image(160,160,"gametitle_mines");
        gameTitleMines.anchor.setTo(0.5,0.5);
        gameTitleMines.angle = (2+Math.random()*5)*(Math.random()>0.5?1:-1);
        var minesTween = this.add.tween(gameTitleMines);
		minesTween.to({
			angle: -gameTitleMines.angle
		},5000+Math.random()*5000,Phaser.Easing.Linear.None,true,0,1000,true);
		
		//play 按钮
  		var playButton = this.add.button(160,320,"playbutton",this.playTheGame,this)
		playButton.anchor.setTo(0.5,0.5);
        playButton.angle = (2+Math.random()*5)*(Math.random()>0.5?1:-1);
        var playTween = this.add.tween(playButton);
		playTween.to({
			angle: -playButton.angle
		},5000+Math.random()*5000,Phaser.Easing.Linear.None,true,0,1000,true);
		
        //画面从黑到亮，在2秒内显示出来
        var blackFade = this.add.sprite(0,0,"blackfade");
        var fadeTween = this.add.tween(blackFade);
		fadeTween.to({
			alpha:0
		},2000,Phaser.Easing.Cubic.Out,true);
	},
	
	playTheGame: function(){
		if(!startGame){
			startGame = true
			alert("Start the game!!");	
		}
	}
}