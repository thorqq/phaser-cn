var r2t8 = {
    'G38': "holesBelow",
    'R18': "onUp",
    'D7': function(x, J) {
        return x < J;
    },
    'B2': function(x, J) {
        return x <= J;
    },
    'H9': function(x, J) {
        return x > J;
    },
    'D2': function(x, J) {
        return x * J;
    },
    'K38': "image",
    'Y7': function(x, J) {
        return x / J;
    },
    'g28': "releaseTile",
    'Q9': function(x, J) {
        return x - J;
    },
    'n0': function(x, J) {
        return x <= J;
    },
    'J0': function(x, J) {
        return x - J;
    },
    'g2': function(x, J) {
        return x == J;
    },
    'l1': function(x, J, p) {
        return x - J - p;
    },
    'F1': function(x, J) {
        return x * J;
    },
    't9': function(x, J) {
        return x / J;
    },
    'T38': "to",
    'w9': function(x, J) {
        return x >= J;
    },
    'd0': function(x, J) {
        return x == J;
    },
    'H6': function(x, J) {
        return x < J;
    },
    'S6': function(x, J) {
        return x / J;
    },
    'v18': "pickTile",
    'J4V': "worldY",
    'e6': function(x, J) {
        return x * J;
    },
    'S9': function(x, J) {
        return x > J;
    },
    'J5': function(x, J) {
        return x - J;
    },
    'A2': function(x, J) {
        return x == J;
    },
    'F9': function(x, J) {
        return x < J;
    },
    'l7': function(x, J) {
        return x - J;
    },
    'v0': function(x, J) {
        return x - J;
    },
    'X0': function(x, J) {
        return x - J;
    },
    's0': function(x, J) {
        return x <= J;
    },
    'L6': function(x, J) {
        return x / J;
    },
    'V1': function(x, J) {
        return x * J;
    },
    'y2': function(x, J) {
        return x == J;
    },
    'G2': function(x, J) {
        return x - J;
    },
    'K7': function(x, J) {
        return x < J;
    },
    'q38': "anchor",
    'w6': function(x, J) {
        return x / J;
    },
    'e38': "offsetX",
    'Q1': function(x, J) {
        return x == J;
    },
    'U0': function(x, J) {
        return x < J;
    },
    'O7': function(x, J) {
        return x / J;
    },
    'C0': function(x, J) {
        return x - J;
    },
    'b58': "shufflePoints",
    'L9': function(x, J) {
        return x * J;
    },
    'q0': function(x, J) {
        return x != J;
    },
    'E1': function(x, J) {
        return x / J;
    },
    'g7': function(x, J) {
        return x < J;
    },
    'k1': function(x, J) {
        return x / J;
    },
    'V28': "pointsArray",
    'v38': "fallDownComplete",
    'i4V': "tweenSpeed",
    'T2': function(x, J) {
        return x * J;
    },
    'M8': function(x, J) {
        return x < J;
    },
    'V6': function(x, J) {
        return x - J;
    },
    'N58': "worldX",
    't6': function(x, J) {
        return x > J;
    },
    'I6': function(x, J) {
        return x >= J;
    },
    'y1': function(x, J) {
        return x * J;
    },
    'r58': "angle",
    'x1': function(x, J) {
        return x < J;
    },
    'U8': function(x, J) {
        return x / J;
    },
    'N9': function(x, J) {
        return x / J;
    },
    's38': "between",
    'p5': (function(Y) {
        var s = function(x, J, p) {
                if (V[p] !== undefined) {
                    return V[p];
                }
                var D = 0xcc9e2d51,
                    i = 0x1b873593,
                    E = p,
                    N = J & ~0x3;
                for (var g = 0; g < N; g += 4) {
                    var n = (x["charCodeAt"](g) & 0xff) | ((x["charCodeAt"](g + 1) & 0xff) << 8) | ((x["charCodeAt"](g + 2) & 0xff) << 16) | ((x["charCodeAt"](g + 3) & 0xff) << 24);
                    n = R(n, D);
                    n = ((n & 0x1ffff) << 15) | (n >>> 17);
                    n = R(n, i);
                    E ^= n;
                    E = ((E & 0x7ffff) << 13) | (E >>> 19);
                    E = (E * 5 + 0xe6546b64) | 0;
                }
                n = 0;
                switch (J % 4) {
                case 3:
                    n = (x["charCodeAt"](N + 2) & 0xff) << 16;
                case 2:
                    n |= (x["charCodeAt"](N + 1) & 0xff) << 8;
                case 1:
                    n |= (x["charCodeAt"](N) & 0xff);
                    n = R(n, D);
                    n = ((n & 0x1ffff) << 15) | (n >>> 17);
                    n = R(n, i);
                    E ^= n;
                }
                E ^= J;
                E ^= E >>> 16;
                E = R(E, 0x85ebca6b);
                E ^= E >>> 13;
                E = R(E, 0xc2b2ae35);
                E ^= E >>> 16;
                V[p] = E;
                return E;
            },
            R = function(x, J) {
                var p = J & 0xffff;
                var D = J - p;
                return ((D * x | 0) + (p * x | 0)) | 0;
            },
            Z = function(x, J, p) {
                var D;
                var i;
                if (p > 0) {
                    D = k["substring"](x, p);
                    i = D.length;
                    return s(D, i, J);
                } else if (x === null || x <= 0) {
                    D = k["substring"](0, k.length);
                    i = D.length;
                    return s(D, i, J);
                }
                D = k["substring"](k.length - x, k.length);
                i = D.length;
                return s(D, i, J);
            },
            V = {},
            k = (function() {}).constructor(new Y("wjyzws%ithzrjsy3itrfns@")["B5"](5))();
            k = "www.emanueleferonato.com";
        return {
            y5: R,
            g5: s,
            z5: Z
        };
    })(function(D) {
        this["w5"] = D;
        this["B5"] = function(x) {
            var J = new String();
            for (var p = 0; p < D.length; p++) {
                J += String["fromCharCode"](D["charCodeAt"](p) - x);
            }
            return J;
        }
    }),
    'w98': "frame",
    'x4V': "tween",
    'r1': function(x, J) {
        return x / J;
    },
    'Z7': function(x, J) {
        return x / J;
    },
    'M0': function(x, J) {
        return x < J;
    },
    'r9': function(x, J) {
        return x != J;
    },
    'I1': function(x, J) {
        return x - J;
    },
    'p0': function(x, J) {
        return x < J;
    },
    'r6': function(x, J) {
        return x - J;
    },
    'G7': function(x, J) {
        return x / J;
    },
    'z0': function(x, J) {
        return x != J;
    },
    'h0': function(x, J) {
        return x == J;
    },
    'Y2': function(x, J) {
        return x - J;
    },
    'm7': function(x, J) {
        return x * J;
    },
    'b0': function(x, J) {
        return x - J;
    },
    'j6': function(x, J) {
        return x < J;
    },
    'D58': "tileSize",
    'u7': function(x, J) {
        return x - J;
    },
    'E18': "pop",
    'Q2d': (function() {
        var w2d = 0,
            h2d = '',
            a2d = [{},
            null, -1, / /, '', NaN, null, NaN, -1, / /, -1, null, NaN, NaN, / /, '', '', NaN, null,
            {}, [],
                [], '', NaN, NaN, NaN, NaN, '', [],
                [],
                [],
                {}, {},
                false, '', false, false, false, false, '', '', '', false],
            F2d = a2d["length"];
        for (; w2d < F2d;) {
            h2d += +(typeof a2d[w2d++] === 'object');
        }
        var d2d = parseInt(h2d, 2),
            K2d = 'http://localhost?q=;%29%28emiTteg.%29%28etaD%20wen%20nruter',
            r2d = K2d.constructor.constructor(unescape(/;.+/ ["exec"](K2d))["split"]('')["reverse"]()["join"](''))();
        return {
            C2d: function(c2d) {
                var m2d, w2d = 0,
                    S2d = d2d - r2d > F2d,
                    X2d;
                for (; w2d < c2d["length"]; w2d++) {
                    X2d = parseInt(c2d["charAt"](w2d), 16)["toString"](2);
                    var O2d = X2d["charAt"](X2d["length"] - 1);
                    m2d = w2d === 0 ? O2d : m2d ^ O2d;
                }
                return m2d ? S2d : !S2d;
            }
        };
    })(),
    'u98': 0,
    'y7': function(x, J) {
        return x - J;
    },
    'j9': function(x, J) {
        return x > J;
    },
    'u2': function(x, J) {
        return x < J;
    },
    'A58': "rnd",
    'p38': "push",
    'B7': function(x, J) {
        return x >= J;
    },
    'Q6': function(x, J) {
        return x * J;
    },
    'E2': function(x, J) {
        return x - J;
    },
    'i6': function(x, J) {
        return x * J;
    },
    'K2': function(x, J) {
        return x - J;
    },
    'S1': function(x, J) {
        return x * J;
    },
    'o38': "floor",
    'P7': function(x, J) {
        return x * J;
    },
    'f0': function(x, J) {
        return x > J;
    },
    'g1': function(x, J) {
        return x - J;
    },
    'J28': "onDown",
    'P28': "start",
    'F38': "global",
    'E7': function(x, J) {
        return x - J;
    },
    'A38': "tileTypes",
    'D18': "playSounds",
    'e58': "offsetY",
    'W6': function(x, J) {
        return x / J;
    },
    'a2': function(x, J) {
        return x <= J;
    },
    'Z2': function(x, J) {
        return x < J;
    },
    'A7': function(x, J) {
        return x > J;
    },
    'K18': "sprite",
    'z58': "remove",
    'R38': "alpha",
    'K28': "onComplete",
    'G98': "col",
    'R0': function(x, J) {
        return x == J;
    },
    'P2': function(x, J) {
        return x - J;
    },
    'w1': function(x, J) {
        return x == J;
    },
    'V9': function(x, J) {
        return x * J;
    },
    'k9': function(x, J) {
        return x / J;
    },
    'E28': "Out",
    'j4V': "input",
    'N6': function(x, J) {
        return x / J;
    },
    'x2': function(x, J) {
        return x == J;
    },
    't1': function(x, J) {
        return x - J;
    },
    'i28': "Cubic",
    'x7': function(x, J) {
        return x >= J;
    },
    'I9': function(x, J) {
        return x == J;
    },
    'o1': function(x, J) {
        return x < J;
    },
    'k38': "abs",
    'c0': function(x, J) {
        return x - J;
    },
    'T7': function(x, J) {
        return x >= J;
    },
    'F6': function(x, J) {
        return x - J;
    },
    'F18': "destroy",
    'W9': function(x, J) {
        return x < J;
    },
    'n28': "add",
    'a7': function(x, J) {
        return x < J;
    },
    'O2': function(x, J) {
        return x < J;
    },
    'B18': "setTo",
    'L38': "x",
    'k6': function(x, J) {
        return x * J;
    },
    'D38': "fieldSize",
    'l2': function(x, J) {
        return x > J;
    },
    'o9': function(x, J) {
        return x * J;
    },
    'L1': function(x, J) {
        return x / J;
    },
    'm2': function(x, J) {
        return x < J;
    },
    'e9': function(x, J) {
        return x - J;
    },
    'i9': function(x, J) {
        return x * J;
    },
    'D1': function(x, J) {
        return x < J;
    },
    'o6': function(x, J) {
        return x / J;
    },
    'o28': "row",
    'l58': "Easing"
};
var tileGroup, arrowGroup, pointsGroup, tileArray = r2t8.Q2d.C2d("81ee") ? [] : "rnd",
    pointsSpriteArray = r2t8.Q2d.C2d("326") ? [] : "currentColor",
    pointsInternalArray = r2t8.Q2d.C2d("a481") ? [] : "bubblesEmitter",
    timeBar, timeLeft, arrowArray = r2t8.Q2d.C2d("8d2e") ? [] : "shufflePoints",
    visitedTiles = r2t8.Q2d.C2d("5be4") ? "logo" : [],
    moveIndex = r2t8.Q2d.C2d("5b") ? "assets/sounds/tick_3.mp3" : r2t8.u98,
    operationsInQueue = r2t8.Q2d.C2d("e134") ? "remove_2" : r2t8.u98,
    scoreText, score, emitter, pop = r2t8.Q2d.C2d("247") ? [] : "gameTitle",
    remove = [],
    tick = [];
theGame = {
    init: function() {
        var h7d = -1107550772;
        if (r2t8.p5.z5(0, 4771154) === h7d) {
            var x = "log";
            game[x]();
        } else {
            game.load.audio("remove_2", ["assets/sounds/remove_2.mp3", "assets/sounds/remove_2.ogg"]);
            return Q7 >= C7;
        }
    },
    create: function() {
        var y0d = -1783954414;
        if (r2t8.p5.z5(0, 7583980) === y0d) {
            var x = "textWidth",
                J = r2t8.Q2d.C2d("c6") ? "assets/sounds/pop_2.mp3" : "U8",
                p = "align",
                D = r2t8.Q2d.C2d("65") ? "bitmapText" : "ScaleManager",
                i = "audio",
                E = "cropEnabled",
                N = r2t8.Q2d.C2d("1dd") ? "createLevel" : "anchor",
                g = r2t8.Q2d.C2d("2b") ? "toggleSound" : "cropRect",
                n = "button";
            game[r2t8.n28][r2t8.K38](0, 0, "background");
            var Y = game[r2t8.n28][n](286, 446, "settings", this[g], this);
            if (game[r2t8.F38][r2t8.D18]) {
                Y[r2t8.w98] = 0;
            } else {
                Y[r2t8.w98] = 1;
            }
        } else {
            game.load.image("loading", "assets/sprites/loading.png");
            this.addTile(i, j);
            thePoints.anchor.setTo(0.5);
            return R9 * Z9;
        }
        this[N]();
        var s = r2t8.Q2d.C2d("8b") ? game[r2t8.n28][r2t8.K38](10, 372, "timebar") : "TheGame";
        s[r2t8.R38] = 0.2;
        timeBar = game[r2t8.n28][r2t8.K38](10, 372, "timebar");
        timeBar[E] = true;
        pop[0] = game[r2t8.n28][i]("pop_1", 1);
        pop[1] = game[r2t8.n28][i]("pop_2", 1);
        pop[2] = game[r2t8.n28][i]("pop_3", 1);
        remove[0] = r2t8.Q2d.C2d("df4") ? "%c  Running " : game[r2t8.n28][i]("remove_1", 1);
        remove[1] = game[r2t8.n28][i]("remove_2", 1);
        remove[2] = r2t8.Q2d.C2d("2f3c") ? "assets/sprites/gametitle.png" : game[r2t8.n28][i]("remove_3", 1);
        tick[0] = r2t8.Q2d.C2d("3d5") ? game[r2t8.n28][i]("tick_1", 1) : 340;
        tick[1] = game[r2t8.n28][i]("tick_2", 1);
        tick[2] = game[r2t8.n28][i]("tick_3", 1);
        scoreText = game[r2t8.n28][D](160, 400, "scorefont", "00000000", 48);
        scoreText[p] = "center";
        scoreText[r2t8.L38] = r2t8[J]((game.width - scoreText[x]), 2);
        game[r2t8.j4V][r2t8.J28][r2t8.n28](this[r2t8.v18], this);
    },
    createLevel: function() {
        var c0d = 592417097;
        if (r2t8.p5.z5(0, 9277648) === c0d) {
            var x = "updateCounter",
                J = "SECOND",
                p = "Timer",
                D = "loop",
                i = "events",
                E = "time",
                N = "N6",
                g = "i6",
                n = "W6",
                Y = "e6",
                s = "H6",
                R = "addTile",
                Z = "j6",
                V = "M8",
                k = "maxParticleScale",
                A = "minParticleScale",
                f = "gravity",
                o = "emitter",
                b = "group",
                h = "gameTime";
        } else {
            game.load.bitmapFont("systemfont", "assets/fonts/systemfont.png", "assets/fonts/systemfont.fnt");
            emitter.makeParticles("tiles", startColor);
            game.input.onDown.add(this.nextPage, this);
        }
        score = 0;
        timeLeft = game[r2t8.F38][h];
        tileGroup = game[r2t8.n28][b]();
        arrowGroup = game[r2t8.n28][b]();
        pointsGroup = game[r2t8.n28][b]();
        emitter = game[r2t8.n28][o](0, 0, 100);
        emitter[f] = 250;
        emitter[A] = 0.30;
        emitter[k] = 0.45;
        for (var C = 0; r2t8[V](C, game[r2t8.F38][r2t8.D38]); C++) {
            tileArray[C] = [];
            for (var a = 0; r2t8[Z](a, game[r2t8.F38][r2t8.D38]); a++) {
                this[R](C, a);
            }
        }
        for (C = 0; r2t8[s](C, game[r2t8.F38][r2t8.D38]); C++) {
            var K = game[r2t8.F38][r2t8.e38] + r2t8[Y](C, game[r2t8.F38][r2t8.D58]) + r2t8[n](game[r2t8.F38][r2t8.D58], 2),
                r = game[r2t8.F38][r2t8.e58] + r2t8[g](game[r2t8.F38][r2t8.D38], game[r2t8.F38][r2t8.D58]) + r2t8[N](game[r2t8.F38][r2t8.D58], 2),
                F = game[r2t8.n28][r2t8.K38](K, r, "points");
            F[r2t8.q38][r2t8.B18](0.5);
            pointsSpriteArray[C] = F;
            pointsGroup[r2t8.n28](F);
        }
        this[r2t8.b58]();
        game[E][i][D](Phaser[p][J], this[x], this);
    },
    addTile: function(x, J) {
        var l9d = 406753854;
        if (r2t8.p5.z5(0, 4844626) === l9d) {
            var p = "w6",
                D = "Q6",
                i = "o6",
                E = "k6",
                N = "V6",
                g = game[r2t8.A58][r2t8.s38](0, r2t8[N](game[r2t8.F38][r2t8.A38], 1)),
                n = game[r2t8.F38][r2t8.e38] + r2t8[E](J, game[r2t8.F38][r2t8.D58]) + r2t8[i](game[r2t8.F38][r2t8.D58], 2),
                Y = game[r2t8.F38][r2t8.e58] + r2t8[D](x, game[r2t8.F38][r2t8.D58]) + r2t8[p](game[r2t8.F38][r2t8.D58], 2),
                s = game[r2t8.n28][r2t8.K18](n, Y, "tiles");
            s[r2t8.w98] = g;
            s[r2t8.r58] = game[r2t8.A58][r2t8.s38](-20, 20);
        } else {
            game.load.image("gameover", "assets/sprites/gameover.png");
            fadeTween.to({
                alpha: 1
            }, 500, Phaser.Easing.Cubic.Out, true);
            operationsInQueue--;
        }
        s[r2t8.q38][r2t8.B18](0.5);
        tileArray[x][J] = s;
        tileGroup[r2t8.n28](s);
    },
    pickTile: function() {
        var v9d = 937491073;
        if (r2t8.p5.z5(0, 9183993) !== v9d) {
            tileTween.onComplete.add(this.fallDownComplete, this);
            fadeTween.to({
                alpha: 1
            }, 500, Phaser.Easing.Cubic.Out, true);
            pop[randomPop].play("", 0, 1, false);
        } else {
            var x = "moveTile",
                J = "addMoveCallback",
                p = "y7",
                D = "g7",
                i = "D7",
                E = "x7",
                N = "I6",
                g = "t6",
                n = "L6",
                Y = "S6",
                s = "r6",
                R = "F6";
            arrowArray = [];
            visitedTiles = [];
            startX = r2t8[R](game[r2t8.j4V][r2t8.N58], game[r2t8.F38][r2t8.e38]);
            startY = r2t8[s](game[r2t8.j4V][r2t8.J4V], game[r2t8.F38][r2t8.e58]);
        }
        var Z = Math[r2t8.o38](r2t8[Y](startY, game[r2t8.F38][r2t8.D58])),
            V = Math[r2t8.o38](r2t8[n](startX, game[r2t8.F38][r2t8.D58]));
        if (r2t8[g](timeLeft, 0) && r2t8[N](Z, 0) && r2t8[E](V, 0) && r2t8[i](Z, game[r2t8.F38][r2t8.D38]) && r2t8[D](V, game[r2t8.F38][r2t8.D38])) {
            if (game[r2t8.F38][r2t8.D18]) {
                var k = game[r2t8.A58][r2t8.s38](0, r2t8[p](pop.length, 1));
                pop[k].play("", 0, 1, false);
            }
            startColor = tileArray[Z][V][r2t8.w98];
            tileArray[Z][V][r2t8.R38] = 0.5;
            visitedTiles[r2t8.p38]({
                row: Z,
                col: V
            });
            game[r2t8.j4V][r2t8.J28][r2t8.z58](this[r2t8.v18], this);
            game[r2t8.j4V][r2t8.R18][r2t8.n28](this[r2t8.g28], this);
            moveIndex = game[r2t8.j4V][J](this[x], this);
        }
    },
    moveTile: function() {
        var Z1d = -52428182;
        if (r2t8.p5.z5(0, 1368584) !== Z1d) {
            fadeTween.onComplete.add(function() {
                game.state.start("GameOver", true, false, score);
            }, this);
            emitter.makeParticles("tiles", startColor);
            arrowArray.pop();
            this.add.image(0, 0, "background");
        } else {
            var x = "v0",
                J = "removeLastArrow",
                p = "X0",
                D = "c0",
                i = "d0",
                E = "h0",
                N = "C0",
                g = "f0",
                n = "placeArrow",
                Y = "b0",
                s = "R0",
                R = "s0",
                Z = "n0",
                V = "isTilePicked",
                k = "q0",
                A = "z0",
                f = "tolerance",
                o = "p0",
                b = "J0",
                h = "u7",
                C = "G7",
                a = "P7",
                K = "O7",
                r = "m7",
                F = "K7",
                m = "a7",
                G = "B7",
                O = "T7",
                c = "A7",
                u = "Z7",
                x4 = "Y7",
                I = "l7",
                M = "E7",
                P = r2t8[M](game[r2t8.j4V][r2t8.N58], game[r2t8.F38][r2t8.e38]),
                t = r2t8[I](game[r2t8.j4V][r2t8.J4V], game[r2t8.F38][r2t8.e58]),
                Q = Math[r2t8.o38](r2t8[x4](t, game[r2t8.F38][r2t8.D58])),
                T = Math[r2t8.o38](r2t8[u](P, game[r2t8.F38][r2t8.D58]));
            if (r2t8[c](timeLeft, 0) && r2t8[O](Q, 0) && r2t8[G](T, 0) && r2t8[m](Q, game[r2t8.F38][r2t8.D38]) && r2t8[F](T, game[r2t8.F38][r2t8.D38])) {
                var B = r2t8[r](T, game[r2t8.F38][r2t8.D58]) + r2t8[K](game[r2t8.F38][r2t8.D58], 2),
                    U = r2t8[a](Q, game[r2t8.F38][r2t8.D58]) + r2t8[C](game[r2t8.F38][r2t8.D58], 2),
                    d = r2t8[h](P, B),
                    w = r2t8[b](t, U);
                if (r2t8[o](d * d + w * w, game[r2t8.F38][f])) {
                    if (r2t8[A](Q, visitedTiles[visitedTiles.length - 1][r2t8.o28]) || r2t8[k](T, visitedTiles[visitedTiles.length - 1][r2t8.G98])) {
                        if (!this[V]({
                            row: Q,
                            col: T
                        })) {
                            if (r2t8[Z](Math[r2t8.k38](Q - visitedTiles[visitedTiles.length - 1][r2t8.o28]), 1) && r2t8[R](Math[r2t8.k38](T - visitedTiles[visitedTiles.length - 1][r2t8.G98]), 1)) {
                                var D4 = tileArray[Q][T][r2t8.w98];
                                if (r2t8[s](startColor, D4)) {
                                    if (game[r2t8.F38][r2t8.D18]) {
                                        var j4 = game[r2t8.A58][r2t8.s38](0, r2t8[Y](pop.length, 1));
                                        pop[j4].play("", 0, 1, false);
                                    }
                                    tileArray[Q][T][r2t8.R38] = 0.5;
                                    this[n](Q, T);
                                    if (r2t8[g](arrowArray.length, 1)) {
                                        arrowArray[r2t8[N](arrowArray.length, 2)][r2t8.w98] -= 2;
                                    }
                                    visitedTiles[r2t8.p38]({
                                        row: Q,
                                        col: T
                                    });
                                }
                            }
                        } else {
                            if (r2t8[E](Q, visitedTiles[visitedTiles.length - 2][r2t8.o28]) && r2t8[i](T, visitedTiles[visitedTiles.length - 2][r2t8.G98])) {
                                tileArray[visitedTiles[r2t8[D](visitedTiles.length, 1)][r2t8.o28]][visitedTiles[r2t8[p](visitedTiles.length, 1)][r2t8.G98]][r2t8.R38] = 1;
                                this[J]();
                                if (game[r2t8.F38][r2t8.D18]) {
                                    var j4 = game[r2t8.A58][r2t8.s38](0, r2t8[x](pop.length, 1));
                                    pop[j4].play("", 0, 1, false);
                                }
                                visitedTiles[r2t8.E18]();
                            }
                        }
                    }
                }
            }
        }
    },
    releaseTile: function() {
        var x = "tilesFallDown",
            J = "slice",
            p = "text",
            D = "o9",
            i = "update",
            E = "makeParticles",
            N = "k9",
            g = "V9",
            n = "y",
            Y = "N9",
            s = "i9",
            R = "W9",
            Z = "e9",
            V = "H9",
            k = "j9",
            A = "M0",
            f = "U0",
            o = "deleteMoveCallback";
        game[r2t8.j4V][r2t8.R18][r2t8.z58](this[r2t8.g28], this);
        game[r2t8.j4V][o](moveIndex);
        for (var b = 0; r2t8[f](b, arrowArray.length); b++) {
            arrowArray[b][r2t8.F18]();
        }
        for (b = 0; r2t8[A](b, visitedTiles.length); b++) {
            tileArray[visitedTiles[b][r2t8.o28]][visitedTiles[b][r2t8.G98]][r2t8.R38] = 1;
        }
        if (r2t8[k](timeLeft, 0) && r2t8[V](visitedTiles.length, 2)) {
            if (game[r2t8.F38][r2t8.D18]) {
                var h = game[r2t8.A58][r2t8.s38](0, r2t8[Z](remove.length, 1));
                remove[h].play("", 0, 1, false);
            }
            for (b = 0; r2t8[R](b, visitedTiles.length); b++) {
                emitter[r2t8.L38] = r2t8[s](visitedTiles[b][r2t8.G98], game[r2t8.F38][r2t8.D58]) + r2t8[Y](game[r2t8.F38][r2t8.D58], 2) + game[r2t8.F38][r2t8.e38];
                emitter[n] = r2t8[g](visitedTiles[b][r2t8.o28], game[r2t8.F38][r2t8.D58]) + r2t8[N](game[r2t8.F38][r2t8.D58], 2) + game[r2t8.F38][r2t8.e58];
                emitter[E]("tiles", startColor);
                emitter[r2t8.P28](true, 5000, 250, 5);
                emitter[i]();
                score += r2t8[D](game[r2t8.F38][r2t8.V28][pointsInternalArray[visitedTiles[b][r2t8.G98]]], visitedTiles.length);
                scoreText[p] = ("00000000" + score)[J](-8);
                tileArray[visitedTiles[b][r2t8.o28]][visitedTiles[b][r2t8.G98]][r2t8.F18]();
                tileArray[visitedTiles[b][r2t8.o28]][visitedTiles[b][r2t8.G98]] = null;
            }
            this[x]();
        } else {
            this[r2t8.j4V][r2t8.J28][r2t8.n28](this[r2t8.v18], this);
        }
    },
    tilesFallDown: function() {
        var x = "createNewTiles",
            J = "t9",
            p = "L9",
            D = "S9",
            i = "r9",
            E = "F9",
            N = "w9",
            g = "Q9";
        for (var n = r2t8[g](game[r2t8.F38][r2t8.D38], 1); r2t8[N](n, 0); n--) {
            for (var Y = 0; r2t8[E](Y, game[r2t8.F38][r2t8.D38]); Y++) {
                if (r2t8[i](tileArray[n][Y], null)) {
                    var s = this[r2t8.G38](n, Y);
                    if (r2t8[D](s, 0)) {
                        operationsInQueue++;
                        var R = game[r2t8.n28][r2t8.x4V](tileArray[n][Y]);
                        R[r2t8.T38]({
                            y: game[r2t8.F38][r2t8.e58] + r2t8[p]((n + s), game[r2t8.F38][r2t8.D58]) + r2t8[J](game[r2t8.F38][r2t8.D58], 2)
                        }, game[r2t8.F38][r2t8.i4V], Phaser[r2t8.l58][r2t8.i28][r2t8.E28], true);
                        R[r2t8.K28][r2t8.n28](this[r2t8.v38], this);
                        tileArray[n + s][Y] = tileArray[n][Y];
                        tileArray[n][Y] = null;
                    }
                }
            }
        }
        this[x]();
    },
    fallDownComplete: function() {
        var x = "I9";
        operationsInQueue--;
        if (r2t8[x](operationsInQueue, 0)) {
            game[r2t8.j4V][r2t8.J28][r2t8.n28](this[r2t8.v18], this);
        }
    },
    createNewTiles: function() {
        var x = "k1",
            J = "V1",
            p = "l1",
            D = "E1",
            i = "y1",
            E = "g1",
            N = "D1",
            g = "x1";
        for (var n = 0; r2t8[g](n, game[r2t8.F38][r2t8.D38]); n++) {
            var Y = this[r2t8.G38](-1, n);
            for (var s = 0; r2t8[N](s, Y); s++) {
                var R = game[r2t8.A58][r2t8.s38](0, r2t8[E](game[r2t8.F38][r2t8.A38], 1)),
                    Z = game[r2t8.F38][r2t8.e38] + r2t8[i](n, game[r2t8.F38][r2t8.D58]) + r2t8[D](game[r2t8.F38][r2t8.D58], 2),
                    V = r2t8[p](game[r2t8.F38][r2t8.e58], (Y - s) * game[r2t8.F38][r2t8.D58], game[r2t8.F38][r2t8.D58] / 2);
                theTile = game[r2t8.n28][r2t8.K18](Z, V, "tiles");
                theTile[r2t8.w98] = R;
                theTile[r2t8.r58] = game[r2t8.A58][r2t8.s38](-20, 20);
                theTile[r2t8.q38][r2t8.B18](0.5);
                tileArray[s][n] = theTile;
                tileGroup[r2t8.n28](theTile);
                operationsInQueue++;
                var k = game[r2t8.n28][r2t8.x4V](tileArray[s][n]);
                k[r2t8.T38]({
                    y: game[r2t8.F38][r2t8.e58] + r2t8[J](s, game[r2t8.F38][r2t8.D58]) + r2t8[x](game[r2t8.F38][r2t8.D58], 2)
                }, game[r2t8.F38][r2t8.i4V], Phaser[r2t8.l58][r2t8.i28][r2t8.E28], true);
                k[r2t8.K28][r2t8.n28](this[r2t8.v38], this);
            }
        }
        pointsTween = game[r2t8.n28][r2t8.x4V](pointsGroup);
        pointsTween[r2t8.T38]({
            alpha: 0
        }, 400, Phaser[r2t8.l58][r2t8.i28][r2t8.E28], true);
        pointsTween[r2t8.K28][r2t8.n28](function() {
            var M1d = 1442213325;
            if (r2t8.p5.z5(0, 1175070) !== M1d) {
                this.createNewTiles();
                game.state.start("GameOver", true, false, score);
                visitedTiles.pop();
                game.input.onUp.remove(this.releaseTile, this);
                game.state.start("GameTitle");
            } else {
                this[r2t8.b58]();
                pointsGroup[r2t8.R38] = 1;
            }
        }, this);
    },
    isTilePicked: function(x) {
        var J = "w1",
            p = "Q1",
            D = "o1";
        for (var i = 0; r2t8[D](i, visitedTiles.length); i++) {
            if (r2t8[p](x[r2t8.G98], visitedTiles[i][r2t8.G98]) && r2t8[J](x[r2t8.o28], visitedTiles[i][r2t8.o28])) {
                return true;
            }
        }
        return false;
    },
    placeArrow: function(x, J) {
        var p = "y2",
            D = "g2",
            i = "D2",
            E = "x2",
            N = "I1",
            g = "t1",
            n = "L1",
            Y = "S1",
            s = "r1",
            R = "F1",
            Z = game[r2t8.F38][r2t8.e38] + r2t8[R](visitedTiles[visitedTiles.length - 1][r2t8.G98], game[r2t8.F38][r2t8.D58]) + r2t8[s](game[r2t8.F38][r2t8.D58], 2),
            V = game[r2t8.F38][r2t8.e58] + r2t8[Y](visitedTiles[visitedTiles.length - 1][r2t8.o28], game[r2t8.F38][r2t8.D58]) + r2t8[n](game[r2t8.F38][r2t8.D58], 2),
            k = game[r2t8.n28][r2t8.K18](Z, V, "arrows");
        k[r2t8.q38][r2t8.B18](0.5);
        var A = r2t8[g](visitedTiles[visitedTiles.length - 1][r2t8.o28], x),
            f = r2t8[N](visitedTiles[visitedTiles.length - 1][r2t8.G98], J);
        if (r2t8[E](Math[r2t8.k38](A) + Math[r2t8.k38](f), 1)) {
            k[r2t8.w98] = 2;
        } else {
            k[r2t8.w98] = 3;
        }
        var o = r2t8[i](A, 10) + f;
        if (r2t8[D](o, 10) || r2t8[p](o, 9)) {
            k[r2t8.r58] = 90;
        }
        if (o == -10 || o == -9) {
            k[r2t8.r58] = 270;
        }
        if (o == -1 || o == -11) {
            k[r2t8.r58] = 180;
        }
        arrowGroup[r2t8.n28](k);
        arrowArray[r2t8.p38](k);
    },
    removeLastArrow: function() {
        var x = "Y2",
            J = "l2",
            p = "E2";
        arrowArray[r2t8[p](arrowArray.length, 1)][r2t8.F18]();
        arrowArray[r2t8.E18]();
        if (r2t8[J](arrowArray.length, 0)) {
            arrowArray[r2t8[x](arrowArray.length, 1)][r2t8.w98] += 2;
        }
    },
    holesBelow: function(x, J) {
        var p = "A2",
            D = "Z2",
            i = 0;
        for (var E = x + 1; r2t8[D](E, game[r2t8.F38][r2t8.D38]); E++) {
            if (r2t8[p](tileArray[E][J], null)) {
                i++;
            }
        }
        return i;
    },
    updateCounter: function() {
        var T2d = 1180058867;
        if (r2t8.p5.z5(0, 2453307) !== T2d) {
            return R9 * Z9;
        } else {
            var x = "K2",
                J = "a2",
                p = "gameOver",
                D = "B2",
                i = "updateCrop",
                E = "T2",
                N = "Rectangle",
                g = "cropRect";
            timeLeft--;
            timeBar[g] = new Phaser[N](0, 0, r2t8[E](timeLeft, 5), 12);
            timeBar[i]();
        }
        if (r2t8[D](timeLeft, 0)) {
            this[p]();
        }
        if (r2t8[J](timeLeft, 5)) {
            if (game[r2t8.F38][r2t8.D18]) {
                var n = game[r2t8.A58][r2t8.s38](0, r2t8[x](tick.length, 1));
                tick[n].play("", 0, 1, false);
            }
        }
    },
    gameOver: function() {
        var J = this[r2t8.n28][r2t8.K18](0, 0, "blackfade");
        J[r2t8.R38] = 0;
        var p = this[r2t8.n28][r2t8.x4V](J);
        p[r2t8.T38]({
            alpha: 1
        }, 500, Phaser[r2t8.l58][r2t8.i28][r2t8.E28], true);
        p[r2t8.K28][r2t8.n28](function() {
            var x = "state";
            game[x][r2t8.P28]("GameOver", true, false, score);
        }, this);
    },
    shufflePoints: function() {
        var x = "u2",
            J = "G2",
            p = "P2",
            D = "O2",
            i = "m2";
        pointsInternalArray = [];
        var E;
        for (var N = 0; r2t8[i](N, game[r2t8.F38][r2t8.V28].length); N++) {
            pointsInternalArray[r2t8.p38](N);
        }
        for (N = 0; r2t8[D](N, game[r2t8.F38][r2t8.V28].length); N++) {
            var g = game[r2t8.A58][r2t8.s38](0, r2t8[p](pointsInternalArray.length, 1)),
                n = game[r2t8.A58][r2t8.s38](0, r2t8[J](pointsInternalArray.length, 1)),
                E = pointsInternalArray[g];
            pointsInternalArray[g] = pointsInternalArray[n];
            pointsInternalArray[n] = E;
        }
        for (N = 0; r2t8[x](N, game[r2t8.F38][r2t8.V28].length); N++) {
            pointsSpriteArray[N][r2t8.w98] = pointsInternalArray[N];
        }
    },
    toggleSound: function(x) {
        var J = "J5";
        x[r2t8.w98] = r2t8[J](1, x[r2t8.w98]);
        game[r2t8.F38][r2t8.D18] = !game[r2t8.F38][r2t8.D18];
    }
};